/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import static Formularios.Citas.btnbien;
import static Formularios.Citas.btnguardar;
import static Formularios.Citas.btnmal;
import static Formularios.Citas.cbogenero;
import static Formularios.Citas.cbohora;
import static Formularios.Citas.dtfecha;
import static Formularios.Citas.txthora;
import static Formularios.Citas.txtsintomas;
import static Formularios.Citas.txttelefono;
import Logica.FRficha;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static Formularios.Citas.txtnombre;
import static Formularios.Citas.txtnombre;

/**
 *
 * @author radames
 */
public class BuscaCitas extends javax.swing.JFrame {

    /**
     * Creates new form BuscaCitas
     */
    public BuscaCitas() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        this.setLocationRelativeTo(null);
        mostrar("");

    }

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRficha func = new FRficha();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            lbltotalregistro.setText("Total Registrados: " + Integer.toString(func.totalregistro));
            ocultar_columnas();
//            btneliminar.setEnabled(false);
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(3).setMaxWidth(300);
        tablalistado.getColumnModel().getColumn(3).setMinWidth(300);
        tablalistado.getColumnModel().getColumn(3).setPreferredWidth(300);

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(1).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(1).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(1).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(2).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(2).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(2).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(9).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(9).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(9).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(11).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(11).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(11).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(12).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(12).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(12).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(13).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(13).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(13).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(14).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(14).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(14).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(15).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(15).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(15).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(16).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(16).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(16).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(17).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(17).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(17).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(18).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(18).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(18).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(19).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(19).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(19).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(20).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(20).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(20).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(21).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(21).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(21).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(22).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(22).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(22).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(23).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(23).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(23).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(24).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(24).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(24).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(25).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(25).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(25).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(26).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(26).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(26).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(27).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(27).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(27).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(28).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(28).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(28).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(29).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(29).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(29).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(30).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(30).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(30).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(31).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(31).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(31).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(32).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(32).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(32).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(33).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(33).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(33).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(34).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(34).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(34).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(35).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(35).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(35).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(36).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(36).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(36).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(37).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(37).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(37).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(38).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(38).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(38).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(39).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(39).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(39).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(40).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(40).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(40).setPreferredWidth(0);

    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jPanel1 = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        lbltotalregistro = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));
        jDesktopPane1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jDesktopPane1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Citas", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel1.setText("Buscar:");
        jPanel1.add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, -1));

        txtbuscar.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtbuscar.setBorder(null);
        txtbuscar.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtbuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtbuscarMousePressed(evt);
            }
        });
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        jPanel1.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 280, 30));

        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablalistado);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 97, 960, 400));

        lbltotalregistro.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        lbltotalregistro.setText("jLabel2");
        jPanel1.add(lbltotalregistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 500, -1, -1));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 280, 20));

        jDesktopPane1.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 4, 990, 530));

        getContentPane().add(jDesktopPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -6, -1, 530));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        if (evt.getClickCount() == 1) {
            int fila = tablalistado.getSelectedRow();

            String valor;
            String valor1;
            String valor2;

            valor = tablalistado.getValueAt(fila, 3).toString();
            Citas.txtnombre.setText(valor);

            valor = tablalistado.getValueAt(fila, 10).toString();
            Citas.txttelefono.setText(valor);

            valor = tablalistado.getValueAt(fila, 13).toString();
            Citas.txtcorreo.setText(valor);

            txtbuscar.setEnabled(true);
            txttelefono.setEnabled(true);
            txtsintomas.setEnabled(true);
            dtfecha.setEnabled(true);
            cbogenero.setEnabled(true);
            Citas.cbominutos.setEnabled(true);
            txtbuscar.setEnabled(true);
            btnguardar.setEnabled(true);
            txthora.setEnabled(true);
            cbohora.setEnabled(true);
            btnbien.setEnabled(false);
            btnmal.setEnabled(false);
            Citas.txtsintomas.requestFocus();
            this.dispose();

        }         // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoMouseClicked

    private void txtbuscarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtbuscarMousePressed

    }//GEN-LAST:event_txtbuscarMousePressed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BuscaCitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BuscaCitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BuscaCitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BuscaCitas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BuscaCitas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JLabel lbltotalregistro;
    private javax.swing.JTable tablalistado;
    public static javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables
}
