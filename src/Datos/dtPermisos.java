package Datos;

/**
 *
 * @author Fray Alberto Helena
 */
public class dtPermisos {

    private int id_permiso;
    private String nombre_permiso;
    private boolean pacientes;
    private boolean pagos;
    private boolean citas;
    private boolean trabajos_laboratorios;
    private boolean regimen;
    private boolean producto;
    private boolean laboratorio;
    private boolean doctor;
    private boolean usuario;
    private boolean correo;
    private boolean diario;
    private boolean permiso;
    private boolean seguro;
    private boolean empresa;

    public dtPermisos() {

    }

    public dtPermisos(int id_permiso, String nombre_permiso, boolean pacientes, boolean pagos, boolean citas, boolean trabajos_laboratorios, boolean regimen, boolean producto, boolean laboratorio, boolean doctor, boolean usuario, boolean correo, boolean diario, boolean permiso, boolean seguro, boolean empresa) {
        this.id_permiso = id_permiso;
        this.nombre_permiso = nombre_permiso;
        this.pacientes = pacientes;
        this.pagos = pagos;
        this.citas = citas;
        this.trabajos_laboratorios = trabajos_laboratorios;
        this.regimen = regimen;
        this.producto = producto;
        this.laboratorio = laboratorio;
        this.doctor = doctor;
        this.usuario = usuario;
        this.correo = correo;
        this.diario = diario;
        this.permiso = permiso;
        this.seguro = seguro;
        this.empresa = empresa;
    }

    
    public int getId_permiso() {
        return id_permiso;
    }

    public void setId_permiso(int id_permiso) {
        this.id_permiso = id_permiso;
    }

    public String getNombre_permiso() {
        return nombre_permiso;
    }

    public void setNombre_permiso(String nombre_permiso) {
        this.nombre_permiso = nombre_permiso;
    }

    public boolean isPacientes() {
        return pacientes;
    }

    public void setPacientes(boolean pacientes) {
        this.pacientes = pacientes;
    }

    public boolean isPagos() {
        return pagos;
    }

    public void setPagos(boolean pagos) {
        this.pagos = pagos;
    }

    public boolean isCitas() {
        return citas;
    }

    public void setCitas(boolean citas) {
        this.citas = citas;
    }

    public boolean isTrabajos_laboratorios() {
        return trabajos_laboratorios;
    }

    public void setTrabajos_laboratorios(boolean trabajos_laboratorios) {
        this.trabajos_laboratorios = trabajos_laboratorios;
    }

    public boolean isRegimen() {
        return regimen;
    }

    public void setRegimen(boolean regimen) {
        this.regimen = regimen;
    }

    public boolean isProducto() {
        return producto;
    }

    public void setProducto(boolean producto) {
        this.producto = producto;
    }

    public boolean isLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(boolean laboratorio) {
        this.laboratorio = laboratorio;
    }

    public boolean isDoctor() {
        return doctor;
    }

    public void setDoctor(boolean doctor) {
        this.doctor = doctor;
    }

    public boolean isUsuario() {
        return usuario;
    }

    public void setUsuario(boolean usuario) {
        this.usuario = usuario;
    }

    public boolean isCorreo() {
        return correo;
    }

    public void setCorreo(boolean correo) {
        this.correo = correo;
    }

    public boolean isDiario() {
        return diario;
    }

    public void setDiario(boolean diario) {
        this.diario = diario;
    }

    public boolean isPermiso() {
        return permiso;
    }

    public void setPermiso(boolean permiso) {
        this.permiso = permiso;
    }

    public boolean isSeguro() {
        return seguro;
    }

    public void setSeguro(boolean seguro) {
        this.seguro = seguro;
    }

    public boolean isEmpresa() {
        return empresa;
    }

    public void setEmpresa(boolean empresa) {
        this.empresa = empresa;
    }

}
