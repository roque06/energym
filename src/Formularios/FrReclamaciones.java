package Formularios;

import Datos.FdatosEmpresa;
import static Formularios.pagos.codigo;
import Logica.FRdoctor;
import Logica.FRseguroSNS;
import Logica.conexion;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.awt.Color;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class FrReclamaciones extends javax.swing.JFrame {

    public FrReclamaciones() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        this.setLocationRelativeTo(null);
        mostrar("");
        CodNfc();
        dtfecha3.setCalendar(c1);

    }

    Calendar c1 = new GregorianCalendar();

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRseguroSNS func = new FRseguroSNS();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            ocultar_columnas();

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);
    }

    void convertirFecha() {
        String dia = Integer.toString(dtfecha.getCalendar().get(Calendar.DAY_OF_MONTH));
        String mes = Integer.toString(dtfecha.getCalendar().get(Calendar.MONTH) + 1);
        String year = Integer.toString(dtfecha.getCalendar().get(Calendar.YEAR));
        String fecha = (year + "-" + mes + "-" + dia);
        String fechaordenada = (dia + "/" + mes + "/" + year);
        fecha1 = (fecha);

        String dia1 = Integer.toString(dtfecha1.getCalendar().get(Calendar.DAY_OF_MONTH));
        String mes1 = Integer.toString(dtfecha1.getCalendar().get(Calendar.MONTH) + 1);
        String year1 = Integer.toString(dtfecha1.getCalendar().get(Calendar.YEAR));
        String fecha1 = (year1 + "-" + mes1 + "-" + dia1);
        String fechaordenada1 = (dia1 + "/" + mes1 + "/" + year1);
        fecha2 = (fecha1);
        jLabel1.setText(fechaordenada);
        jLabel2.setText(fechaordenada1);

        String dia3 = Integer.toString(dtfecha3.getCalendar().get(Calendar.DAY_OF_MONTH));
        String mes3 = Integer.toString(dtfecha3.getCalendar().get(Calendar.MONTH) + 1);
        String year3 = Integer.toString(dtfecha3.getCalendar().get(Calendar.YEAR));
        String fecha3 = (dia3 + "/" + mes3 + "/" + year3);
        fechaactual = fecha3;

    }

    void total() {
        DecimalFormat formateador = new DecimalFormat("###,###,###.00");
        String total = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT SUM(preciouni)as total from tbreclamacionesodon where fecha between '" + fecha1 + "'AND '" + fecha2 + "'");

            rq2.next();
            total = rq2.getString("total");
            double total2 = Double.parseDouble(total);
            totals = (formateador.format(total2));

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

    }

    void procedimientos() {
        String total = "";
        String servicio = "";

        try {
            Statement sq2 = cn.createStatement();
            Statement sq3 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT COUNT(servicio)as servicio from tbreclamacionesodon where fecha between '" + fecha1 + "'AND '" + fecha2 + "'AND servicio<>'CONSULTA MEDICA'");
            ResultSet rq3 = sq3.executeQuery("SELECT COUNT(servicio)as servicio from tbreclamacionesodon where fecha between '" + fecha1 + "'AND '" + fecha2 + "'AND servicio='CONSULTA MEDICA'");

            rq2.next();
            rq3.next();
            total = rq2.getString("servicio");
            servicio = rq3.getString("servicio");

            procedimiento = ((total));
            consulta = servicio;
//            jLabel4.setText(servicio);
//            jLabel5.setText(servicio);

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

    }

    void CodNfc() {
        String codigo = "";
        String nfc = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT nfc,codigo from tbempresa");

            rq2.next();
            nfc = rq2.getString("nfc");
            codigo = rq2.getString("codigo");

            txtnfc.setText(nfc);
            txtcodigo.setText(codigo);

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

    }

    void Actualizar() {
        FdatosEmpresa dts = new FdatosEmpresa();
        FRdoctor func = new FRdoctor();
        dts.setNfc(txtnfc.getText());
        dts.setCodigo(txtcodigo.getText());
         dts.setId(id);

        if (func.ActualizarNFC(dts)) {
        }

    }

    void imprimir() {

        try {
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile("src/Reportes/ReportSena.jasper");
            HashMap parametro = new HashMap();
            parametro.put("fecha1", fecha1);
            parametro.put("fecha2", fecha2);
            parametro.put("total", totals);

            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void imprimir2() {

        try {
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile("src/Reportes/RptProcedimeintosSena.jasper");
            HashMap parametro = new HashMap();
            parametro.put("consulta", consulta);
            parametro.put("procedimiento", procedimiento);
            parametro.put("fecha1", jLabel1.getText());
            parametro.put("fecha2", jLabel2.getText());
            parametro.put("fechaactual", fechaactual);
            parametro.put("Total", totals);

            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private String fecha1;
    private String fecha2;
    private String totals;
    private String procedimiento;
    private String consulta;
    private String fechaactual;
    private int id=1;


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel3 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        dtfecha3 = new com.toedter.calendar.JDateChooser();
        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        userLabel31 = new javax.swing.JLabel();
        userLabel33 = new javax.swing.JLabel();
        dtfecha1 = new com.toedter.calendar.JDateChooser();
        loginBtn1 = new javax.swing.JPanel();
        btnimprimir = new javax.swing.JLabel();
        dtfecha = new com.toedter.calendar.JDateChooser();
        userLabel32 = new javax.swing.JLabel();
        txtnfc = new javax.swing.JTextField();
        userLabel34 = new javax.swing.JLabel();
        txtcodigo = new javax.swing.JTextField();

        jLabel3.setText("jLabel3");

        jLabel1.setText("jLabel1");

        jLabel2.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Reclamaciones Odontologicas Senasa", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        jScrollPane1.setViewportView(tablalistado);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 850, 390));

        userLabel31.setFont(new java.awt.Font("Roboto Light", 0, 12)); // NOI18N
        userLabel31.setText("Hasta:");
        jPanel1.add(userLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(220, 100, -1, 20));

        userLabel33.setFont(new java.awt.Font("Roboto Light", 0, 12)); // NOI18N
        userLabel33.setText("Desde:");
        jPanel1.add(userLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, -1, 20));

        dtfecha1.setDateFormatString("dd-MM-yyyy");
        dtfecha1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dtfecha1KeyPressed(evt);
            }
        });
        jPanel1.add(dtfecha1, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 100, 130, -1));

        loginBtn1.setBackground(new java.awt.Color(0, 134, 190));

        btnimprimir.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnimprimir.setForeground(new java.awt.Color(255, 255, 255));
        btnimprimir.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnimprimir.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/impresora.png"))); // NOI18N
        btnimprimir.setText("Imprimir");
        btnimprimir.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnimprimir.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnimprimirMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnimprimirMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnimprimirMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn1Layout = new javax.swing.GroupLayout(loginBtn1);
        loginBtn1.setLayout(loginBtn1Layout);
        loginBtn1Layout.setHorizontalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        loginBtn1Layout.setVerticalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnimprimir, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(loginBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 90, -1, 30));

        dtfecha.setDateFormatString("dd-MM-yyyy");
        dtfecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dtfechaKeyPressed(evt);
            }
        });
        jPanel1.add(dtfecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 100, 130, -1));

        userLabel32.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel32.setText("NCF:");
        jPanel1.add(userLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 40, -1, 20));
        jPanel1.add(txtnfc, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 60, 130, -1));

        userLabel34.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel34.setText("Codigo:");
        jPanel1.add(userLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 40, 60, 20));
        jPanel1.add(txtcodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 60, 130, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 889, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 566, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnimprimirMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnimprimirMouseExited
        btnimprimir.setBackground(new Color(0, 134, 190));        // TODO add your handling code here:
    }//GEN-LAST:event_btnimprimirMouseExited

    private void btnimprimirMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnimprimirMouseEntered
        btnimprimir.setBackground(new Color(0, 156, 223));        // TODO add your handling code here:
    }//GEN-LAST:event_btnimprimirMouseEntered

    private void btnimprimirMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnimprimirMouseClicked

        convertirFecha();
        procedimientos();
        total();
        Actualizar();
        CodNfc();
        imprimir();
        imprimir2();
    }//GEN-LAST:event_btnimprimirMouseClicked

    private void dtfecha1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dtfecha1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_dtfecha1KeyPressed

    private void dtfechaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dtfechaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_dtfechaKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FrReclamaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FrReclamaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FrReclamaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FrReclamaciones.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FrReclamaciones().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btnimprimir;
    public static com.toedter.calendar.JDateChooser dtfecha;
    public static com.toedter.calendar.JDateChooser dtfecha1;
    public static com.toedter.calendar.JDateChooser dtfecha3;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JPanel loginBtn1;
    private javax.swing.JTable tablalistado;
    private javax.swing.JTextField txtcodigo;
    private javax.swing.JTextField txtnfc;
    private javax.swing.JLabel userLabel31;
    private javax.swing.JLabel userLabel32;
    private javax.swing.JLabel userLabel33;
    private javax.swing.JLabel userLabel34;
    // End of variables declaration//GEN-END:variables

    conexion cc = new conexion();
    Connection cn = cc.conectar();
}
