package Formularios;

import Datos.Fregimen;
import Logica.FRafiliado;
import Logica.FRregimen;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Afiliado extends javax.swing.JFrame {

    public Afiliado() {
        initComponents();
        this.setLocationRelativeTo(null);
       setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        mostrar("");
    }

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRafiliado func = new FRafiliado();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            lbltotalregistro.setText("Total Registrados: " + Integer.toString(func.totalregistro));
            ocultar_columnas();
            inhabilitat();

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(55);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(55);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(55);

    }

    void guardar() {

        if (txtregimen.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debe Ingresar un Tipo de Regimen");
            return;
        }

        Fregimen dts = new Fregimen();
        FRafiliado func = new FRafiliado();

        dts.setRegimen(txtregimen.getText());

        if (accion.equals("guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(null, "Guardado Exitosamente");
                mostrar("");
                txtregimen.setText("");
            }
        } else if (accion.equals("editar")) {
            dts.setId(Integer.parseInt(id));
            if (func.editar(dts)) {
                JOptionPane.showMessageDialog(null, "Editado Exitosamente");
                mostrar("");
                txtregimen.setText("");
            }

        }
        inhabilitat();
    }

    void eliminar() {
        
        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estas Seguro que Desea Eliminar el Registro", "Confirmar", 2);
        Fregimen dts = new Fregimen();
        FRafiliado func = new FRafiliado();

        if (confirmacion == 0) {
            dts.setId(Integer.parseInt(id));
            if (func.eliminar(dts)) {
                JOptionPane.showMessageDialog(null, "Registro Eliminado");
                inhabilitat();
                mostrar("");
            }

        }

    }

    void habilitar() {
        txtregimen.setEnabled(true);
        btneliminar.setEnabled(false);

        txtregimen.setText("");

    }

    void inhabilitat() {
        txtregimen.setEnabled(false);
        btneliminar.setEnabled(false);
        txtregimen.setText("");
        accion = "guardar";
        btnguardar.setText("Guardar");
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbltotalregistro = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        txtbuscar = new javax.swing.JTextField();
        jScrollPane8 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        txtregimen = new javax.swing.JTextField();
        jSeparator14 = new javax.swing.JSeparator();
        userLabel30 = new javax.swing.JLabel();
        loginBtn7 = new javax.swing.JPanel();
        btncobro4 = new javax.swing.JLabel();
        loginBtn8 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        userLabel31 = new javax.swing.JLabel();
        jSeparator15 = new javax.swing.JSeparator();
        loginBtn9 = new javax.swing.JPanel();
        btneliminar = new javax.swing.JLabel();

        lbltotalregistro.setFont(new java.awt.Font("Calibri", 0, 15)); // NOI18N
        lbltotalregistro.setText("Nombre:");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, new java.awt.Color(204, 204, 204), null, null), "Registrar Tipo Afiliado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtbuscar.setBorder(null);
        jPanel1.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 40, 180, 30));

        tablalistado.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(tablalistado);

        jPanel1.add(jScrollPane8, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 80, 360, 290));

        txtregimen.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtregimen.setBorder(null);
        txtregimen.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtregimenKeyTyped(evt);
            }
        });
        jPanel1.add(txtregimen, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 250, 30));

        jSeparator14.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator14, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 70, 170, 20));

        userLabel30.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel30.setText("Buscar:");
        jPanel1.add(userLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 40, -1, 30));

        loginBtn7.setBackground(new java.awt.Color(0, 134, 190));

        btncobro4.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncobro4.setForeground(new java.awt.Color(255, 255, 255));
        btncobro4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncobro4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        btncobro4.setText("Nuevo");
        btncobro4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncobro4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncobro4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncobro4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncobro4MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn7Layout = new javax.swing.GroupLayout(loginBtn7);
        loginBtn7.setLayout(loginBtn7Layout);
        loginBtn7Layout.setHorizontalGroup(
            loginBtn7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro4, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn7Layout.setVerticalGroup(
            loginBtn7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro4, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 170, -1, 30));

        loginBtn8.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn8Layout = new javax.swing.GroupLayout(loginBtn8);
        loginBtn8.setLayout(loginBtn8Layout);
        loginBtn8Layout.setHorizontalGroup(
            loginBtn8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn8Layout.setVerticalGroup(
            loginBtn8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn8, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 170, -1, -1));

        userLabel31.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel31.setText("Descripcion:");
        jPanel1.add(userLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, 30));

        jSeparator15.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 250, 20));

        loginBtn9.setBackground(new java.awt.Color(0, 134, 190));

        btneliminar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btneliminar.setForeground(new java.awt.Color(255, 255, 255));
        btneliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btneliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/boton-eliminar.png"))); // NOI18N
        btneliminar.setText("Eliminar");
        btneliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btneliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneliminarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn9Layout = new javax.swing.GroupLayout(loginBtn9);
        loginBtn9.setLayout(loginBtn9Layout);
        loginBtn9Layout.setHorizontalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
        loginBtn9Layout.setVerticalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn9, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 40, 100, -1));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, -1, 760, 400));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btncobro4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseClicked
        accion = "guardar";
        btnguardar.setText("Guardar");
        habilitar();
        txtregimen.requestFocus();     // TODO add your handling code here:
    }//GEN-LAST:event_btncobro4MouseClicked

    private void btncobro4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro4MouseEntered

    private void btncobro4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro4MouseExited

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        
        if (btnguardar.isEnabled()==false){
        
        return; 
        }
        
        guardar();        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseExited

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());
        habilitar();
        btneliminar.setEnabled(true);

        accion = "editar";
        btnguardar.setText("Editar");
        id = (tablalistado.getValueAt(fila, 0).toString());

        txtregimen.setText(tablalistado.getValueAt(fila, 1).toString());
        // TODO add your handling code here:        // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoMouseClicked

    private void btneliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseClicked
if (btneliminar.isEnabled()==false){
return;
}
        eliminar();        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseClicked

    private void btneliminarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseEntered

    private void btneliminarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseExited

    private void txtregimenKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtregimenKeyTyped
     char validar1 = evt.getKeyChar();
        if (Character.isLowerCase(validar1)) {
            evt.setKeyChar(Character.toUpperCase(validar1));

        }
        char validar = evt.getKeyChar();
        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Letras");
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtregimenKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Afiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Afiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Afiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Afiliado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Afiliado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btncobro4;
    public static javax.swing.JLabel btneliminar;
    public static javax.swing.JLabel btnguardar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JLabel lbltotalregistro;
    private javax.swing.JPanel loginBtn7;
    private javax.swing.JPanel loginBtn8;
    private javax.swing.JPanel loginBtn9;
    private javax.swing.JTable tablalistado;
    private javax.swing.JTextField txtbuscar;
    public static javax.swing.JTextField txtregimen;
    private javax.swing.JLabel userLabel30;
    private javax.swing.JLabel userLabel31;
    // End of variables declaration//GEN-END:variables
private String accion = "guardar";
    private String id;

}
