/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Datos.dtEmpleado;
import Logica.Controlador;
import Logica.FRempleado;
import Logica.conexion;
import java.awt.Color;
import java.sql.Connection;
import java.sql.Date;
import java.util.Calendar;
import java.util.HashSet;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

/**
 *
 * @author radames
 */
public class Fempleado extends javax.swing.JFrame {

    public Fempleado() {
        initComponents();
        llenarPermiso();
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        this.setLocationRelativeTo(null);
        inhabilitar();

    }
    dtEmpleado dts = new dtEmpleado();

    void habilitar() {
        txtnombre.setEnabled(true);
        txtcedula.setEnabled(true);
        dtnaci.setEnabled(true);
        txtedad.setEnabled(true);
        txtusuario.setEnabled(true);
        txtcontraseña.setEnabled(true);
        cbopermiso.setEnabled(true);
        btnguardar.setEnabled(true);
        dtnaci.setDate(null);
        txttelefono.setEnabled(true);
        txtedad.setText("");

        txtnombre.setText("");
        txtcedula.setText("");
        txtusuario.setText("");
        txtcontraseña.setText("");
        txttelefono.setText("");
        llenarPermiso();

    }

    void inhabilitar() {
        txtnombre.setEnabled(false);
        txtcedula.setEnabled(false);
        dtnaci.setEnabled(false);
        txtedad.setEnabled(false);
        txtusuario.setEnabled(false);
        txtcontraseña.setEnabled(false);
        cbopermiso.setEnabled(false);
        btnguardar.setEnabled(false);
        txttelefono.setEnabled(false);

        txtnombre.setText("");
        txtcedula.setText("");
        txtusuario.setText("");
        txtcontraseña.setText("");
        dtnaci.setDate(null);
        txtedad.setText("");
        txttelefono.setText("");
        llenarPermiso();

    }

    void llenarPermiso() {

        String consulta = "SELECT nombre_permiso FROM tblpermisos ORDER BY nombre_permiso ASC";
        controlador.llenarCombo(cbopermiso, consulta, 1);
    }

    void guardar() {
        if (txtnombre.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Introducir un Nombre");
            txtnombre.requestFocus();
            return;

        }

        if (txtcedula.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Introducir un Numero de Cedula");
            txtcedula.requestFocus();
            return;

        }

        if (txttelefono.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Introducir un Numero de Telefono");
            txttelefono.requestFocus();
            return;

        }

        if (dtnaci.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Debes Introducir la Fecha de Nacimiento");
            return;

        }

        if (txtedad.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Introducir la Edad");
            txtedad.requestFocus();
            return;

        }

        if (txtusuario.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Introducir el Usuario");
            txtusuario.requestFocus();
            return;

        }
        if (txtcontraseña.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Introducir la contraseña");
            txtcontraseña.requestFocus();
            return;

        }
        if (cbopermiso.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(null, "Debes Seleccionar el Tipo de permiso");
            return;

        }

        FRempleado func = new FRempleado();

        dts.setNombre(txtnombre.getText());
        dts.setCedula(txtcedula.getText());
        dts.setTelefono(txttelefono.getText());
        Calendar cal;

        int d, m, a;
        cal = dtnaci.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha(new Date(a, m, d));

        dts.setEdad(txtedad.getText());
        dts.setUsuario(txtusuario.getText());
        dts.setContraseña(txtcontraseña.getText());
        int seleccionado = cbopermiso.getSelectedIndex();
        dts.setPermiso((String) cbopermiso.getItemAt(seleccionado));

        if (accion.equals("guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(null, "Guardado Correctamente");
                inhabilitar();

            }
        } else if (accion.equals("editar")) {
            dts.setId(Integer.parseInt(id));
            if (func.editar(dts)) {
                JOptionPane.showMessageDialog(null, "Editado Correctamente");
                inhabilitar();

            }

        }

    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        userLabel7 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        userLabel8 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        txtcedula = new javax.swing.JFormattedTextField();
        userLabel12 = new javax.swing.JLabel();
        dtnaci = new com.toedter.calendar.JDateChooser();
        jSeparator13 = new javax.swing.JSeparator();
        userLabel9 = new javax.swing.JLabel();
        txtedad = new javax.swing.JFormattedTextField();
        jSeparator10 = new javax.swing.JSeparator();
        userLabel10 = new javax.swing.JLabel();
        txtusuario = new javax.swing.JTextField();
        jSeparator11 = new javax.swing.JSeparator();
        userLabel11 = new javax.swing.JLabel();
        txtcontraseña = new javax.swing.JPasswordField();
        jSeparator2 = new javax.swing.JSeparator();
        cbopermiso = new javax.swing.JComboBox<>();
        userLabel13 = new javax.swing.JLabel();
        loginBtn = new javax.swing.JPanel();
        loginBtnTxt = new javax.swing.JLabel();
        loginBtn1 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        btnbuscar = new javax.swing.JButton();
        userLabel14 = new javax.swing.JLabel();
        jSeparator12 = new javax.swing.JSeparator();
        txttelefono = new javax.swing.JFormattedTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Registrar Empleado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 16))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        userLabel7.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel7.setText("Cedula:");
        jPanel1.add(userLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 120, -1, -1));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 170, 280, 20));

        userLabel8.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel8.setText("Nombre:");
        jPanel1.add(userLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 50, -1, -1));

        txtnombre.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtnombre.setBorder(null);
        txtnombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtnombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtnombreMousePressed(evt);
            }
        });
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreKeyTyped(evt);
            }
        });
        jPanel1.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 70, 280, 30));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 100, 280, 20));

        txtcedula.setBorder(null);
        try {
            txtcedula.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-#######-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtcedula.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtcedula.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtcedulaMouseClicked(evt);
            }
        });
        jPanel1.add(txtcedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 140, 280, 30));

        userLabel12.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel12.setText("Fecha Nacimiento:");
        jPanel1.add(userLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 270, -1, -1));
        jPanel1.add(dtnaci, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 290, 290, 30));

        jSeparator13.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator13, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 320, 290, 20));

        userLabel9.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel9.setText("Edad:");
        jPanel1.add(userLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 50, -1, -1));

        txtedad.setBorder(null);
        try {
            txtedad.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("##")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtedad.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtedad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtedadMouseClicked(evt);
            }
        });
        jPanel1.add(txtedad, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 70, 280, 30));

        jSeparator10.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 100, 280, 20));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Permiso: ");
        jPanel1.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 260, -1, -1));

        txtusuario.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtusuario.setBorder(null);
        txtusuario.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtusuario.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtusuarioMousePressed(evt);
            }
        });
        txtusuario.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtusuarioActionPerformed(evt);
            }
        });
        jPanel1.add(txtusuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 140, 280, 30));

        jSeparator11.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 170, 280, 20));

        userLabel11.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel11.setText("Contraseña:");
        jPanel1.add(userLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 180, -1, -1));

        txtcontraseña.setBorder(null);
        txtcontraseña.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtcontraseñaMousePressed(evt);
            }
        });
        jPanel1.add(txtcontraseña, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 200, 280, 30));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 230, 280, 20));

        cbopermiso.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(cbopermiso, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 290, 280, 30));

        userLabel13.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel13.setText("Usuario:");
        jPanel1.add(userLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 120, -1, -1));

        loginBtn.setBackground(new java.awt.Color(0, 134, 190));

        loginBtnTxt.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        loginBtnTxt.setForeground(new java.awt.Color(255, 255, 255));
        loginBtnTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loginBtnTxt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        loginBtnTxt.setText("Nuevo");
        loginBtnTxt.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        loginBtnTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtnLayout = new javax.swing.GroupLayout(loginBtn);
        loginBtn.setLayout(loginBtnLayout);
        loginBtnLayout.setHorizontalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtnLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(loginBtnTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        loginBtnLayout.setVerticalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtnLayout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(loginBtnTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(loginBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 370, 120, 30));

        loginBtn1.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn1Layout = new javax.swing.GroupLayout(loginBtn1);
        loginBtn1.setLayout(loginBtn1Layout);
        loginBtn1Layout.setHorizontalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnguardar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        loginBtn1Layout.setVerticalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnguardar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(loginBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 370, -1, -1));

        btnbuscar.setBackground(new java.awt.Color(0, 134, 190));
        btnbuscar.setForeground(new java.awt.Color(0, 134, 190));
        btnbuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        btnbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarActionPerformed(evt);
            }
        });
        jPanel1.add(btnbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(820, 10, 40, 30));

        userLabel14.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel14.setText("Telefono:");
        jPanel1.add(userLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 180, -1, -1));

        jSeparator12.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 230, 280, 20));

        txttelefono.setBorder(null);
        try {
            txttelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txttelefono.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel1.add(txttelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(100, 200, 280, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 879, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 467, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtnombreMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnombreMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreMousePressed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtcedulaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtcedulaMouseClicked
        txtcedula.requestFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_txtcedulaMouseClicked

    private void txtedadMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtedadMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txtedadMouseClicked

    private void txtusuarioMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtusuarioMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtusuarioMousePressed

    private void txtusuarioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtusuarioActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtusuarioActionPerformed

    private void txtcontraseñaMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtcontraseñaMousePressed

    }//GEN-LAST:event_txtcontraseñaMousePressed

    private void loginBtnTxtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseClicked
        habilitar();
        btnguardar.setText("guardar");
        accion = "guardar";
        txtnombre.requestFocus();
    }//GEN-LAST:event_loginBtnTxtMouseClicked

    private void loginBtnTxtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseEntered
        loginBtn.setBackground(new Color(0, 156, 223));
    }//GEN-LAST:event_loginBtnTxtMouseEntered

    private void loginBtnTxtMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseExited
        loginBtn.setBackground(new Color(0, 134, 190));
    }//GEN-LAST:event_loginBtnTxtMouseExited

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        if (btnguardar.isEnabled() == false) {
            return;
        }
        guardar();        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered
        btnguardar.setBackground(new Color(0, 156, 223));        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited
        btnguardar.setBackground(new Color(0, 134, 190));        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseExited

    private void btnbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarActionPerformed

        new FvistaEmpleado().setVisible(true);       // TODO add your handling code here:
    }//GEN-LAST:event_btnbuscarActionPerformed

    private void txtnombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyTyped
 char validar1 = evt.getKeyChar();
        if (Character.isLowerCase(validar1)) {
            evt.setKeyChar(Character.toUpperCase(validar1));

        }
        char validar = evt.getKeyChar();
        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Letras");
        }          // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Fempleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Fempleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Fempleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Fempleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Fempleado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnbuscar;
    public static javax.swing.JLabel btnguardar;
    public static javax.swing.JComboBox<String> cbopermiso;
    public static com.toedter.calendar.JDateChooser dtnaci;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JPanel loginBtn;
    private javax.swing.JPanel loginBtn1;
    private javax.swing.JLabel loginBtnTxt;
    public static javax.swing.JFormattedTextField txtcedula;
    public static javax.swing.JPasswordField txtcontraseña;
    public static javax.swing.JFormattedTextField txtedad;
    public static javax.swing.JTextField txtnombre;
    public static javax.swing.JFormattedTextField txttelefono;
    public static javax.swing.JTextField txtusuario;
    private javax.swing.JLabel userLabel10;
    private javax.swing.JLabel userLabel11;
    private javax.swing.JLabel userLabel12;
    private javax.swing.JLabel userLabel13;
    private javax.swing.JLabel userLabel14;
    private javax.swing.JLabel userLabel7;
    private javax.swing.JLabel userLabel8;
    private javax.swing.JLabel userLabel9;
    // End of variables declaration//GEN-END:variables
    Controlador controlador = new Controlador();
    conexion cc = new conexion();
    Connection cn = cc.conectar();
    public static String accion = "guardar";
    public static String id;

}
