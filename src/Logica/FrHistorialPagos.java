package Logica;

import Datos.Fcitas;
import Datos.Fpagos;
import Datos.Fregimen;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FrHistorialPagos {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "No.REGISTRO", "Fecha Pagos", "CodPaciente", "Nombre", "Concepto", "Abono", "Monto Incial", "No.Cuotas", "Fecha Pagado", "Monto Pagado", "Cuotas", "Empleado"};

        String[] registro = new String[15];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select idrecibo,codrecibo,DATE_FORMAT(fecha,'%d/%m/%Y')as fecha,suma,nombre,concepto,abono,montoini,meses,DATE_FORMAT (fechapagos,'%d/%m/%Y %r')as pagos,montomes,cuotan,empleado From tbpagado  where concat (nombre,'',fecha) like '%" + buscar + "%' order by fechapagos desc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idrecibo");
                registro[1] = rs.getString("codrecibo");
                registro[2] = rs.getString("fecha");
                registro[3] = rs.getString("suma");
                registro[4] = rs.getString("nombre");
                registro[5] = rs.getString("concepto");
                registro[6] = rs.getString("abono");
                registro[7] = rs.getString("montoini");
                registro[8] = rs.getString("meses");
                registro[9] = rs.getString("pagos");
                registro[10] = rs.getString("montomes");
                registro[11] = rs.getString("cuotan");
                registro[12] = rs.getString("empleado");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fpagos dts) {
        sSQL = "insert into tbpagado (codrecibo,fecha,nombre,concepto,suma,abono,montoini,meses,fechapagos,montomes,cuotan,empleado)"
                + "VALUES(?,?,?,?,?,?,?,?,now(),?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getCodrecibos());
            pst.setDate(2, dts.getFecha2());
            pst.setString(3, dts.getNombre());
            pst.setString(4, dts.getConcepto());
            pst.setString(5, dts.getSuma());
            pst.setString(6, dts.getAbono());
            pst.setString(7, dts.getMontoini());
            pst.setString(8, dts.getMeses());
//            pst.setString(9, dts.getFechapagado());
            pst.setString(9, dts.getMontomes());
            pst.setString(10, dts.getCuotan());
            pst.setString(11, dts.getEmpleado());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fcitas dts) {
        sSQL = "UPDATE tbpagado set nombre=?,telefono=?,correo=?,sintomas=?,fecha=?,genero=?,hora=?"
                + "where idcitas=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getTelefono());
            pst.setString(3, dts.getCorreo());
            pst.setString(4, dts.getSintomas());
            pst.setDate(5, dts.getFecha());
            pst.setString(6, dts.getGenero());
            pst.setString(7, dts.getHora());
            pst.setInt(8, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fcitas dts) {
        sSQL = "DELETE FROM tbpagado WHERE idrecibos=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
