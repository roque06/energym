package Logica;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;

/**
 *
 * @author Fray Alberto Helena
 */
public class lgConsultarPermiso {

    public static boolean comprobarPermiso(int id_permiso, String campo) {

        conexion mysql = new conexion();
        Connection cn = mysql.conectar();

        String sSQL = "select " + campo + " from tblpermisos where id_permiso = " + id_permiso + "";
        boolean valor = false;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                valor = rs.getBoolean(campo);
            }
        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return valor;
    }

}
