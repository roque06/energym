package Logica;

import Datos.FdatosEmpresa;
import Datos.Fregimen;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRdoctor {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Descripcion"};

        String[] registro = new String[8];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From doctor  where concat (descripcion) like '%" + buscar + "%' order by descripcion asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("iddoctor");
                registro[1] = rs.getString("descripcion");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fregimen dts) {
        sSQL = "insert into doctor (iddoctor,descripcion)"
                + "VALUES(?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getId());
            pst.setString(2, dts.getRegimen());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fregimen dts) {
        sSQL = "UPDATE doctor set descripcion=?"
                + "where iddoctor=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getRegimen());

            pst.setInt(2, dts.getId());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fregimen dts) {
        sSQL = "DELETE FROM doctor WHERE descripcion=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public DefaultTableModel MostrarEmpresa(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Empresa", "Direccion", "Telefono", "Celular", "NFC", "Cedula", "Codigo", "RNC"};

        String[] registro = new String[10];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From tbempresa  where concat (empresa) like '%" + buscar + "%' order by id asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("empresa");
                registro[2] = rs.getString("direccion");
                registro[3] = rs.getString("telefono");
                registro[4] = rs.getString("celular");
                registro[5] = rs.getString("nfc");
                registro[6] = rs.getString("cedula");
                registro[7] = rs.getString("codigo");
                registro[8] = rs.getString("rnc");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean InsertDatosEmpresa(FdatosEmpresa dts) {
        sSQL = "insert into tbempresa (empresa,direccion,telefono,celular,nfc,cedula,codigo,rnc)"
                + "VALUES(?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getId());
            pst.setString(2, dts.getEmpresa());
            pst.setString(3, dts.getDireccion());
            pst.setString(4, dts.getTelefono());
            pst.setString(5, dts.getCelular());
            pst.setString(6, dts.getNfc());
            pst.setString(7, dts.getCedula());
            pst.setString(8, dts.getCodigo());
            pst.setString(9, dts.getRnc());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean ActualizarDatos(FdatosEmpresa dts) {
        sSQL = "UPDATE tbempresa set empresa=?,direccion=?,telefono=?,celular=?,nfc=?,cedula=?,codigo=?,rnc=?"
                + "where id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getEmpresa());
            pst.setString(2, dts.getDireccion());
            pst.setString(3, dts.getTelefono());
            pst.setString(4, dts.getCelular());
            pst.setString(5, dts.getNfc());
            pst.setString(6, dts.getCedula());
            pst.setString(7, dts.getCodigo());
            pst.setString(8, dts.getRnc());
            pst.setInt(9, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean ActualizarNFC(FdatosEmpresa dts) {
        sSQL = "UPDATE tbempresa set codigo=?,nfc=?"
                + "where id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getCodigo());
            pst.setString(2, dts.getNfc());
            pst.setInt(3, dts.getId());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
