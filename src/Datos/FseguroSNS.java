package Datos;

import java.sql.Date;

public class FseguroSNS {

    private int id;
    private Date fehca;
    private String nombre;
    private String servicio;
    private String nss;
    private String cod;
    private String precio;
    private String cant;

    public FseguroSNS() {
    }

    public FseguroSNS(int id, Date fehca, String nombre, String servicio, String nss, String cod, String precio, String cant) {
        this.id = id;
        this.fehca = fehca;
        this.nombre = nombre;
        this.servicio = servicio;
        this.nss = nss;
        this.cod = cod;
        this.precio = precio;
        this.cant = cant;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Date getFehca() {
        return fehca;
    }

    public void setFehca(Date fehca) {
        this.fehca = fehca;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getCod() {
        return cod;
    }

    public void setCod(String cod) {
        this.cod = cod;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getCant() {
        return cant;
    }

    public void setCant(String cant) {
        this.cant = cant;
    }


}
