
package Datos;

public class FdatosEmpresa {
    
    private int id;
    private String empresa;
    private String direccion;
    private String telefono;
    private String celular; 
    private String nfc;
    private String cedula;
    private String codigo;
    private String rnc;

    public FdatosEmpresa() {
    }

    public FdatosEmpresa(int id, String empresa, String direccion, String telefono, String celular, String nfc, String cedula, String codigo, String rnc) {
        this.id = id;
        this.empresa = empresa;
        this.direccion = direccion;
        this.telefono = telefono;
        this.celular = celular;
        this.nfc = nfc;
        this.cedula = cedula;
        this.codigo = codigo;
        this.rnc = rnc;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpresa() {
        return empresa;
    }

    public void setEmpresa(String empresa) {
        this.empresa = empresa;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    public String getNfc() {
        return nfc;
    }

    public void setNfc(String nfc) {
        this.nfc = nfc;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getRnc() {
        return rnc;
    }

    public void setRnc(String rnc) {
        this.rnc = rnc;
    }

    
    
    
    
    
}
