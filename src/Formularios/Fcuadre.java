/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import static Formularios.pagos.codigopaciente;
import Logica.Controlador;
import Logica.conexion;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

/**
 *
 * @author radames
 */
public class Fcuadre extends javax.swing.JFrame {

    /**
     * Creates new form Fcuadre
     */
    public Fcuadre() {
        initComponents();
        this.setLocationRelativeTo(null);
        dtfecha.setCalendar(c1);
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        llenarCombo();
    }
    Calendar c1 = new GregorianCalendar();

    void llenarCombo() {

        String consulta = "SELECT nombre FROM tbempleado ORDER BY nombre ASC";
        controlador.llenarCombo(cbousuario, consulta, 1);
    }

    void total() {

        DecimalFormat formateador = new DecimalFormat("###,###,###.00");
        String abono = "";
        String montomes = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT SUM(montoseguro)as TotalAbono,SUM(montopagado) as TotalMes from tbreportediario where empleado='" + cbousuario.getSelectedItem().toString() + "'and fecha='" + fechaHoy + "'");
            rq2.next();
            abono = rq2.getString("TotalAbono");
            montomes = rq2.getString("TotalMes");
//
            double TotalAbonos = Double.parseDouble(abono);
            double Totalpagos = Double.parseDouble(montomes);

            TotalAbono = (formateador.format(TotalAbonos));
            TotalMes = formateador.format(Totalpagos);

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

//        Double TotalGeneral = numero1 + numero2;
//        totalgeneral = formateador.format(TotalGeneral);
    }

    private String TotalAbono;
    private String TotalMes;
    private String totalgeneral;

    void convertirFecha() {
        String dia = Integer.toString(dtfecha.getCalendar().get(Calendar.DAY_OF_MONTH));
        String mes = Integer.toString(dtfecha.getCalendar().get(Calendar.MONTH) + 1);
        String year = Integer.toString(dtfecha.getCalendar().get(Calendar.YEAR));
        String fecha = (year + "-" + mes + "-" + dia);
        fechaHoy = (fecha);
        jTextField3.setText(fecha);
    }

    void imprimir() {
        jTextField1.setText(fechaHoy);
        jTextField2.setText(cbousuario.getSelectedItem().toString());

        try {
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile("src/Reportes/ReporteDiario.jasper");
            HashMap parametro = new HashMap();
            parametro.put("empleado", jTextField2.getText());
            parametro.put("fecha", jTextField1.getText());
            parametro.put("TotalSeguro", TotalAbono);
            parametro.put("TotalPagado", TotalMes);

            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTextField1 = new javax.swing.JTextField();
        jTextField2 = new javax.swing.JTextField();
        jPanel1 = new javax.swing.JPanel();
        userLabel31 = new javax.swing.JLabel();
        jSeparator15 = new javax.swing.JSeparator();
        cbousuario = new javax.swing.JComboBox<>();
        userLabel2 = new javax.swing.JLabel();
        dtfecha = new com.toedter.calendar.JDateChooser();
        jSeparator23 = new javax.swing.JSeparator();
        loginBtn8 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        jTextField3 = new javax.swing.JTextField();

        jTextField1.setText("jTextField1");

        jTextField2.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Reporte Diario", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        userLabel31.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel31.setText("Usuario:");
        jPanel1.add(userLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, -1, 30));

        jSeparator15.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 260, 20));

        cbousuario.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel1.add(cbousuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 260, 30));

        userLabel2.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel2.setText("Fecha:");
        jPanel1.add(userLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 90, -1, -1));
        jPanel1.add(dtfecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 110, 220, 30));

        jSeparator23.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator23, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 140, 220, 20));

        loginBtn8.setBackground(new java.awt.Color(0, 134, 190));
        loginBtn8.setPreferredSize(new java.awt.Dimension(85, 24));

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Generar Reporte");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn8Layout = new javax.swing.GroupLayout(loginBtn8);
        loginBtn8.setLayout(loginBtn8Layout);
        loginBtn8Layout.setHorizontalGroup(
            loginBtn8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 280, Short.MAX_VALUE)
        );
        loginBtn8Layout.setVerticalGroup(
            loginBtn8Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn8, new org.netbeans.lib.awtextra.AbsoluteConstraints(140, 170, 280, 30));

        jTextField3.setText("jTextField3");
        jPanel1.add(jTextField3, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 230, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 614, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 306, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        convertirFecha();
        total();
        imprimir();
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Fcuadre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Fcuadre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Fcuadre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Fcuadre.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Fcuadre().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btnguardar;
    private javax.swing.JComboBox<String> cbousuario;
    public static com.toedter.calendar.JDateChooser dtfecha;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator23;
    private javax.swing.JTextField jTextField1;
    private javax.swing.JTextField jTextField2;
    private javax.swing.JTextField jTextField3;
    private javax.swing.JPanel loginBtn8;
    private javax.swing.JLabel userLabel2;
    private javax.swing.JLabel userLabel31;
    // End of variables declaration//GEN-END:variables

    Controlador controlador = new Controlador();
    conexion cc = new conexion();
    Connection cn = cc.conectar();

    private String fechaHoy;
}
