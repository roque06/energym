/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Datos.Fficha;
import static Formularios.Ficha.id;
import static Formularios.pagos.btncobro;
import static Formularios.pagos.btnguardar;
import static Formularios.pagos.btnservicio;
import static Formularios.pagos.dtfecha;
import static Formularios.pagos.txtconcepto;
import static Formularios.pagos.txtinicial;
import static Formularios.pagos.txtmeses;
import static Formularios.pagos.txtnombre;
import Logica.FRficha;
import Logica.Metodos;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.sql.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Roque
 */
public class VistaPagos extends javax.swing.JFrame {

    /**
     * Creates new form Vista
     */
    public VistaPagos() {
        initComponents();
        mostrar("");
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        this.setLocationRelativeTo(null);

    }

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRficha func = new FRficha();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            lbltotalregistro.setText("Total Registrados: " + Integer.toString(func.totalregistro));
            ocultar_columnas();
            btneliminar.setEnabled(false);
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(3).setMaxWidth(300);
        tablalistado.getColumnModel().getColumn(3).setMinWidth(300);
        tablalistado.getColumnModel().getColumn(3).setPreferredWidth(300);

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(1).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(1).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(1).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(2).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(2).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(2).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(9).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(9).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(9).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(11).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(11).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(11).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(12).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(12).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(12).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(13).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(13).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(13).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(14).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(14).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(14).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(15).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(15).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(15).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(16).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(16).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(16).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(17).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(17).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(17).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(18).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(18).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(18).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(19).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(19).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(19).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(20).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(20).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(20).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(21).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(21).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(21).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(22).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(22).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(22).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(23).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(23).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(23).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(24).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(24).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(24).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(25).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(25).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(25).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(26).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(26).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(26).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(27).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(27).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(27).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(28).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(28).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(28).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(29).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(29).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(29).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(30).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(30).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(30).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(31).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(31).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(31).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(32).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(32).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(32).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(33).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(33).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(33).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(34).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(34).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(34).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(35).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(35).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(35).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(36).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(36).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(36).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(37).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(37).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(37).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(38).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(38).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(38).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(39).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(39).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(39).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(40).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(40).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(40).setPreferredWidth(0);

    }

    void eliminar() {
        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estás seguro de Eliminar El Paciente?", "Confirmar", 2);

        if (confirmacion == 0) {
            Fficha dts = new Fficha();
            FRficha func = new FRficha();

            dts.setId(Integer.parseInt(id));
            if (func.eliminar(dts)) {
                JOptionPane.showMessageDialog(null, "Paciente Eliminado");
                mostrar("");

            }

        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        lbltotalregistro = new javax.swing.JLabel();
        loginBtn9 = new javax.swing.JPanel();
        btneliminar = new javax.swing.JLabel();
        userLabel10 = new javax.swing.JLabel();
        txtbuscar = new javax.swing.JTextField();
        jSeparator15 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Buscar Paciente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablalistado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablalistado.setGridColor(new java.awt.Color(255, 255, 255));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablalistado);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 990, 460));

        lbltotalregistro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbltotalregistro.setText("jLabel2");
        jPanel1.add(lbltotalregistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 540, -1, -1));

        loginBtn9.setBackground(new java.awt.Color(0, 134, 190));

        btneliminar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btneliminar.setForeground(new java.awt.Color(255, 255, 255));
        btneliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btneliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/boton-eliminar.png"))); // NOI18N
        btneliminar.setText("Eliminar");
        btneliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btneliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneliminarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn9Layout = new javax.swing.GroupLayout(loginBtn9);
        loginBtn9.setLayout(loginBtn9Layout);
        loginBtn9Layout.setHorizontalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn9Layout.setVerticalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 35, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn9, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 30, -1, -1));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Buscar:");
        jPanel1.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 30, -1, 30));

        txtbuscar.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtbuscar.setBorder(null);
        txtbuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtbuscarMousePressed(evt);
            }
        });
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
        });
        jPanel1.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 30, 250, 30));

        jSeparator15.setBackground(new java.awt.Color(0, 134, 190));
        jSeparator15.setForeground(new java.awt.Color(0, 134, 190));
        jSeparator15.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        jPanel1.add(jSeparator15, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 250, 20));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1016, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 586, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());

        if (evt.getClickCount() == 1) {
            String valor;
            Date valor1;

            valor = tablalistado.getValueAt(fila, 41).toString();
            pagos.codigopaciente = (valor);

            valor = tablalistado.getValueAt(fila, 3).toString();
            pagos.txtnombre.setText(valor);

            valor = tablalistado.getValueAt(fila, 13).toString();
            pagos.txtcorreo.setText(valor);

            valor = tablalistado.getValueAt(fila, 42).toString();
            pagos.cboseguro.setSelectedItem(valor);
            
             valor = tablalistado.getValueAt(fila, 42).toString();
            pagos.lblseguro.setText(valor);

            valor = tablalistado.getValueAt(fila, 5).toString();
            pagos.nss=(valor);

            pagos.txtnombre.setForeground(Color.black);
            pagos.txtcorreo.setForeground(Color.black);
            txtconcepto.setForeground(Color.black);
            this.dispose();

       
//            dtfecha.setEnabled(true);
            txtnombre.setEnabled(true);
            txtconcepto.setEnabled(true);
//        txtabono.setEnabled(true);
            txtinicial.setEnabled(true);
            txtmeses.setEnabled(true);
            txtnombre.setEditable(true);
            txtconcepto.setEditable(true);
            txtconcepto.setEditable(true);
            txtmeses.setEditable(true);
//            pagos.txtdeduciones.setEditable(true);
//            btnguardar.setEnabled(true);
            pagos.txtnombre.setEnabled(true);
            btnservicio.setEnabled(true);
            txtconcepto.requestFocus();
            pagos.btnbuscar.setEnabled(false);
//            pagos.txtdeduciones.setEnabled(true);

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoMouseClicked

    private void btneliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseClicked
        if (btneliminar.isEnabled() == false) {
            return;
        }

        eliminar();        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseClicked

    private void btneliminarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseEntered

    private void btneliminarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseExited

    private void txtbuscarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtbuscarMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarMousePressed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            mostrar(txtbuscar.getText());

        }           // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyPressed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaPagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaPagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaPagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaPagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaPagos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btneliminar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JLabel lbltotalregistro;
    private javax.swing.JPanel loginBtn9;
    private javax.swing.JTable tablalistado;
    public static javax.swing.JTextField txtbuscar;
    private javax.swing.JLabel userLabel10;
    // End of variables declaration//GEN-END:variables
public static String cabeza;
    private String cara;
    private String atm;
    private String cuello;
    private String lengua;
    private String labios;
    private String id;

}
