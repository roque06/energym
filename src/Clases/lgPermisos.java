package Clases;

import Datos.dtPermisos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Fray Alberto Helena
 */
public class lgPermisos {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"IDPermiso", "Nombre", "Clientes", "Empleados", "Reporte", "Permiso", "Servicio", "Reporte Mes"};
        Object[] registro = new Object[18];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select * from tblpermisos order by nombre_permiso asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getInt("id_permiso");
                registro[1] = rs.getString("nombre_permiso");
                registro[2] = rs.getBoolean("clientes");
                registro[3] = rs.getBoolean("empleados");
                registro[4] = rs.getBoolean("diario");
                registro[5] = rs.getBoolean("permiso");
                registro[6] = rs.getBoolean("servicio");
                registro[7] = rs.getBoolean("rptmensual");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(dtPermisos dts) {
        sSQL = "INSERT INTO tblpermisos "
                + "(nombre_permiso, clientes, empleados, diario, permiso,servicio,rptmensual)"
                + " VALUES(?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre_permiso());
            pst.setBoolean(2, dts.isPacientes());
            pst.setBoolean(3, dts.isPagos());
            pst.setBoolean(4, dts.isCitas());
            pst.setBoolean(5, dts.isTrabajos_laboratorios());
            pst.setBoolean(6, dts.isRegimen());
            pst.setBoolean(7, dts.isProducto());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(dtPermisos dts) {
        sSQL = "UPDATE tblpermisos SET nombre_permiso=?, clientes=?, empleados=?, diario=?, permiso=?,servicio=?,rptmensual=? WHERE id_permiso=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre_permiso());
            pst.setBoolean(2, dts.isPacientes());
            pst.setBoolean(3, dts.isPagos());
            pst.setBoolean(4, dts.isCitas());
            pst.setBoolean(5, dts.isTrabajos_laboratorios());
            pst.setBoolean(6, dts.isRegimen());
            pst.setBoolean(7, dts.isProducto());
            pst.setInt(8, dts.getId_permiso());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(dtPermisos dts) {
        sSQL = "DELETE FROM tblpermisos WHERE id_permiso=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId_permiso());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
