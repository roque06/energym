package Logica;

import Datos.Fregimen;
import Datos.FseguroSNS;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRseguroSNS {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Fecha", "Nombre", "Nss No.servicio", "Servicio", "Cod-Autorizacion", "Precio Unitario"};

        String[] registro = new String[8];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From tbreclamacionesodon  where concat (fecha,nombre,codautorizacion) like '%" + buscar + "%' order by id desc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("fecha");
                registro[2] = rs.getString("nombre");
                registro[3] = rs.getString("nss");
                registro[4] = rs.getString("servicio");
                registro[5] = rs.getString("codautorizacion");
                registro[6] = rs.getString("preciouni");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public DefaultTableModel MostrarHumano(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Fecha", "Nombre", "No.Afiliado", "Servicio", "Cod-Autorizacion", "Precio Unitario"};

        String[] registro = new String[8];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From tbhumano  where concat (fecha,nombre,codautorizacion) like '%" + buscar + "%' order by id desc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("fecha");
                registro[2] = rs.getString("nombre");
                registro[3] = rs.getString("nss");
                registro[4] = rs.getString("servicio");
                registro[5] = rs.getString("codautorizacion");
                registro[6] = rs.getString("preciouni");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public DefaultTableModel MostrarPrHumano(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Fecha", "Nombre", "No.Afiliado", "Servicio", "Cod-Autorizacion", "Precio Unitario"};

        String[] registro = new String[8];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From tbprimera_humano  where concat (fecha,nombre,codautorizacion) like '%" + buscar + "%' order by id desc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("fecha");
                registro[2] = rs.getString("nombre");
                registro[3] = rs.getString("nss");
                registro[4] = rs.getString("servicio");
                registro[5] = rs.getString("codautorizacion");
                registro[6] = rs.getString("preciouni");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(FseguroSNS dts) {
        sSQL = "insert into tbreclamacionesodon (fecha,nombre,nss,servicio,codautorizacion,preciouni)"
                + "VALUES(now(),?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getNss());
            pst.setString(3, dts.getServicio());
            pst.setString(4, dts.getCod());
            pst.setString(5, dts.getPrecio());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean InsertarHumano(FseguroSNS dts) {
        sSQL = "insert into tbhumano (fecha,nombre,nss,servicio,codautorizacion,preciouni,cantidad)"
                + "VALUES(now(),?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getNss());
            pst.setString(3, dts.getServicio());
            pst.setString(4, dts.getCod());
            pst.setString(5, dts.getPrecio());
            pst.setString(6, dts.getCant());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }
    
        public boolean InsertarPrHumano(FseguroSNS dts) {
        sSQL = "insert into tbprimera_humano (fecha,nombre,nss,servicio,codautorizacion,preciouni,cantidad)"
                + "VALUES(now(),?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getNss());
            pst.setString(3, dts.getServicio());
            pst.setString(4, dts.getCod());
            pst.setString(5, dts.getPrecio());
            pst.setString(6, dts.getCant());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }


    public boolean editar(Fregimen dts) {
        sSQL = "UPDATE tbafiliado set tipoafiliado=?"
                + "where id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getRegimen());

            pst.setInt(2, dts.getId());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fregimen dts) {
        sSQL = "DELETE FROM tbafiliado WHERE id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
