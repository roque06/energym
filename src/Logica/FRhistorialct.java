package Logica;

import Datos.Fcitas;
import Datos.Fregimen;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRhistorialct {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Nombre", "Telefono", "Sintomas", "Registro", "Fecha", "Genero", "Hora", "Status"};

        String[] registro = new String[15];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select idcitas,nombre,telefono,sintomas,DATE_FORMAT (fecha,'%d/%m/%Y')as registro,fecha,genero,hora,status From tbhistorialct  where concat (nombre,'',fecha,status) like '%" + buscar + "%'order by idcitas desc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idcitas");
                registro[1] = rs.getString("nombre");
                registro[2] = rs.getString("telefono");
                registro[3] = rs.getString("sintomas");
                registro[4] = rs.getString("registro");
                registro[5] = rs.getString("fecha");
                registro[6] = rs.getString("genero");
                registro[7] = rs.getString("hora");
                registro[8] = rs.getString("status");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fcitas dts) {
        sSQL = "insert into tbhistorialct (nombre,telefono,sintomas,fecha,genero,hora,status)"
                + "VALUES(?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getTelefono());
            pst.setString(3, dts.getSintomas());
            pst.setDate(4, dts.getFecha());
            pst.setString(5, dts.getGenero());
            pst.setString(6, dts.getHora());
            pst.setString(7, dts.getStatus());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fcitas dts) {
        sSQL = "UPDATE tbhistorialct set nombre=?,telefono=?,sintomas=?,fecha=?,genero=?,hora=?,status=?"
                + "where idcitas=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getTelefono());
            pst.setString(3, dts.getSintomas());
            pst.setDate(4, dts.getFecha());
            pst.setString(5, dts.getGenero());
            pst.setString(6, dts.getHora());
            pst.setString(7, dts.getStatus());
            pst.setInt(8, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fcitas dts) {
        sSQL = "DELETE FROM tbhistorialct WHERE idcitas=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
