package Datos;

/**
 *
 * @author Fray Alberto Helena
 */
public class dtoUsuario {

    public static int id_usuario ;
    public static String nombre ;
    public static String usuario ;
    public static String contrasena ;
    public static int id_permisos ;

    public dtoUsuario() {

    }

    public static int getId_usuario() {
        return id_usuario;
    }

    public static void setId_usuario(int id_usuario) {
        dtoUsuario.id_usuario = id_usuario;
    }

    public static String getNombre() {
        return nombre;
    }

    public static void setNombre(String nombre) {
        dtoUsuario.nombre = nombre;
    }

    public static String getUsuario() {
        return usuario;
    }

    public static void setUsuario(String usuario) {
        dtoUsuario.usuario = usuario;
    }

    public static String getContrasena() {
        return contrasena;
    }

    public static void setContrasena(String contrasena) {
        dtoUsuario.contrasena = contrasena;
    }

    public static int getId_permisos() {
        return id_permisos;
    }

    public static void setId_permisos(int id_permisos) {
        dtoUsuario.id_permisos = id_permisos;
    }

}
