package Formularios;

import Datos.Fimprimir;
import Datos.Fpagos;
import Datos.Fprocedimiento;
import Datos.FreporteD;
import Datos.FseguroSNS;
import Logica.ClaseConvertirNumeroEnLetras;
import Logica.FRpagos;
import Logica.FRprocedimientos;
import Logica.FrHistorialPagos;
import Logica.conexion;
import Logica.ClaseNumerosLetras;
import Logica.Controlador;
import Logica.FRimprimir;
import Logica.FRreporteD;
import Logica.FRseguroSNS;
import Logica.FuncionesGenerales;
import com.toedter.calendar.JDateChooser;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.io.File;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.sql.Date;
import java.text.DateFormat;
import java.util.GregorianCalendar;
import java.util.HashMap;
import java.util.Map;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class pagos extends javax.swing.JFrame {

    Controlador controlador = new Controlador();

    public pagos() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        this.setLocationRelativeTo(null);
        CodigoPaciente();
        inhabilitar();
        Codigo();
        Fecha();
        llenarSeguro();
        fechaHoy.setCalendar(c1);
        dtfecha.setCalendar(c1);

    }

    FseguroSNS dts = new FseguroSNS();
    Calendar c1 = new GregorianCalendar();

    void GuardarSeguro() {

        dts.setNombre(txtnombre.getText());
        dts.setServicio(txtconcepto.getText());
        dts.setNss((nss));
        dts.setCod((txtautorizacion.getText()));
        dts.setPrecio(txtseguro.getText());
        dts.setCant(txtcant.getText());

    }

    public static String Fecha_Mas_1Mes(JDateChooser fechaData, int sumar) {
        String fecha = ((JTextField) fechaData.getDateEditor().getUiComponent()).getText();
        String[] pos = new String[3];
        pos = fecha.split("-");
        int dia = Integer.parseInt(pos[0]);
        int mes = Integer.parseInt(pos[1]) + sumar;
        int año = Integer.parseInt(pos[2]);
        if (dia >= 31) {
            dia = 1;
        }
        if (dia == 1) {
            mes = mes + 1;
        }
        if (mes >= 13) {
            mes = 1;
            año = año + 1;
        }
        return año + "-" + mes + "-" + dia;
    }
    public static String SumaDeFecha = null;

    void bucle() {
        nocuotas = txtmeses.getText();
        mess = 1;
        int cantMeses = Integer.parseInt(txtmeses.getText());
        for (int i = 0; i < cantMeses; i++) {
            SumaDeFecha = FuncionesGenerales.sumarMesAFecha(dtfecha, i + 1);
            lbFecha.setText(SumaDeFecha + "");
            total2();
            guardar();
            mess = mess + 1;
            Actualizar();
        }
        JOptionPane.showMessageDialog(null, "Guardado Exitosamente");
    }

    void llenarSeguro() {

        String consulta = "SELECT descripcion FROM tiposeguro ORDER BY descripcion ASC";
        controlador.llenarCombo(cboseguro, consulta, 1);
    }

    void total2() {
        DecimalFormat formateador = new DecimalFormat("#########.00");

        float numero1;
        float numero2;
        numero1 = Float.parseFloat(txtinicial.getText());
        numero2 = Float.parseFloat(txtmeses.getText());
        double total1 = numero1 / numero2;

        pagomensual = (formateador.format(total1));
        txtmensual.setText(formateador.format(total1));
    }

    void deduciones() {
        DecimalFormat formateador = new DecimalFormat("#########.00");

        float numero1;
        float numero2;
        numero1 = Float.parseFloat(txtinicial.getText());
        numero2 = Float.parseFloat(txtdeduciones.getText());
        double total1 = numero1 - numero2;

        txtinicial.setText(formateador.format(total1));
    }

    void total() {
        DecimalFormat formateador = new DecimalFormat("##########.00");

        float numero1;
        float numero2;
        float numero3;
        numero1 = Float.parseFloat(txtpago.getText());
        numero2 = Float.parseFloat(abono);
        numero3 = Float.parseFloat(txtpago.getText());

        double total1 = numero1 + numero2;

        total = (formateador.format(total1));
        abono1 = (formateador.format(numero2));
        pago1 = (formateador.format(numero3));
    }

    private String totales;
    private String mensualdidad;

    void imprimir() {
        DecimalFormat formateador = new DecimalFormat("###,###,###.00");
        if (txtmeses.getText().equals(cuotas)) {
            mesrestante = "0";
            resta = "0.00";
        }
        if (resta.equals(".00")) {
            resta = "0.00";
        }
        mesrestante = txtmeses.getText();
        int numero = Integer.parseInt(mesrestante);
        String numero2 = numero + "";
        mensualdidad = numero2;
        double pagar = Double.parseDouble(txtpago.getText());
        double abonar = Double.parseDouble(abono);
        double Total = pagar + abonar;
        String total1 = Total + "";
        totales = (formateador.format(Total));
        abono = (formateador.format(abonar));
        txtpago.setText(formateador.format(pagar));
        if (abono.equals(".00")) {
            abono = "0.00";
        }

        try {
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile("src/Reportes/Pagos.jasper");
            HashMap parametro = new HashMap();
            parametro.put("codrecibo", codigo);
            parametro.put("Pago", txtpago.getText());
            parametro.put("Total", totales);
            parametro.put("abono", abono);
            parametro.put("RestanteM", numero2);
            parametro.put("MontoMensual", montomensual);
            parametro.put("Resta", resta);
            parametro.put("suma", sumas);

            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void ImprimirDeduciones() {
        DecimalFormat formateador = new DecimalFormat("###,###,###.00");
        if (txtmeses.getText().equals(cuotas)) {
            mesrestante = "0";
            resta = "0.00";
        }
        if (resta.equals(".00")) {
            resta = "0.00";
        }
        mesrestante = txtmeses.getText();
        int numero = Integer.parseInt(mesrestante);
        String numero2 = numero + "";
        mensualdidad = numero2;
        double pagar = Double.parseDouble(txtpago.getText());
        double abonar = Double.parseDouble(abono);
        double Total = pagar + abonar;
        String total1 = Total + "";
        totales = (formateador.format(Total));
        abono = (formateador.format(abonar));
        txtpago.setText(formateador.format(pagar));
        if (abono.equals(".00")) {
            abono = "0.00";
        }
        double montoabo = Double.parseDouble(MontoAbonado);
        double montoseg = Double.parseDouble(MontoSeguros);
        double montoini = Double.parseDouble(MontoInicial);
        MontoAbonado = formateador.format(montoabo);
        MontoSeguros = formateador.format(montoseg);
        MontoInicial = formateador.format(montoini);
        double TotalRestante = montoini - montoseg - montoabo;
        TotalGen = formateador.format(TotalRestante);

        try {
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile("src/Reportes/PrimerPago.jasper");
            HashMap parametro = new HashMap();
            parametro.put("codrecibo", codigo);
            parametro.put("Pago", txtpago.getText());
            parametro.put("Total", totales);
            parametro.put("abono", abono);
            parametro.put("RestanteM", numero2);
            parametro.put("MontoMensual", montomensual);
            parametro.put("Resta", resta);
            parametro.put("suma", sumas1);
            parametro.put("MontoInicial", MontoInicial);
            parametro.put("MontoSeguro", MontoSeguros);
            parametro.put("MontoAbono", MontoAbonado);
            parametro.put("MontoRestante", TotalGen);

            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
            JasperViewer jv = new JasperViewer(jp, false);
            jv.setVisible(true);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    void pago() {

        DecimalFormat formateador = new DecimalFormat("############.00");
        float numero1;
        float numero2;
        numero1 = (Float.parseFloat(txtmensual.getText()));
        numero2 = Float.parseFloat(txtpago.getText());

        double total1 = numero1 - numero2;
        pago = (formateador.format(total1));

        float numero3;
        float numero4;

        float suma;
        float suma1;

        suma = (Float.parseFloat(pagototal));
        suma1 = (Float.parseFloat(pago));

        float sumatotal = suma + suma1;
        pagototal = (formateador.format(sumatotal));

        mes = "1";

        float suma3;
        float suma4;

        suma3 = (Float.parseFloat(txtmeses.getText()));
        suma4 = (Float.parseFloat(mes));

        float sumatotal1 = suma3 - suma4;
        mes = (formateador.format(sumatotal1));

        numero3 = (Float.parseFloat(pagototal));
        numero4 = Float.parseFloat(mes);
        float dividir = numero3 / numero4;
        pagomensual = (formateador.format(dividir));

    }
    private String pago;

    void restar() {
        cuotas();

        DecimalFormat formateador = new DecimalFormat("############.00");
        float numero1;
        float numero2;
        numero1 = (Float.parseFloat(pagototal));
        numero2 = Float.parseFloat(abono);

        float total1 = numero1 - numero2;
        pagototal = (formateador.format(total1));

        float numero3;
        float numero4;

        mes = "1";

        float suma;
        float suma1;

        suma = (Float.parseFloat(txtmeses.getText()));
        suma1 = (Float.parseFloat(mes));

        float sumatotal = suma - suma1;
        mes = (formateador.format(sumatotal));

//        float cuota1 = Float.parseFloat(cuotas);
//        cuotas=(formateador.format(cuota1));
        numero3 = (Float.parseFloat(pagototal));
        numero4 = Float.parseFloat(cuotas);
        float dividir = numero3 / numero4;
        pagomensual = (formateador.format(dividir));

    }

    void procedimientos() {
        Fprocedimiento dts = new Fprocedimiento();
        FRprocedimientos func = new FRprocedimientos();

        dts.setNombre(txtnombre.getText());
        dts.setProcedimiento(txtconcepto.getText());
        dts.setCodigo(codigopaciente);

        dts.setFecha(lblfecha.getText());

        if (func.insertar(dts)) {
//            JOptionPane.showMessageDialog(null, "Cita Registrada Correctamente");

        }
    }

    void guardar2() {
        Fpagos dts = new Fpagos();
        FRpagos func = new FRpagos();

        dts.setNombre(txtnombre.getText());
        dts.setConcepto(txtconcepto.getText());
        dts.setSuma(txtnombre.getText());
        dts.setCodrecibos(codigo);
        dts.setCodigopaciente(codigopaciente);
        dts.setAbono(abono);
        dts.setMontoini(txtinicial.getText());
        dts.setMeses(mess + "");
        dts.setMontomes(pagomensual);
        dts.setFecha(lblfecha.getText());

        Calendar cal;
        int d, m, a;
        cal = dtfecha.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha2(new Date(a, m, d));

        if (func.insertar(dts)) {
//            JOptionPane.showMessageDialog(null, "Cita Registrada Correctamente");

        }

    }

    void montorestante() {
        DecimalFormat formateador = new DecimalFormat("###,###,###.00");
        double restante = Double.parseDouble(lblpagomensual.getText());
        double pago = Double.parseDouble(txtpago.getText());
        double total = restante - pago;
        resta = formateador.format(total);

    }

    void guardar() {
        CodigoPaciente();
        Fpagos dts = new Fpagos();
        FRpagos func = new FRpagos();

        dts.setNombre(txtnombre.getText());
        dts.setConcepto(txtconcepto.getText());
        dts.setSuma(txtcorreo.getText());
        dts.setCodrecibos(codigo);
        dts.setCodigopaciente(codigopaciente);
        dts.setAbono(abono);
        dts.setMontoini(txtinicial.getText());
        dts.setMeses(mess + "");
        dts.setMontomes(pagomensual);
        dts.setFecha(lblfecha.getText());

        dts.setFechapagos(lbFecha.getText() + "");
        dts.setCuotan(nocuotas);

        if (func.insertar(dts)) {
//            JOptionPane.showMessageDialog(null, "Cita Registrada Correctamente");

        }
    }

    void cobrar() {

        Fpagos dts = new Fpagos();
        FrHistorialPagos func = new FrHistorialPagos();

        dts.setNombre(txtnombre.getText());
        dts.setConcepto(txtconcepto.getText());
        dts.setSuma(codigopaciente);
        dts.setCodrecibos(codigo);
        dts.setAbono(abono);
        dts.setMontoini(txtinicial.getText());
        dts.setMeses(txtmeses.getText());
        dts.setMontomes(txtpago.getText());
        dts.setFecha(lblfecha.getText());

        Calendar cal;
        int d, m, a;
        cal = dtfecha.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha2(new Date(a, m, d));

//        dts.setFechapagado(fechapago);
        dts.setCuotan(nocuotas);
        dts.setEmpleado(Login.nombre);

        if (func.insertar(dts)) {
            JOptionPane.showMessageDialog(null, "Cobro Procesado");

        }
    }

    void GuardarImp() {

        Fimprimir dts = new Fimprimir();
        FRimprimir func = new FRimprimir();
        montomensual = "N/A";
        dts.setCodigo(codigo);
        dts.setPago(txtpago.getText());
        dts.setTotales(totales);
        dts.setAbono(abono);
        dts.setRestante(mensualdidad);
        dts.setMontomensual(montomensual);
        dts.setResta(resta);
        dts.setSumas(sumas);

        if (func.insertar(dts)) {

        }
    }

    void CodigoPaciente() {
        String idpaciente = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT idcodigo from codrecibo");

            rq2.next();
            idpaciente = rq2.getString("idcodigo");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        secuencia = secuencia;
        codigo = (String.valueOf(secuencia));

    }

    void Fecha() {
        String idpaciente = "";
        String pago = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT DATE_FORMAT (NOW(),'%d/%m/%Y') as fecha,DATE_FORMAT(NOW(),'%Y-%m-%d') AS pago");

            rq2.next();
            idpaciente = rq2.getString("fecha");
            pago = rq2.getString("pago");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        secuencia = secuencia;
        String fechapagos = (pago);
        pago = pago;
        lblfecha.setText(String.valueOf(secuencia));
        fechapago = (String.valueOf(pago));

    }

    public void Actualizar() {

        try {
            PreparedStatement psU = cn.prepareStatement("Update codrecibo set idcodigo= idcodigo+1");
            psU.executeUpdate();

        } catch (Exception e) {

            JOptionPane.showConfirmDialog(rootPane, e);

        }

    }

    public void Abono() {

        Fpagos dts = new Fpagos();
        FRpagos func = new FRpagos();
        dts.setMontomes(pagomensual);

        dts.setCodigopaciente(codigopaciente);
        if (func.actualizar(dts)) {
        }

//        CodigoPaciente();
    }

    public void ActualizarMes() {

        Fpagos dts = new Fpagos();
        FRpagos func = new FRpagos();
        dts.setMeses(meses);

        dts.setCodigopaciente(codigopaciente);
        if (actualizarmes(dts)) {
        }

    }

    private String PagoNeto;
    double igual;

    void eliminar() {

        double numero1 = Double.parseDouble(txtpago.getText());
        double numero2 = Double.parseDouble(lblpagomensual.getText());

        DecimalFormat formateador = new DecimalFormat("##########.00");
        float valor1 = Float.parseFloat(txtinicial.getText());
        float valor2 = Float.parseFloat(txtmensual.getText());
        PagoNeto = formateador.format(valor1);
        pagado = formateador.format(valor2);

        Fpagos dts = new Fpagos();
        FRpagos func = new FRpagos();
        dts.setCodrecibos(codigo);
        if (func.eliminar(dts)) {
        }
        if (igual == numero2) {
            dts.setNombre(txtnombre.getText());
            if (func.eliminarAll(dts)) {
            }
        }

    }

    void cargartotal() {
        DecimalFormat formateador = new DecimalFormat("##########.00");
        String total = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT SUM(montomes) as total from tbrecibo where codigopaciente='" + codigopaciente + "'");
            rq2.next();
            total = rq2.getString("total");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (total);
        secuencia = secuencia;
        pagototal = ((secuencia));
    }

    void cuotas() {
        String total = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT COUNT(*) as cuotas  from tbrecibo where nombre='" + txtnombre.getText() + "'");
            rq2.next();
            total = rq2.getString("cuotas");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (total);

        cuotas = (secuencia);
    }

    void Codigo() {
        String idpaciente = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT idpaciente from codpaciente");

            rq2.next();
            idpaciente = rq2.getString("idpaciente");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        secuencia = secuencia;
        codigopaciente = ("PST-" + String.valueOf(secuencia));

    }

    void Servicio() {
        String servicio = "";
        String precio = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT servicio,precio from servicio where codservicio='" + txtconcepto.getText() + "'");

            rq2.next();
            servicio = rq2.getString("servicio");
            precio = rq2.getString("precio");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, "Codigo Incorreto");
        }

        String secuencia = (servicio);
        String secuencia1 = (precio);
        secuencia = secuencia;
        secuencia1 = secuencia1;
        txtconcepto.setText(secuencia);
        txtinicial.setText(secuencia1);
//        btnseguro.setEnabled(true);
        txtseguro.setEnabled(true);

    }

    void ActualizarCodigo() {

        try {
            PreparedStatement psU = cn.prepareStatement("Update codpaciente set idpaciente= idpaciente+1");
            psU.executeUpdate();

        } catch (Exception e) {

            JOptionPane.showConfirmDialog(rootPane, e);

        }
        Codigo();

    }

    void Restarmes() {
        DecimalFormat formateador = new DecimalFormat("############.00");
        float numero1;
        float numero2;
        numero1 = (Float.parseFloat(txtmeses.getText()));
        numero2 = Float.parseFloat(mes);

        float total1 = numero1 - numero2;
//        txtmeses.setText(formateador.format(total1));
        meses = (formateador.format(total1));
    }

    public static void habilitar() {
        dtfecha.setEnabled(false);
        txtnombre.setEnabled(true);
        txtconcepto.setEnabled(false);
        txtinicial.setEnabled(true);
        txtmeses.setEnabled(true);
        lblfecha.setEnabled(true);
        txtnombre.setEnabled(true);
        txtpago.setVisible(false);
        lblpago.setVisible(false);
        jSeparator7.setVisible(false);
        btnseguro.setEnabled(false);
        txtseguro.setEnabled(false);
        txtcant.setVisible(false);
        lblcantidad.setVisible(false);
        lblseguro.setText("");
        txtseguro.setText("0.00");
        txtpago.setText("");
        dtfecha.setDate(null);
        txtnombre.setText("");
        txtconcepto.setText("");
        txtdeduciones.setText("0.00");
        txtdeduciones.setEnabled(false);
        txtautorizacion.setText("00");

        txtinicial.setText("0.00");
        txtmeses.setText("");
        txtnombre.setText("");
        lblpagomensual.setText("0.00");
        txtmensual.setText("0.00");
        txtcorreo.setText("");
        btnguardar.setEnabled(false);
        btncobro.setEnabled(false);
        meses = "";
        btncorreo.setEnabled(false);
        btnservicio.setEnabled(false);
        cboseguro.setEnabled(false);
        txtautorizacion.setEnabled(false);
        btnbuscar.setEnabled(true);
    }

    void inhabilitar() {

        dtfecha.setEnabled(false);
        txtnombre.setEnabled(false);
        txtconcepto.setEnabled(false);
//        txtinicial.setEnabled(false);
        txtmeses.setEnabled(false);
        txtnombre.setEnabled(false);
        txtpago.setVisible(false);
        lblpago.setVisible(false);
        jSeparator7.setVisible(false);
        btnseguro.setEnabled(false);
        txtseguro.setEnabled(false);
        txtdeduciones.setEnabled(false);
        txtautorizacion.setEnabled(false);
        txtcant.setVisible(false);
        lblcantidad.setVisible(false);
        lblseguro.setText("");

        txtseguro.setText("0.00");
        txtpago.setText("");

        dtfecha.setDate(null);
        txtnombre.setText("");
        txtconcepto.setText("");
        txtinicial.setText("0.00");
        txtmeses.setText("");
        txtmensual.setText("0.00");
        lblpagomensual.setText("0.00");
        txtdeduciones.setText("0.00");
        txtcorreo.setText("");
        txtautorizacion.setText("00");

        txtnombre.setText("");

        btnguardar.setEnabled(false);
        btncobro.setEnabled(false);
        btncorreo.setEnabled(false);
        btnservicio.setEnabled(false);
        btnbuscar.setEnabled(true);
        meses = "";
        txtmeses.setText("1");
        cboseguro.setEnabled(false);
        fechaHoy.setCalendar(c1);
        dtfecha.setCalendar(c1);

    }
    public String sSQL = "";

    void Comprobar() {
//        double seguro = Double.parseDouble(txtseguro.getText());
//        double abono = Double.parseDouble(txtdeduciones.getText());
        double inicial = Double.parseDouble(txtinicial.getText());

        if (inicial < 0) {
            JOptionPane.showMessageDialog(null, "No Puedes Ingresar un Valor Mayor Que el Costo Del Producto");
            txtseguro.setText("");
            txtseguro.setEnabled(true);
            txtseguro.requestFocus();
//            txtinicial.setText("0.00");
            return;
        }
    }

    public boolean actualizarmes(Fpagos dts) {
        sSQL = "UPDATE tbrecibo set meses=?"
                + "where codigopaciente=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getMeses());
            pst.setString(2, dts.getCodigopaciente());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    void GuadarDiario() {
        FreporteD dts = new FreporteD();
        FRreporteD func = new FRreporteD();

        dts.setEmpleado(Login.nombre);
        dts.setPaciente(txtnombre.getText());
        dts.setTiposeguro(lblseguro.getText());
        dts.setProcedimiento(txtconcepto.getText());
        if (lblseguro.getText().equals("Privado")) {
            String asegurado = "No";
            dts.setAsegurado(asegurado);
        } else {
            String asegurado = "Si";
            dts.setAsegurado(asegurado);

        }
        dts.setMontoseguro(txtseguro.getText());

        if (txtdeduciones.getText().equals("0.00")) {
            dts.setMontopagado(txtmensual.getText());
        } else {
            dts.setMontopagado(txtdeduciones.getText());
        }

        if (func.insertar(dts)) {

        }

    }

    void GuadarDiario1() {
        FreporteD dts = new FreporteD();
        FRreporteD func = new FRreporteD();

        dts.setEmpleado(Login.nombre);
        dts.setPaciente(txtnombre.getText());
        dts.setTiposeguro(lblseguro.getText());
        dts.setProcedimiento(txtconcepto.getText());
        if (lblseguro.getText().equals("Privado")) {
            String asegurado = "No";
            dts.setAsegurado(asegurado);
        } else {
            String asegurado = "Si";
            dts.setAsegurado(asegurado);

        }
        dts.setMontoseguro(txtseguro.getText());

        if (txtdeduciones.getText().equals("0.00")) {
            dts.setMontopagado(txtmensual.getText());
        } else {
            dts.setMontopagado(txtpago.getText());
        }

        if (func.insertar(dts)) {

        }

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        lbFecha = new javax.swing.JLabel();
        lbFecha1 = new javax.swing.JLabel();
        fechaHoy = new com.toedter.calendar.JDateChooser();
        txtcod = new javax.swing.JLabel();
        txtcod1 = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        loginBtn5 = new javax.swing.JPanel();
        btncobro3 = new javax.swing.JLabel();
        btnseguro = new javax.swing.JButton();
        cboseguro = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        dtfecha = new com.toedter.calendar.JDateChooser();
        btnbuscar = new javax.swing.JButton();
        lblpagomensual = new javax.swing.JLabel();
        txtmensual = new javax.swing.JLabel();
        lblfecha = new javax.swing.JLabel();
        txtinicial = new javax.swing.JLabel();
        userLabel = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        userLabel1 = new javax.swing.JLabel();
        userLabel2 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        txtnombre = new javax.swing.JTextField();
        txtcorreo = new javax.swing.JTextField();
        txtconcepto = new javax.swing.JTextField();
        lblcantidad = new javax.swing.JLabel();
        jSeparator3 = new javax.swing.JSeparator();
        txtdeduciones = new javax.swing.JTextField();
        userLabel4 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        userLabel5 = new javax.swing.JLabel();
        txtmeses = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        txtpago = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        lblpago = new javax.swing.JLabel();
        jSeparator7 = new javax.swing.JSeparator();
        lblseguro = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        userLabel8 = new javax.swing.JLabel();
        loginBtn = new javax.swing.JPanel();
        loginBtnTxt = new javax.swing.JLabel();
        loginBtn1 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        loginBtn2 = new javax.swing.JPanel();
        btncobro = new javax.swing.JLabel();
        userLabel6 = new javax.swing.JLabel();
        userLabel9 = new javax.swing.JLabel();
        userLabel10 = new javax.swing.JLabel();
        loginBtn3 = new javax.swing.JPanel();
        btncobro1 = new javax.swing.JLabel();
        loginBtn4 = new javax.swing.JPanel();
        btncobro2 = new javax.swing.JLabel();
        loginBtn6 = new javax.swing.JPanel();
        btncorreo = new javax.swing.JLabel();
        btnservicio = new javax.swing.JButton();
        userLabel11 = new javax.swing.JLabel();
        txtseguro = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        userLabel12 = new javax.swing.JLabel();
        jSeparator47 = new javax.swing.JSeparator();
        jSeparator10 = new javax.swing.JSeparator();
        txtautorizacion = new javax.swing.JTextField();
        userLabel13 = new javax.swing.JLabel();
        userLabel14 = new javax.swing.JLabel();
        txtcant = new javax.swing.JTextField();
        userLabel7 = new javax.swing.JLabel();

        lbFecha.setFont(new java.awt.Font("Calibri", 0, 15)); // NOI18N
        lbFecha.setText("Fecha registro:");

        lbFecha1.setFont(new java.awt.Font("Calibri", 0, 15)); // NOI18N
        lbFecha1.setText("Fecha registro:");

        fechaHoy.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                fechaHoyKeyPressed(evt);
            }
        });

        txtcod.setText("jLabel1");

        txtcod1.setText("jLabel1");

        jButton1.setText("jButton1");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        loginBtn5.setBackground(new java.awt.Color(0, 134, 190));

        btncobro3.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncobro3.setForeground(new java.awt.Color(255, 255, 255));
        btncobro3.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncobro3.setText("HISTORIAL DE PAGOS");
        btncobro3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncobro3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncobro3MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncobro3MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncobro3MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn5Layout = new javax.swing.GroupLayout(loginBtn5);
        loginBtn5.setLayout(loginBtn5Layout);
        loginBtn5Layout.setHorizontalGroup(
            loginBtn5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro3, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
        );
        loginBtn5Layout.setVerticalGroup(
            loginBtn5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginBtn5Layout.createSequentialGroup()
                .addComponent(btncobro3, javax.swing.GroupLayout.DEFAULT_SIZE, 33, Short.MAX_VALUE)
                .addContainerGap())
        );

        btnseguro.setBackground(new java.awt.Color(0, 134, 190));
        btnseguro.setForeground(new java.awt.Color(0, 134, 190));
        btnseguro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/coronavirus_health_protection_icon_141047.png"))); // NOI18N
        btnseguro.setToolTipText("Seguro Medico");
        btnseguro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnseguroActionPerformed(evt);
            }
        });

        cboseguro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cboseguro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboseguroActionPerformed(evt);
            }
        });

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        dtfecha.setDateFormatString("dd-MM-yyyy");
        dtfecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dtfechaKeyPressed(evt);
            }
        });
        jPanel1.add(dtfecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 330, 280, 30));

        btnbuscar.setBackground(new java.awt.Color(0, 134, 190));
        btnbuscar.setForeground(new java.awt.Color(0, 134, 190));
        btnbuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        btnbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarActionPerformed(evt);
            }
        });
        jPanel1.add(btnbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 140, 40, 30));

        lblpagomensual.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblpagomensual.setForeground(new java.awt.Color(255, 0, 0));
        lblpagomensual.setText("0.00");
        jPanel1.add(lblpagomensual, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 400, 100, -1));

        txtmensual.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtmensual.setForeground(new java.awt.Color(0, 51, 255));
        txtmensual.setText("0.00");
        jPanel1.add(txtmensual, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 360, 100, -1));

        lblfecha.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblfecha.setText("0.00");
        lblfecha.setToolTipText("Fecha en que se registro el pago");
        jPanel1.add(lblfecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 70, 117, 20));

        txtinicial.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        txtinicial.setForeground(new java.awt.Color(0, 153, 0));
        txtinicial.setText("0.00");
        txtinicial.setToolTipText("Fecha en que se registro el pago");
        jPanel1.add(txtinicial, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 320, 120, -1));

        userLabel.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel.setText("Fecha:");
        jPanel1.add(userLabel, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, -1, -1));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 360, 280, 20));

        userLabel1.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel1.setText("Servicio:");
        jPanel1.add(userLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, -1, -1));

        userLabel2.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel2.setText("Fecha de Pagos:");
        jPanel1.add(userLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 300, -1, -1));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 290, 280, 20));

        txtnombre.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtnombre.setBorder(null);
        txtnombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtnombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtnombreMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtnombreMousePressed(evt);
            }
        });
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtnombreKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreKeyTyped(evt);
            }
        });
        jPanel1.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 280, 30));

        txtcorreo.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtcorreo.setBorder(null);
        txtcorreo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtcorreoMousePressed(evt);
            }
        });
        txtcorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcorreoActionPerformed(evt);
            }
        });
        jPanel1.add(txtcorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 210, 280, 30));

        txtconcepto.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtconcepto.setForeground(new java.awt.Color(204, 204, 204));
        txtconcepto.setText("Codigo de servicio");
        txtconcepto.setBorder(null);
        txtconcepto.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtconcepto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtconceptoMousePressed(evt);
            }
        });
        txtconcepto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtconceptoActionPerformed(evt);
            }
        });
        txtconcepto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtconceptoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtconceptoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtconceptoKeyTyped(evt);
            }
        });
        jPanel1.add(txtconcepto, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 280, 280, 30));

        lblcantidad.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblcantidad.setText("Cantidad:");
        jPanel1.add(lblcantidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, -1, -1));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 230, 280, 20));

        txtdeduciones.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtdeduciones.setBorder(null);
        txtdeduciones.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtdeduciones.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtdeducionesMousePressed(evt);
            }
        });
        txtdeduciones.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdeducionesActionPerformed(evt);
            }
        });
        txtdeduciones.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtdeducionesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtdeducionesKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtdeducionesKeyTyped(evt);
            }
        });
        jPanel1.add(txtdeduciones, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 260, 280, 30));

        userLabel4.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        userLabel4.setText("FORMULARIO DE RECIBOS");
        jPanel1.add(userLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 0, -1, -1));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 30, 320, 20));

        userLabel5.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel5.setText("Monto Restante: ");
        jPanel1.add(userLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 400, 120, -1));

        txtmeses.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtmeses.setBorder(null);
        txtmeses.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtmeses.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtmesesMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtmesesMousePressed(evt);
            }
        });
        txtmeses.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmesesActionPerformed(evt);
            }
        });
        txtmeses.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmesesKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtmesesKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtmesesKeyTyped(evt);
            }
        });
        jPanel1.add(txtmeses, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 200, 280, 30));

        jSeparator5.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 310, 280, 20));

        txtpago.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtpago.setBorder(null);
        txtpago.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtpago.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtpagoMousePressed(evt);
            }
        });
        txtpago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtpagoActionPerformed(evt);
            }
        });
        txtpago.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtpagoKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtpagoKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtpagoKeyTyped(evt);
            }
        });
        jPanel1.add(txtpago, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 390, 280, 30));

        jSeparator6.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, 280, 20));

        lblpago.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblpago.setText("Monto a  Pagar:");
        jPanel1.add(lblpago, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 370, -1, -1));

        jSeparator7.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator7, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 420, 280, 20));

        lblseguro.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        jPanel1.add(lblseguro, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 350, 290, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, 280, 20));

        userLabel8.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel8.setText("Cuotas:");
        jPanel1.add(userLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 180, -1, -1));

        loginBtn.setBackground(new java.awt.Color(0, 134, 190));

        loginBtnTxt.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        loginBtnTxt.setForeground(new java.awt.Color(255, 255, 255));
        loginBtnTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loginBtnTxt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        loginBtnTxt.setText("Nuevo");
        loginBtnTxt.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        loginBtnTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtnLayout = new javax.swing.GroupLayout(loginBtn);
        loginBtn.setLayout(loginBtnLayout);
        loginBtnLayout.setHorizontalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginBtnTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtnLayout.setVerticalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginBtnTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 430, 120, 30));

        loginBtn1.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn1Layout = new javax.swing.GroupLayout(loginBtn1);
        loginBtn1.setLayout(loginBtn1Layout);
        loginBtn1Layout.setHorizontalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn1Layout.setVerticalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(150, 430, 120, 30));

        loginBtn2.setBackground(new java.awt.Color(0, 134, 190));

        btncobro.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncobro.setForeground(new java.awt.Color(255, 255, 255));
        btncobro.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncobro.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/recibir-cantidad.png"))); // NOI18N
        btncobro.setText("Cobrar");
        btncobro.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncobro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncobroMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncobroMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncobroMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn2Layout = new javax.swing.GroupLayout(loginBtn2);
        loginBtn2.setLayout(loginBtn2Layout);
        loginBtn2Layout.setHorizontalGroup(
            loginBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn2Layout.setVerticalGroup(
            loginBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginBtn2Layout.createSequentialGroup()
                .addComponent(btncobro, javax.swing.GroupLayout.PREFERRED_SIZE, 21, Short.MAX_VALUE)
                .addContainerGap())
        );

        jPanel1.add(loginBtn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 430, -1, -1));

        userLabel6.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel6.setText("Monto a Cubrir:");
        jPanel1.add(userLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 50, -1, 20));

        userLabel9.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel9.setText("Monto Inicial RD$:");
        jPanel1.add(userLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 320, -1, -1));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Monto Mensual: ");
        jPanel1.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 360, 120, -1));

        loginBtn3.setBackground(new java.awt.Color(0, 134, 190));

        btncobro1.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncobro1.setForeground(new java.awt.Color(255, 255, 255));
        btncobro1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncobro1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/transferir-dinero.png"))); // NOI18N
        btncobro1.setText("HISTORIAL DE PAGOS");
        btncobro1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncobro1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncobro1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncobro1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncobro1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn3Layout = new javax.swing.GroupLayout(loginBtn3);
        loginBtn3.setLayout(loginBtn3Layout);
        loginBtn3Layout.setHorizontalGroup(
            loginBtn3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro1, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
        );
        loginBtn3Layout.setVerticalGroup(
            loginBtn3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro1, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn3, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 110, -1, -1));

        loginBtn4.setBackground(new java.awt.Color(0, 134, 190));

        btncobro2.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncobro2.setForeground(new java.awt.Color(255, 255, 255));
        btncobro2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncobro2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/pago.png"))); // NOI18N
        btncobro2.setText("PAGOS PENDIENTES");
        btncobro2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncobro2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncobro2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncobro2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncobro2MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn4Layout = new javax.swing.GroupLayout(loginBtn4);
        loginBtn4.setLayout(loginBtn4Layout);
        loginBtn4Layout.setHorizontalGroup(
            loginBtn4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro2, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
        );
        loginBtn4Layout.setVerticalGroup(
            loginBtn4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro2, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn4, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 40, -1, -1));

        loginBtn6.setBackground(new java.awt.Color(0, 134, 190));

        btncorreo.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncorreo.setForeground(new java.awt.Color(255, 255, 255));
        btncorreo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncorreo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/correo-electronico.png"))); // NOI18N
        btncorreo.setText("ENVIAR CORREO");
        btncorreo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncorreo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncorreoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncorreoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncorreoMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn6Layout = new javax.swing.GroupLayout(loginBtn6);
        loginBtn6.setLayout(loginBtn6Layout);
        loginBtn6Layout.setHorizontalGroup(
            loginBtn6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncorreo, javax.swing.GroupLayout.DEFAULT_SIZE, 204, Short.MAX_VALUE)
        );
        loginBtn6Layout.setVerticalGroup(
            loginBtn6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncorreo, javax.swing.GroupLayout.DEFAULT_SIZE, 44, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn6, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 180, -1, -1));

        btnservicio.setBackground(new java.awt.Color(0, 134, 190));
        btnservicio.setForeground(new java.awt.Color(0, 134, 190));
        btnservicio.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        btnservicio.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnservicioActionPerformed(evt);
            }
        });
        jPanel1.add(btnservicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 280, 40, 30));

        userLabel11.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel11.setText("Monto a Abonar:");
        jPanel1.add(userLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 240, -1, -1));

        txtseguro.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtseguro.setToolTipText("Indica el monto que cubre el seguro");
        txtseguro.setBorder(null);
        txtseguro.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtseguro.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtseguroMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtseguroMousePressed(evt);
            }
        });
        txtseguro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtseguroActionPerformed(evt);
            }
        });
        txtseguro.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtseguroKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtseguroKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtseguroKeyTyped(evt);
            }
        });
        jPanel1.add(txtseguro, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 70, 280, 30));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 100, 280, 20));

        userLabel12.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel12.setText("Tipo Seguro:");
        jPanel1.add(userLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, -1, 20));

        jSeparator47.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator47, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 380, 290, 20));

        jSeparator10.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 160, 280, 20));

        txtautorizacion.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtautorizacion.setToolTipText("Indica el monto que cubre el seguro");
        txtautorizacion.setBorder(null);
        txtautorizacion.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtautorizacion.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtautorizacionMouseClicked(evt);
            }
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtautorizacionMousePressed(evt);
            }
        });
        txtautorizacion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtautorizacionActionPerformed(evt);
            }
        });
        txtautorizacion.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtautorizacionKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtautorizacionKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtautorizacionKeyTyped(evt);
            }
        });
        jPanel1.add(txtautorizacion, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 130, 280, 30));

        userLabel13.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel13.setText("Codigo Autorizacion:");
        jPanel1.add(userLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 110, -1, 20));

        userLabel14.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel14.setText("Nombre:");
        jPanel1.add(userLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, -1));

        txtcant.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtcantKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtcantKeyTyped(evt);
            }
        });
        jPanel1.add(txtcant, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 390, 60, -1));

        userLabel7.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel7.setText("Correo:");
        jPanel1.add(userLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 1070, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarActionPerformed

        new VistaPagos().setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_btnbuscarActionPerformed

    void mayor() {
        float numero1 = Float.parseFloat(txtpago.getText());
        float numero2 = Float.parseFloat(txtmensual.getText());
        DecimalFormat formateador = new DecimalFormat("##########.00");

        float total = numero1 - numero2;

        abono = (formateador.format(total));
        txtpago.setText(txtmensual.getText());
    }
    private String pagado;
    private void dtfechaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dtfechaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_dtfechaKeyPressed

    private void fechaHoyKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_fechaHoyKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_fechaHoyKeyPressed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        imprimir();        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtnombreMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnombreMousePressed


    }//GEN-LAST:event_txtnombreMousePressed

    private void txtcorreoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtcorreoMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcorreoMousePressed

    private void txtconceptoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtconceptoMousePressed
        txtconcepto.setForeground(Color.black);        // TODO add your handling code here:
    }//GEN-LAST:event_txtconceptoMousePressed

    private void txtcorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcorreoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcorreoActionPerformed

    private void txtconceptoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtconceptoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtconceptoActionPerformed

    private void txtconceptoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtconceptoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            Servicio();
            txtseguro.requestFocus();

            if (pagos.cboseguro.getSelectedItem().equals("Privado")) {
                pagos.txtseguro.setEnabled(false);
                pagos.txtautorizacion.setText("00");
                pagos.txtautorizacion.setEnabled(false);
                pagos.txtmeses.requestFocus();

            }
            if (pagos.lblseguro.getText().equalsIgnoreCase("Primera Humano")) {
                pagos.txtcant.setVisible(true);
                pagos.txtcant.setText("1");
                pagos.txtcant.requestFocus();
                pagos.lblcantidad.setVisible(true);

            }

            if (pagos.lblseguro.getText().equals("Humano")) {
                pagos.txtcant.setVisible(true);
                pagos.txtcant.setText("1");
                pagos.txtcant.requestFocus();
                pagos.lblcantidad.setVisible(true);

            }

        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtconceptoKeyPressed

    private void txtdeducionesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtdeducionesMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdeducionesMousePressed

    private void txtdeducionesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdeducionesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdeducionesActionPerformed

    private void txtdeducionesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdeducionesKeyPressed
        Comprobar();
        DecimalFormat formateador = new DecimalFormat("############.00");
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            deduciones();
            double ft = Double.parseDouble(txtdeduciones.getText());
            txtdeduciones.setText(formateador.format(ft));
            txtdeduciones.setEnabled(false);
            txtmeses.setForeground(Color.black);
            txtmeses.requestFocus();
            dtfecha.getDateEditor().getUiComponent().requestFocusInWindow();

        }

// TODO add your handling code here:
    }//GEN-LAST:event_txtdeducionesKeyPressed

    private void txtmesesMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtmesesMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmesesMousePressed

    private void txtmesesActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmesesActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmesesActionPerformed

    private void txtmesesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmesesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            double cuo = Double.parseDouble(txtmeses.getText());

            if (txtmeses.getText().length() == 0) {

                JOptionPane.showMessageDialog(null, "FAVOR DE INTRODUCIR LA CANTIDAD DE MESES");
                txtmeses.requestFocus();
                return;

            }

            if (cuo == 1) {
                dtfecha.setCalendar(c1);
                txtdeduciones.setEnabled(false);
                dtfecha.setEnabled(false);
                btnguardar.setEnabled(true);
            }

            if (cuo > 1) {

                txtdeduciones.setEnabled(true);
                txtdeduciones.requestFocus();
                dtfecha.setEnabled(true);
            } else {
                dtfecha.getDateEditor().getUiComponent().requestFocusInWindow();

            }

            total2();

        }

        btnguardar.setEnabled(true);

// TODO add your handling code here:
    }//GEN-LAST:event_txtmesesKeyPressed

    private void txtpagoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtpagoMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpagoMousePressed

    private void txtpagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtpagoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtpagoActionPerformed

    private void txtpagoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpagoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtpago.setText(txtmensual.getText());
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtpagoKeyPressed

    private void txtpagoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpagoKeyReleased
        sumas = (ClaseNumerosLetras.convertNumberToLetter(txtpago.getText()));

    }//GEN-LAST:event_txtpagoKeyReleased

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void loginBtnTxtMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseExited
        loginBtn.setBackground(new Color(0, 134, 190));
    }//GEN-LAST:event_loginBtnTxtMouseExited

    private void loginBtnTxtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseEntered
        loginBtn.setBackground(new Color(0, 156, 223));
    }//GEN-LAST:event_loginBtnTxtMouseEntered

    private void loginBtnTxtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseClicked
        habilitar();
        Fecha();
        txtmeses.setText("1");
        txtnombre.setEditable(true);
        txtconcepto.setEditable(true);
        txtconcepto.setEditable(true);
        txtmeses.setEditable(true);
        pagos.txtdeduciones.setEditable(true);
        dtfecha.setCalendar(c1);
        abono = "0.00";
        btnbuscar.setEnabled(true);
        codigopaciente = "";
        txtnombre.requestFocus();     }//GEN-LAST:event_loginBtnTxtMouseClicked

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        Comprobar();
        montopagado = txtmeses.getText();
        MontoSeguros = txtseguro.getText();
        MontoAbonado = txtdeduciones.getText();

        if (btnguardar.isEnabled() == false) {

            return;

        }

        if (txtnombre.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR EL NOMBRE");
            txtnombre.requestFocus();
            return;

        }

        if (txtnombre.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR EL CORREO ELECTRONICO");
            txtnombre.requestFocus();
            return;

        }

        if (txtconcepto.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR EL TIPO DE SERVICIO");
            txtconcepto.requestFocus();
            return;

        }

        if (txtdeduciones.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR LA DEDUCION");
            txtdeduciones.requestFocus();
            return;

        }

        if (txtmeses.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR LA CANTIDAD DE CUOTAS");
            txtmeses.requestFocus();
            return;

        }

        if (dtfecha.getDate() == null) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR LA FECHA");
            dtfecha.getDateEditor().getUiComponent().requestFocusInWindow();
            return;

        }

        if (txtseguro.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "NO PUEDES DEJAR ESTE CAMPO VACIO");
            txtseguro.requestFocus();
            return;

        }

        if (cboseguro.getSelectedItem().equals("Senasa") && !txtseguro.getText().equals("0.00")) {

            FRseguroSNS func = new FRseguroSNS();
            GuardarSeguro();
            if (func.insertar(dts)) {
            }

        }

        if (lblseguro.getText().equals("Humano")) {

            FRseguroSNS func = new FRseguroSNS();
            GuardarSeguro();
            if (func.InsertarHumano(dts)) {
            }

        }

        if (lblseguro.getText().equalsIgnoreCase("Primera Humano")) {

            FRseguroSNS func = new FRseguroSNS();
            GuardarSeguro();
            if (func.InsertarPrHumano(dts)) {
            }

        }

        int numero = Integer.parseInt(txtmeses.getText());

        if (codigopaciente.equals("")) {
            Codigo();
            ActualizarCodigo();
        }
        bucle();
        procedimientos();

        double deducir = Double.parseDouble(txtdeduciones.getText());

        if (deducir > 0) {
            txtpago.setText(txtmensual.getText());
            sumas = (ClaseNumerosLetras.convertNumberToLetter(txtdeduciones.getText()));
            txtpago.setText(txtdeduciones.getText());

            montorestante();
            GuadarDiario();
            cobrar();

            resta = "0.00";
            ImprimirDeduciones();
            GuardarImp();

        }

        if (numero == 1) {
            int confirmacion = JOptionPane.showConfirmDialog(rootPane, "DESEA PROCESAR LA FACTURA ?", "CONFIRMAR", 2);
            if (confirmacion == 0) {
                txtpago.setText(txtmensual.getText());
                sumas = (ClaseNumerosLetras.convertNumberToLetter(txtmensual.getText()));

                montorestante();
                eliminar();
                GuadarDiario();
                cobrar();

                resta = "0.00";
                imprimir();
                GuardarImp();
                inhabilitar();
                Fecha();

            }
        }
//        CodigoPaciente();
        inhabilitar();        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered
        btnguardar.setBackground(new Color(0, 156, 223));        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited
        btnguardar.setBackground(new Color(0, 134, 190));        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseExited
    void EliminarAll() {
        double numero1 = Double.parseDouble(txtpago.getText());
        double numero2 = Double.parseDouble(lblpagomensual.getText());

        if (numero1 == numero2) {

        }
    }
    private void btncobroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobroMouseClicked
        igual = Double.parseDouble(txtpago.getText());
        montopagado = txtmeses.getText();

        if (btncobro.isEnabled() == false) {

            return;

        }

        if (txtpago.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Introduzca el Monto");
            txtpago.requestFocus();
            return;

        }
        PagoNeto = lblpagomensual.getText();
        pagado = txtpago.getText();

        montorestante();
        double numero3 = Double.parseDouble(lblpagomensual.getText());

        float numero1 = Float.parseFloat(txtpago.getText());
        float numero2 = Float.parseFloat(txtmensual.getText());

        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estas Seguro que Desea Procesar Esta Factura", "Confirmar", 2);

        if (confirmacion == 0) {
            if (numero1 > numero2) {
                mayor();
                eliminar();
                cargartotal();

                if (igual == numero3) {
                    cobrar();
                    GuadarDiario1();
                    imprimir();
                    GuardarImp();
                    inhabilitar();
                    return;

                } else {
                    restar();
                }

                for (int i = 0; i < Integer.parseInt(txtmeses.getText()); i++) {
                    Abono();
                }
                cobrar();
                GuadarDiario1();
                imprimir();
                GuardarImp();

                inhabilitar();
            }

        }

        if (confirmacion == 0) {
            if (numero1 < numero2) {
                eliminar();
                cargartotal();
                pago();
                for (int i = 0; i < Integer.parseInt(txtmeses.getText()); i++) {
                    Abono();
                }
                cobrar();
                GuadarDiario1();
                imprimir();
                GuardarImp();
                inhabilitar();
                Fecha();
            } else if (numero2 == numero1) {
                eliminar();
                cobrar();
                GuadarDiario1();
                imprimir();
                GuardarImp();
                inhabilitar();
                Fecha();

            }

        }        // TODO add your handling code here:
    }//GEN-LAST:event_btncobroMouseClicked

    private void btncobroMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobroMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobroMouseEntered

    private void btncobroMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobroMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobroMouseExited

    private void btncobro1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro1MouseClicked
        new FhistorialPagos().setVisible(true);         // TODO add your handling code here:
    }//GEN-LAST:event_btncobro1MouseClicked

    private void btncobro1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro1MouseEntered

    private void btncobro1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro1MouseExited
        // TODO add your handling code here:
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro1MouseExited

    private void btncobro2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro2MouseClicked
        new BuscarPagos().setVisible(true);         // TODO add your handling code here:
    }//GEN-LAST:event_btncobro2MouseClicked

    private void btncobro2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro2MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro2MouseEntered

    private void btncobro2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro2MouseExited

    private void btncobro3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro3MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro3MouseClicked

    private void btncobro3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro3MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro3MouseEntered

    private void btncobro3MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro3MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro3MouseExited

    private void btncorreoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncorreoMouseClicked
        if (btncobro.isEnabled() == false) {

            return;

        }
        new FcorreoPagos().setVisible(true);
        FcorreoPagos.txtdestino.setText(txtcorreo.getText());
        // TODO add your handling code here:
    }//GEN-LAST:event_btncorreoMouseClicked

    private void btncorreoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncorreoMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncorreoMouseEntered

    private void btncorreoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncorreoMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncorreoMouseExited

    private void txtmesesMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtmesesMouseClicked
        txtmeses.setForeground(Color.black);
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmesesMouseClicked

    private void btnservicioActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnservicioActionPerformed
        new VistaServicios().setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_btnservicioActionPerformed

    private void txtmesesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmesesKeyReleased
//        int numero = Integer.parseInt(txtmeses.getText());
//
//        if (!txtmeses.getText().equals("1"));
//        {
//            dtfecha.setEnabled(true);
//            
//        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtmesesKeyReleased

    private void txtmesesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmesesKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }
        txtmeses.setText(txtmeses.getText().trim());        // TODO add your handling code here:
    }//GEN-LAST:event_txtmesesKeyTyped

    private void txtdeducionesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdeducionesKeyTyped
//        char validar = evt.getKeyChar();
//        if (Character.isLetter(validar)) {
//            getToolkit().beep();
//            evt.consume();
//            JOptionPane.showMessageDialog(null, "Solo Se Admiten Numeros");
//        }
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }
        txtdeduciones.setText(txtdeduciones.getText().trim());      // TODO add your handling code here:
    }//GEN-LAST:event_txtdeducionesKeyTyped

    private void txtpagoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtpagoKeyTyped
        char validar = evt.getKeyChar();
        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Numeros");
        }
        txtpago.setText(txtpago.getText().trim());
    }//GEN-LAST:event_txtpagoKeyTyped

    private void txtnombreMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnombreMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreMouseClicked

    private void txtnombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyTyped
        char validar = evt.getKeyChar();
        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Letras");
        }
        char validar1 = evt.getKeyChar();
        if (Character.isLowerCase(validar1)) {
            evt.setKeyChar(Character.toUpperCase(validar1));

        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyTyped

    private void btnseguroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnseguroActionPerformed
        new SeguroMedico().setVisible(true);
        SeguroMedico.montototal.setText(txtinicial.getText());

    }//GEN-LAST:event_btnseguroActionPerformed

    private void txtconceptoKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtconceptoKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtconceptoKeyReleased

    private void txtseguroMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtseguroMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtseguroMousePressed

    private void txtseguroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtseguroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtseguroActionPerformed
    String MontoSeguro;
    private void txtseguroKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtseguroKeyPressed

        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            DecimalFormat formateador = new DecimalFormat("###,###,###,###.00");
            DecimalFormat formateador2 = new DecimalFormat("############.00");
            double MontoDebito = Double.parseDouble(txtseguro.getText());
            double MontoTotal = Double.parseDouble(txtinicial.getText());
            double total = MontoTotal - MontoDebito;
            double MontoIni = Double.parseDouble(txtinicial.getText());
            MontoInicial = formateador2.format(MontoIni);

            txtinicial.setText(formateador2.format(total));
            Comprobar();

            double aut = Double.parseDouble(txtseguro.getText());
            if (aut > 0) {
                double ft = Double.parseDouble(txtseguro.getText());
                txtseguro.setText(formateador2.format(ft));
                txtautorizacion.setEnabled(true);
                txtautorizacion.requestFocus();
                txtseguro.setEnabled(false);
            } else {
                txtseguro.setText("0.00");
                txtmeses.requestFocus();
                txtseguro.setEnabled(false);
            }

        }
        // TODO add your handling code here:
    }//GEN-LAST:event_txtseguroKeyPressed

    private void txtseguroKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtseguroKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtseguroKeyReleased

    private void txtseguroKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtseguroKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }

        txtseguro.setText(txtseguro.getText().trim());        // TODO add your handling code here:
    }//GEN-LAST:event_txtseguroKeyTyped

    private void txtseguroMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtseguroMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txtseguroMouseClicked

    private void txtdeducionesKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtdeducionesKeyReleased
        sumas1 = (ClaseNumerosLetras.convertNumberToLetter(txtdeduciones.getText()));        // TODO add your handling code here:
    }//GEN-LAST:event_txtdeducionesKeyReleased

    private void txtnombreKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyReleased
        txtconcepto.setEnabled(true);        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyReleased

    private void txtconceptoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtconceptoKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }
        txtconcepto.setText(txtconcepto.getText().trim());        // TODO add your handling code here:


    }//GEN-LAST:event_txtconceptoKeyTyped

    private void txtautorizacionMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtautorizacionMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_txtautorizacionMouseClicked

    private void txtautorizacionMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtautorizacionMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtautorizacionMousePressed

    private void txtautorizacionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtautorizacionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtautorizacionActionPerformed

    private void txtautorizacionKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtautorizacionKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmeses.requestFocus();

        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtautorizacionKeyPressed

    private void txtautorizacionKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtautorizacionKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtautorizacionKeyReleased

    private void txtautorizacionKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtautorizacionKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }
        txtautorizacion.setText(txtautorizacion.getText().trim());        // TODO add your handling code here:
    }//GEN-LAST:event_txtautorizacionKeyTyped

    private void cboseguroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboseguroActionPerformed
//        if (pagos.cboseguro.getSelectedItem().equals("Privado")) {
//            pagos.txtseguro.setEnabled(false);
//            pagos.txtautorizacion.setText("00");
//            pagos.txtautorizacion.setEnabled(false);
//            pagos.txtmeses.requestFocus();
//
//        }
    }//GEN-LAST:event_cboseguroActionPerformed

    private void txtcantKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcantKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtseguro.requestFocus();
            txtseguro.setText("");
        }

// TODO add your handling code here:
    }//GEN-LAST:event_txtcantKeyPressed

    private void txtcantKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtcantKeyTyped
       char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }

        txtcant.setText(txtcant.getText().trim());         // TODO add your handling code here:
    }//GEN-LAST:event_txtcantKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(pagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(pagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(pagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(pagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new pagos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnbuscar;
    public static javax.swing.JLabel btncobro;
    public static javax.swing.JLabel btncobro1;
    public static javax.swing.JLabel btncobro2;
    public static javax.swing.JLabel btncobro3;
    public static javax.swing.JLabel btncorreo;
    public static javax.swing.JLabel btnguardar;
    public static javax.swing.JButton btnseguro;
    public static javax.swing.JButton btnservicio;
    public static javax.swing.JComboBox<String> cboseguro;
    public static com.toedter.calendar.JDateChooser dtfecha;
    public static com.toedter.calendar.JDateChooser fechaHoy;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    public static javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator47;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    public static javax.swing.JSeparator jSeparator7;
    private javax.swing.JSeparator jSeparator8;
    public static javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lbFecha;
    private javax.swing.JLabel lbFecha1;
    public static javax.swing.JLabel lblcantidad;
    public static javax.swing.JLabel lblfecha;
    public static javax.swing.JLabel lblpago;
    public static javax.swing.JLabel lblpagomensual;
    public static javax.swing.JLabel lblseguro;
    private javax.swing.JPanel loginBtn;
    private javax.swing.JPanel loginBtn1;
    private javax.swing.JPanel loginBtn2;
    private javax.swing.JPanel loginBtn3;
    private javax.swing.JPanel loginBtn4;
    private javax.swing.JPanel loginBtn5;
    private javax.swing.JPanel loginBtn6;
    private javax.swing.JLabel loginBtnTxt;
    public static javax.swing.JTextField txtautorizacion;
    public static javax.swing.JTextField txtcant;
    public static javax.swing.JLabel txtcod;
    public static javax.swing.JLabel txtcod1;
    public static javax.swing.JTextField txtconcepto;
    public static javax.swing.JTextField txtcorreo;
    public static javax.swing.JTextField txtdeduciones;
    public static javax.swing.JLabel txtinicial;
    public static javax.swing.JLabel txtmensual;
    public static javax.swing.JTextField txtmeses;
    public static javax.swing.JTextField txtnombre;
    public static javax.swing.JTextField txtpago;
    public static javax.swing.JTextField txtseguro;
    private javax.swing.JLabel userLabel;
    private javax.swing.JLabel userLabel1;
    private javax.swing.JLabel userLabel10;
    private javax.swing.JLabel userLabel11;
    private javax.swing.JLabel userLabel12;
    private javax.swing.JLabel userLabel13;
    private javax.swing.JLabel userLabel14;
    private javax.swing.JLabel userLabel2;
    private javax.swing.JLabel userLabel4;
    private javax.swing.JLabel userLabel5;
    private javax.swing.JLabel userLabel6;
    private javax.swing.JLabel userLabel7;
    private javax.swing.JLabel userLabel8;
    private javax.swing.JLabel userLabel9;
    // End of variables declaration//GEN-END:variables
    public static String concepto;
    public static String suma = "Efectivo";
    public static String codigo;
    public static String codigopaciente;
    public static String pagototal;
    public static String pagomensual;
    public static String mes = "1";
    public static String meses;
    private int mess;
    public static String abono = "0.00";
    private String abono1;
    private String pago1;
    public static String fechapago;
    private String total;
    private String resta;
    private String montomensual;
    private String mesrestante;
    private String sumas;
    private String cuotas;
    private String MontoInicial;
    private String MontoAbonado;
    private String MontoSeguros;
    private String TotalGen;
    private String sumas1;
    public static String nocuotas;
    conexion cc = new conexion();
    Connection cn = cc.conectar();
    public static String empleado;
    public static String nss;
    private String montopagado;

}
