package Datos;

import java.util.Date;

public class Fficha {

    private int id;
    private String nombrepss;
    private Date fecha;
    private String nombre;
    private String edad;
    private String nss;
    private String regimen;
    private String cedula;
    private String afiliado;
    private String lugar;
    private String telefono;
    private Date fechanaci;
    private String direccion;
    private String correo;
    private String cabeza;
    private String cara;
    private String cuello;
    private String atm;
    private String labios;
    private String lengua;
    private String asma;
    private String alergia;
    private String epilepcia;
    private String anemia;
    private String hepatitis;
    private String tuberculosis;
    private String renal;
    private String diabetes;
    private String hipertencion;
    private String hipotencion;
    private String otros;
    private String caries;
    private String periodontal;
    private String hemorragia;
    private String abscesos;
    private String fistulas;
    private String dolor;
    private String fracturas;
    private String bruxismo;
    private String abrasion;
    private String otros2;
    private String codpaciente;
    private String tiposeguro;

    public Fficha() {
    }

    public Fficha(int id, String nombrepss, Date fecha, String nombre, String edad, String nss, String regimen, String cedula, String afiliado, String lugar, String telefono, Date fechanaci, String direccion, String correo, String cabeza, String cara, String cuello, String atm, String labios, String lengua, String asma, String alergia, String epilepcia, String anemia, String hepatitis, String tuberculosis, String renal, String diabetes, String hipertencion, String hipotencion, String otros, String caries, String periodontal, String hemorragia, String abscesos, String fistulas, String dolor, String fracturas, String bruxismo, String abrasion, String otros2, String codpaciente, String tiposeguro) {
        this.id = id;
        this.nombrepss = nombrepss;
        this.fecha = fecha;
        this.nombre = nombre;
        this.edad = edad;
        this.nss = nss;
        this.regimen = regimen;
        this.cedula = cedula;
        this.afiliado = afiliado;
        this.lugar = lugar;
        this.telefono = telefono;
        this.fechanaci = fechanaci;
        this.direccion = direccion;
        this.correo = correo;
        this.cabeza = cabeza;
        this.cara = cara;
        this.cuello = cuello;
        this.atm = atm;
        this.labios = labios;
        this.lengua = lengua;
        this.asma = asma;
        this.alergia = alergia;
        this.epilepcia = epilepcia;
        this.anemia = anemia;
        this.hepatitis = hepatitis;
        this.tuberculosis = tuberculosis;
        this.renal = renal;
        this.diabetes = diabetes;
        this.hipertencion = hipertencion;
        this.hipotencion = hipotencion;
        this.otros = otros;
        this.caries = caries;
        this.periodontal = periodontal;
        this.hemorragia = hemorragia;
        this.abscesos = abscesos;
        this.fistulas = fistulas;
        this.dolor = dolor;
        this.fracturas = fracturas;
        this.bruxismo = bruxismo;
        this.abrasion = abrasion;
        this.otros2 = otros2;
        this.codpaciente = codpaciente;
        this.tiposeguro = tiposeguro;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombrepss() {
        return nombrepss;
    }

    public void setNombrepss(String nombrepss) {
        this.nombrepss = nombrepss;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getEdad() {
        return edad;
    }

    public void setEdad(String edad) {
        this.edad = edad;
    }

    public String getNss() {
        return nss;
    }

    public void setNss(String nss) {
        this.nss = nss;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getAfiliado() {
        return afiliado;
    }

    public void setAfiliado(String afiliado) {
        this.afiliado = afiliado;
    }

    public String getLugar() {
        return lugar;
    }

    public void setLugar(String lugar) {
        this.lugar = lugar;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public Date getFechanaci() {
        return fechanaci;
    }

    public void setFechanaci(Date fechanaci) {
        this.fechanaci = fechanaci;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getCabeza() {
        return cabeza;
    }

    public void setCabeza(String cabeza) {
        this.cabeza = cabeza;
    }

    public String getCara() {
        return cara;
    }

    public void setCara(String cara) {
        this.cara = cara;
    }

    public String getCuello() {
        return cuello;
    }

    public void setCuello(String cuello) {
        this.cuello = cuello;
    }

    public String getAtm() {
        return atm;
    }

    public void setAtm(String atm) {
        this.atm = atm;
    }

    public String getLabios() {
        return labios;
    }

    public void setLabios(String labios) {
        this.labios = labios;
    }

    public String getLengua() {
        return lengua;
    }

    public void setLengua(String lengua) {
        this.lengua = lengua;
    }

    public String getAsma() {
        return asma;
    }

    public void setAsma(String asma) {
        this.asma = asma;
    }

    public String getAlergia() {
        return alergia;
    }

    public void setAlergia(String alergia) {
        this.alergia = alergia;
    }

    public String getEpilepcia() {
        return epilepcia;
    }

    public void setEpilepcia(String epilepcia) {
        this.epilepcia = epilepcia;
    }

    public String getAnemia() {
        return anemia;
    }

    public void setAnemia(String anemia) {
        this.anemia = anemia;
    }

    public String getHepatitis() {
        return hepatitis;
    }

    public void setHepatitis(String hepatitis) {
        this.hepatitis = hepatitis;
    }

    public String getTuberculosis() {
        return tuberculosis;
    }

    public void setTuberculosis(String tuberculosis) {
        this.tuberculosis = tuberculosis;
    }

    public String getRenal() {
        return renal;
    }

    public void setRenal(String renal) {
        this.renal = renal;
    }

    public String getDiabetes() {
        return diabetes;
    }

    public void setDiabetes(String diabetes) {
        this.diabetes = diabetes;
    }

    public String getHipertencion() {
        return hipertencion;
    }

    public void setHipertencion(String hipertencion) {
        this.hipertencion = hipertencion;
    }

    public String getHipotencion() {
        return hipotencion;
    }

    public void setHipotencion(String hipotencion) {
        this.hipotencion = hipotencion;
    }

    public String getOtros() {
        return otros;
    }

    public void setOtros(String otros) {
        this.otros = otros;
    }

    public String getCaries() {
        return caries;
    }

    public void setCaries(String caries) {
        this.caries = caries;
    }

    public String getPeriodontal() {
        return periodontal;
    }

    public void setPeriodontal(String periodontal) {
        this.periodontal = periodontal;
    }

    public String getHemorragia() {
        return hemorragia;
    }

    public void setHemorragia(String hemorragia) {
        this.hemorragia = hemorragia;
    }

    public String getAbscesos() {
        return abscesos;
    }

    public void setAbscesos(String abscesos) {
        this.abscesos = abscesos;
    }

    public String getFistulas() {
        return fistulas;
    }

    public void setFistulas(String fistulas) {
        this.fistulas = fistulas;
    }

    public String getDolor() {
        return dolor;
    }

    public void setDolor(String dolor) {
        this.dolor = dolor;
    }

    public String getFracturas() {
        return fracturas;
    }

    public void setFracturas(String fracturas) {
        this.fracturas = fracturas;
    }

    public String getBruxismo() {
        return bruxismo;
    }

    public void setBruxismo(String bruxismo) {
        this.bruxismo = bruxismo;
    }

    public String getAbrasion() {
        return abrasion;
    }

    public void setAbrasion(String abrasion) {
        this.abrasion = abrasion;
    }

    public String getOtros2() {
        return otros2;
    }

    public void setOtros2(String otros2) {
        this.otros2 = otros2;
    }

    public String getCodpaciente() {
        return codpaciente;
    }

    public void setCodpaciente(String codpaciente) {
        this.codpaciente = codpaciente;
    }

    public String getTiposeguro() {
        return tiposeguro;
    }

    public void setTiposeguro(String tiposeguro) {
        this.tiposeguro = tiposeguro;
    }


   
    
    
}
