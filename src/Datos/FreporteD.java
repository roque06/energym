
package Datos;


public class FreporteD {
    
    private int id;
    private String empleado;
    private String paciente;
    private String tiposeguro;
    private String procedimiento;
    private String asegurado;
    private String montoseguro;
    private String montopagado;

    public FreporteD() {
    }

    public FreporteD(int id, String empleado, String paciente, String tiposeguro, String procedimiento, String asegurado, String montoseguro, String montopagado) {
        this.id = id;
        this.empleado = empleado;
        this.paciente = paciente;
        this.tiposeguro = tiposeguro;
        this.procedimiento = procedimiento;
        this.asegurado = asegurado;
        this.montoseguro = montoseguro;
        this.montopagado = montopagado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getTiposeguro() {
        return tiposeguro;
    }

    public void setTiposeguro(String tiposeguro) {
        this.tiposeguro = tiposeguro;
    }

    public String getProcedimiento() {
        return procedimiento;
    }

    public void setProcedimiento(String procedimiento) {
        this.procedimiento = procedimiento;
    }

    public String getAsegurado() {
        return asegurado;
    }

    public void setAsegurado(String asegurado) {
        this.asegurado = asegurado;
    }

    public String getMontoseguro() {
        return montoseguro;
    }

    public void setMontoseguro(String montoseguro) {
        this.montoseguro = montoseguro;
    }

    public String getMontopagado() {
        return montopagado;
    }

    public void setMontopagado(String montopagado) {
        this.montopagado = montopagado;
    }
    
    
    
}
