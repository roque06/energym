package Datos;

import java.sql.Date;

public class Fpagos {

    private int idrecibos;
    private String codrecibos;
    private String fecha;
    private String nombre;
    private String concepto;
    private String suma;
    private String abono;
    private String montoini;
    private String meses;
    private String fechapagos;
    private  String fechapagado;
    private String montomes;
    private Date fecha2;
    public String codigopaciente;
    private String cuotan;
    private String empleado;
    

    public Fpagos() {
    }

    public Fpagos(int idrecibos, String codrecibos, String fecha, String nombre, String concepto, String suma, String abono, String montoini, String meses, String fechapagos, String fechapagado, String montomes, Date fecha2, String codigopaciente, String cuotan, String empleado) {
        this.idrecibos = idrecibos;
        this.codrecibos = codrecibos;
        this.fecha = fecha;
        this.nombre = nombre;
        this.concepto = concepto;
        this.suma = suma;
        this.abono = abono;
        this.montoini = montoini;
        this.meses = meses;
        this.fechapagos = fechapagos;
        this.fechapagado = fechapagado;
        this.montomes = montomes;
        this.fecha2 = fecha2;
        this.codigopaciente = codigopaciente;
        this.cuotan = cuotan;
        this.empleado = empleado;
    }

    public int getIdrecibos() {
        return idrecibos;
    }

    public void setIdrecibos(int idrecibos) {
        this.idrecibos = idrecibos;
    }

    public String getCodrecibos() {
        return codrecibos;
    }

    public void setCodrecibos(String codrecibos) {
        this.codrecibos = codrecibos;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getConcepto() {
        return concepto;
    }

    public void setConcepto(String concepto) {
        this.concepto = concepto;
    }

    public String getSuma() {
        return suma;
    }

    public void setSuma(String suma) {
        this.suma = suma;
    }

    public String getAbono() {
        return abono;
    }

    public void setAbono(String abono) {
        this.abono = abono;
    }

    public String getMontoini() {
        return montoini;
    }

    public void setMontoini(String montoini) {
        this.montoini = montoini;
    }

    public String getMeses() {
        return meses;
    }

    public void setMeses(String meses) {
        this.meses = meses;
    }

    public String getFechapagos() {
        return fechapagos;
    }

    public void setFechapagos(String fechapagos) {
        this.fechapagos = fechapagos;
    }

    public String getFechapagado() {
        return fechapagado;
    }

    public void setFechapagado(String fechapagado) {
        this.fechapagado = fechapagado;
    }

    public String getMontomes() {
        return montomes;
    }

    public void setMontomes(String montomes) {
        this.montomes = montomes;
    }

    public Date getFecha2() {
        return fecha2;
    }

    public void setFecha2(Date fecha2) {
        this.fecha2 = fecha2;
    }

    public String getCodigopaciente() {
        return codigopaciente;
    }

    public void setCodigopaciente(String codigopaciente) {
        this.codigopaciente = codigopaciente;
    }

    public String getCuotan() {
        return cuotan;
    }

    public void setCuotan(String cuotan) {
        this.cuotan = cuotan;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }



   
}
