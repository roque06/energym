package Formularios;

import Datos.FdatosEmpresa;
import Logica.FRdoctor;
import java.util.HashSet;
import java.util.Set;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Fempresa extends javax.swing.JFrame {

    public Fempresa() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        this.setLocationRelativeTo(null);
        mostrar("");
        inhabilitar();

    }
    FdatosEmpresa dts = new FdatosEmpresa();

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRdoctor func = new FRdoctor();
            modelo = func.MostrarEmpresa(buscar);

            tablalistado.setModel(modelo);
            ocultar_columnas();

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);
    }

    void guardar() {
        FRdoctor func = new FRdoctor();

        dts.setEmpresa(txtempresa.getText());
        dts.setDireccion(txtdireccion.getText());
        dts.setTelefono(txttelefono.getText());
        dts.setCelular(txtcelular.getText());
        dts.setNfc(txtnfc.getText());
        dts.setCedula(txtcedula.getText());
        dts.setCodigo(txtcodigo.getText());
        dts.setRnc(txtrnc.getText());

        if (accion.equals("actualizar")) {
            dts.setId(Integer.parseInt(id));
            if (func.ActualizarDatos(dts)) {
                JOptionPane.showMessageDialog(null, "Datos Registrado Exitosamente");
            }

        }
    }

    void habilitar() {
        txtempresa.setEnabled(true);
        txtdireccion.setEnabled(true);
        txttelefono.setEnabled(true);
        txtcelular.setEnabled(true);
        txtnfc.setEnabled(true);
        txtcedula.setEnabled(true);
        txtcodigo.setEnabled(true);
        txtrnc.setEnabled(true);
        txtcodigo.setText("");
        txtrnc.setText("");
        txtempresa.setText("");
        txtdireccion.setText("");
        txttelefono.setText("");
        txtcelular.setText("");
        txtnfc.setText("");
        txtcedula.setText("");
        btnactualizar.setEnabled(true);

    }

    void inhabilitar() {
        txtempresa.setEnabled(false);
        txtdireccion.setEnabled(false);
        txttelefono.setEnabled(false);
        txtcelular.setEnabled(false);
        txtnfc.setEnabled(false);
        txtcedula.setEnabled(false);
        txtcodigo.setEnabled(false);
        txtrnc.setEnabled(false);
        txtcodigo.setText("");
        txtrnc.setText("");
        txtempresa.setText("");
        txtdireccion.setText("");
        txttelefono.setText("");
        txtcelular.setText("");
        txtnfc.setText("");
        txtcedula.setText("");

        btnactualizar.setEnabled(false);

    }
    private String id;
    private String accion = "guardar";
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        userLabel32 = new javax.swing.JLabel();
        txtempresa = new javax.swing.JTextField();
        jSeparator15 = new javax.swing.JSeparator();
        userLabel33 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jSeparator16 = new javax.swing.JSeparator();
        userLabel34 = new javax.swing.JLabel();
        jSeparator17 = new javax.swing.JSeparator();
        userLabel35 = new javax.swing.JLabel();
        jSeparator18 = new javax.swing.JSeparator();
        jSeparator19 = new javax.swing.JSeparator();
        txtcelular = new javax.swing.JFormattedTextField();
        txttelefono = new javax.swing.JFormattedTextField();
        jSeparator20 = new javax.swing.JSeparator();
        userLabel36 = new javax.swing.JLabel();
        txtcedula = new javax.swing.JFormattedTextField();
        userLabel37 = new javax.swing.JLabel();
        jSeparator21 = new javax.swing.JSeparator();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        loginBtn2 = new javax.swing.JPanel();
        btnactualizar = new javax.swing.JLabel();
        jSeparator22 = new javax.swing.JSeparator();
        userLabel38 = new javax.swing.JLabel();
        userLabel39 = new javax.swing.JLabel();
        jSeparator23 = new javax.swing.JSeparator();
        txtcodigo = new javax.swing.JTextField();
        txtrnc = new javax.swing.JTextField();
        txtnfc = new javax.swing.JTextField();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setResizable(false);
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Informacion Del Centro Odontologico", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        userLabel32.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel32.setText("Empresa:");
        jPanel1.add(userLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 40, -1, 20));

        txtempresa.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtempresa.setBorder(null);
        jPanel1.add(txtempresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, 250, 30));

        jSeparator15.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 250, 20));

        userLabel33.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel33.setText("Direccion:");
        jPanel1.add(userLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 100, -1, 20));

        txtdireccion.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtdireccion.setBorder(null);
        jPanel1.add(txtdireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, 250, 30));

        jSeparator16.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator16, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 290, 250, 30));

        userLabel34.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel34.setText("Telefono:");
        jPanel1.add(userLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 170, -1, 20));

        jSeparator17.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 90, 250, 20));

        userLabel35.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel35.setText("Celular:");
        jPanel1.add(userLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 240, -1, 20));

        jSeparator18.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 250, 30));

        jSeparator19.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 250, 30));

        txtcelular.setBorder(null);
        try {
            txtcelular.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtcelular, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 260, 250, 30));

        txttelefono.setBorder(null);
        try {
            txttelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txttelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 190, 250, 30));

        jSeparator20.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 360, 250, 10));

        userLabel36.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel36.setText("NFC:");
        jPanel1.add(userLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 310, -1, 20));

        txtcedula.setBorder(null);
        try {
            txtcedula.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-#######-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtcedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, 250, 30));

        userLabel37.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel37.setText("Cedula:");
        jPanel1.add(userLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 370, -1, 20));

        jSeparator21.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator21, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 420, 250, 30));

        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablalistado);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(300, 70, 580, 210));

        loginBtn2.setBackground(new java.awt.Color(0, 134, 190));

        btnactualizar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnactualizar.setForeground(new java.awt.Color(255, 255, 255));
        btnactualizar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnactualizar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnactualizar.setText("Guardar");
        btnactualizar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnactualizar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnactualizarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnactualizarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnactualizarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn2Layout = new javax.swing.GroupLayout(loginBtn2);
        loginBtn2.setLayout(loginBtn2Layout);
        loginBtn2Layout.setHorizontalGroup(
            loginBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnactualizar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn2Layout.setVerticalGroup(
            loginBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnactualizar, javax.swing.GroupLayout.DEFAULT_SIZE, 38, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(310, 300, -1, -1));

        jSeparator22.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator22, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 490, 250, 10));

        userLabel38.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel38.setText("Codigo:");
        jPanel1.add(userLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 440, -1, 20));

        userLabel39.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel39.setText("RNC:");
        jPanel1.add(userLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 500, -1, 20));

        jSeparator23.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator23, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 550, 250, 30));

        txtcodigo.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtcodigo.setBorder(null);
        jPanel1.add(txtcodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 460, 250, 30));

        txtrnc.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtrnc.setBorder(null);
        jPanel1.add(txtrnc, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 520, 250, 30));

        txtnfc.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtnfc.setBorder(null);
        jPanel1.add(txtnfc, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 330, 250, 30));

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 910, 610));

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void btnactualizarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnactualizarMouseClicked
        guardar();
        mostrar("");// TODO add your handling code here:
    }//GEN-LAST:event_btnactualizarMouseClicked

    private void btnactualizarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnactualizarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnactualizarMouseEntered

    private void btnactualizarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnactualizarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnactualizarMouseExited

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());
        accion = "actualizar";
        btnactualizar.setText("Guardar");
        habilitar();
        txtempresa.requestFocus();
        if (evt.getClickCount() == 1) {

            id = (tablalistado.getValueAt(fila, 0).toString());
            txtempresa.setText(tablalistado.getValueAt(fila, 1).toString());
            txtdireccion.setText(tablalistado.getValueAt(fila, 2).toString());
            txttelefono.setText(tablalistado.getValueAt(fila, 3).toString());
            txtcelular.setText(tablalistado.getValueAt(fila, 4).toString());
            txtnfc.setText(tablalistado.getValueAt(fila, 5).toString());
            txtcedula.setText(tablalistado.getValueAt(fila, 6).toString());
            txtcodigo.setText(tablalistado.getValueAt(fila, 7).toString());
            txtrnc.setText(tablalistado.getValueAt(fila, 8).toString());

        }        // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoMouseClicked

    public static void main(String args[]) {

        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Fempresa().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btnactualizar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator17;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JSeparator jSeparator21;
    private javax.swing.JSeparator jSeparator22;
    private javax.swing.JSeparator jSeparator23;
    private javax.swing.JPanel loginBtn2;
    private javax.swing.JTable tablalistado;
    private javax.swing.JFormattedTextField txtcedula;
    private javax.swing.JFormattedTextField txtcelular;
    public static javax.swing.JTextField txtcodigo;
    public static javax.swing.JTextField txtdireccion;
    public static javax.swing.JTextField txtempresa;
    public static javax.swing.JTextField txtnfc;
    public static javax.swing.JTextField txtrnc;
    private javax.swing.JFormattedTextField txttelefono;
    private javax.swing.JLabel userLabel32;
    private javax.swing.JLabel userLabel33;
    private javax.swing.JLabel userLabel34;
    private javax.swing.JLabel userLabel35;
    private javax.swing.JLabel userLabel36;
    private javax.swing.JLabel userLabel37;
    private javax.swing.JLabel userLabel38;
    private javax.swing.JLabel userLabel39;
    // End of variables declaration//GEN-END:variables
}
