package Clases;

import Datos.dtEmpleado;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRempleado {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Nombre", "Cedula", "Telefono", "Fecha Nacimiento", "Edad", "Usuario", "Contraseña", "Permiso"};

        String[] registro = new String[10];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From tbempleado  where concat (nombre) like '%" + buscar + "%' order by nombre asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idempleado");
                registro[1] = rs.getString("nombre");
                registro[2] = rs.getString("cedula");
                registro[3] = rs.getString("telefono");
                registro[4] = rs.getString("fecha");
                registro[5] = rs.getString("edad");
                registro[6] = rs.getString("usuario");
                registro[7] = rs.getString("contraseña");
                registro[8] = rs.getString("permiso");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(dtEmpleado dts) {
        sSQL = "insert into tbempleado (idempleado,nombre,cedula,telefono,fecha,edad,usuario,contraseña,permiso)"
                + "VALUES(?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getId());
            pst.setString(2, dts.getNombre());
            pst.setString(3, dts.getCedula());
            pst.setString(4, dts.getTelefono());
            pst.setDate(5, dts.getFecha());
            pst.setString(6, dts.getEdad());
            pst.setString(7, dts.getUsuario());
            pst.setString(8, dts.getContraseña());
            pst.setString(9, dts.getPermiso());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(dtEmpleado dts) {
        sSQL = "UPDATE tbempleado set nombre=?,cedula=?,telefono=?,fecha=?,edad=?,usuario=?,contraseña=?,permiso=?"
                + "where idempleado=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getCedula());
            pst.setString(3, dts.getTelefono());
            pst.setDate(4, dts.getFecha());
            pst.setString(5, dts.getEdad());
            pst.setString(6, dts.getUsuario());
            pst.setString(7, dts.getContraseña());
            pst.setString(8, dts.getPermiso());
            pst.setInt(9, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(dtEmpleado dts) {
        sSQL = "DELETE FROM tbempleado WHERE idempleado=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
