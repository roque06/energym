
package Datos;

import java.sql.Date;


public class dtfactura {
    private int idfactura;
    private  String nofactura;
    private String cliente;
    private String codigocli;
    private Date fecha;
    private int cantidadmes;
    private String descripcion;
    private String monto;
    private String empleado;
    private  String precio;

    public dtfactura() {
    }

    public dtfactura(int idfactura, String nofactura, String cliente, String codigocli, Date fecha, int cantidadmes, String descripcion, String monto, String empleado, String precio) {
        this.idfactura = idfactura;
        this.nofactura = nofactura;
        this.cliente = cliente;
        this.codigocli = codigocli;
        this.fecha = fecha;
        this.cantidadmes = cantidadmes;
        this.descripcion = descripcion;
        this.monto = monto;
        this.empleado = empleado;
        this.precio = precio;
    }

    public int getIdfactura() {
        return idfactura;
    }

    public void setIdfactura(int idfactura) {
        this.idfactura = idfactura;
    }

    public String getNofactura() {
        return nofactura;
    }

    public void setNofactura(String nofactura) {
        this.nofactura = nofactura;
    }

    public String getCliente() {
        return cliente;
    }

    public void setCliente(String cliente) {
        this.cliente = cliente;
    }

    public String getCodigocli() {
        return codigocli;
    }

    public void setCodigocli(String codigocli) {
        this.codigocli = codigocli;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public int getCantidadmes() {
        return cantidadmes;
    }

    public void setCantidadmes(int cantidadmes) {
        this.cantidadmes = cantidadmes;
    }

    public String getDescripcion() {
        return descripcion;
    }

    public void setDescripcion(String descripcion) {
        this.descripcion = descripcion;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getEmpleado() {
        return empleado;
    }

    public void setEmpleado(String empleado) {
        this.empleado = empleado;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    
    
    
    
}
