/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Logica;

import Datos.Fficha;
import Formularios.Ficha;
import static Formularios.Ficha.cboregimen;
import static Formularios.Ficha.dtnaci;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import javax.swing.JOptionPane;

/**
 *
 * @author Roque
 */
public class Metodos {

    public static void habilitar() {
        Ficha.txtpss.setEnabled(true);
        Ficha.dtfecha.setEnabled(true);
        Ficha.txtnombre.setEnabled(true);
        Ficha.txtedad.setEnabled(true);
        Ficha.txtnss.setEnabled(true);
        Ficha.cboregimen.setEnabled(true);
        Ficha.txtcedula.setEnabled(true);
        Ficha.cboafiliado.setEnabled(true);
        Ficha.txtlugar.setEnabled(true);
        Ficha.txttelefono.setEnabled(true);
        Ficha.dtnaci.setEnabled(true);
        Ficha.checkcabeza.setEnabled(true);
        Ficha.checkcara.setEnabled(true);
        Ficha.checkcuello.setEnabled(true);
        Ficha.checkatm.setEnabled(true);
        Ficha.checklabios.setEnabled(true);
        Ficha.checklengua.setEnabled(true);
        Ficha.cboasma.setEnabled(true);
        Ficha.cboalergia.setEnabled(true);
        Ficha.cboepilepsia.setEnabled(true);
        Ficha.cboanemia.setEnabled(true);
        Ficha.cbohipertencion.setEnabled(true);
        Ficha.cbohepatitis.setEnabled(true);
        Ficha.cbotuberculosis.setEnabled(true);
        Ficha.cborenal.setEnabled(true);
        Ficha.cbodiabetes.setEnabled(true);
        Ficha.cbohipotension.setEnabled(true);
        Ficha.txtotros.setEnabled(true);
        Ficha.cbocaries.setEnabled(true);
        Ficha.cboabscesos.setEnabled(true);
        Ficha.cbofistulas.setEnabled(true);
        Ficha.cbohemorragia.setEnabled(true);
        Ficha.cboabrasion.setEnabled(true);
        Ficha.cbodolor.setEnabled(true);
        Ficha.cbofracturas.setEnabled(true);
        Ficha.cbobruxismo.setEnabled(true);
        Ficha.cboperiodontal.setEnabled(true);
        Ficha.txtotros2.setEnabled(true);
        Ficha.txtdireccion.setEnabled(true);
        Ficha.btnguardar.setEnabled(true);
        Ficha.txtcorreo.setEnabled(true);
        Ficha.cboseguro.setEnabled(true);

        Ficha.btnguardar2.setEnabled(true);

        Ficha.dtfecha.setDate(null);
        Ficha.txtlugar.setText("");
        Ficha.txtedad.setText("");
        Ficha.txtnss.setText("");
        Ficha.cboregimen.setSelectedItem("");
        Ficha.txtcedula.setText("");
        Ficha.cboafiliado.setSelectedItem("");
        Ficha.txtlugar.setText("");
        Ficha.txttelefono.setText("");
        Ficha.dtnaci.setDate(null);
        Ficha.txtdireccion.setText("");
        Ficha.checkcabeza.setSelected(false);
        Ficha.checkcara.setSelected(false);
        Ficha.checkatm.setSelected(false);
        Ficha.checkcuello.setSelected(false);
        Ficha.checklabios.setSelected(false);
        Ficha.checklengua.setSelected(false);
        Ficha.cboasma.setSelectedItem("No");
        Ficha.cboalergia.setSelectedItem("No");
        Ficha.cboepilepsia.setSelectedItem("No");
        Ficha.cboanemia.setSelectedItem("No");
        Ficha.cbohipertencion.setSelectedItem("No");
        Ficha.cbohepatitis.setSelectedItem("No");
        Ficha.cbotuberculosis.setSelectedItem("No");
        Ficha.cborenal.setSelectedItem("No");
        Ficha.cbodiabetes.setSelectedItem("No");
        Ficha.cbohipotension.setSelectedItem("No");
        Ficha.txtotros.setText("");
        Ficha.cbocaries.setSelectedItem("No");
        Ficha.cboabscesos.setSelectedItem("No");
        Ficha.cbofistulas.setSelectedItem("No");
        Ficha.cbohemorragia.setSelectedItem("No");
        Ficha.cboabrasion.setSelectedItem("No");
        Ficha.cbodolor.setSelectedItem("No");
        Ficha.cbofracturas.setSelectedItem("No");
        Ficha.cbobruxismo.setSelectedItem("No");
        Ficha.cboperiodontal.setSelectedItem("No");
        Ficha.txtotros2.setText("");
        Ficha.txtcorreo.setText("");
        Ficha.txtnombre.requestFocus();

    }

    public static void inhabilitar() {

        Ficha.txtpss.setEnabled(false);
        Ficha.dtfecha.setEnabled(false);
        Ficha.txtnombre.setEnabled(false);
        Ficha.txtedad.setEnabled(false);
        Ficha.txtnss.setEnabled(false);
        Ficha.cboregimen.setEnabled(false);
        Ficha.txtcedula.setEnabled(false);
        Ficha.cboafiliado.setEnabled(false);
        Ficha.txtlugar.setEnabled(false);
        Ficha.txttelefono.setEnabled(false);
        Ficha.dtnaci.setEnabled(false);
        Ficha.checkcabeza.setEnabled(false);
        Ficha.checkcara.setEnabled(false);
        Ficha.checkcuello.setEnabled(false);
        Ficha.checkatm.setEnabled(false);
        Ficha.checklabios.setEnabled(false);
        Ficha.checklengua.setEnabled(false);
        Ficha.cboasma.setEnabled(false);
        Ficha.cboalergia.setEnabled(false);
        Ficha.cboepilepsia.setEnabled(false);
        Ficha.cboanemia.setEnabled(false);
        Ficha.cbohipertencion.setEnabled(false);
        Ficha.cbohepatitis.setEnabled(false);
        Ficha.cbotuberculosis.setEnabled(false);
        Ficha.cborenal.setEnabled(false);
        Ficha.cbodiabetes.setEnabled(false);
        Ficha.cbohipotension.setEnabled(false);
        Ficha.txtotros.setEnabled(false);
        Ficha.cbocaries.setEnabled(false);
        Ficha.cboabscesos.setEnabled(false);
        Ficha.cbofistulas.setEnabled(false);
        Ficha.cbohemorragia.setEnabled(false);
        Ficha.cboabrasion.setEnabled(false);
        Ficha.cbodolor.setEnabled(false);
        Ficha.cbofracturas.setEnabled(false);
        Ficha.cbobruxismo.setEnabled(false);
        Ficha.cboperiodontal.setEnabled(false);
        Ficha.txtotros2.setEnabled(false);
        Ficha.txtdireccion.setEnabled(false);
        Ficha.btnguardar.setEnabled(false);
        Ficha.btnguardar2.setEnabled(false);
        Ficha.txtcorreo.setEnabled(false);
        Ficha.cboseguro.setEnabled(false);

        Ficha.dtfecha.setDate(null);
        Ficha.txtlugar.setText("");
        Ficha.txtedad.setText("");
        Ficha.txtnss.setText("");
        Ficha.cboregimen.setSelectedItem("");
        Ficha.txtcedula.setText("");
        Ficha.cboafiliado.setSelectedItem("");
        Ficha.txtlugar.setText("");
        Ficha.txttelefono.setText("");
        Ficha.dtnaci.setDate(null);
        Ficha.txtdireccion.setText("");
        Ficha.checkcabeza.setSelected(false);
        Ficha.checkcara.setSelected(false);
        Ficha.checkatm.setSelected(false);
        Ficha.checkcuello.setSelected(false);
        Ficha.checklabios.setSelected(false);
        Ficha.checklengua.setSelected(false);
        Ficha.cboasma.setSelectedItem("No");
        Ficha.cboalergia.setSelectedItem("No");
        Ficha.cboepilepsia.setSelectedItem("No");
        Ficha.cboanemia.setSelectedItem("No");
        Ficha.cbohipertencion.setSelectedItem("No");
        Ficha.cbohepatitis.setSelectedItem("No");
        Ficha.cbotuberculosis.setSelectedItem("No");
        Ficha.cborenal.setSelectedItem("No");
        Ficha.cbodiabetes.setSelectedItem("No");
        Ficha.cbohipotension.setSelectedItem("No");
        Ficha.txtotros.setText("");
        Ficha.txtcorreo.setText("");

        Ficha.cbocaries.setSelectedItem("No");
        Ficha.cboabscesos.setSelectedItem("No");
        Ficha.cbofistulas.setSelectedItem("No");
        Ficha.cbohemorragia.setSelectedItem("No");
        Ficha.cboabrasion.setSelectedItem("No");
        Ficha.cbodolor.setSelectedItem("No");
        Ficha.cbofracturas.setSelectedItem("No");
        Ficha.cbobruxismo.setSelectedItem("No");
        Ficha.cboperiodontal.setSelectedItem("No");
        Ficha.txtotros2.setText("");
//        Ficha.txtnombre.requestFocus();

    }

    public static void guardar() {

        Fficha dts = new Fficha();
        FRficha func = new FRficha();

        int seleccionado = Ficha.txtpss.getSelectedIndex();
        dts.setNombrepss((String) Ficha.txtpss.getItemAt(seleccionado));
        Calendar cal;
        int d, m, a;
        cal = Ficha.dtfecha.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha(new Date(a, m, d));

        dts.setNombre(Ficha.txtnombre.getText());
        dts.setEdad(Ficha.txtedad.getText());
        dts.setNss(Ficha.txtnss.getText());

        seleccionado = Ficha.cboregimen.getSelectedIndex();
        dts.setRegimen((String) Ficha.cboregimen.getItemAt(seleccionado));

        dts.setCedula(Ficha.txtcedula.getText());

        seleccionado = Ficha.cboafiliado.getSelectedIndex();
        dts.setAfiliado((String) Ficha.cboafiliado.getItemAt(seleccionado));

        dts.setLugar(Ficha.txtlugar.getText());
        dts.setTelefono(Ficha.txttelefono.getText());

        cal = Ficha.dtnaci.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFechanaci(new Date(a, m, d));

        dts.setDireccion(Ficha.txtdireccion.getText());

        if (Ficha.checkcabeza.isSelected()) {
            dts.setCabeza(Ficha.cabeza);
        } else {
            Ficha.cabeza = "No";
            dts.setCabeza(Ficha.cabeza);
        }
        if (Ficha.checkcara.isSelected()) {
            dts.setCara(Ficha.cara);
        } else {
            Ficha.cara = "No";
            dts.setCara(Ficha.cara);
        }

        if (Ficha.checkcuello.isSelected()) {
            dts.setCuello(Ficha.cuello);
        } else {
            Ficha.cuello = "No";
            dts.setCuello(Ficha.cuello);
        }
        if (Ficha.checkatm.isSelected()) {
            dts.setAtm(Ficha.atm);
        } else {
            Ficha.atm = "No";
            dts.setAtm(Ficha.atm);
        }
        if (Ficha.checklabios.isSelected()) {
            dts.setLabios(Ficha.labios);
        } else {
            Ficha.labios = "No";
            dts.setLabios(Ficha.labios);

        }
        if (Ficha.checklengua.isSelected()) {
            dts.setLengua(Ficha.lengua);
        } else {
            Ficha.lengua = "No";
            dts.setLengua(Ficha.lengua);

        }

        seleccionado = Ficha.cboasma.getSelectedIndex();
        dts.setAsma((String) Ficha.cboasma.getItemAt(seleccionado));

        seleccionado = Ficha.cboalergia.getSelectedIndex();
        dts.setAlergia((String) Ficha.cboalergia.getItemAt(seleccionado));

        seleccionado = Ficha.cboepilepsia.getSelectedIndex();
        dts.setEpilepcia((String) Ficha.cboepilepsia.getItemAt(seleccionado));

        seleccionado = Ficha.cboanemia.getSelectedIndex();
        dts.setAnemia((String) Ficha.cboanemia.getItemAt(seleccionado));

        seleccionado = Ficha.cbohipertencion.getSelectedIndex();
        dts.setHipertencion((String) Ficha.cbohipertencion.getItemAt(seleccionado));

        seleccionado = Ficha.cbohepatitis.getSelectedIndex();
        dts.setHepatitis((String) Ficha.cbohepatitis.getItemAt(seleccionado));

        seleccionado = Ficha.cbotuberculosis.getSelectedIndex();
        dts.setTuberculosis((String) Ficha.cbotuberculosis.getItemAt(seleccionado));

        seleccionado = Ficha.cborenal.getSelectedIndex();
        dts.setRenal((String) Ficha.cborenal.getItemAt(seleccionado));

        seleccionado = Ficha.cbodiabetes.getSelectedIndex();
        dts.setDiabetes((String) Ficha.cbodiabetes.getItemAt(seleccionado));

        seleccionado = Ficha.cbohipotension.getSelectedIndex();
        dts.setHipotencion((String) Ficha.cbohipotension.getItemAt(seleccionado));

        dts.setOtros(Ficha.txtotros.getText());

        seleccionado = Ficha.cbocaries.getSelectedIndex();
        dts.setCaries((String) Ficha.cbocaries.getItemAt(seleccionado));

        seleccionado = Ficha.cboabscesos.getSelectedIndex();
        dts.setAbscesos((String) Ficha.cboabscesos.getItemAt(seleccionado));

        seleccionado = Ficha.cbofistulas.getSelectedIndex();
        dts.setFistulas((String) Ficha.cbofistulas.getItemAt(seleccionado));

        seleccionado = Ficha.cbohemorragia.getSelectedIndex();
        dts.setHemorragia((String) Ficha.cbohemorragia.getItemAt(seleccionado));

        seleccionado = Ficha.cboabrasion.getSelectedIndex();
        dts.setAbrasion((String) Ficha.cboabrasion.getItemAt(seleccionado));

        seleccionado = Ficha.cbodolor.getSelectedIndex();
        dts.setDolor((String) Ficha.cbodolor.getItemAt(seleccionado));

        seleccionado = Ficha.cbofracturas.getSelectedIndex();
        dts.setFracturas((String) Ficha.cbofracturas.getItemAt(seleccionado));

        seleccionado = Ficha.cbobruxismo.getSelectedIndex();
        dts.setBruxismo((String) Ficha.cbobruxismo.getItemAt(seleccionado));

        seleccionado = Ficha.cboperiodontal.getSelectedIndex();
        dts.setPeriodontal((String) Ficha.cboperiodontal.getItemAt(seleccionado));

        dts.setOtros2(Ficha.txtotros2.getText());

        dts.setCodpaciente(Ficha.lblcodigo.getText());
        dts.setCorreo(Ficha.txtcorreo.getText());
        seleccionado = Ficha.cboseguro.getSelectedIndex();
        dts.setTiposeguro((String) Ficha.cboseguro.getItemAt(seleccionado));

        if (Ficha.accion.equals("guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(null, "Paciente Registrado");
                inhabilitar();

            }

        } else if (Ficha.accion.equals("editar")) {
            dts.setId(Integer.parseInt(Ficha.id));
            if (func.editar(dts)) {
                JOptionPane.showMessageDialog(null, "Paciente Actualizado Correctamente");
                inhabilitar();

            }

        }

    }

}
