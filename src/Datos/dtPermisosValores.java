/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Datos;

/**
 *
 * @author Fray Alberto Helena
 */
public class dtPermisosValores {

    public static String PACIENTES = "pacientes";
    public static String PAGOS = "pagos";
    public static String CITAS = "citas";
    public static String TRABAJOS_LABORATORIOS = "trabajos_laboratorios";
    public static String REGIMEN = "regimen_afiliado";
    public static String SERVICIOS = "servicio_producto";
    public static String LABORATORIO = "laboratorio";
    public static String DOCTOR = "doctor";
    public static String USUARIO = "USUARIO";
    public static String CORREOS = "correo";
    public static String REPORTE_DIARIO = "reporte_diario";
    public static String PERMISO = "permiso";
    public static String TipoSeguro = "tipo_seguro";
    public static String Seguros = "rpseguros";
    public static String EMPRESA = "empresa";

    public dtPermisosValores() {

    }

    public String getPACIENTES() {
        return PACIENTES;
    }

    public String getPAGOS() {
        return PAGOS;
    }

    public String getCITAS() {
        return CITAS;
    }

    public String getTRABAJOS_LABORATORIOS() {
        return TRABAJOS_LABORATORIOS;
    }

    public static String getREGIMEN() {
        return REGIMEN;
    }

    public static String getSERVICIOS() {
        return SERVICIOS;
    }

    public static String getLABORATORIO() {
        return LABORATORIO;
    }

    public static String getDOCTOR() {
        return DOCTOR;
    }

    public static String getUSUARIO() {
        return USUARIO;
    }

    public static String getCORREOS() {
        return CORREOS;
    }

    public static String getREPORTE() {
        return REPORTE_DIARIO;
    }

    public static String getPERMISO() {
        return PERMISO;
    }

    public static String getTipoSeguro() {
        return TipoSeguro;
    }

    public static String getSeguros() {
        return Seguros;
    }
    
     public static String getEMPRESA() {
        return EMPRESA;
    }
    

}
