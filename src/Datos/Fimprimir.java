package Datos;

public class Fimprimir {

    private String codigo;
    private String pago;
    private String totales;
    private String abono;
    private String restante;
    private String montomensual;
    private String resta;
    private String sumas;

    public Fimprimir() {
    }

    public Fimprimir(String codigo, String pago, String totales, String abono, String restante, String montomensual, String resta, String sumas) {
        this.codigo = codigo;
        this.pago = pago;
        this.totales = totales;
        this.abono = abono;
        this.restante = restante;
        this.montomensual = montomensual;
        this.resta = resta;
        this.sumas = sumas;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getPago() {
        return pago;
    }

    public void setPago(String pago) {
        this.pago = pago;
    }

    public String getTotales() {
        return totales;
    }

    public void setTotales(String totales) {
        this.totales = totales;
    }

    public String getAbono() {
        return abono;
    }

    public void setAbono(String abono) {
        this.abono = abono;
    }

    public String getRestante() {
        return restante;
    }

    public void setRestante(String restante) {
        this.restante = restante;
    }

    public String getMontomensual() {
        return montomensual;
    }

    public void setMontomensual(String montomensual) {
        this.montomensual = montomensual;
    }

    public String getResta() {
        return resta;
    }

    public void setResta(String resta) {
        this.resta = resta;
    }

    public String getSumas() {
        return sumas;
    }

    public void setSumas(String sumas) {
        this.sumas = sumas;
    }

    
    
}
