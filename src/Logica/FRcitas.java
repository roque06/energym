package Logica;

import Datos.Fcitas;
import Datos.Fregimen;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRcitas {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Nombre", "Telefono", "Correo", "Sintomas", "Registro", "Fecha", "Genero", "Hora", "Horasm", "Minutos", "AM"};

        String[] registro = new String[12];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select idcitas,nombre,telefono,correo,sintomas,DATE_FORMAT (fecha,'%d/%m/%Y')as registro,fecha,genero,hora,horam,minutos,am From tbcitas  where concat (nombre,'',fecha) like '%" + buscar + "%' order by idcitas asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idcitas");
                registro[1] = rs.getString("nombre");
                registro[2] = rs.getString("telefono");
                registro[3] = rs.getString("correo");
                registro[4] = rs.getString("sintomas");
                registro[5] = rs.getString("registro");
                registro[6] = rs.getString("fecha");
                registro[7] = rs.getString("genero");
                registro[8] = rs.getString("hora");
                registro[9] = rs.getString("horam");
                registro[10] = rs.getString("minutos");
                registro[11] = rs.getString("am");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fcitas dts) {
        sSQL = "insert into tbcitas (nombre,telefono,correo,sintomas,fecha,genero,hora,horam,minutos,am)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getTelefono());
            pst.setString(3, dts.getCorreo());
            pst.setString(4, dts.getSintomas());
            pst.setDate(5, dts.getFecha());
            pst.setString(6, dts.getGenero());
            pst.setString(7, dts.getHora());
            pst.setString(8, dts.getHoram());
            pst.setString(9, dts.getMinutos());
            pst.setString(10, dts.getAm());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fcitas dts) {
        sSQL = "UPDATE tbcitas set nombre=?,telefono=?,correo=?,sintomas=?,fecha=?,genero=?,hora=?,horam=?,minutos=?,am=?"
                + "where idcitas=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getTelefono());
            pst.setString(3, dts.getCorreo());
            pst.setString(4, dts.getSintomas());
            pst.setDate(5, dts.getFecha());
            pst.setString(6, dts.getGenero());
            pst.setString(7, dts.getHora());
            pst.setString(8, dts.getHoram());
            pst.setString(9, dts.getMinutos());
            pst.setString(10, dts.getAm());
            pst.setInt(11, dts.getId());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fcitas dts) {
        sSQL = "DELETE FROM tbcitas WHERE idcitas=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
