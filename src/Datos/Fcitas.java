package Datos;

import java.sql.Date;

public class Fcitas {

   private int id;
   private String nombre;
   private String telefono;
   private String sintomas;
   private Date fecha;
   private  String genero;
   private String hora;
   private String status;
   private String correo;
   private String  horam;
   private String  minutos;
   private String am;
    public Fcitas() {
    }

    public Fcitas(int id, String nombre, String telefono, String sintomas, Date fecha, String genero, String hora, String status, String correo, String horam, String minutos, String am) {
        this.id = id;
        this.nombre = nombre;
        this.telefono = telefono;
        this.sintomas = sintomas;
        this.fecha = fecha;
        this.genero = genero;
        this.hora = hora;
        this.status = status;
        this.correo = correo;
        this.horam = horam;
        this.minutos = minutos;
        this.am = am;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getSintomas() {
        return sintomas;
    }

    public void setSintomas(String sintomas) {
        this.sintomas = sintomas;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getGenero() {
        return genero;
    }

    public void setGenero(String genero) {
        this.genero = genero;
    }

    public String getHora() {
        return hora;
    }

    public void setHora(String hora) {
        this.hora = hora;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getHoram() {
        return horam;
    }

    public void setHoram(String horam) {
        this.horam = horam;
    }

    public String getMinutos() {
        return minutos;
    }

    public void setMinutos(String minutos) {
        this.minutos = minutos;
    }

    public String getAm() {
        return am;
    }

    public void setAm(String am) {
        this.am = am;
    }
 
   
   
   
    
}
