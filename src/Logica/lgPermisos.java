package Logica;

import Datos.dtPermisos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Fray Alberto Helena
 */
public class lgPermisos {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"IDPermiso", "Nombre", "Pacientes", "Pagos", "Citas", "Trabajos Laboratorios", "Regimen y Afiliado", "Servicios", "Laboratorio", "Doctor", "Usuario", "Correo", "Reporte Diario", "Permisos", "Seguros", "Empresa"};
//ocultar columna con este codigo -> jTable1.getColumnModel().getColumn(7).setPreferredWidth(0);
        Object[] registro = new Object[18];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select * from tblpermisos order by nombre_permiso asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getInt("id_permiso");
                registro[1] = rs.getString("nombre_permiso");
                registro[2] = rs.getBoolean("pacientes");
                registro[3] = rs.getBoolean("pagos");
                registro[4] = rs.getBoolean("citas");
                registro[5] = rs.getBoolean("trabajos_laboratorios");
                registro[6] = rs.getBoolean("regimen_afiliado");
                registro[7] = rs.getBoolean("servicio_producto");
                registro[8] = rs.getBoolean("laboratorio");
                registro[9] = rs.getBoolean("doctor");
                registro[10] = rs.getBoolean("usuario");
                registro[11] = rs.getBoolean("correo");
                registro[12] = rs.getBoolean("reporte_diario");
                registro[13] = rs.getBoolean("permiso");
                registro[14] = rs.getBoolean("rpseguros");
                registro[15] = rs.getBoolean("empresa");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(dtPermisos dts) {
        sSQL = "INSERT INTO tblpermisos "
                + "(nombre_permiso, pacientes, pagos, citas, trabajos_laboratorios,regimen_afiliado,servicio_producto,laboratorio,doctor,usuario,correo,reporte_diario,permiso,rpseguros,empresa)"
                + " VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre_permiso());
            pst.setBoolean(2, dts.isPacientes());
            pst.setBoolean(3, dts.isPagos());
            pst.setBoolean(4, dts.isCitas());
            pst.setBoolean(5, dts.isTrabajos_laboratorios());
            pst.setBoolean(6, dts.isRegimen());
            pst.setBoolean(7, dts.isProducto());
            pst.setBoolean(8, dts.isLaboratorio());
            pst.setBoolean(9, dts.isDoctor());
            pst.setBoolean(10, dts.isUsuario());
            pst.setBoolean(11, dts.isCorreo());
            pst.setBoolean(12, dts.isDiario());
            pst.setBoolean(13, dts.isPermiso());
            pst.setBoolean(14, dts.isSeguro());
            pst.setBoolean(15, dts.isEmpresa());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(dtPermisos dts) {
        sSQL = "UPDATE tblpermisos SET nombre_permiso = ? , pacientes = ?, pagos = ?, citas = ?, trabajos_laboratorios = ?,regimen_afiliado= ?,servicio_producto= ?,laboratorio= ?,doctor= ?,usuario= ?,correo= ?,reporte_diario= ?,permiso= ?,rpseguros=?,empresa=? WHERE id_permiso =?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre_permiso());
            pst.setBoolean(2, dts.isPacientes());
            pst.setBoolean(3, dts.isPagos());
            pst.setBoolean(4, dts.isCitas());
            pst.setBoolean(5, dts.isTrabajos_laboratorios());
            pst.setBoolean(6, dts.isRegimen());
            pst.setBoolean(7, dts.isProducto());
            pst.setBoolean(8, dts.isLaboratorio());
            pst.setBoolean(9, dts.isDoctor());
            pst.setBoolean(10, dts.isUsuario());
            pst.setBoolean(11, dts.isCorreo());
            pst.setBoolean(12, dts.isDiario());
            pst.setBoolean(13, dts.isPermiso());
            pst.setBoolean(14, dts.isSeguro());
            pst.setBoolean(15, dts.isEmpresa());
            pst.setInt(16, dts.getId_permiso());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(dtPermisos dts) {
        sSQL = "DELETE FROM tblpermisos WHERE id_permiso=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId_permiso());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (SQLException e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
