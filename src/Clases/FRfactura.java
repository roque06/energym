package Clases;

import Datos.dtcliente;
import Datos.dtfactura;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRfactura {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"IDfactura", "No.Factura", "CLIENTE", "CODIGO", "FECHA", "Cant", "DESCRIPCION", "MONTO", "EMPLEADO", "PRECIO"};

        String[] registro = new String[16];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select idfactura,nofactura,cliente,codigocli,fecha,cantidadmes,descripcion,monto,empleado,precio From tbfactura where concat (cliente,codigocli) like '%" + buscar + "%' order by idfactura desc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idfactura");
                registro[1] = rs.getString("nofactura");
                registro[2] = rs.getString("cliente");
                registro[3] = rs.getString("codigocli");
                registro[4] = rs.getString("fecha");
                registro[5] = rs.getString("cantidadmes");
                registro[6] = rs.getString("descripcion");
                registro[7] = rs.getString("monto");
                registro[8] = rs.getString("empleado");
                registro[9] = rs.getString("precio");
             
                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(dtfactura dts) {
        sSQL = "insert into tbfactura (idfactura,nofactura,cliente,codigocli,fecha,cantidadmes,descripcion,monto,empleado,precio)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getIdfactura());
            pst.setString(2, dts.getNofactura());
            pst.setString(3, dts.getCliente());
            pst.setString(4, dts.getCodigocli());
            pst.setDate(5, dts.getFecha());
            pst.setInt(6, dts.getCantidadmes());
            pst.setString(7, dts.getDescripcion());
            pst.setString(8, dts.getMonto());
            pst.setString(9, dts.getEmpleado());
              pst.setString(10, dts.getPrecio());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(dtfactura dts) {
        sSQL = "UPDATE tbfactura set nofactura=?,cliente=?,codigocli=?,fecha=?,cantidadmes=?,descripcion=?"
                + "where idfactura=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNofactura());
            pst.setString(2, dts.getCliente());
            pst.setString(3, dts.getCodigocli());
            pst.setDate(4, dts.getFecha());
            pst.setInt(5, dts.getCantidadmes());
            pst.setString(6, dts.getDescripcion());

            pst.setInt(7, dts.getIdfactura());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(dtfactura dts) {
        sSQL = "DELETE FROM tbfactura WHERE idfactura=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getIdfactura());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
