
package Datos;

import java.sql.Date;

public class Fprocedimiento {
    
    private int id;
    private String codigo;
    private String procedimiento;
    private String nombre;
    private String fecha;

    public Fprocedimiento() {
    }

    public Fprocedimiento(int id, String codigo, String procedimiento, String nombre, String fecha) {
        this.id = id;
        this.codigo = codigo;
        this.procedimiento = procedimiento;
        this.nombre = nombre;
        this.fecha = fecha;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }

    public String getProcedimiento() {
        return procedimiento;
    }

    public void setProcedimiento(String procedimiento) {
        this.procedimiento = procedimiento;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getFecha() {
        return fecha;
    }

    public void setFecha(String fecha) {
        this.fecha = fecha;
    }
    
    
    
    
}
