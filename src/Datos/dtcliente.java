
package Datos;

import java.sql.Date;

public class dtcliente {
    private int id;
    private int codigo;
    private String nombre;
    private String telefono;
    private String email;
    private String cedula;
    private String direccion;
    private Date registro;
    private Date pagos;
    private String mensualidad;
    private String meses;
    private String monto;
    private String estado;

    public dtcliente() {
    }

    public dtcliente(int id, int codigo, String nombre, String telefono, String email, String cedula, String direccion, Date registro, Date pagos, String mensualidad, String meses, String monto, String estado) {
        this.id = id;
        this.codigo = codigo;
        this.nombre = nombre;
        this.telefono = telefono;
        this.email = email;
        this.cedula = cedula;
        this.direccion = direccion;
        this.registro = registro;
        this.pagos = pagos;
        this.mensualidad = mensualidad;
        this.meses = meses;
        this.monto = monto;
        this.estado = estado;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getCodigo() {
        return codigo;
    }

    public void setCodigo(int codigo) {
        this.codigo = codigo;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getTelefono() {
        return telefono;
    }

    public void setTelefono(String telefono) {
        this.telefono = telefono;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCedula() {
        return cedula;
    }

    public void setCedula(String cedula) {
        this.cedula = cedula;
    }

    public String getDireccion() {
        return direccion;
    }

    public void setDireccion(String direccion) {
        this.direccion = direccion;
    }

    public Date getRegistro() {
        return registro;
    }

    public void setRegistro(Date registro) {
        this.registro = registro;
    }

    public Date getPagos() {
        return pagos;
    }

    public void setPagos(Date pagos) {
        this.pagos = pagos;
    }

    public String getMensualidad() {
        return mensualidad;
    }

    public void setMensualidad(String mensualidad) {
        this.mensualidad = mensualidad;
    }

    public String getMeses() {
        return meses;
    }

    public void setMeses(String meses) {
        this.meses = meses;
    }

    public String getMonto() {
        return monto;
    }

    public void setMonto(String monto) {
        this.monto = monto;
    }

    public String getEstado() {
        return estado;
    }

    public void setEstado(String estado) {
        this.estado = estado;
    }
    

}
