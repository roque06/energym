
SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for odontograma
-- ----------------------------
DROP TABLE IF EXISTS `odontograma`;
CREATE TABLE `odontograma`  (
  `id_pacientes` int(8) NOT NULL,
  `cedula` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `hora` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `diagnostico` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `plan_trata` varchar(400) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `costo` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `estatus` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  INDEX `cedula`(`cedula`) USING BTREE,
  INDEX `id_pacientes`(`id_pacientes`) USING BTREE,
  CONSTRAINT `odontograma_ibfk_1` FOREIGN KEY (`cedula`) REFERENCES `pacientes` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `odontograma_ibfk_2` FOREIGN KEY (`id_pacientes`) REFERENCES `pacientes` (`id_pacientes`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of odontograma
-- ----------------------------
INSERT INTO `odontograma` VALUES (4, '22345789', '09/06/2017', '06:23:25', 'hola', 'fnkdsbb', '105000', 'Iniciado');
INSERT INTO `odontograma` VALUES (5, '34567890', '12/06/2017', '7:00 am', 'lkdfn', 'hvkh', '65789', 'Iniciado');
INSERT INTO `odontograma` VALUES (5, '34567890', '18/06/2017', '11:09:51', 'ggujkgu', 'bcjg j', 'hvhkvh', 'Iniciado');
INSERT INTO `odontograma` VALUES (5, '34567890', '18/06/2017', '01:53:22', 'fgf', 'dff', '134567', 'Iniciado');
INSERT INTO `odontograma` VALUES (5, '34567890', '18/06/2017', '01:56:25', 'hbv h', 'jvh', 'b m', 'Iniciado');
INSERT INTO `odontograma` VALUES (5, '34567890', '18/06/2017', '02:28:35', 'jajaj', 'jejej', '2000', 'Iniciado');
INSERT INTO `odontograma` VALUES (4, '22345789', '18/06/2017', '02:41:41', 'je', 'fbn', '90', 'Iniciado');
INSERT INTO `odontograma` VALUES (4, '22345789', '18/06/2017', '02:42:45', 'jo', 'nn', '80', 'Iniciado');
INSERT INTO `odontograma` VALUES (7, '5678934', '08/07/2017', '7:00 am', '.kjbjk.', 'ubj', '9000', 'Iniciado');
INSERT INTO `odontograma` VALUES (8, '9876543', '08/07/2017', '2:00 pm', 'kk kj', 'jbo', '90000', 'Iniciado');
INSERT INTO `odontograma` VALUES (9, '23456789', '20/07/2017', '7:00 am', 'hvvhyvhy.vhv', 'vkv kvu', '45000', 'En Proceso');
INSERT INTO `odontograma` VALUES (11, '25560589', '27/07/2017', '7:00 am', 'diente 11 fractura', 'exodoncia', '9000', 'Iniciado');
INSERT INTO `odontograma` VALUES (16, '5678909866', '03/08/2017', '7:00 am', 'yhgyjvy', 'jhjgjbu', 'v j j', 'En Proceso');
INSERT INTO `odontograma` VALUES (17, '678900987', '03/08/2017', '7:00 am', 'vhbjklk', 'ghkl', '679986', 'Iniciado');
INSERT INTO `odontograma` VALUES (18, '577889987', '03/08/2017', '7:00 am', 'jjhu', 'hjjh', '', 'Iniciado');
INSERT INTO `odontograma` VALUES (19, '6789086', '03/08/2017', '7:00 am', 'fghjk', 'cbm,', '', 'Iniciado');
INSERT INTO `odontograma` VALUES (20, '0987654', '03/08/2017', '7:00 am', 'bjk,j', 'bnm,m', '', 'Iniciado');
INSERT INTO `odontograma` VALUES (21, '5678909', '04/08/2017', '7:00 am', 'hjkl{k', 'ghkll', '567886', 'Iniciado');
INSERT INTO `odontograma` VALUES (21, '5678909', '15/08/2017', '07:13:51', 'hhinkjjj', 'ghkll', '567886', 'Iniciado');
INSERT INTO `odontograma` VALUES (1, '25560587', '03/03/2022', '11:29:19', 'sdfsdf', 'sdfsdf', '33', 'En Proceso');

-- ----------------------------
-- Table structure for pacientes
-- ----------------------------
DROP TABLE IF EXISTS `pacientes`;
CREATE TABLE `pacientes`  (
  `id_pacientes` int(8) NOT NULL AUTO_INCREMENT,
  `cedula` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nacionalidad` varchar(2) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fecha` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `nombre` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `apellido` varchar(30) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `fn` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `sexo` varchar(9) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `estado_civil` varchar(15) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `ocupacion` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `direccion` varchar(120) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `cod_tlf` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telefono` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `cod_tlfm` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `telefono_movil` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `imagen` longblob NOT NULL,
  `ruta_imagen` varchar(100) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `email` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `motivo` varchar(500) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `emer_nom` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `emer_cod` varchar(4) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `emer_tel` varchar(12) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `emer_parente` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  PRIMARY KEY (`id_pacientes`) USING BTREE,
  UNIQUE INDEX `cedula`(`cedula`) USING BTREE
) ENGINE = InnoDB AUTO_INCREMENT = 22 CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of pacientes
-- ----------------------------
INSERT INTO `pacientes` VALUES (1, '25560587', 'V', '18/05/2017', 'Janethd', 'Perez', '18/05/1996', 'Femenino', 'Soltero', 'Estudiantes', 'av. las americas', '0273', '2632024', '0416', '3541258', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'janeth@gmail.com', 'meulea', 'yanet', '0426', '9170035', 'Madre');
INSERT INTO `pacientes` VALUES (2, '22654742', 'V', '21/05/2017', 'genesis', 'lobo', '21/05/2010', 'Femenino', 'Soltero', 'estudiante', 'lkdnfiw', '0274', '2631024', '0416', '9170035', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'lobo@gmail.com', 'dolor de muela', 'maria', '0417', '9876543', 'Madre');
INSERT INTO `pacientes` VALUES (3, '21185952', 'V', '26/05/2017', 'luis', 'maldonado', '25/05/2011', 'Masculino', 'Soltero', 'masculino', 'belenzatw', '0273', '2632024', '0424', '2345678', null, 'C:\\Users\\jitzell perez\\Documents\\Doc1_archivos\\image001.png', 'luis@gmail.com', 'lkfnvlke', 'kfnv', '0417', '1234567', 'Hermano(a)');
INSERT INTO `pacientes` VALUES (4, '22345789', 'V', '09/06/2017', 'olivia', 'morani', '17/10/2000', 'Masculino', 'Soltero', 'estudiante', 'fn vk.s', '0273', '2435678', '0414', '1234567', null, 'C:\\Users\\jitzell perez\\Documents\\Doc1_archivos\\image002.jpg', 'hsdalfh1@gmail.com', 'knfkvnskn snvsnskbllblsn ihfihs vis i vi ifvis sfv', 'fkdvlds', '0424', '2345678', 'Esposo(a)');
INSERT INTO `pacientes` VALUES (5, '34567890', 'V', '12/06/2017', 'lfd', 'lsdm', '01/06/2017', 'Femenino', 'Viudo', 'knlk', 'adnak nfiaerf', '0273', '567678', '0414', '556677', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'nkkak@gmail.com', 'lknklanv', 'jkdbc', '0424', '2345678', 'Conocido(a)');
INSERT INTO `pacientes` VALUES (6, '24567890', 'V', '08/07/2017', 'mjido', 'knindkn', '15/07/2017', 'Femenino', 'Soltero', 'zdff', 'nafleanf', '0273', '3434546', '0414', '2345678', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'fvfs@gmail.com', 'knkfvr', 'fgtt', '0416', '3443', 'Hermano(a)');
INSERT INTO `pacientes` VALUES (7, '5678934', 'V', '08/07/2017', 'hvhjvh', 'hvk', '13/07/1996', 'Femenino', 'Soltero', 'uguii', 'dnkwdf', '0212', '2345566', '0414', '77878', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'hvjh@gmail.com', 'lkknsklds', 'jbl', '0416', '5678987', 'Bisabuelo(a)');
INSERT INTO `pacientes` VALUES (8, '9876543', 'V', '08/07/2017', 'hvhu', 'fvfvd', '01/07/2005', 'Femenino', 'Soltero', 'fdfdf', 'dfaerf', '0212', '3434', '0414', '234343', null, 'C:\\Users\\jitzell perez\\Desktop\\adrii\\imagenes\\1.jpg', 'bck@gmail.com', 'efearf', 'wedea', '0416', '757587', 'Bisabuelo(a)');
INSERT INTO `pacientes` VALUES (9, '23456789', 'V', '20/07/2017', 'hvv', 'klfnkl', '14/07/2000', 'Femenino', 'Soltero', 'kkbkugu', 'n vhk knvh', '0273', '4567890', '0414', '6677899', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'lkfk@gmail.com', 'kb dks', 'jhjg', '0414', '456789', 'Bisabuelo(a)');
INSERT INTO `pacientes` VALUES (10, '25560588', 'V', '27/07/2017', 'sol', 'Maldonado', '02/08/1996', 'Femenino', 'Soltero', 'estudiante', 'av las americas', '0273', '2632924', '0414', '0414567', null, 'C:\\Users\\jitzell perez\\Desktop\\bloques\\imagenes\\1498785885_girl.png', 'sol@gmail.com', 'dolor de muela', 'ramon', '0424', '3456789', 'Amigo(a)');
INSERT INTO `pacientes` VALUES (11, '25560589', 'V', '27/07/2017', 'Sol', 'Maldonado', '09/08/1996', 'Femenino', 'Soltero', 'estudiante', 'av las americas', '0273', '2534789', '0414', '2456789', null, 'C:\\Users\\jitzell perez\\Desktop\\bloques\\imagenes\\1498785885_girl.png', 'sol@gmail.com', 'dolor de muela', 'raon', '0424', '7893076', 'Abuelo(a)');
INSERT INTO `pacientes` VALUES (12, '22340768', 'V', '30/07/2017', 'jnlfv', 'ndfvkad', '24/07/1992', 'Femenino', 'Soltero', 'zfvzdfg', 'anfdakfn', '0273', '1234567', '0414', '1234567', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'khnkj@gmail.com', 'lkdnfdenfl	', 'dfref', '0424', '1234567', 'Abuelo(a)');
INSERT INTO `pacientes` VALUES (13, '34567898', 'V', '30/07/2017', 'kdzfnvk', 'fvnkfvn', '27/07/2002', 'Femenino', 'Soltero', 'SADEF', 'DFCV|', '0273', '34567', '0414', '23456', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'DCGH@GMAIL.COM', 'KLFCNKLFV', 'DFVF', '0424', '5678986', 'Abuelo(a)');
INSERT INTO `pacientes` VALUES (14, '4578999', 'V', '03/08/2017', 'cdfkv', 'wfre', '14/08/1998', 'Femenino', 'Soltero', 'dfghjjgfg', 'hbdjbjef', '0212', '245678', '0414', '7654321', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'jkdnjkd@gmail.com', 'jcbhjdbvjdf', 'fghjkjhg', '0424', '3456789', 'Abuelo(a)');
INSERT INTO `pacientes` VALUES (15, '76543289', 'V', '03/08/2017', 'hjvh', 'hghg', '13/08/1993', 'Masculino', 'Soltero', 'hgcjg', 'c nm,.kjh', '0212', '4567898', '0414', '4567898', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'jhhjkjh@gmail.com', 'ghjkl{{kj', 'lkjhhjk', '0424', '7656787', 'Amigo(a)');
INSERT INTO `pacientes` VALUES (16, '5678909866', 'V', '03/08/2017', 'dgdgh', 'dfgdr', '19/08/1995', 'Femenino', 'Soltero', 'ghjkkjhg', 'sdsef', '0212', '4567876', '0424', '9876543', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'hjkkjhj@gmail.com', 'sdfdsef', 'frrrree', '0414', '7898765', 'Abuelo(a)');
INSERT INTO `pacientes` VALUES (17, '678900987', 'V', '03/08/2017', 'ghkllkjh', 'ghjkllj', '23/08/1996', 'Femenino', 'Soltero', 'hjkllkj', 'gvjvj', '0212', '6789087', '0416', '9876543', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'hjklj@gmail.com', 'huin unkui', 'njyujnmjyuj', '0414', '3456789', 'Abuelo(a)');
INSERT INTO `pacientes` VALUES (18, '577889987', 'V', '03/08/2017', 'gbgf', 'fdjhj', '13/08/1999', 'Femenino', 'Soltero', 'bbbb', 'dfvfjuuuu', '0273', '7777788', '0414', '7678989', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'njoki@gmail.com', 'bygygygt6', 'ghygyg', '0414', '7654326', 'Abuelo(a)');
INSERT INTO `pacientes` VALUES (19, '6789086', 'V', '03/08/2017', 'hkloi', 'hjkllkjhghjk', '16/08/1996', 'Femenino', 'Soltero', 'ghjkllkjhgf', 'hjklkjhg', '0212', '8765432', '0426', '9876543', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'hjklkj@gmail.com', 'hghjkllkj', 'ghkllj', '0414', '7890086', 'Abuelo(a)');
INSERT INTO `pacientes` VALUES (20, '0987654', 'V', '03/08/2017', 'vbbbb', 'dfhh', '15/08/1992', '', 'Soltero', '', 'hjklkj', '---', '', '0416', '8767899', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', '', 'fghjkl.kj', '', '---', '', '---');
INSERT INTO `pacientes` VALUES (21, '5678909', 'V', '04/08/2017', 'janeh', 'perez', '13/08/1993', 'Femenino', 'Soltero', 'fghjkllkj', 'ghjkl.kj', '0212', '5678998', '0416', '6789009', null, 'C:\\Users\\jitzell perez\\Documents\\CamSuite Gallery\\3.jpg', 'janeh@gmail.com', 'dolor de muela', 'jaja', '0414', '041398', 'Cuado(a)');

-- ----------------------------
-- Table structure for piezas
-- ----------------------------
DROP TABLE IF EXISTS `piezas`;
CREATE TABLE `piezas`  (
  `id_pacientes` int(6) NOT NULL,
  `cedula` varchar(10) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `id_diente` varchar(20) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `estatus_d` varchar(50) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  `tipo` varchar(6) CHARACTER SET latin1 COLLATE latin1_swedish_ci NOT NULL,
  INDEX `cedula`(`cedula`) USING BTREE,
  INDEX `id_pacientes`(`id_pacientes`) USING BTREE,
  CONSTRAINT `piezas_ibfk_1` FOREIGN KEY (`cedula`) REFERENCES `pacientes` (`cedula`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `piezas_ibfk_2` FOREIGN KEY (`id_pacientes`) REFERENCES `pacientes` (`id_pacientes`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE = InnoDB CHARACTER SET = latin1 COLLATE = latin1_swedish_ci ROW_FORMAT = Dynamic;

-- ----------------------------
-- Records of piezas
-- ----------------------------
INSERT INTO `piezas` VALUES (4, '22345789', '11', 'Ausente', '');
INSERT INTO `piezas` VALUES (4, '22345789', '27', 'Fracturo', '');
INSERT INTO `piezas` VALUES (4, '22345789', '36', 'Tratamiento Conducto Bueno', '');
INSERT INTO `piezas` VALUES (4, '22345789', '41', 'Tratamiento Conducto Malo', '');
INSERT INTO `piezas` VALUES (4, '22345789', '12a', 'Buen Estado', '');
INSERT INTO `piezas` VALUES (4, '22345789', '12b', 'Buen Estado', '');
INSERT INTO `piezas` VALUES (4, '22345789', '12c', 'Buen Estado', '');
INSERT INTO `piezas` VALUES (4, '22345789', '12d', 'Buen Estado', '');
INSERT INTO `piezas` VALUES (4, '22345789', '12e', 'Buen Estado', '');
INSERT INTO `piezas` VALUES (4, '22345789', '14', 'Exodoncia', '');
INSERT INTO `piezas` VALUES (4, '22345789', '22', 'Corona Buena', '');
INSERT INTO `piezas` VALUES (4, '22345789', '44', 'Tratamiento Conducto Malo', '');
INSERT INTO `piezas` VALUES (5, '34567890', '11', 'Fracturo', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '16', 'Corona Buena', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '21a', 'Carie', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '21b', 'Carie', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '21c', 'Carie', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '21d', 'Carie', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '21e', 'Carie', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '43', 'Tratamiento Conducto Bueno', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '37', 'Tratamiento Conducto Bueno', 'L');
INSERT INTO `piezas` VALUES (5, '34567890', '11', 'Exodoncia', 'A');
INSERT INTO `piezas` VALUES (5, '34567890', '41', 'Tratamiento Conducto Bueno', 'A');
INSERT INTO `piezas` VALUES (6, '24567890', '13', 'Corona Buena', 'L');
INSERT INTO `piezas` VALUES (6, '24567890', '17a', 'Buen Estado', 'L');
INSERT INTO `piezas` VALUES (6, '24567890', '21', 'Fracturo', 'L');
INSERT INTO `piezas` VALUES (6, '24567890', '31d', 'Buen Estado', 'L');
INSERT INTO `piezas` VALUES (6, '24567890', '31e', 'Buen Estado', 'L');
INSERT INTO `piezas` VALUES (6, '24567890', '41', 'Corona Buena', 'L');
INSERT INTO `piezas` VALUES (6, '24567890', '42', 'Fracturo', 'L');
INSERT INTO `piezas` VALUES (6, '24567890', '46', 'Tratamiento Conducto Bueno', 'L');
INSERT INTO `piezas` VALUES (6, '24567890', '47', 'Tratamiento Conducto Bueno', 'L');
INSERT INTO `piezas` VALUES (7, '5678934', '11', 'Corona Buena', 'A');
INSERT INTO `piezas` VALUES (7, '5678934', '23', 'Tratamiento Conducto Malo', 'A');
INSERT INTO `piezas` VALUES (7, '5678934', '32', 'Corona Mala', 'A');
INSERT INTO `piezas` VALUES (7, '5678934', '41a', 'Carie', 'A');
INSERT INTO `piezas` VALUES (7, '5678934', '41b', 'Carie', 'A');
INSERT INTO `piezas` VALUES (7, '5678934', '41c', 'Carie', 'A');
INSERT INTO `piezas` VALUES (7, '5678934', '41d', 'Carie', 'A');
INSERT INTO `piezas` VALUES (7, '5678934', '41e', 'Carie', 'A');
INSERT INTO `piezas` VALUES (7, '5678934', '42', 'Tratamiento Conducto Bueno', 'A');
INSERT INTO `piezas` VALUES (8, '9876543', '11', 'Corona Buena', 'A');
INSERT INTO `piezas` VALUES (8, '9876543', '14', 'Fracturo', 'A');
INSERT INTO `piezas` VALUES (8, '9876543', '16a', 'Buen Estado', 'A');
INSERT INTO `piezas` VALUES (8, '9876543', '16c', 'Buen Estado', 'A');
INSERT INTO `piezas` VALUES (8, '9876543', '42', 'Tratamiento Conducto Bueno', 'A');
INSERT INTO `piezas` VALUES (9, '23456789', '11', 'Fracturo', 'A');
INSERT INTO `piezas` VALUES (9, '23456789', '11a', 'Buen Estado', 'A');
INSERT INTO `piezas` VALUES (9, '23456789', '11b', 'Buen Estado', 'A');
INSERT INTO `piezas` VALUES (9, '23456789', '11c', 'Carie', 'A');
INSERT INTO `piezas` VALUES (9, '23456789', '11d', 'Buen Estado', 'A');
INSERT INTO `piezas` VALUES (9, '23456789', '11e', 'Buen Estado', 'A');
INSERT INTO `piezas` VALUES (9, '23456789', '31', 'Tratamiento Conducto Bueno', 'A');
INSERT INTO `piezas` VALUES (11, '25560589', '11', 'Fracturo', 'A');
INSERT INTO `piezas` VALUES (11, '25560589', '13c', 'Buen Estado', 'A');
INSERT INTO `piezas` VALUES (11, '25560589', '22', 'Corona Buena', 'A');
INSERT INTO `piezas` VALUES (11, '25560589', '31', 'Tratamiento Conducto Bueno', 'A');
INSERT INTO `piezas` VALUES (11, '25560589', '45', 'Fracturo', 'A');
INSERT INTO `piezas` VALUES (21, '5678909', '11', 'Ausente', 'A');
INSERT INTO `piezas` VALUES (21, '5678909', '31', 'Corona Mala', 'A');
INSERT INTO `piezas` VALUES (21, '5678909', '34', '', 'A');
INSERT INTO `piezas` VALUES (21, '5678909', '41', 'Fracturo', 'A');
INSERT INTO `piezas` VALUES (21, '5678909', '14', 'Tratamiento Conducto Bueno', 'A');
INSERT INTO `piezas` VALUES (21, '5678909', '48', 'Corona Buena', 'A');
INSERT INTO `piezas` VALUES (1, '25560587', '16', 'Ausente', 'L');
INSERT INTO `piezas` VALUES (1, '25560587', '16c', 'Buen Estado', 'L');
INSERT INTO `piezas` VALUES (1, '25560587', '17', 'Ausente', 'L');

SET FOREIGN_KEY_CHECKS = 1;
