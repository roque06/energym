package Logica;

import Datos.Fcitas;
import Datos.Fpagos;
import Datos.Fregimen;
import Formularios.pagos;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRpagos {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "No.REGISTRO", "Registro", "Fecha", "Nombre", "Concepto", "Suma", "Abono", "Monto Incial", "No.Cuotas", "Fecha de Pagos", "Fechapa", "Monto Mensual", "CodPaciente", "No.Cuotas"};

        String[] registro = new String[15];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select idrecibo,codrecibo,DATE_FORMAT(fecha,'%d/%m/%Y') as registro,fecha,nombre,concepto,suma,abono,montoini,meses,DATE_FORMAT (fechapagos,'%d/%m/%Y')as pagos,fechapagos,montomes,codigopaciente,cuotan From tbrecibo  where concat (nombre,'',fecha,'',codigopaciente) like '%" + buscar + "%' order by idrecibo asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idrecibo");
                registro[1] = rs.getString("codrecibo");
                registro[2] = rs.getString("registro");
                registro[3] = rs.getString("fecha");
                registro[4] = rs.getString("nombre");
                registro[5] = rs.getString("concepto");
                registro[6] = rs.getString("suma");
                registro[7] = rs.getString("abono");
                registro[8] = rs.getString("montoini");
                registro[9] = rs.getString("meses");
                registro[10] = rs.getString("pagos");
                registro[11] = rs.getString("fechapagos");
                registro[12] = rs.getString("montomes");
                registro[13] = rs.getString("codigopaciente");
                registro[14] = rs.getString("cuotan");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fpagos dts) {
        sSQL = "insert into tbrecibo (codrecibo,fecha,nombre,concepto,suma,abono,montoini,meses,fechapagos,montomes,codigopaciente,cuotan)"
                + "VALUES(?,now(),?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getCodrecibos());
//            pst.setString(2, dts.getFecha());
            pst.setString(2, dts.getNombre());
            pst.setString(3, dts.getConcepto());
            pst.setString(4, dts.getSuma());
            pst.setString(5, dts.getAbono());
            pst.setString(6, dts.getMontoini());
            pst.setString(7, dts.getMeses());
            pst.setString(8, dts.getFechapagos());
            pst.setString(9, dts.getMontomes());
            pst.setString(10, dts.getCodigopaciente());
            pst.setString(11, dts.getCuotan());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fcitas dts) {
        sSQL = "UPDATE tbcitas set nombre=?,telefono=?,correo=?,sintomas=?,fecha=?,genero=?,hora=?"
                + "where idcitas=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getTelefono());
            pst.setString(3, dts.getCorreo());
            pst.setString(4, dts.getSintomas());
            pst.setDate(5, dts.getFecha());
            pst.setString(6, dts.getGenero());
            pst.setString(7, dts.getHora());
            pst.setInt(8, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fpagos dts) {
        sSQL = "DELETE FROM tbrecibo WHERE codrecibo=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getCodrecibos());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean actualizar(Fpagos dts) {
        sSQL = "UPDATE tbrecibo set montomes=?"
                + "where codigopaciente=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getMontomes());
            pst.setString(2, dts.getCodigopaciente());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }
    
     public boolean eliminarAll(Fpagos dts) {
        sSQL = "DELETE FROM tbrecibo WHERE nombre=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getNombre());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
