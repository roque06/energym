/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Datos.Fficha;
import Logica.Controlador;
import Logica.FRficha;
import Logica.Metodos;
import Logica.conexion;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import java.awt.Graphics;
import java.awt.Image;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;

public class Ficha extends javax.swing.JFrame {

    public Ficha() {
        initComponents();
        this.setLocationRelativeTo(null);
        Metodos.inhabilitar();
        llenarRegimen();
        llenarAfiliado();
        llenarSeguro();
        CodigoPaciente();
        Doctor();

        dtfecha.setCalendar(c1);
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());

    }

    Calendar c1 = new GregorianCalendar();

    void llenarRegimen() {

        String consulta = "SELECT tiporegimen FROM tbregimen ORDER BY tiporegimen ASC";
        controlador.llenarCombo(cboregimen, consulta, 1);
    }

    void Doctor() {

        String consulta = "SELECT descripcion FROM doctor ORDER BY descripcion ASC";
        controlador.llenarCombo(txtpss, consulta, 1);
    }

    void llenarAfiliado() {

        String consulta = "SELECT tipoafiliado FROM tbafiliado ORDER BY tipoafiliado ASC";
        controlador.llenarCombo(cboafiliado, consulta, 1);
    }

    void llenarSeguro() {

        String consulta = "SELECT descripcion FROM tiposeguro ORDER BY descripcion ASC";
        controlador.llenarCombo(cboseguro, consulta, 1);
    }

    void validacion() {
        if (txtpss.getSelectedItem() == null) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Seleccionar El Nombre PSS ");
            txtpss.requestFocus();
            return;

        }

        if (dtfecha.getDate() == null) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar La Fecha Del Registro");
            dtfecha.requestFocus();
            return;

        }

        if (txtnombre.getText().length() == 0) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar El Nombre del Paciente ");
            txtnombre.requestFocus();
            return;

        }
        if (txtedad.getText().length() == 0) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar La Edad Del Paciente");
            txtedad.requestFocus();
            return;

        }
        if (txtnss.getText().length() == 0) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar El Nss O No.Control Del Paciente");
            txtnss.requestFocus();
            return;

        }

        if (txtcedula.getText().length() == 0) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar La Cedula Del Paciente ");
            txtcedula.requestFocus();
            return;

        }

        if (txtlugar.getText().length() == 0) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar El Lugar Donde Labora El Paciente ");
            txtlugar.requestFocus();
            return;

        }

        if (txttelefono.getText().length() == 0) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar El Telefono Del Paciente");
            txttelefono.requestFocus();
            return;

        }

        if (dtnaci.getDate() == null) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar La Fecha de Naciemiento del Paciente");
            dtnaci.requestFocus();
            return;

        }

        if (Ficha.txtdireccion.getText().length() == 0) {
            JOptionPane.showConfirmDialog(rootPane, "Debes Ingresar La Direccion Del Paciente ");
            Ficha.txtdireccion.requestFocus();
            return;

        }
        Metodos.guardar();
        if (accion.equals("editar")) {
            return;
        } else {
            Actualizar();
        }
        CodigoPaciente();
        llenarSeguro();

    }

    void CodigoPaciente() {
        String idpaciente = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT idpaciente from codpaciente");

            rq2.next();
            idpaciente = rq2.getString("idpaciente");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        secuencia = secuencia;
        lblcodigo.setText("PST-" + String.valueOf(secuencia));

    }

    public void Actualizar() {

        try {
            PreparedStatement psU = cn.prepareStatement("Update codpaciente set idpaciente= idpaciente+1");
            psU.executeUpdate();

        } catch (Exception e) {

            JOptionPane.showConfirmDialog(rootPane, e);

        }
        CodigoPaciente();

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jTabbedPane1 = new javax.swing.JTabbedPane();
        jPanel2 = new javax.swing.JPanel();
        dtfecha = new com.toedter.calendar.JDateChooser();
        jButton1 = new javax.swing.JButton();
        lblcodigo = new javax.swing.JLabel();
        userLabel1 = new javax.swing.JLabel();
        jSeparator8 = new javax.swing.JSeparator();
        userLabel2 = new javax.swing.JLabel();
        userLabel3 = new javax.swing.JLabel();
        jSeparator9 = new javax.swing.JSeparator();
        userLabel4 = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        userLabel5 = new javax.swing.JLabel();
        userLabel6 = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JSeparator();
        txtlugar = new javax.swing.JTextField();
        userLabel7 = new javax.swing.JLabel();
        jSeparator12 = new javax.swing.JSeparator();
        userLabel8 = new javax.swing.JLabel();
        cboregimen = new javax.swing.JComboBox<>();
        jSeparator13 = new javax.swing.JSeparator();
        userLabel9 = new javax.swing.JLabel();
        txtedad = new javax.swing.JTextField();
        jSeparator14 = new javax.swing.JSeparator();
        userLabel10 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jSeparator15 = new javax.swing.JSeparator();
        txtcedula = new javax.swing.JFormattedTextField();
        userLabel11 = new javax.swing.JLabel();
        jSeparator16 = new javax.swing.JSeparator();
        txttelefono = new javax.swing.JFormattedTextField();
        userLabel12 = new javax.swing.JLabel();
        jSeparator17 = new javax.swing.JSeparator();
        txtcorreo = new javax.swing.JTextField();
        jSeparator18 = new javax.swing.JSeparator();
        userLabel13 = new javax.swing.JLabel();
        dtnaci = new com.toedter.calendar.JDateChooser();
        cboafiliado = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtdireccion = new javax.swing.JTextArea();
        userLabel14 = new javax.swing.JLabel();
        txtnss = new javax.swing.JTextField();
        jSeparator19 = new javax.swing.JSeparator();
        userLabel15 = new javax.swing.JLabel();
        jSeparator20 = new javax.swing.JSeparator();
        userLabel16 = new javax.swing.JLabel();
        jSeparator21 = new javax.swing.JSeparator();
        userLabel17 = new javax.swing.JLabel();
        jSeparator22 = new javax.swing.JSeparator();
        jSeparator23 = new javax.swing.JSeparator();
        loginBtn1 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        loginBtn2 = new javax.swing.JPanel();
        btnguardar1 = new javax.swing.JLabel();
        txtpss = new javax.swing.JComboBox<>();
        userLabel42 = new javax.swing.JLabel();
        jSeparator47 = new javax.swing.JSeparator();
        cboseguro = new javax.swing.JComboBox<>();
        loginBtn3 = new javax.swing.JPanel();
        btnodonto = new javax.swing.JLabel();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        checkcabeza = new javax.swing.JCheckBox();
        checkcara = new javax.swing.JCheckBox();
        checkcuello = new javax.swing.JCheckBox();
        checkatm = new javax.swing.JCheckBox();
        checklabios = new javax.swing.JCheckBox();
        checklengua = new javax.swing.JCheckBox();
        userLabel18 = new javax.swing.JLabel();
        jSeparator24 = new javax.swing.JSeparator();
        cboasma = new javax.swing.JComboBox<>();
        userLabel19 = new javax.swing.JLabel();
        userLabel20 = new javax.swing.JLabel();
        cboalergia = new javax.swing.JComboBox<>();
        jSeparator26 = new javax.swing.JSeparator();
        userLabel21 = new javax.swing.JLabel();
        cboepilepsia = new javax.swing.JComboBox<>();
        jSeparator27 = new javax.swing.JSeparator();
        userLabel22 = new javax.swing.JLabel();
        cboanemia = new javax.swing.JComboBox<>();
        jSeparator28 = new javax.swing.JSeparator();
        userLabel23 = new javax.swing.JLabel();
        cbohipertencion = new javax.swing.JComboBox<>();
        userLabel24 = new javax.swing.JLabel();
        jSeparator30 = new javax.swing.JSeparator();
        cbohepatitis = new javax.swing.JComboBox<>();
        userLabel25 = new javax.swing.JLabel();
        cbotuberculosis = new javax.swing.JComboBox<>();
        jSeparator31 = new javax.swing.JSeparator();
        userLabel26 = new javax.swing.JLabel();
        userLabel27 = new javax.swing.JLabel();
        cborenal = new javax.swing.JComboBox<>();
        jSeparator32 = new javax.swing.JSeparator();
        userLabel28 = new javax.swing.JLabel();
        cbodiabetes = new javax.swing.JComboBox<>();
        jSeparator33 = new javax.swing.JSeparator();
        userLabel29 = new javax.swing.JLabel();
        cbohipotension = new javax.swing.JComboBox<>();
        jSeparator34 = new javax.swing.JSeparator();
        userLabel30 = new javax.swing.JLabel();
        jScrollPane4 = new javax.swing.JScrollPane();
        txtotros = new javax.swing.JTextArea();
        jSeparator35 = new javax.swing.JSeparator();
        userLabel31 = new javax.swing.JLabel();
        jSeparator36 = new javax.swing.JSeparator();
        jSeparator25 = new javax.swing.JSeparator();
        userLabel32 = new javax.swing.JLabel();
        cbocaries = new javax.swing.JComboBox<>();
        jSeparator37 = new javax.swing.JSeparator();
        cboabscesos = new javax.swing.JComboBox<>();
        jSeparator38 = new javax.swing.JSeparator();
        userLabel34 = new javax.swing.JLabel();
        cbofistulas = new javax.swing.JComboBox<>();
        jSeparator39 = new javax.swing.JSeparator();
        userLabel35 = new javax.swing.JLabel();
        cbohemorragia = new javax.swing.JComboBox<>();
        userLabel36 = new javax.swing.JLabel();
        cboabrasion = new javax.swing.JComboBox<>();
        jSeparator40 = new javax.swing.JSeparator();
        jSeparator41 = new javax.swing.JSeparator();
        jSeparator42 = new javax.swing.JSeparator();
        userLabel37 = new javax.swing.JLabel();
        userLabel33 = new javax.swing.JLabel();
        cbodolor = new javax.swing.JComboBox<>();
        jSeparator43 = new javax.swing.JSeparator();
        userLabel38 = new javax.swing.JLabel();
        cbofracturas = new javax.swing.JComboBox<>();
        jSeparator44 = new javax.swing.JSeparator();
        userLabel39 = new javax.swing.JLabel();
        cbobruxismo = new javax.swing.JComboBox<>();
        jSeparator45 = new javax.swing.JSeparator();
        userLabel40 = new javax.swing.JLabel();
        cboperiodontal = new javax.swing.JComboBox<>();
        jSeparator46 = new javax.swing.JSeparator();
        loginBtn5 = new javax.swing.JPanel();
        btnguardar2 = new javax.swing.JLabel();
        loginBtn6 = new javax.swing.JPanel();
        btnguardar5 = new javax.swing.JLabel();
        userLabel41 = new javax.swing.JLabel();
        jScrollPane5 = new javax.swing.JScrollPane();
        txtotros2 = new javax.swing.JTextArea();
        jSeparator29 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jTabbedPane1.setBackground(new java.awt.Color(0, 134, 190));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createBevelBorder(0));
        jPanel2.setMinimumSize(new java.awt.Dimension(1091, 510));
        jPanel2.setPreferredSize(new java.awt.Dimension(1091, 510));
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());
        jPanel2.add(dtfecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 80, 220, 30));

        jButton1.setBackground(new java.awt.Color(0, 134, 190));
        jButton1.setForeground(new java.awt.Color(0, 134, 190));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel2.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 10, 45, -1));

        lblcodigo.setFont(new java.awt.Font("Roboto Light", 0, 14)); // NOI18N
        lblcodigo.setText("....");
        jPanel2.add(lblcodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 80, 220, 30));

        userLabel1.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel1.setText("Buscar");
        jPanel2.add(userLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(930, 10, 50, 40));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 30, 280, 20));

        userLabel2.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel2.setText("Fecha Registro:");
        jPanel2.add(userLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 60, -1, -1));

        userLabel3.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel3.setText("Codigo Paciente:");
        jPanel2.add(userLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(710, 60, -1, -1));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(720, 110, 220, 20));

        userLabel4.setFont(new java.awt.Font("Roboto Light", 1, 20)); // NOI18N
        userLabel4.setText("DATOS DEL AFILIADO");
        jPanel2.add(userLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 130, 210, -1));

        jSeparator10.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 110, 250, 20));

        userLabel5.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel5.setText("Lugar Labora:");
        jPanel2.add(userLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 350, -1, -1));

        userLabel6.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel6.setText("Nombre PSS:");
        jPanel2.add(userLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 60, -1, -1));

        jSeparator11.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 160, 220, 20));

        txtlugar.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtlugar.setBorder(null);
        txtlugar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtlugarMousePressed(evt);
            }
        });
        txtlugar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtlugarActionPerformed(evt);
            }
        });
        jPanel2.add(txtlugar, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 380, 250, 30));

        userLabel7.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        userLabel7.setText("FICHA ODONTOLOGICA");
        jPanel2.add(userLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 0, 280, -1));

        jSeparator12.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 410, 250, 20));

        userLabel8.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel8.setText("Direccion:");
        jPanel2.add(userLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 429, -1, 20));

        cboregimen.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(cboregimen, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 300, 250, 30));

        jSeparator13.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 490, 250, 20));

        userLabel9.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel9.setText("Tipo Seguro:");
        jPanel2.add(userLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 200, -1, -1));

        txtedad.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtedad.setBorder(null);
        txtedad.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtedadMousePressed(evt);
            }
        });
        txtedad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtedadActionPerformed(evt);
            }
        });
        jPanel2.add(txtedad, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 220, 250, 30));

        jSeparator14.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator14, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 330, 250, 20));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Nombre:");
        jPanel2.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, -1, -1));

        txtnombre.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtnombre.setBorder(null);
        txtnombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtnombreMousePressed(evt);
            }
        });
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreKeyTyped(evt);
            }
        });
        jPanel2.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 220, 250, 30));

        jSeparator15.setBackground(new java.awt.Color(0, 134, 190));
        jSeparator15.setForeground(new java.awt.Color(0, 134, 190));
        jPanel2.add(jSeparator15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, 250, 20));

        txtcedula.setBorder(null);
        try {
            txtcedula.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-#######-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txtcedula.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        txtcedula.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                txtcedulaMouseClicked(evt);
            }
        });
        jPanel2.add(txtcedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 300, 250, 30));

        userLabel11.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel11.setText("Telefono:");
        jPanel2.add(userLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 350, -1, -1));

        jSeparator16.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator16, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 330, 250, 20));

        txttelefono.setBorder(null);
        try {
            txttelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txttelefono.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jPanel2.add(txttelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 380, 250, 30));

        userLabel12.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel12.setText("Fecha Nacimiento:");
        jPanel2.add(userLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 430, -1, -1));

        jSeparator17.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator17, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 250, 250, 20));

        txtcorreo.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtcorreo.setBorder(null);
        txtcorreo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtcorreoMousePressed(evt);
            }
        });
        txtcorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcorreoActionPerformed(evt);
            }
        });
        jPanel2.add(txtcorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 460, 260, 30));

        jSeparator18.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator18, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 410, 250, 20));

        userLabel13.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel13.setText("Cedula:");
        jPanel2.add(userLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 280, -1, -1));
        jPanel2.add(dtnaci, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 460, 250, 30));

        cboafiliado.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(cboafiliado, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 380, 250, 30));

        txtdireccion.setColumns(20);
        txtdireccion.setLineWrap(true);
        txtdireccion.setRows(2);
        txtdireccion.setBorder(null);
        jScrollPane1.setViewportView(txtdireccion);

        jPanel2.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 450, 260, 50));

        userLabel14.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel14.setText("Edad:");
        jPanel2.add(userLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 200, -1, -1));

        txtnss.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtnss.setBorder(null);
        txtnss.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtnssMousePressed(evt);
            }
        });
        txtnss.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnssActionPerformed(evt);
            }
        });
        txtnss.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnssKeyTyped(evt);
            }
        });
        jPanel2.add(txtnss, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, 250, 30));

        jSeparator19.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator19, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 410, 250, 20));

        userLabel15.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel15.setText("NSS o No.Afiliado:");
        jPanel2.add(userLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, -1, -1));

        jSeparator20.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator20, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 330, 250, 20));

        userLabel16.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel16.setText("Tipo Afiliado:");
        jPanel2.add(userLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 350, -1, -1));

        jSeparator21.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator21, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 500, 260, 20));

        userLabel17.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel17.setText("Correo");
        jPanel2.add(userLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 440, -1, -1));

        jSeparator22.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator22, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 490, 260, 20));

        jSeparator23.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator23, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 110, 220, 20));

        loginBtn1.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn1Layout = new javax.swing.GroupLayout(loginBtn1);
        loginBtn1.setLayout(loginBtn1Layout);
        loginBtn1Layout.setHorizontalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn1Layout.setVerticalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel2.add(loginBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(530, 530, -1, -1));

        loginBtn2.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar1.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar1.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        btnguardar1.setText("Nuevo");
        btnguardar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardar1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardar1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardar1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn2Layout = new javax.swing.GroupLayout(loginBtn2);
        loginBtn2.setLayout(loginBtn2Layout);
        loginBtn2Layout.setHorizontalGroup(
            loginBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnguardar1, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        loginBtn2Layout.setVerticalGroup(
            loginBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnguardar1, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.add(loginBtn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 530, -1, -1));

        txtpss.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(txtpss, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 80, 250, 30));

        userLabel42.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel42.setText("Regimen:");
        jPanel2.add(userLabel42, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 270, -1, -1));

        jSeparator47.setForeground(new java.awt.Color(0, 0, 0));
        jPanel2.add(jSeparator47, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 250, 250, 20));

        cboseguro.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        jPanel2.add(cboseguro, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 220, 250, 30));

        loginBtn3.setBackground(new java.awt.Color(0, 134, 190));

        btnodonto.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnodonto.setForeground(new java.awt.Color(255, 255, 255));
        btnodonto.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnodonto.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/cirujano.png"))); // NOI18N
        btnodonto.setText("Odontograma");
        btnodonto.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnodonto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnodontoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnodontoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnodontoMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn3Layout = new javax.swing.GroupLayout(loginBtn3);
        loginBtn3.setLayout(loginBtn3Layout);
        loginBtn3Layout.setHorizontalGroup(
            loginBtn3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn3Layout.createSequentialGroup()
                .addComponent(btnodonto, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        loginBtn3Layout.setVerticalGroup(
            loginBtn3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn3Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnodonto, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel2.add(loginBtn3, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 530, 130, 30));

        jTabbedPane1.addTab("Ficha odontologica", jPanel2);

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createBevelBorder(0));
        jPanel6.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        checkcabeza.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        checkcabeza.setText("Cabeza");
        jPanel6.add(checkcabeza, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 49, -1, 20));

        checkcara.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        checkcara.setText("Cara");
        jPanel6.add(checkcara, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 50, 70, 20));

        checkcuello.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        checkcuello.setText("Cuello");
        jPanel6.add(checkcuello, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 50, 80, 20));

        checkatm.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        checkatm.setText("ATM");
        jPanel6.add(checkatm, new org.netbeans.lib.awtextra.AbsoluteConstraints(520, 50, 76, 20));

        checklabios.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        checklabios.setText("Labios");
        jPanel6.add(checklabios, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 50, 100, 20));

        checklengua.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        checklengua.setText("Lengua");
        jPanel6.add(checklengua, new org.netbeans.lib.awtextra.AbsoluteConstraints(730, 50, 98, 20));

        userLabel18.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        userLabel18.setText("Antecedentes Odontologicos");
        jPanel6.add(userLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 280, 350, -1));

        jSeparator24.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator24, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 130, 60, 20));

        cboasma.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cboasma, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 100, 60, 30));

        userLabel19.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel19.setText("Hipertension arterial:");
        jPanel6.add(userLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 100, -1, 30));

        userLabel20.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel20.setText("Hipotension arterial:");
        jPanel6.add(userLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 160, -1, 30));

        cboalergia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cboalergia, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 100, 60, 30));

        jSeparator26.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator26, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 290, 260, 20));

        userLabel21.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel21.setText("Alergia:");
        jPanel6.add(userLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(200, 100, -1, 30));

        cboepilepsia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cboepilepsia, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 100, 60, 30));

        jSeparator27.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator27, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 190, 60, 20));

        userLabel22.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel22.setText("Epilepsia:");
        jPanel6.add(userLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 100, -1, 30));

        cboanemia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cboanemia, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 100, 60, 30));

        jSeparator28.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator28, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 130, 60, 20));

        userLabel23.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel23.setText("Anemia:");
        jPanel6.add(userLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(550, 100, -1, 30));

        cbohipertencion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbohipertencion, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 100, 60, 30));

        userLabel24.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel24.setText("Hemorragia post quirurgica:");
        jPanel6.add(userLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 340, -1, 30));

        jSeparator30.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator30, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 130, 60, 20));

        cbohepatitis.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbohepatitis, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 160, 60, 30));

        userLabel25.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel25.setText("Hepatitis:");
        jPanel6.add(userLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 160, -1, 30));

        cbotuberculosis.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbotuberculosis, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 160, 60, 30));

        jSeparator31.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator31, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 130, 60, 20));

        userLabel26.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel26.setText("Tuberculosis:");
        jPanel6.add(userLabel26, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, -1, 30));

        userLabel27.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel27.setText("Tuberculosis:");
        jPanel6.add(userLabel27, new org.netbeans.lib.awtextra.AbsoluteConstraints(160, 160, -1, 30));

        cborenal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cborenal, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 160, 60, 30));

        jSeparator32.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator32, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 190, 60, 10));

        userLabel28.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel28.setText("Enf.renal:");
        jPanel6.add(userLabel28, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 160, -1, 30));

        cbodiabetes.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbodiabetes, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 160, 60, 30));

        jSeparator33.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator33, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 130, 60, 20));

        userLabel29.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel29.setText("Enf.renal:");
        jPanel6.add(userLabel29, new org.netbeans.lib.awtextra.AbsoluteConstraints(540, 160, -1, 30));

        cbohipotension.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbohipotension, new org.netbeans.lib.awtextra.AbsoluteConstraints(900, 160, 60, 30));

        jSeparator34.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator34, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 370, 60, 20));

        userLabel30.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel30.setText("Asma:");
        jPanel6.add(userLabel30, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 100, -1, 30));

        txtotros.setColumns(20);
        txtotros.setLineWrap(true);
        txtotros.setRows(2);
        txtotros.setBorder(null);
        jScrollPane4.setViewportView(txtotros);

        jPanel6.add(jScrollPane4, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 240, 260, 50));

        jSeparator35.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator35, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 430, 60, 20));

        userLabel31.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        userLabel31.setText("Cuestionario de Salud");
        jPanel6.add(userLabel31, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 0, 280, -1));

        jSeparator36.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator36, new org.netbeans.lib.awtextra.AbsoluteConstraints(410, 30, 260, 20));

        jSeparator25.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator25, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 310, 330, 20));

        userLabel32.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel32.setText("Otros:");
        jPanel6.add(userLabel32, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 440, -1, 30));

        cbocaries.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbocaries, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 340, 60, 30));

        jSeparator37.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator37, new org.netbeans.lib.awtextra.AbsoluteConstraints(80, 190, 60, 20));

        cboabscesos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cboabscesos, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 340, 60, 30));

        jSeparator38.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator38, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 190, 60, 20));

        userLabel34.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel34.setText("Abscesos:");
        jPanel6.add(userLabel34, new org.netbeans.lib.awtextra.AbsoluteConstraints(260, 340, -1, 30));

        cbofistulas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbofistulas, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 340, 60, 30));

        jSeparator39.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator39, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 430, 60, 20));

        userLabel35.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel35.setText("Abrasion:");
        jPanel6.add(userLabel35, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 340, -1, 30));

        cbohemorragia.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbohemorragia, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 340, 60, 30));

        userLabel36.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel36.setText("Fistulas:");
        jPanel6.add(userLabel36, new org.netbeans.lib.awtextra.AbsoluteConstraints(430, 340, -1, 30));

        cboabrasion.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        cboabrasion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cboabrasionActionPerformed(evt);
            }
        });
        jPanel6.add(cboabrasion, new org.netbeans.lib.awtextra.AbsoluteConstraints(950, 340, 60, 30));

        jSeparator40.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator40, new org.netbeans.lib.awtextra.AbsoluteConstraints(620, 190, 60, 20));

        jSeparator41.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator41, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 370, 60, 20));

        jSeparator42.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator42, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 370, 60, 20));

        userLabel37.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel37.setText("Caries:");
        jPanel6.add(userLabel37, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 340, -1, 30));

        userLabel33.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel33.setText("Engermedad periodontal:");
        jPanel6.add(userLabel33, new org.netbeans.lib.awtextra.AbsoluteConstraints(580, 400, -1, 30));

        cbodolor.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbodolor, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 400, 60, 30));

        jSeparator43.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator43, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 370, 60, 20));

        userLabel38.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel38.setText("Dolor dental:");
        jPanel6.add(userLabel38, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 400, -1, 30));

        cbofracturas.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbofracturas, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 400, 60, 30));

        jSeparator44.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator44, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 370, 60, 20));

        userLabel39.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel39.setText("Fracturas dentales:");
        jPanel6.add(userLabel39, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 400, -1, 30));

        cbobruxismo.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cbobruxismo, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 400, 60, 30));

        jSeparator45.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator45, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 430, 60, 20));

        userLabel40.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel40.setText("Bruxismo:");
        jPanel6.add(userLabel40, new org.netbeans.lib.awtextra.AbsoluteConstraints(420, 400, -1, 30));

        cboperiodontal.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "No", "Si" }));
        jPanel6.add(cboperiodontal, new org.netbeans.lib.awtextra.AbsoluteConstraints(770, 400, 60, 30));

        jSeparator46.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator46, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 430, 60, 20));

        loginBtn5.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar2.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar2.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar2.setText("Guardar");
        btnguardar2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardar2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardar2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardar2MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn5Layout = new javax.swing.GroupLayout(loginBtn5);
        loginBtn5.setLayout(loginBtn5Layout);
        loginBtn5Layout.setHorizontalGroup(
            loginBtn5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar2, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn5Layout.setVerticalGroup(
            loginBtn5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar2, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel6.add(loginBtn5, new org.netbeans.lib.awtextra.AbsoluteConstraints(870, 460, -1, -1));

        loginBtn6.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar5.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar5.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar5.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        btnguardar5.setText("Nuevo");
        btnguardar5.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardar5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardar5MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardar5MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn6Layout = new javax.swing.GroupLayout(loginBtn6);
        loginBtn6.setLayout(loginBtn6Layout);
        loginBtn6Layout.setHorizontalGroup(
            loginBtn6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar5, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn6Layout.setVerticalGroup(
            loginBtn6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar5, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel6.add(loginBtn6, new org.netbeans.lib.awtextra.AbsoluteConstraints(740, 460, -1, -1));

        userLabel41.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel41.setText("Otros:");
        jPanel6.add(userLabel41, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 210, -1, 30));

        txtotros2.setColumns(20);
        txtotros2.setLineWrap(true);
        txtotros2.setRows(2);
        jScrollPane5.setViewportView(txtotros2);

        jPanel6.add(jScrollPane5, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 470, 260, 50));

        jSeparator29.setForeground(new java.awt.Color(0, 0, 0));
        jPanel6.add(jSeparator29, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 520, 260, 20));

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 1091, Short.MAX_VALUE)
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, 605, Short.MAX_VALUE)
        );

        jTabbedPane1.addTab("Cuestionario", jPanel5);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jTabbedPane1)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        checkcabeza.setSelected(false);
        checkcara.setSelected(false);
        checkatm.setSelected(false);
        checkcuello.setSelected(false);
        checklabios.setSelected(false);
        checklengua.setSelected(false);

        new Vista().setVisible(true);       // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtlugarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtlugarMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtlugarMousePressed

    private void txtlugarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtlugarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtlugarActionPerformed

    private void txtedadMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtedadMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtedadMousePressed

    private void txtedadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtedadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtedadActionPerformed

    private void txtnombreMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnombreMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreMousePressed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtcorreoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtcorreoMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcorreoMousePressed

    private void txtcorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcorreoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcorreoActionPerformed

    private void txtnssMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnssMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnssMousePressed

    private void txtnssActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnssActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnssActionPerformed

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        if (btnguardar.isEnabled() == false) {
            return;
        }
        validacion();
        cabeza = "Cabeza";
        cara = "Cara";
        labios = "Labios";
        atm = "ATM";
        cuello = "Cuello";
        lengua = "Lengua";
        CodigoPaciente();
        txtnombre.setText("");// TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered
        btnguardar.setBackground(new Color(0, 156, 223));        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited
        btnguardar.setBackground(new Color(0, 134, 190));        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseExited

    private void btnguardar1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar1MouseClicked
        Metodos.habilitar();
        llenarRegimen();
        llenarAfiliado();
        CodigoPaciente();
        llenarSeguro();
        dtfecha.setCalendar(c1);
        accion = "guardar";
        btnguardar.setText("Guardar");
        txtnombre.setText("");
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar1MouseClicked

    private void btnguardar1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar1MouseEntered

    private void btnguardar1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar1MouseExited

    private void txtcedulaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtcedulaMouseClicked
        txtcedula.requestFocus();        // TODO add your handling code here:
    }//GEN-LAST:event_txtcedulaMouseClicked

    private void cboabrasionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cboabrasionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cboabrasionActionPerformed

    private void btnguardar2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar2MouseClicked
        if (btnguardar.isEnabled() == false) {
            return;
        }
        validacion();
        cabeza = "Cabeza";
        cara = "Cara";
        labios = "Labios";
        atm = "ATM";
        cuello = "Cuello";
        lengua = "Lengua";
        CodigoPaciente();         // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar2MouseClicked

    private void btnguardar2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar2MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar2MouseEntered

    private void btnguardar2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar2MouseExited

    private void btnguardar5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar5MouseClicked
        Metodos.habilitar();
        llenarRegimen();
        llenarAfiliado();
        CodigoPaciente();
        dtfecha.setCalendar(c1);
        accion = "guardar";
        btnguardar.setText("Guardar");
        txtnombre.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar5MouseClicked

    private void btnguardar5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar5MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar5MouseEntered

    private void btnguardar5MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardar5MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardar5MouseExited

    private void txtnombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyTyped
        char validar1 = evt.getKeyChar();
        if (Character.isLowerCase(validar1)) {
            evt.setKeyChar(Character.toUpperCase(validar1));

        }
        char validar = evt.getKeyChar();
        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Letras");
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyTyped

    private void txtnssKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnssKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }

        txtnss.setText(txtnss.getText().trim());        // TODO add your handling code here:
    }//GEN-LAST:event_txtnssKeyTyped

    private void btnodontoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnodontoMouseClicked
        if (btnodonto.isEnabled() == false) {
            return;
        }

        new odontograma.registro().setVisible(true);
        odontograma.registro.txtnombre.setText(txtnombre.getText());
        odontograma.registro.lblcodigo.setText(lblcodigo.getText());

        btnodonto.setEnabled(false);

        // TODO add your handling code here:
    }//GEN-LAST:event_btnodontoMouseClicked

    private void btnodontoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnodontoMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnodontoMouseEntered

    private void btnodontoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnodontoMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnodontoMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Ficha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Ficha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Ficha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Ficha.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Ficha().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btnguardar;
    public static javax.swing.JLabel btnguardar1;
    public static javax.swing.JLabel btnguardar2;
    public static javax.swing.JLabel btnguardar5;
    public static javax.swing.JLabel btnodonto;
    public static javax.swing.JComboBox<String> cboabrasion;
    public static javax.swing.JComboBox<String> cboabscesos;
    public static javax.swing.JComboBox<String> cboafiliado;
    public static javax.swing.JComboBox<String> cboalergia;
    public static javax.swing.JComboBox<String> cboanemia;
    public static javax.swing.JComboBox<String> cboasma;
    public static javax.swing.JComboBox<String> cbobruxismo;
    public static javax.swing.JComboBox<String> cbocaries;
    public static javax.swing.JComboBox<String> cbodiabetes;
    public static javax.swing.JComboBox<String> cbodolor;
    public static javax.swing.JComboBox<String> cboepilepsia;
    public static javax.swing.JComboBox<String> cbofistulas;
    public static javax.swing.JComboBox<String> cbofracturas;
    public static javax.swing.JComboBox<String> cbohemorragia;
    public static javax.swing.JComboBox<String> cbohepatitis;
    public static javax.swing.JComboBox<String> cbohipertencion;
    public static javax.swing.JComboBox<String> cbohipotension;
    public static javax.swing.JComboBox<String> cboperiodontal;
    public static javax.swing.JComboBox<String> cboregimen;
    public static javax.swing.JComboBox<String> cborenal;
    public static javax.swing.JComboBox<String> cboseguro;
    public static javax.swing.JComboBox<String> cbotuberculosis;
    public static javax.swing.JCheckBox checkatm;
    public static javax.swing.JCheckBox checkcabeza;
    public static javax.swing.JCheckBox checkcara;
    public static javax.swing.JCheckBox checkcuello;
    public static javax.swing.JCheckBox checklabios;
    public static javax.swing.JCheckBox checklengua;
    public static com.toedter.calendar.JDateChooser dtfecha;
    public static com.toedter.calendar.JDateChooser dtnaci;
    private javax.swing.JButton jButton1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane4;
    private javax.swing.JScrollPane jScrollPane5;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator16;
    private javax.swing.JSeparator jSeparator17;
    private javax.swing.JSeparator jSeparator18;
    private javax.swing.JSeparator jSeparator19;
    private javax.swing.JSeparator jSeparator20;
    private javax.swing.JSeparator jSeparator21;
    private javax.swing.JSeparator jSeparator22;
    private javax.swing.JSeparator jSeparator23;
    private javax.swing.JSeparator jSeparator24;
    private javax.swing.JSeparator jSeparator25;
    private javax.swing.JSeparator jSeparator26;
    private javax.swing.JSeparator jSeparator27;
    private javax.swing.JSeparator jSeparator28;
    private javax.swing.JSeparator jSeparator29;
    private javax.swing.JSeparator jSeparator30;
    private javax.swing.JSeparator jSeparator31;
    private javax.swing.JSeparator jSeparator32;
    private javax.swing.JSeparator jSeparator33;
    private javax.swing.JSeparator jSeparator34;
    private javax.swing.JSeparator jSeparator35;
    private javax.swing.JSeparator jSeparator36;
    private javax.swing.JSeparator jSeparator37;
    private javax.swing.JSeparator jSeparator38;
    private javax.swing.JSeparator jSeparator39;
    private javax.swing.JSeparator jSeparator40;
    private javax.swing.JSeparator jSeparator41;
    private javax.swing.JSeparator jSeparator42;
    private javax.swing.JSeparator jSeparator43;
    private javax.swing.JSeparator jSeparator44;
    private javax.swing.JSeparator jSeparator45;
    private javax.swing.JSeparator jSeparator46;
    private javax.swing.JSeparator jSeparator47;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTabbedPane jTabbedPane1;
    public static javax.swing.JLabel lblcodigo;
    private javax.swing.JPanel loginBtn1;
    private javax.swing.JPanel loginBtn2;
    private javax.swing.JPanel loginBtn3;
    private javax.swing.JPanel loginBtn5;
    private javax.swing.JPanel loginBtn6;
    public static javax.swing.JFormattedTextField txtcedula;
    public static javax.swing.JTextField txtcorreo;
    public static javax.swing.JTextArea txtdireccion;
    public static javax.swing.JTextField txtedad;
    public static javax.swing.JTextField txtlugar;
    public static javax.swing.JTextField txtnombre;
    public static javax.swing.JTextField txtnss;
    public static javax.swing.JTextArea txtotros;
    public static javax.swing.JTextArea txtotros2;
    public static javax.swing.JComboBox<String> txtpss;
    public static javax.swing.JFormattedTextField txttelefono;
    private javax.swing.JLabel userLabel1;
    private javax.swing.JLabel userLabel10;
    private javax.swing.JLabel userLabel11;
    private javax.swing.JLabel userLabel12;
    private javax.swing.JLabel userLabel13;
    private javax.swing.JLabel userLabel14;
    private javax.swing.JLabel userLabel15;
    private javax.swing.JLabel userLabel16;
    private javax.swing.JLabel userLabel17;
    private javax.swing.JLabel userLabel18;
    private javax.swing.JLabel userLabel19;
    private javax.swing.JLabel userLabel2;
    private javax.swing.JLabel userLabel20;
    private javax.swing.JLabel userLabel21;
    private javax.swing.JLabel userLabel22;
    private javax.swing.JLabel userLabel23;
    private javax.swing.JLabel userLabel24;
    private javax.swing.JLabel userLabel25;
    private javax.swing.JLabel userLabel26;
    private javax.swing.JLabel userLabel27;
    private javax.swing.JLabel userLabel28;
    private javax.swing.JLabel userLabel29;
    private javax.swing.JLabel userLabel3;
    private javax.swing.JLabel userLabel30;
    private javax.swing.JLabel userLabel31;
    private javax.swing.JLabel userLabel32;
    private javax.swing.JLabel userLabel33;
    private javax.swing.JLabel userLabel34;
    private javax.swing.JLabel userLabel35;
    private javax.swing.JLabel userLabel36;
    private javax.swing.JLabel userLabel37;
    private javax.swing.JLabel userLabel38;
    private javax.swing.JLabel userLabel39;
    private javax.swing.JLabel userLabel4;
    private javax.swing.JLabel userLabel40;
    private javax.swing.JLabel userLabel41;
    private javax.swing.JLabel userLabel42;
    private javax.swing.JLabel userLabel5;
    private javax.swing.JLabel userLabel6;
    private javax.swing.JLabel userLabel7;
    private javax.swing.JLabel userLabel8;
    private javax.swing.JLabel userLabel9;
    // End of variables declaration//GEN-END:variables
public static String cabeza = "Cabeza";
    public static String cara = "Cara";
    public static String atm = "ATM";
    public static String labios = "Labios";
    public static String lengua = "Lengua";
    public static String cuello = "Cuello";
    public static String id;
    public static String accion = "guardar";
    public static String cabeza2;
    Controlador controlador = new Controlador();
    conexion cc = new conexion();
    Connection cn = cc.conectar();

}
