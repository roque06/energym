                                          package odontograma;

import com.toedter.calendar.JDateChooser;
import java.awt.GridBagLayout;
import java.awt.Toolkit;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.sql.*;
import javax.swing.*;
import java.sql.Connection;
import java.sql.PreparedStatement;
import javax.swing.JOptionPane;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.sql.SQLException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import javax.swing.ImageIcon;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.time.LocalDate;
import java.time.Period;
import java.time.format.DateTimeFormatter;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumnModel;

public class registro extends javax.swing.JFrame {

    SimpleDateFormat df = new SimpleDateFormat("dd/MM/yyyy");
    //DateFormat df = DateFormat.getDateInstance();
    conexionDB con = new conexionDB();
    Connection cn = con.conexion();

    GridBagLayout layout = new GridBagLayout();
    odonto p6;
    odontonino p7;
    DefaultTableModel modelo;
    DefaultTableModel model;

    piezasnino o = new piezasnino();
//    funcion c = new funcion();
    piezas d = new piezas();
//    detalles z = new detalles();
    static String ceduli;
    static String id_odont;
    static String id_usuario;
    private Dimension dim;
    int tam;

    public registro() {
        initComponents();
        dim = this.getToolkit().getScreenSize();
        //this.setSize(dim);
        //this.setExtendedState(MAXIMIZED_BOTH);
        Dimension pantalla = Toolkit.getDefaultToolkit().getScreenSize();
        Dimension ventana = getSize();

        setLocation((pantalla.width - ventana.width) / 2, 0);
//                panelRegistro.setSize(dim);
        setLocationRelativeTo(null);
        tam = pantalla.height;

        if (tam < 800) {
            setResizable(true);

        } else {
            setResizable(false);
        }

        setTitle("Historia Medica");
        setIconImage(new ImageIcon(getClass().getResource("/images/diente2.png")).getImage());
        ((JPanel) getContentPane()).setOpaque(false);
        this.setVisible(false);

//        inhabilitar();
//        antecedentes();
        //txtfecha.setText(FechaActual());
//        nacio.addItem("V");
//        nacio.addItem("E");
//        z.estado();
//        z.parentezco();
//        z.codtelefono();
//        z.estatusregistro();
        JDateChooser chooser = new JDateChooser();
//         JTextFieldDateEditor editor = (JTextFieldDateEditor) calendarFecha.getDateEditor(); 
//         editor.setEditable(false);

        JDateChooser chooseer = new JDateChooser();
//         JTextFieldDateEditor editor2 = (JTextFieldDateEditor) fn.getDateEditor(); 
//         editor2.setEditable(false);
//         txtruta.setEditable(false);
//         Mañana.setSelected(true);
//        c.horama();
//        c.getturno(Mañana.getLabel());

        p6 = new odonto();
        p7 = new odontonino();
        dynamicPanel.setLayout(layout);
        GridBagConstraints c = new GridBagConstraints();
        c.gridx = 0;
        c.gridy = 0;
        dynamicPanel.add(p6, c);

        c.gridx = 0;
        c.gridy = 0;
        dynamicPanel.add(p7, c);
        p6.setVisible(true);
        p7.setVisible(false);
        adulto.setSelected(true);

        cerrar();
    }

    public void cerrar() {
        try {
            this.setDefaultCloseOperation(registro.DISPOSE_ON_CLOSE);
            addWindowListener(new WindowAdapter() {
                public void windowClosing(WindowEvent e) {
                    confirmarSalida();
                }
            });
            this.setVisible(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void confirmarSalida() {
        //int valor = JOptionPane.showConfirmDialog(this, "¿Esta Seguro de cerrar la Aplicacion?", "Advertencia", JOptionPane.YES_NO_OPTION, JOptionPane.WARNING_MESSAGE);
        //if (valor == JOptionPane.YES_NO_OPTION){
        //JOptionPane.showMessageDialog(null,"gracias por su visita","gracias", JOptionPane.INFORMATION_MESSAGE);
//        PantallaPrincipal.registro=false;
//        PantallaPrincipal.registro1=false;
        dispose();
        //}
    }

    public void odontoaa() {
        p6.removeAll();
        p7.removeAll();
        GridBagLayout layou = new GridBagLayout();
        p6 = new odonto();
        p7 = new odontonino();
        dynamicPanel.setLayout(layou);
        GridBagConstraints u = new GridBagConstraints();
        u.gridx = 0;
        u.gridy = 0;
        dynamicPanel.add(p6, u);

        u.gridx = 0;
        u.gridy = 0;
        dynamicPanel.add(p7, u);
        p6.setVisible(true);
        p7.setVisible(false);
        adulto.setSelected(true);
        d.limpieza();
        d.limpiezaArray();
        o.limpieza();
        o.limpiezaArray();
    }

//    void habilitar() {
//        btnimagen.setEnabled(true);
//        txtfecha.setEnabled(true);
//        txtfecha.setEditable(false);
//        txtcedula.setEnabled(true);
//        nacio.setEnabled(true);
//        txtnombre.setEnabled(true);
//        txtapellido.setEnabled(true);
//        f.setEnabled(true);
//        m.setEnabled(true);
//        estado.setEnabled(true);
//        txtdirecci.setEnabled(true);
//        txttelefono.setEnabled(true);
//        txtconsulta.setEnabled(true);
//        //txtnum.setEnabled(true);
//
//        btnguardar.setEnabled(true);
//        panelRegistro.setEnabled(true);
//        txtcedula.setEditable(true);
//        txtnombre.setEditable(true);
//        txtapellido.setEditable(true);
//        txttelefono.setEditable(true);
//        txtconsulta.setEditable(true);
//        txtdirecci.setEditable(true);
//        txthora.setEditable(true);
//        txttratamiento.setEditable(true);
//        txtocupacion.setEditable(true);
//        txtemail.setEditable(true);
//        txttelefonom.setEditable(true);
//        txtemernom.setEditable(true);
//        txtemertele.setEditable(true);
//        //calendarFecha.setEditable(false);
//        btnguardar.setEnabled(true);
//        btnguardar2.setEnabled(true);
//        txtcedulacita.setEnabled(true);
//        txtpaciente.setEnabled(true);
//        txthora.setEnabled(true);
//        txttratamiento.setEnabled(true);
//        txtcedula.setText("");
//        txtnombre.setText("");
//        txtapellido.setText("");
//        txtdirecci.setText("");
//        txttelefono.setText("");
//        txtconsulta.setText("");
//        txtcedulacita.setText("");
//        txtpaciente.setText("");
//        txtocupacion.setText("");
//
//        txttratamiento.setText("");
//        calendarFecha.setDate(null);
//        fn.setDate(null);
//        fn.setEnabled(true);
//        txtocupacion.setEnabled(true);
//
//        txtemernom.setEnabled(true);
//        txtemernom.setText("");
//        txtemertele.setEnabled(true);
//        txtemertele.setText("");
//        txtemerparen.setEnabled(true);
//        txtemail.setEnabled(true);
//        txtemail.setText("");
//        txttelefonom.setEnabled(true);
//        txttelefonom.setText("");
//        txtruta.setText("");
//        txtruta.setOpaque(false);
//        txtruta.setBorder(null);
//        panelRegistro.setEnabledAt(1, false);
//        btnimagen.setEnabled(true);
//        btncancelar.setEnabled(true);
//        telf.setEnabled(true);
//        telfm.setEnabled(true);
//        telfm1.setEnabled(true);
//        txtdiagnostico.setEditable(true);
//        txttrata.setEditable(true);
//        txtcosto.setEditable(true);
//        lblfoto.setText("");
//        btnguardar3.setEnabled(true);
//    }
//
//    void inhabilitar() {
//        //pestaña antecedentes
//        txtfecha2.setEditable(false);
//        txtcedula2.setEditable(false);
//        txtnom2.setEditable(false);
//        fn.setEnabled(false);
//        txtfecha.setEnabled(false);
//        nacio.setEnabled(false);
//        txtcedula.setEnabled(false);
//        txtnombre.setEnabled(false);
//        txtapellido.setEnabled(false);
//        f.setEnabled(false);
//        m.setEnabled(false);
//        estado.setEnabled(false);
//        txtdirecci.setEnabled(false);
//        txttelefono.setEnabled(false);
//        txtconsulta.setEnabled(false);
//        txtocupacion.setEnabled(false);
//        txtcedula.setText("");
//        txtnombre.setText("");
//        txtapellido.setText("");
//        txtdirecci.setText("");
//        txttelefono.setText("");
//        txtconsulta.setText("");
//        //txtnum.setText("");
//        txtocupacion.setText("");
//        btnguardar.setEnabled(false);
//        btnimagen.setEnabled(false);
//        //odontodiagrama.setEnabled(false);
//        panelRegistro.setEnabledAt(1, false);
//        panelRegistro.setEnabledAt(2, false);
//        panelRegistro.setEnabledAt(3, false);
//        //  btncancelar.setEnabled(false);
//        txtcedulacita.setEnabled(false);
//        txtpaciente.setEnabled(false);
//        txthora.setEnabled(false);
//        txttratamiento.setEnabled(false);
//        txtcedulacita.setText("");
//        txtpaciente.setText("");
//        calendarFecha.setDate(null);
//        fn.setDate(null);
//        txttratamiento.setText("");
//        txtemernom.setEnabled(false);
//        txtemernom.setText("");
//        txtemertele.setEnabled(false);
//        txtemertele.setText("");
//        txtemerparen.setEnabled(false);
//        txtemail.setEnabled(false);
//        txtemail.setText("");
//        txttelefonom.setEnabled(false);
//        txttelefonom.setText("");
//        txtruta.setOpaque(false);
//        txtruta.setBorder(null);
//        telf.setEnabled(false);
//        telfm.setEnabled(false);
//        telfm1.setEnabled(false);
//        btncancelar.setEnabled(false);
//        txtna2.setEditable(false);
//        txtcedulacita.setEditable(false);
//        txtpaciente.setEditable(false);
//    }
//
//    void antecedentes() {
//
//        txtdiabe.setEnabled(false);
//        txthiper.setEnabled(false);
//        txttuber.setEnabled(false);
//        txthepa.setEnabled(false);
//        txtale.setEnabled(false);
//        txtul.setEnabled(false);
//        txtemba.setEnabled(false);
//        txtmeto.setEnabled(false);
//        txtets.setEnabled(false);
//        txtcardio.setEnabled(false);
//        txtrespi.setEnabled(false);
//        txtgastro.setEnabled(false);
//        txtproblem.setEnabled(false);
//
//    }
//
//    void antecedentesLimpiar() {
//        s.setSelected(false);
//        n.setSelected(false);
//        s1.setSelected(false);
//        n1.setSelected(false);
//        s2.setSelected(false);
//        n2.setSelected(false);
//        s3.setSelected(false);
//        n3.setSelected(false);
//        s4.setSelected(false);
//        n4.setSelected(false);
//        s5.setSelected(false);
//        n5.setSelected(false);
//        s6.setSelected(false);
//        n6.setSelected(false);
//        s7.setSelected(false);
//        n7.setSelected(false);
//        s8.setSelected(false);
//        n8.setSelected(false);
//        s9.setSelected(false);
//        n9.setSelected(false);
//        s10.setSelected(false);
//        n10.setSelected(false);
//        s11.setSelected(false);
//        n11.setSelected(false);
//        s12.setSelected(false);
//        n12.setSelected(false);
//        txtdiabe.setText("");
//        txthiper.setText("");
//        txttuber.setText("");
//        txthepa.setText("");
//        txtale.setText("");
//        txtul.setText("");
//        txtemba.setText("");
//        txtmeto.setText("");
//        txtets.setText("");
//        txtcardio.setText("");
//        txtrespi.setText("");
//        txtgastro.setText("");
//        txtproblem.setText("");
//        txtmedi.setText("");
//        txtobserva.setText("");
//        txthabitos.setText("");
//        txtantece.setText("");
//        txtdiagnostico.setText("");
//        txtcosto.setText("");
//        txttrata.setText("");
//    }
//
//    void antecedentesedit() {
//        btnguardar1.setEnabled(false);
//        txtdiabe.setEditable(false);
//        txthiper.setEditable(false);
//        txttuber.setEditable(false);
//        txthepa.setEditable(false);
//        txtale.setEditable(false);
//        txtul.setEditable(false);
//        txtemba.setEditable(false);
//        txtmeto.setEditable(false);
//        txtets.setEditable(false);
//        txtcardio.setEditable(false);
//        txtrespi.setEditable(false);
//        txtgastro.setEditable(false);
//        txtproblem.setEditable(false);
//        txtmedi.setEditable(false);
//        txtobserva.setEditable(false);
//        txthabitos.setEditable(false);
//        txtantece.setEditable(false);
//    }
//
//    void antecedentes2() {
//        btnguardar1.setEnabled(true);
//        txtdiabe.setEditable(true);
//        txthiper.setEditable(true);
//        txttuber.setEditable(true);
//        txthepa.setEditable(true);
//        txtale.setEditable(true);
//        txtul.setEditable(true);
//        txtemba.setEditable(true);
//        txtmeto.setEditable(true);
//        txtets.setEditable(true);
//        txtcardio.setEditable(true);
//        txtrespi.setEditable(true);
//        txtgastro.setEditable(true);
//        txtproblem.setEditable(true);
//        txtmedi.setEditable(true);
//        txtobserva.setEditable(true);
//        txthabitos.setEditable(true);
//        txtantece.setEditable(true);
//    }
    void odontograma() {
        btnguardar3.setEnabled(false);
//        txtdiagnostico.setEditable(false);
//        txttrata.setEditable(false);
//        txtcosto.setEditable(false);
    }

    void odontograma2() {
        btnguardar3.setEnabled(true);

    }
    int añoActual;
    int mesActual;
    int diaActual;

    public String FechaActual() {

        Date fecha = new Date();
        SimpleDateFormat formato = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat sdfDia = new SimpleDateFormat("dd");
        SimpleDateFormat sdfMes = new SimpleDateFormat("MM");
        SimpleDateFormat sdfAño = new SimpleDateFormat("yyyy");
        añoActual = Integer.parseInt(sdfAño.format(fecha));
        mesActual = Integer.parseInt(sdfMes.format(fecha));
        diaActual = Integer.parseInt(sdfDia.format(fecha));
        return formato.format(fecha);

    }

    public static String hora() {
        Date fech = new Date();
        DateFormat hourFormat = new SimpleDateFormat("hh:mm:ss");
        return hourFormat.format(fech);
    }

//    void Editar() {
//        panelRegistro.setEnabledAt(0, true);
//        btnimagen.setEnabled(false);
//        txtcedula.setEditable(false);
//        txtnombre.setEditable(false);
//        txtapellido.setEditable(false);
//        txttelefono.setEditable(false);
//        txttelefonom.setEditable(false);
//        txtconsulta.setEditable(false);
//        txtdirecci.setEditable(false);
//        f.setEnabled(false);
//        m.setEnabled(false);
//        txtfecha.setEditable(false);
//        txtocupacion.setEditable(false);
//        txtemail.setEditable(false);
//        txtemernom.setEditable(false);
//        txtemertele.setEditable(false);
//        txtemerparen.setEditable(false);
//        txtna2.setEditable(false);
//        txtcedulacita.setEditable(false);
//        txtpaciente.setEditable(false);
//        btnguardar.setEnabled(false);
//        btncancelar.setEnabled(false);
//        panelRegistro.setEnabledAt(2, true);
//        panelRegistro.setEnabledAt(3, true);
//    }
//
//    void Editar2() {
//        txthora.setEditable(false);
//        txttratamiento.setEditable(false);
//        btnguardar2.setEnabled(false);
//    }
    String num = "";

    void consultar(String cedula) {
        String sSQL = "";
        String nom = "", fecha = "", imagen = "", ape = "", na = "", idpa = "";
        sSQL = "SELECT * FROM pacientes WHERE cedula=" + cedula;

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {

                //registro[0] = rs.getString("cedula");
                idpa = rs.getString("id_pacientes");
                na = rs.getString("nacionalidad");
                nom = rs.getString("nombre");
                ape = rs.getString("apellido");

                ByteArrayOutputStream output = new ByteArrayOutputStream();
                InputStream datos = rs.getBinaryStream("imagen");

                int tem = datos.read();
                while (tem >= 0) {
                    output.write((char) tem);
                    tem = datos.read();
                }

//                Image image = Toolkit.getDefaultToolkit().createImage(output.toByteArray());
//                image = image.getScaledInstance(120, 105, 1);
//                foto.setIcon(new ImageIcon(image));
//                txtfoto.setIcon(new ImageIcon(image));
//                n_historia.setText(idpa);
//                num = idpa;
//                txtfecha2.setText(FechaActual());
//                txtnom2.setText(nom);
//                txtcedula2.setText(cedula);
//                txtfecha3.setText(FechaActual());
//                txtnom3.setText(nom);
//                txtape3.setText(ape);
//                txtcedula3.setText(cedula);
//                txtfoto.setIcon(new ImageIcon(image));
//                txtcedulacita.setText(cedula);
//                txtpaciente.setText(nom);
//                txtna2.setText(na);
            }

        } catch (SQLException ex) {
            JOptionPane.showMessageDialog(null, "Error de conexión");
        } catch (IOException ex) {
            Logger.getLogger(registro.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public void cargarTabla(String valor) {

        String[] titulos = {"Hora", "Estado"};
        String[] registro = new String[2];
        modelo = new DefaultTableModel(null, titulos);

        String Ma = "Mañana";
        String sql = "";
        sql = "SELECT * FROM citas WHERE fecha LIKE '%" + valor + "%' AND turnoc LIKE '%" + Ma + "%' AND id_odon LIKE '%" + id_odont + "%' ";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sql);

            while (rs.next()) {

                registro[0] = rs.getString("hora");
                registro[1] = rs.getString("estado");

                modelo.addRow(registro);

            }

            int a = modelo.getRowCount();

            if (a == 0) {

//                reiniciarJTable(tblci);
//                tblci.setModel(modelo);
            } else {

//                tblci.setModel(modelo);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
            //JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
        }

        String[] titulo = {"hora", "Estado"};
        String[] registr = new String[5];
        model = new DefaultTableModel(null, titulo) {
            @Override
            public boolean isCellEditable(int fila, int columna) {
                return false;
            }
        };
        String tar = "Tarde";
        String SQL = "";

        SQL = "SELECT * FROM citas WHERE fecha LIKE '%" + valor + "%' AND turnoc LIKE '%" + tar + "%' AND id_odon LIKE '%" + id_odont + "%' ORDER BY hora desc limit 6";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(SQL);

            while (rs.next()) {

                registr[0] = rs.getString("hora");
                //registro[1] = rs.getString("fecha");
                registr[1] = rs.getString("estado");
                model.addRow(registr);

            }
            int a = model.getRowCount();

            if (a == 0) {

//                reiniciarJTable(tblci1);
//                tblci1.setModel(model);
            } else {

//                tblci1.setModel(model);
            }

        } catch (SQLException ex) {
            System.out.println(ex);
            //JOptionPane.showMessageDialog(null, ex);

        }

    }

    public static void reiniciarJTable(javax.swing.JTable Tabla) {
        DefaultTableModel modelo = (DefaultTableModel) Tabla.getModel();
        while (modelo.getRowCount() > 0) {
            modelo.removeRow(0);
        }

        TableColumnModel modCol = Tabla.getColumnModel();
        while (modCol.getColumnCount() > 0) {
            modCol.removeColumn(modCol.getColumn(0));
        }

    }

    int edadd = 0;

    void edad(String fecha) {
        DateTimeFormatter fmt = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        LocalDate fechaNac = LocalDate.parse(fecha, fmt);
        LocalDate ahora = LocalDate.now();

        Period periodo = Period.between(fechaNac, ahora);
        System.out.printf("Tu edad es: %s años, %s meses y %s días",
                periodo.getYears(), periodo.getMonths(), periodo.getDays());
        edadd = periodo.getYears();

        if (edadd <= 9) {
            niño.setSelected(true);
            p7.setVisible(true);
            p6.setVisible(false);
        } else if (edadd > 10) {
            adulto.setSelected(true);
            niño.setEnabled(false);
        }
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        gruposexo = new javax.swing.ButtonGroup();
        diabe = new javax.swing.ButtonGroup();
        hiper = new javax.swing.ButtonGroup();
        tuber = new javax.swing.ButtonGroup();
        hepa = new javax.swing.ButtonGroup();
        aler = new javax.swing.ButtonGroup();
        ulce = new javax.swing.ButtonGroup();
        emba = new javax.swing.ButtonGroup();
        meto = new javax.swing.ButtonGroup();
        ets = new javax.swing.ButtonGroup();
        cardio = new javax.swing.ButtonGroup();
        respi = new javax.swing.ButtonGroup();
        gastro = new javax.swing.ButtonGroup();
        problem = new javax.swing.ButtonGroup();
        odontograma1 = new javax.swing.ButtonGroup();
        grupoturno = new javax.swing.ButtonGroup();
        jPasswordField1 = new javax.swing.JPasswordField();
        jPanel34 = new javax.swing.JPanel();
        jPanel5 = new javax.swing.JPanel();
        btncancelar2 = new javax.swing.JButton();
        btnguardar3 = new javax.swing.JButton();
        jPanel37 = new javax.swing.JPanel();
        adulto = new javax.swing.JRadioButton();
        niño = new javax.swing.JRadioButton();
        xr = new javax.swing.JToggleButton();
        xa = new javax.swing.JToggleButton();
        pa = new javax.swing.JToggleButton();
        pr = new javax.swing.JToggleButton();
        tca = new javax.swing.JToggleButton();
        ca = new javax.swing.JToggleButton();
        frac = new javax.swing.JToggleButton();
        tcr = new javax.swing.JToggleButton();
        cr = new javax.swing.JToggleButton();
        sana = new javax.swing.JToggleButton();
        dynamicPanel = new javax.swing.JPanel();
        jLabel1 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jLabel2 = new javax.swing.JLabel();
        lblcodigo = new javax.swing.JLabel();

        jPasswordField1.setText("jPasswordField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel34.setBackground(new java.awt.Color(248, 248, 248));

        jPanel5.setBackground(new java.awt.Color(234, 239, 242));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder("Herramientas"));
        jPanel5.setToolTipText("");

        btncancelar2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Remove.png"))); // NOI18N
        btncancelar2.setBorder(null);
        btncancelar2.setBorderPainted(false);
        btncancelar2.setContentAreaFilled(false);
        btncancelar2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncancelar2.setHideActionText(true);
        btncancelar2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btncancelar2.setIconTextGap(-3);
        btncancelar2.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btncancelar2.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btncancelar2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncancelar2ActionPerformed(evt);
            }
        });

        btnguardar3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/Save (1).png"))); // NOI18N
        btnguardar3.setBorder(null);
        btnguardar3.setBorderPainted(false);
        btnguardar3.setContentAreaFilled(false);
        btnguardar3.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar3.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        btnguardar3.setIconTextGap(-3);
        btnguardar3.setVerticalAlignment(javax.swing.SwingConstants.BOTTOM);
        btnguardar3.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        btnguardar3.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnguardar3ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(btncancelar2)
                    .addComponent(btnguardar3))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addComponent(btnguardar3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addComponent(btncancelar2)
                .addGap(23, 23, 23))
        );

        jPanel37.setBackground(new java.awt.Color(234, 239, 242));
        jPanel37.setBorder(javax.swing.BorderFactory.createTitledBorder("Herramientas"));

        odontograma1.add(adulto);
        adulto.setText("Adulto");
        adulto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                adultoMouseClicked(evt);
            }
        });

        odontograma1.add(niño);
        niño.setText("Niño");
        niño.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                niñoActionPerformed(evt);
            }
        });

        xr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tooth-48m3x2.png"))); // NOI18N
        xr.setToolTipText("Exodoncia");
        xr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                xrMouseClicked(evt);
            }
        });

        xa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tooth-48m3xa2.png"))); // NOI18N
        xa.setToolTipText("Ausente");
        xa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                xaMouseClicked(evt);
            }
        });

        pa.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/puntoaz.png"))); // NOI18N
        pa.setToolTipText("Buen estado");
        pa.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                paMouseClicked(evt);
            }
        });

        pr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/punto.png"))); // NOI18N
        pr.setToolTipText("Caries");
        pr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                prMouseClicked(evt);
            }
        });

        tca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tca.png"))); // NOI18N
        tca.setToolTipText("Tratamiento Conducto Buen Estado");
        tca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tcaMouseClicked(evt);
            }
        });

        ca.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/circuloazul.png"))); // NOI18N
        ca.setToolTipText("Corona Buen Estado");
        ca.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                caMouseClicked(evt);
            }
        });

        frac.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/fr.png"))); // NOI18N
        frac.setToolTipText("Fractura");
        frac.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                fracMouseClicked(evt);
            }
        });

        tcr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/tcr.png"))); // NOI18N
        tcr.setToolTipText("Tratamiento Conducto mal Estado");
        tcr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tcrMouseClicked(evt);
            }
        });

        cr.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/circulorojo.png"))); // NOI18N
        cr.setToolTipText("Corona Mal Estado");
        cr.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                crMouseClicked(evt);
            }
        });

        sana.setIcon(new javax.swing.ImageIcon(getClass().getResource("/images/borrador.png"))); // NOI18N
        sana.setToolTipText("Borrar");
        sana.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sanaMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                sanaMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout jPanel37Layout = new javax.swing.GroupLayout(jPanel37);
        jPanel37.setLayout(jPanel37Layout);
        jPanel37Layout.setHorizontalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel37Layout.createSequentialGroup()
                        .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(pr, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(cr, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(sana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                            .addComponent(tcr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel37Layout.createSequentialGroup()
                                .addGap(27, 27, 27)
                                .addComponent(pa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel37Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                    .addComponent(tca, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 62, Short.MAX_VALUE)
                                    .addComponent(frac, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                    .addComponent(ca, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)))))
                    .addGroup(jPanel37Layout.createSequentialGroup()
                        .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(adulto)
                            .addComponent(xr, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(niño)
                            .addComponent(xa, javax.swing.GroupLayout.PREFERRED_SIZE, 62, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addContainerGap())
        );
        jPanel37Layout.setVerticalGroup(
            jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel37Layout.createSequentialGroup()
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(adulto)
                    .addComponent(niño))
                .addGap(12, 12, 12)
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(xa, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(xr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(pr)
                    .addComponent(pa))
                .addGap(18, 18, 18)
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(ca)
                    .addComponent(cr))
                .addGap(18, 18, 18)
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(frac, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(sana, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(19, 19, 19)
                .addGroup(jPanel37Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(tcr, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(tca, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addContainerGap(13, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout dynamicPanelLayout = new javax.swing.GroupLayout(dynamicPanel);
        dynamicPanel.setLayout(dynamicPanelLayout);
        dynamicPanelLayout.setHorizontalGroup(
            dynamicPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 218, Short.MAX_VALUE)
        );
        dynamicPanelLayout.setVerticalGroup(
            dynamicPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 127, Short.MAX_VALUE)
        );

        jLabel1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel1.setText("Nombre:");

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Codigo:");

        lblcodigo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblcodigo.setText("....");

        javax.swing.GroupLayout jPanel34Layout = new javax.swing.GroupLayout(jPanel34);
        jPanel34.setLayout(jPanel34Layout);
        jPanel34Layout.setHorizontalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                    .addComponent(jPanel5, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                    .addComponent(jPanel37, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(dynamicPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 598, Short.MAX_VALUE)
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel1)
                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 181, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel2)
                    .addComponent(lblcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 121, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(38, 38, 38))
        );
        jPanel34Layout.setVerticalGroup(
            jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel34Layout.createSequentialGroup()
                .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addContainerGap()
                        .addGroup(jPanel34Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(dynamicPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jPanel37, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel34Layout.createSequentialGroup()
                        .addGap(22, 22, 22)
                        .addComponent(jLabel2)
                        .addGap(5, 5, 5)
                        .addComponent(lblcodigo)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel34, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
    String accion = "Insertar";
    private void sanaMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sanaMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_sanaMouseEntered

    private void sanaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sanaMouseClicked
        xr.setSelected(false);
        xa.setSelected(false);
        pr.setSelected(false);
        pa.setSelected(false);
        cr.setSelected(false);
        ca.setSelected(false);
        tcr.setSelected(false);
        tca.setSelected(false);
        frac.setSelected(false);
    }//GEN-LAST:event_sanaMouseClicked

    private void crMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_crMouseClicked
        xr.setSelected(false);
        xa.setSelected(false);
        pr.setSelected(false);
        pa.setSelected(false);
        cr.setSelected(true);
        ca.setSelected(false);
        tcr.setSelected(false);
        tca.setSelected(false);
        frac.setSelected(false);
        sana.setSelected(false);
    }//GEN-LAST:event_crMouseClicked

    private void tcrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tcrMouseClicked
        xr.setSelected(false);
        xa.setSelected(false);
        pr.setSelected(false);
        pa.setSelected(false);
        cr.setSelected(false);
        ca.setSelected(false);
        tcr.setSelected(true);
        tca.setSelected(false);
        frac.setSelected(false);
        sana.setSelected(false);
    }//GEN-LAST:event_tcrMouseClicked

    private void fracMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_fracMouseClicked
        xr.setSelected(false);
        xa.setSelected(false);
        pr.setSelected(false);
        pa.setSelected(false);
        cr.setSelected(false);
        ca.setSelected(false);
        tcr.setSelected(false);
        tca.setSelected(false);
        frac.setSelected(true);
        sana.setSelected(false);
    }//GEN-LAST:event_fracMouseClicked

    private void caMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_caMouseClicked
        xr.setSelected(false);
        xa.setSelected(false);
        pr.setSelected(false);
        pa.setSelected(false);
        cr.setSelected(false);
        ca.setSelected(true);
        tcr.setSelected(false);
        tca.setSelected(false);
        frac.setSelected(false);
        sana.setSelected(false);
    }//GEN-LAST:event_caMouseClicked

    private void tcaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tcaMouseClicked
        xr.setSelected(false);
        xa.setSelected(false);
        pr.setSelected(false);
        pa.setSelected(false);
        cr.setSelected(false);
        ca.setSelected(false);
        tcr.setSelected(false);
        tca.setSelected(true);
        frac.setSelected(false);
        sana.setSelected(false);
    }//GEN-LAST:event_tcaMouseClicked

    private void prMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_prMouseClicked
        xr.setSelected(false);
        xa.setSelected(false);
        pr.setSelected(true);
        pa.setSelected(false);
        cr.setSelected(false);
        ca.setSelected(false);
        tcr.setSelected(false);
        tca.setSelected(false);
        frac.setSelected(false);
        sana.setSelected(false);
    }//GEN-LAST:event_prMouseClicked

    private void paMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_paMouseClicked
        xr.setSelected(false);
        xa.setSelected(false);
        pr.setSelected(false);
        pa.setSelected(true);
        cr.setSelected(false);
        ca.setSelected(false);
        tcr.setSelected(false);
        tca.setSelected(false);
        frac.setSelected(false);
        sana.setSelected(false);
    }//GEN-LAST:event_paMouseClicked

    private void xaMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_xaMouseClicked
        xr.setSelected(false);
        xa.setSelected(true);
        pr.setSelected(false);
        pa.setSelected(false);
        cr.setSelected(false);
        ca.setSelected(false);
        tcr.setSelected(false);
        tca.setSelected(false);
        frac.setSelected(false);
        sana.setSelected(false);
    }//GEN-LAST:event_xaMouseClicked

    private void xrMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_xrMouseClicked
        xr.setSelected(true);
        xa.setSelected(false);
        pr.setSelected(false);
        pa.setSelected(false);
        cr.setSelected(false);
        ca.setSelected(false);
        tcr.setSelected(false);
        tca.setSelected(false);
        frac.setSelected(false);
        sana.setSelected(false);
    }//GEN-LAST:event_xrMouseClicked

    private void niñoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_niñoActionPerformed
        p7.setVisible(true);
        p6.setVisible(false);
    }//GEN-LAST:event_niñoActionPerformed

    private void adultoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_adultoMouseClicked
        p6.setVisible(true);
        p7.setVisible(false);
    }//GEN-LAST:event_adultoMouseClicked

    private void btnguardar3ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnguardar3ActionPerformed

        String sql = "INSERT INTO piezas(id_pacientes, cedula, id_diente, estatus_d, tipo) VALUES (?,?,?,?,?)";

        int n = 0;
        if (adulto.isSelected()) {
            d.array();

            String aok = odonto.tipo;
            try {

                PreparedStatement pps = cn.prepareStatement(sql);

                for (int i = 0; i < d.dientes.size(); i++) {
                    pps.setString(1, "1001");
                    pps.setString(2, "22345789");
                    pps.setString(3, d.dientes.get(i));
                    pps.setString(4, d.estado.get(i));
                    pps.setString(5, aok);
                    n = pps.executeUpdate();

                }

                if (n > 0) {
                    // JOptionPane.showMessageDialog(null, mensaje);

                }
            } catch (SQLException ex) {
                System.out.println(ex);
                //JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
            }
            odontograma();

        } else if (niño.isSelected()) {
            o.array();
            String ni = odontonino.tipo2;
            try {

                PreparedStatement pps = cn.prepareStatement(sql);

                for (int i = 0; i < o.dientes.size(); i++) {
                    pps.setString(1, "1001");
                    pps.setString(2, "22345789");
                    pps.setString(3, o.dientes.get(i));
                    pps.setString(4, o.estado.get(i));
                    pps.setString(5, ni);
                    n = pps.executeUpdate();

                }

                if (n > 0) {
                    //JOptionPane.showMessageDialog(null, mensaje);

                }
            } catch (SQLException ex) {
                System.out.println(ex);
                //JOptionPane.showMessageDialog(null, ex, "Error", JOptionPane.ERROR_MESSAGE);
            }
            odontograma();

        }

        sql = "INSERT INTO odontograma(id_pacientes, cedula, fecha, hora, diagnostico, plan_trata, costo, estatus) VALUES (?,?,?,?,?,?,?,?)";

        try {

            PreparedStatement pps = cn.prepareStatement(sql);
            pps.setString(1, "1001");
            pps.setString(2, "22345789");
            pps.setString(3, detalles.FechaActual());
            pps.setString(4, detalles.hora());
            pps.setString(5, "Ortodonciia");
            pps.setString(6, "Sacar muela");
            pps.setString(7, "5000");
            pps.setString(8, "Ausente");

            int n1 = pps.executeUpdate();
            if (n1 > 0) {
                JOptionPane.showMessageDialog(null, "Actualizado!");
//                panelRegistro.setSelectedIndex(3);
//                panelRegistro.setEnabledAt(3, true);
//                String cedula = txtcedula.getText();
                odontograma();
            }
        } catch (SQLException ex) {
            System.out.println(ex);
            JOptionPane.showMessageDialog(null, "Error de Conexión", "Error", JOptionPane.ERROR_MESSAGE);
        }
    }//GEN-LAST:event_btnguardar3ActionPerformed

    private void btncancelar2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncancelar2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_btncancelar2ActionPerformed

    public boolean isEmail(String correo) {
        Pattern pat = null;
        pat = Pattern.compile("^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$");
        mat = pat.matcher(correo);
        if (mat.find()) {
            return true;
        } else {
            return false;
        }
    }
    Matcher mat = null;

    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(registro.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new registro().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JRadioButton adulto;
    private javax.swing.ButtonGroup aler;
    private javax.swing.JButton btncancelar2;
    private javax.swing.JButton btnguardar3;
    public static javax.swing.JToggleButton ca;
    private javax.swing.ButtonGroup cardio;
    public static javax.swing.JToggleButton cr;
    private javax.swing.ButtonGroup diabe;
    private javax.swing.JPanel dynamicPanel;
    private javax.swing.ButtonGroup emba;
    private javax.swing.ButtonGroup ets;
    public static javax.swing.JToggleButton frac;
    private javax.swing.ButtonGroup gastro;
    public static javax.swing.ButtonGroup gruposexo;
    private javax.swing.ButtonGroup grupoturno;
    private javax.swing.ButtonGroup hepa;
    private javax.swing.ButtonGroup hiper;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel34;
    private javax.swing.JPanel jPanel37;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPasswordField jPasswordField1;
    public static javax.swing.JLabel lblcodigo;
    private javax.swing.ButtonGroup meto;
    private javax.swing.JRadioButton niño;
    private javax.swing.ButtonGroup odontograma1;
    public static javax.swing.JToggleButton pa;
    public static javax.swing.JToggleButton pr;
    private javax.swing.ButtonGroup problem;
    private javax.swing.ButtonGroup respi;
    public static javax.swing.JToggleButton sana;
    public static javax.swing.JToggleButton tca;
    public static javax.swing.JToggleButton tcr;
    private javax.swing.ButtonGroup tuber;
    public static javax.swing.JTextField txtnombre;
    private javax.swing.ButtonGroup ulce;
    public static javax.swing.JToggleButton xa;
    public static javax.swing.JToggleButton xr;
    // End of variables declaration//GEN-END:variables
}
