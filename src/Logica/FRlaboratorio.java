package Logica;

import Datos.Flaboratorios;
import Datos.Fregimen;
import Formularios.Flaboratorio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRlaboratorio {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "No.Orden", "Fecha Envio", "Registro", "Paciente", "Laboratorio", "Tipo/Tratamiento", "Fecha Recibido", "CodPaciente"};

        String[] registro = new String[10];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select  id,orden,DATE_FORMAT(fecha,'%d/%m/%Y')as registro,fecha,paciente,laboratorio,tratamiento,fecharecibo,codpaciente From tblaboratorio  where concat ('orden','paciente','codpaciente') like '%" + buscar + "%' order by orden desc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("orden");
                registro[2] = rs.getString("registro");
                registro[3] = rs.getString("fecha");
                registro[4] = rs.getString("paciente");
                registro[5] = rs.getString("laboratorio");
                registro[6] = rs.getString("tratamiento");
                registro[7] = rs.getString("fecharecibo");
                registro[8] = rs.getString("codpaciente");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Flaboratorios dts) {

        sSQL = "insert into tblaboratorio (orden,fecha,paciente,laboratorio,tratamiento,fecharecibo,codpaciente)"
                + "VALUES(?,?,?,?,?,?,?)";

        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getOrden());
            pst.setDate(2, dts.getFecha());
            pst.setString(3, dts.getPaciente());
            pst.setString(4, dts.getLaboratorio());
            pst.setString(5, dts.getTratamiento());
            if (dts.getFecharecibo() == null) {
                pst.setString(6, dts.getRecibido());
            } else {
                pst.setDate(6, dts.getFecharecibo());
            }
            pst.setString(7, dts.getCodpaciente());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Flaboratorios dts) {

        sSQL = "UPDATE tblaboratorio set orden=?,fecha=?,paciente=?,laboratorio=?,tratamiento=?,fecharecibo=?"
                + "where id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getOrden());
            pst.setDate(2, dts.getFecha());
            pst.setString(3, dts.getPaciente());
            pst.setString(4, dts.getLaboratorio());
            pst.setString(5, dts.getTratamiento());

            if (dts.getFecharecibo() == null) {
                pst.setString(6, dts.getRecibido());

            } else {
                pst.setDate(6, dts.getFecharecibo());

            }

            pst.setInt(7, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Flaboratorios dts) {
        sSQL = "DELETE FROM tblaboratorio WHERE id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
