/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Datos.Fcitas;
import Logica.FRcitas;
import Logica.FRhistorialct;
import java.awt.Graphics;
import java.awt.Image;
import java.sql.Date;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author radames
 */
public class Citas extends javax.swing.JFrame {

    public Citas() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        this.setLocationRelativeTo(null);
        mostrar("");
        inhabilitar();
        btncorreo.setEnabled(false);
        dtfecha.setCalendar(c1);
    }
    Calendar c1 = new GregorianCalendar();

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRcitas func = new FRcitas();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            ocultar_columnas();

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(1).setMaxWidth(230);
        tablalistado.getColumnModel().getColumn(1).setMinWidth(230);
        tablalistado.getColumnModel().getColumn(1).setPreferredWidth(230);

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(3).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(3).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(3).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(5).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(5).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(5).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(4).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(4).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(4).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(9).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(9).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(9).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(10).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(10).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(10).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(11).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(11).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(11).setPreferredWidth(0);

    }

    void guardar() {
        if (txtnombre.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar el Nombre del Paciente");
            txtnombre.requestFocus();
            return;
        }

        if (txttelefono.getText().length() == 2) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar el Telefono del Paciente");
            txttelefono.requestFocus();
            return;
        }
        if (txtnombre.getText().length() == 2) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar el Correo Electronico del Paciente");
            txtnombre.requestFocus();
            return;
        }

        if (txtsintomas.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar el Motivo de la visita del Paciente");
            txtsintomas.requestFocus();
            return;
        }

        if (dtfecha.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar la Fecha del Registro");
            return;
        }

        if (cbogenero.getSelectedItem().equals("Seleccionar")) {
            JOptionPane.showMessageDialog(null, "Debes Seleccionar el Genero del Paciente");
            return;
        }

//        if (txthora.getText().length() == 1) {
//            JOptionPane.showMessageDialog(null, "Debes Ingresar la Hora de la Cita");
//            txthora.requestFocus();
//            return;
//        }
        Fcitas dts = new Fcitas();
        FRcitas func = new FRcitas();

        dts.setNombre(txtnombre.getText());
        dts.setTelefono(txttelefono.getText());
        dts.setCorreo(txtcorreo.getText());
        dts.setSintomas(txtsintomas.getText());

        Calendar cal;
        int d, m, a;
        cal = dtfecha.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha(new Date(a, m, d));

        int seleccionado = cbogenero.getSelectedIndex();
        dts.setGenero((String) cbogenero.getItemAt(seleccionado));
        dts.setHora(hora);
        seleccionado = txthora.getSelectedIndex();
        dts.setHoram((String) txthora.getItemAt(seleccionado));

        seleccionado = cbominutos.getSelectedIndex();
        dts.setMinutos((String) cbominutos.getItemAt(seleccionado));

        seleccionado = cbohora.getSelectedIndex();
        dts.setAm((String) cbohora.getItemAt(seleccionado));

        if (accion.equals("guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(null, "Cita Registrada Correctamente");

            }

        } else if (accion.equals("editar")) {
            dts.setId(Integer.parseInt(id));
            if (func.editar(dts)) {
                JOptionPane.showMessageDialog(null, "Cita Actualizada Correctamente");

            }

        }
        mostrar("");
        inhabilitar();

    }

    void GuardarHistorial() {
        if (txtnombre.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar el Nombre del Paciente");
            txtnombre.requestFocus();
            return;
        }

        if (txttelefono.getText().length() == 2) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar el Telefono del Paciente");
            txttelefono.requestFocus();
            return;
        }

        if (txtsintomas.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar el Motivo de la visita del Paciente");
            txtsintomas.requestFocus();
            return;
        }

        if (dtfecha.getDate() == null) {
            JOptionPane.showMessageDialog(null, "Debes Ingresar la Fecha del Registro");
            return;
        }

        if (cbogenero.getSelectedItem().equals("Seleccionar")) {
            JOptionPane.showMessageDialog(null, "Debes Seleccionar el Genero del Paciente");
            return;
        }

//        if (txthora.getText().length() == 1) {
//            JOptionPane.showMessageDialog(null, "Debes Ingresar la Hora de la Cita");
//            txthora.requestFocus();
//            return;
//        }
        Fcitas dts = new Fcitas();
        FRhistorialct func = new FRhistorialct();

        dts.setNombre(txtnombre.getText());
        dts.setTelefono(txttelefono.getText());

        dts.setSintomas(txtsintomas.getText());

        Calendar cal;
        int d, m, a;
        cal = dtfecha.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha(new Date(a, m, d));

        int seleccionado = cbogenero.getSelectedIndex();
        dts.setGenero((String) cbogenero.getItemAt(seleccionado));
        dts.setHora(hora);
        dts.setStatus(status);

        if (accion.equals("guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(null, "Paciente Procesado Exitosamente");

            }

        }
        mostrar("");
        inhabilitar();

    }

    void elimininar() {
        Fcitas dts = new Fcitas();
        FRcitas func = new FRcitas();

        dts.setId(Integer.parseInt(id));

        if (func.eliminar(dts)) {
//            JOptionPane.showMessageDialog(null, "Paciente Eliminado");
        }
        mostrar("");
    }

    public static void habilitar() {
        txtnombre.setEnabled(true);
        txttelefono.setEnabled(true);
        txtsintomas.setEnabled(true);
        dtfecha.setEnabled(true);
        cbogenero.setEnabled(true);
        btnguardar.setEnabled(true);
        txthora.setEnabled(true);
        cbohora.setEnabled(true);
        btnbien.setEnabled(false);
        btnmal.setEnabled(false);
        txtcorreo.setEnabled(true);
        cbominutos.setEnabled(true);
        txtnombre.setText("");
        txtcorreo.setText("");
        txttelefono.setText("");
        txtsintomas.setText("");
        dtfecha.setDate(null);
        txthora.setSelectedItem("00");
        cbominutos.setSelectedItem("00");
        cbogenero.setSelectedItem("AM");
//        txthora.setText("");
        cbogenero.setSelectedItem("Seleccionar");

    }

    void inhabilitar() {
        txtnombre.setEnabled(false);
        txttelefono.setEnabled(false);
        txtsintomas.setEnabled(false);
        dtfecha.setEnabled(false);
        cbogenero.setEnabled(false);
        btnguardar.setEnabled(false);
        txthora.setEnabled(false);
        cbohora.setEnabled(false);
        btnbien.setEnabled(false);
        btnmal.setEnabled(false);
        txtcorreo.setEnabled(false);
        cbominutos.setEnabled(false);
        txtnombre.setText("");
        txttelefono.setText("");
        txtsintomas.setText("");
        dtfecha.setDate(null);
        cbogenero.setSelectedItem("AM");
//        txthora.setText("");
        txtcorreo.setText("");
        txthora.setSelectedItem("00");
        cbominutos.setSelectedItem("00");
        cbogenero.setSelectedItem("Seleccionar");

    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        dtfecha = new com.toedter.calendar.JDateChooser();
        txttelefono = new javax.swing.JFormattedTextField();
        cbogenero = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        cbohora = new javax.swing.JComboBox<>();
        txthora = new javax.swing.JComboBox<>();
        cbominutos = new javax.swing.JComboBox<>();
        jScrollPane2 = new javax.swing.JScrollPane();
        txtsintomas = new javax.swing.JTextArea();
        userLabel7 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        btnbuscar = new javax.swing.JButton();
        userLabel4 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        userLabel8 = new javax.swing.JLabel();
        jSeparator9 = new javax.swing.JSeparator();
        userLabel9 = new javax.swing.JLabel();
        txtcorreo = new javax.swing.JTextField();
        txtnombre2 = new javax.swing.JTextField();
        userLabel10 = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JSeparator();
        userLabel11 = new javax.swing.JLabel();
        jSeparator12 = new javax.swing.JSeparator();
        jSeparator13 = new javax.swing.JSeparator();
        userLabel12 = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        userLabel13 = new javax.swing.JLabel();
        jSeparator14 = new javax.swing.JSeparator();
        userLabel14 = new javax.swing.JLabel();
        jSeparator15 = new javax.swing.JSeparator();
        loginBtn4 = new javax.swing.JPanel();
        btncobro2 = new javax.swing.JLabel();
        loginBtn = new javax.swing.JPanel();
        loginBtnTxt = new javax.swing.JLabel();
        loginBtn1 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        loginBtn5 = new javax.swing.JPanel();
        btnbien = new javax.swing.JLabel();
        loginBtn6 = new javax.swing.JPanel();
        btnmal = new javax.swing.JLabel();
        loginBtn7 = new javax.swing.JPanel();
        btncorreo = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        dtfecha.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.add(dtfecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 170, 220, 30));

        txttelefono.setBorder(null);
        try {
            txttelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        txttelefono.setToolTipText("");
        jPanel1.add(txttelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 150, 230, 30));

        cbogenero.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        cbogenero.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Seleccionar", "Masculino", "Femenino" }));
        jPanel1.add(cbogenero, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 240, 220, 30));

        tablalistado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablalistado);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 830, 250));

        txtbuscar.setBorder(null);
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        jPanel1.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 280, 200, 30));

        cbohora.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "AM", "PM" }));
        jPanel1.add(cbohora, new org.netbeans.lib.awtextra.AbsoluteConstraints(830, 100, 50, 30));

        txthora.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12" }));
        jPanel1.add(txthora, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 100, 50, 30));

        cbominutos.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "00", "01", "02", "03", "04", "05", "06", "07", "08", "09", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20", "21", "22", "23", "24", "25", "26", "27", "28", "29", "30", "31", "32", "33", "34", "35", "36", "37", "38", "39", "40", "41", "42", "43", "44", "45", "46", "47", "48", "49", "50", "51", "52", "53", "54", "55", "56", "57", "58", "59" }));
        jPanel1.add(cbominutos, new org.netbeans.lib.awtextra.AbsoluteConstraints(760, 100, 50, 30));

        txtsintomas.setColumns(20);
        txtsintomas.setLineWrap(true);
        txtsintomas.setRows(3);
        txtsintomas.setBorder(null);
        jScrollPane2.setViewportView(txtsintomas);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 80, 220, 60));

        userLabel7.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel7.setText("Genero:");
        jPanel1.add(userLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 210, -1, -1));

        txtnombre.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtnombre.setBorder(null);
        txtnombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtnombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtnombreMousePressed(evt);
            }
        });
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreKeyTyped(evt);
            }
        });
        jPanel1.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 80, 230, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 130, 190, 20));

        btnbuscar.setBackground(new java.awt.Color(0, 134, 190));
        btnbuscar.setForeground(new java.awt.Color(0, 134, 190));
        btnbuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        btnbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarActionPerformed(evt);
            }
        });
        jPanel1.add(btnbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, 40, 30));

        userLabel4.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        userLabel4.setText("FORMULARIO DE CITAS");
        jPanel1.add(userLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 0, -1, -1));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(360, 30, 280, 20));

        userLabel8.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel8.setText("Nombre:");
        jPanel1.add(userLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 60, -1, -1));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 110, 230, 20));

        userLabel9.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel9.setText("Telefono:");
        jPanel1.add(userLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 120, -1, 20));

        txtcorreo.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtcorreo.setBorder(null);
        txtcorreo.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtcorreo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtcorreoMousePressed(evt);
            }
        });
        txtcorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcorreoActionPerformed(evt);
            }
        });
        jPanel1.add(txtcorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 220, 230, 30));

        txtnombre2.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtnombre2.setBorder(null);
        txtnombre2.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtnombre2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtnombre2MousePressed(evt);
            }
        });
        txtnombre2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombre2ActionPerformed(evt);
            }
        });
        jPanel1.add(txtnombre2, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 270, 200, 30));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Buscar:");
        jPanel1.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 290, -1, -1));

        jSeparator11.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 200, 220, 20));

        userLabel11.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel11.setText("Sintomas:");
        jPanel1.add(userLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 60, -1, -1));

        jSeparator12.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 270, 220, 20));

        jSeparator13.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator13, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 310, 200, 20));

        userLabel12.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel12.setText("Fecha:");
        jPanel1.add(userLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 150, -1, -1));

        jSeparator10.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 180, 230, 20));

        userLabel13.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel13.setText("Hora:");
        jPanel1.add(userLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(690, 70, -1, -1));

        jSeparator14.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator14, new org.netbeans.lib.awtextra.AbsoluteConstraints(400, 140, 220, 20));

        userLabel14.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel14.setText("Correo:");
        jPanel1.add(userLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 200, -1, -1));

        jSeparator15.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 250, 230, 20));

        loginBtn4.setBackground(new java.awt.Color(0, 134, 190));

        btncobro2.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncobro2.setForeground(new java.awt.Color(255, 255, 255));
        btncobro2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncobro2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        btncobro2.setText("Historial");
        btncobro2.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncobro2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncobro2MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncobro2MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncobro2MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn4Layout = new javax.swing.GroupLayout(loginBtn4);
        loginBtn4.setLayout(loginBtn4Layout);
        loginBtn4Layout.setHorizontalGroup(
            loginBtn4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro2, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        loginBtn4Layout.setVerticalGroup(
            loginBtn4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro2, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn4, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 320, 140, 40));

        loginBtn.setBackground(new java.awt.Color(0, 134, 190));

        loginBtnTxt.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        loginBtnTxt.setForeground(new java.awt.Color(255, 255, 255));
        loginBtnTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loginBtnTxt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        loginBtnTxt.setText("Nuevo");
        loginBtnTxt.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        loginBtnTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtnLayout = new javax.swing.GroupLayout(loginBtn);
        loginBtn.setLayout(loginBtnLayout);
        loginBtnLayout.setHorizontalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginBtnTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtnLayout.setVerticalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginBtnTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(660, 210, 120, 30));

        loginBtn1.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn1Layout = new javax.swing.GroupLayout(loginBtn1);
        loginBtn1.setLayout(loginBtn1Layout);
        loginBtn1Layout.setHorizontalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn1Layout.setVerticalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(800, 210, -1, 30));

        loginBtn5.setBackground(new java.awt.Color(0, 134, 190));

        btnbien.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnbien.setForeground(new java.awt.Color(255, 255, 255));
        btnbien.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnbien.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/cheque.png"))); // NOI18N
        btnbien.setText("Realizada");
        btnbien.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnbien.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnbienMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnbienMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnbienMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn5Layout = new javax.swing.GroupLayout(loginBtn5);
        loginBtn5.setLayout(loginBtn5Layout);
        loginBtn5Layout.setHorizontalGroup(
            loginBtn5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnbien, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        loginBtn5Layout.setVerticalGroup(
            loginBtn5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnbien, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn5, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 370, 140, -1));

        loginBtn6.setBackground(new java.awt.Color(0, 134, 190));

        btnmal.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnmal.setForeground(new java.awt.Color(255, 255, 255));
        btnmal.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnmal.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/cancelar.png"))); // NOI18N
        btnmal.setText("No Realizada");
        btnmal.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnmal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnmalMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnmalMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnmalMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn6Layout = new javax.swing.GroupLayout(loginBtn6);
        loginBtn6.setLayout(loginBtn6Layout);
        loginBtn6Layout.setHorizontalGroup(
            loginBtn6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnmal, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        loginBtn6Layout.setVerticalGroup(
            loginBtn6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnmal, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn6, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 420, 140, -1));

        loginBtn7.setBackground(new java.awt.Color(0, 134, 190));

        btncorreo.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncorreo.setForeground(new java.awt.Color(255, 255, 255));
        btncorreo.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncorreo.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/correo-electronico.png"))); // NOI18N
        btncorreo.setText("Enviar Correo");
        btncorreo.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncorreo.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncorreoMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncorreoMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncorreoMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn7Layout = new javax.swing.GroupLayout(loginBtn7);
        loginBtn7.setLayout(loginBtn7Layout);
        loginBtn7Layout.setHorizontalGroup(
            loginBtn7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncorreo, javax.swing.GroupLayout.DEFAULT_SIZE, 140, Short.MAX_VALUE)
        );
        loginBtn7Layout.setVerticalGroup(
            loginBtn7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncorreo, javax.swing.GroupLayout.DEFAULT_SIZE, 40, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn7, new org.netbeans.lib.awtextra.AbsoluteConstraints(850, 470, 140, 40));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 1005, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 582, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());

        if (evt.getClickCount() == 1) {

            accion = "editar";
            btnguardar.setText("Editar");
            habilitar();
            btnbien.setEnabled(true);
            btnmal.setEnabled(true);
            btncorreo.setEnabled(true);

            id = (tablalistado.getValueAt(fila, 0).toString());

            txtnombre.setText(tablalistado.getValueAt(fila, 1).toString());
            txttelefono.setText(tablalistado.getValueAt(fila, 2).toString());
            txtcorreo.setText(tablalistado.getValueAt(fila, 3).toString());
            txtsintomas.setText(tablalistado.getValueAt(fila, 4).toString());
            dtfecha.setDate(Date.valueOf(tablalistado.getValueAt(fila, 6).toString()));
            cbogenero.setSelectedItem(tablalistado.getValueAt(fila, 7).toString());
            txthora.setSelectedItem(tablalistado.getValueAt(fila, 9).toString());
            cbominutos.setSelectedItem(tablalistado.getValueAt(fila, 10).toString());
            cbohora.setSelectedItem(tablalistado.getValueAt(fila, 11).toString());

        }

    }//GEN-LAST:event_tablalistadoMouseClicked

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        mostrar(txtbuscar.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void txtnombreMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnombreMousePressed

    }//GEN-LAST:event_txtnombreMousePressed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void btnbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarActionPerformed
        new BuscaCitas().setVisible(true);
        // TODO add your handling code here:
    }//GEN-LAST:event_btnbuscarActionPerformed

    private void txtcorreoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtcorreoMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcorreoMousePressed

    private void txtcorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcorreoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcorreoActionPerformed

    private void txtnombre2MousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnombre2MousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombre2MousePressed

    private void txtnombre2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombre2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombre2ActionPerformed

    private void btncobro2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro2MouseClicked
        new HistorialCt().setVisible(true);
    }//GEN-LAST:event_btncobro2MouseClicked

    private void btncobro2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro2MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro2MouseEntered

    private void btncobro2MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro2MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro2MouseExited

    private void loginBtnTxtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseClicked
        btnguardar.setText("Guardar");
        accion = "guardar";
        txtnombre.requestFocus();
        habilitar();
    }//GEN-LAST:event_loginBtnTxtMouseClicked

    private void loginBtnTxtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseEntered
    }//GEN-LAST:event_loginBtnTxtMouseEntered

    private void loginBtnTxtMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseExited
    }//GEN-LAST:event_loginBtnTxtMouseExited

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        if (btnguardar.isEnabled() == false) {
            return;
        }

        hora = txthora.getSelectedItem() + ":" + cbominutos.getSelectedItem() + " " + cbohora.getSelectedItem();
        guardar();         // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseExited

    private void btnbienMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbienMouseClicked
        if (btnbien.isEnabled() == false) {
            return;
        }

        hora = txthora.getSelectedItem() + ":" + cbominutos.getSelectedItem() + " " + cbohora.getSelectedItem();
        status = "Cita Realizada";
        accion = "guardar";
        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estas Seguro Que se ha Atendido el Paciente Seleccionado ?", "Confirmar", 2);
        if (confirmacion == 0) {
            GuardarHistorial();
            elimininar();
        } else {
            return;
        }        // TODO add your handling code here:
    }//GEN-LAST:event_btnbienMouseClicked

    private void btnbienMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbienMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnbienMouseEntered

    private void btnbienMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnbienMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnbienMouseExited

    private void btnmalMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnmalMouseClicked
        if (btnmal.isEnabled() == false) {
            return;
        }

        hora = txthora.getSelectedItem() + ":" + cbominutos.getSelectedItem() + " " + cbohora.getSelectedItem();
        status = "Cita No Realizada";
        accion = "guardar";
        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estas Seguro Que  No se ha Atendido el Paciente Seleccionado ?", "Confirmar", 2);
        int correo = JOptionPane.showConfirmDialog(rootPane, "Desea Enviarle un Correo al Paciente", "Confirmar", 2);

        if (confirmacion == 0) {
            if (correo == 0) {
                new Fcorreo().setVisible(true);
                Fcorreo.txtdestino.setText(txtnombre.getText());
            } else {
                return;
            }
            GuardarHistorial();
            elimininar();
        } else {
            return;
        }        // TODO add your handling code here:
    }//GEN-LAST:event_btnmalMouseClicked

    private void btnmalMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnmalMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btnmalMouseEntered

    private void btnmalMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnmalMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btnmalMouseExited

    private void btncorreoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncorreoMouseClicked
        if (btncorreo.isEnabled() == false) {
            return;
        }

        new Fcorreo().setVisible(true);
        Fcorreo.txtdestino.setText(txtcorreo.getText());
        btncorreo.setEnabled(false);        // TODO add your handling code here:
    }//GEN-LAST:event_btncorreoMouseClicked

    private void btncorreoMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncorreoMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncorreoMouseEntered

    private void btncorreoMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncorreoMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncorreoMouseExited

    private void txtnombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyTyped
     char validar1 = evt.getKeyChar();
        if (Character.isLowerCase(validar1)) {
            evt.setKeyChar(Character.toUpperCase(validar1));

        }
        char validar = evt.getKeyChar();
        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Letras");
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyTyped

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Citas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Citas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Citas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Citas.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Citas().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btnbien;
    public static javax.swing.JButton btnbuscar;
    public static javax.swing.JLabel btncobro2;
    public static javax.swing.JLabel btncorreo;
    public static javax.swing.JLabel btnguardar;
    public static javax.swing.JLabel btnmal;
    public static javax.swing.JComboBox<String> cbogenero;
    public static javax.swing.JComboBox<String> cbohora;
    public static javax.swing.JComboBox<String> cbominutos;
    public static com.toedter.calendar.JDateChooser dtfecha;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator14;
    private javax.swing.JSeparator jSeparator15;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JPanel loginBtn;
    private javax.swing.JPanel loginBtn1;
    private javax.swing.JPanel loginBtn4;
    private javax.swing.JPanel loginBtn5;
    private javax.swing.JPanel loginBtn6;
    private javax.swing.JPanel loginBtn7;
    private javax.swing.JLabel loginBtnTxt;
    private javax.swing.JTable tablalistado;
    private javax.swing.JTextField txtbuscar;
    public static javax.swing.JTextField txtcorreo;
    public static javax.swing.JComboBox<String> txthora;
    public static javax.swing.JTextField txtnombre;
    public static javax.swing.JTextField txtnombre2;
    public static javax.swing.JTextArea txtsintomas;
    public static javax.swing.JFormattedTextField txttelefono;
    private javax.swing.JLabel userLabel10;
    private javax.swing.JLabel userLabel11;
    private javax.swing.JLabel userLabel12;
    private javax.swing.JLabel userLabel13;
    private javax.swing.JLabel userLabel14;
    private javax.swing.JLabel userLabel4;
    private javax.swing.JLabel userLabel7;
    private javax.swing.JLabel userLabel8;
    private javax.swing.JLabel userLabel9;
    // End of variables declaration//GEN-END:variables
private String accion = "guardar";
    private String id;
    private String hora;
    private String status;

}
