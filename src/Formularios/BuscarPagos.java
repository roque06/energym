/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import static Formularios.pagos.btnbuscar;
import static Formularios.pagos.btncobro;
import static Formularios.pagos.btnguardar;
import static Formularios.pagos.btnservicio;
import static Formularios.pagos.codigopaciente;
import static Formularios.pagos.dtfecha;
import static Formularios.pagos.jSeparator7;
import static Formularios.pagos.lblpago;

import static Formularios.pagos.lblpagomensual;
import static Formularios.pagos.pagototal;
import static Formularios.pagos.txtconcepto;
import static Formularios.pagos.txtinicial;
import static Formularios.pagos.txtmeses;
import static Formularios.pagos.txtpago;
import Logica.FRpagos;
import Logica.conexion;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Calendar;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import static Formularios.pagos.txtnombre;
import java.awt.Color;

/**
 *
 * @author radames
 */
public class BuscarPagos extends javax.swing.JFrame {

    /**
     * Creates new form BuscarPagos
     */
    public BuscarPagos() {
        initComponents();
        this.setLocationRelativeTo(null);
//        mostrar("");
        cargartotal();
        
    }
    
    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRpagos func = new FRpagos();
            modelo = func.mostrar(buscar);
            setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
            
            tablalistado.setModel(modelo);

//            lbltotalregistro.setText("Total Registrados: " + Integer.toString(func.totalregistro));
            ocultar_columnas();
//            total();
//            btneliminar.setEnabled(false);
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }
    
    void cargartotal() {
        DecimalFormat formateador = new DecimalFormat("#####.##");
        String total = "";
        
        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT SUM(montomes) as total from tbrecibo where codigopaciente='" + codigopaciente + "'");
            rq2.next();
            total = rq2.getString("total");
            
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
        
        String secuencia = (total);
        secuencia = secuencia;
        
        pagototales = (secuencia);

//        txtprueba.setText(formateador.format(secuencia));
    }
    
    void ocultar_columnas() {
        
        tablalistado.getColumnModel().getColumn(1).setMaxWidth(100);
        tablalistado.getColumnModel().getColumn(1).setMinWidth(100);
        tablalistado.getColumnModel().getColumn(1).setPreferredWidth(100);
        
        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);
        
        tablalistado.getColumnModel().getColumn(3).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(3).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(3).setPreferredWidth(0);
        
        tablalistado.getColumnModel().getColumn(4).setMaxWidth(200);
        tablalistado.getColumnModel().getColumn(4).setMinWidth(200);
        tablalistado.getColumnModel().getColumn(4).setPreferredWidth(200);
        
        tablalistado.getColumnModel().getColumn(5).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(5).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(5).setPreferredWidth(0);
        
        tablalistado.getColumnModel().getColumn(6).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(6).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(6).setPreferredWidth(0);
        
        tablalistado.getColumnModel().getColumn(7).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(7).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(7).setPreferredWidth(0);
        
        tablalistado.getColumnModel().getColumn(11).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(11).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(11).setPreferredWidth(0);
        
        tablalistado.getColumnModel().getColumn(14).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(14).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(14).setPreferredWidth(0);
        
    }
    
    void Servicio() {
        String servicio = "";
        String precio = "";
        
        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT tiposeguro from tbficha where nombre='" + txtnombre.getText() + "'");
            
            rq2.next();
            servicio = rq2.getString("tiposeguro");
            seguro = servicio;
            
        } catch (Exception e) {
        }
        
    }
    
    private String nombre;
    private String seguro;


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        jButton1 = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        txtbuscar = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "PAGOS", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablalistado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null},
                {null, null, null, null, null, null, null, null}
            },
            new String [] {
                "No.Registro", "Registro", "Nombre", "Monto Inicial", "No.Cuotas", "Fecha de Pagos", "Monto Mensual", "CodPaciente"
            }
        ) {
            boolean[] canEdit = new boolean [] {
                false, false, false, false, false, false, false, false
            };

            public boolean isCellEditable(int rowIndex, int columnIndex) {
                return canEdit [columnIndex];
            }
        });
        tablalistado.setGridColor(new java.awt.Color(255, 255, 255));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablalistado);
        if (tablalistado.getColumnModel().getColumnCount() > 0) {
            tablalistado.getColumnModel().getColumn(0).setResizable(false);
            tablalistado.getColumnModel().getColumn(1).setResizable(false);
            tablalistado.getColumnModel().getColumn(2).setResizable(false);
            tablalistado.getColumnModel().getColumn(3).setResizable(false);
            tablalistado.getColumnModel().getColumn(4).setResizable(false);
            tablalistado.getColumnModel().getColumn(5).setResizable(false);
            tablalistado.getColumnModel().getColumn(6).setResizable(false);
            tablalistado.getColumnModel().getColumn(7).setResizable(false);
        }

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 70, 918, -1));

        jButton1.setBackground(new java.awt.Color(0, 134, 190));
        jButton1.setForeground(new java.awt.Color(0, 134, 190));
        jButton1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });
        jPanel1.add(jButton1, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 30, 45, -1));

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel2.setText("Buscar:");
        jPanel1.add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 30, -1, 30));

        txtbuscar.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtbuscar.setBorder(null);
        txtbuscar.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtbuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtbuscarMousePressed(evt);
            }
        });
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
        });
        jPanel1.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 30, 280, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(90, 60, 280, 20));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 963, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 547, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());
        
        if (evt.getClickCount() == 1) {
            String valor;
            Date valor1;
            
            valor = tablalistado.getValueAt(fila, 1).toString();
            pagos.codigo = (valor);
            pagos.txtcod.setText(valor);
            
            valor = tablalistado.getValueAt(fila, 2).toString();
            pagos.lblfecha.setText(valor);
            
            valor = tablalistado.getValueAt(fila, 4).toString();
            pagos.txtnombre.setText(valor);
            nombre = (valor);
            Servicio();
            pagos.lblseguro.setText(seguro);
            
            valor = tablalistado.getValueAt(fila, 5).toString();
            pagos.txtconcepto.setText(valor);
            
            valor = tablalistado.getValueAt(fila, 6).toString();
            pagos.txtcorreo.setText(valor);
            
            valor = tablalistado.getValueAt(fila, 7).toString();
            pagos.abono = (valor);
            
            valor = tablalistado.getValueAt(fila, 8).toString();
            pagos.txtinicial.setText(valor);
            
            valor = tablalistado.getValueAt(fila, 9).toString();
            pagos.txtmeses.setText(valor);
            pagos.mes = (valor);
            
            valor1 = (Date.valueOf(tablalistado.getValueAt(fila, 11).toString()));
            pagos.dtfecha.setDate(valor1);
            
            valor = tablalistado.getValueAt(fila, 12).toString();
            pagos.txtmensual.setText(valor);

//             valor = tablalistado.getValueAt(fila, 12).toString();
//            pagos.meses=(valor); 
            valor = tablalistado.getValueAt(fila, 13).toString();
            pagos.codigopaciente = (valor);
            
            valor = tablalistado.getValueAt(fila, 14).toString();
            pagos.nocuotas = (valor);
            cargartotal();
            pagos.lblpagomensual.setText(pagototales);
            
            pagos.txtnombre.setForeground(Color.black);
            pagos.txtcorreo.setForeground(Color.black);
            txtconcepto.setForeground(Color.black);
            dtfecha.setForeground(Color.black);
        }
        dtfecha.setEnabled(false);
        txtnombre.setEditable(false);
        txtconcepto.setEditable(false);
        txtconcepto.setEditable(false);
        txtmeses.setEditable(false);
        pagos.txtdeduciones.setEditable(false);
        
        txtmeses.setEnabled(true);
        pagos.txtdeduciones.setEnabled(true);
        pagos.txtseguro.setEnabled(false);
        
        btnguardar.setEnabled(true);
        btncobro.setEnabled(true);
        btnguardar.setEnabled(false);
        btnbuscar.setEnabled(false);
        pagos.btncorreo.setEnabled(true);
        pagos.txtnombre.setEnabled(true);
        txtpago.setVisible(true);
        lblpago.setVisible(true);
        jSeparator7.setVisible(true);
        txtpago.requestFocus();
        
        this.dispose();

        // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoMouseClicked

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
        if (txtbuscar.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debe introducir un nombre o codigo del paciente");
            txtbuscar.requestFocus();
            return;
        }
        
        mostrar(txtbuscar.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_jButton1ActionPerformed

    private void txtbuscarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtbuscarMousePressed

    }//GEN-LAST:event_txtbuscarMousePressed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            mostrar(txtbuscar.getText());
            
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyPressed
    conexion cc = new conexion();
    Connection cn = cc.conectar();
    private String pagototales;

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                    
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(BuscarPagos.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
            
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(BuscarPagos.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
            
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(BuscarPagos.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
            
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(BuscarPagos.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new BuscarPagos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton jButton1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JTable tablalistado;
    public static javax.swing.JTextField txtbuscar;
    // End of variables declaration//GEN-END:variables

    private String totalmes;
    private int totalmesresta;
}
