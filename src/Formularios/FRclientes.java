/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Clases.FRCliente;
import Clases.FRfactura;
import Clases.conexion;
import Datos.dtcliente;
import Datos.dtfactura;
import Datos.dtoUsuario;
import java.awt.event.KeyEvent;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.view.JasperViewer;

public class FRclientes extends javax.swing.JFrame {

    dtoUsuario dts = new dtoUsuario();

    public FRclientes() {
        initComponents();
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/KILO.png")).getImage());
        dtregistro1.setCalendar(c1);
        dtregistro.setCalendar(c1);
        btncobrar.setEnabled(false);
        No_Factura();
        Codigo();
        txtservicio.setText(descripcion);
        txtservicio.setEditable(false);

    }

    conexion cc = new conexion();
    Connection cn = cc.conectar();

    Calendar c1 = new GregorianCalendar();

    void Codigo() {
        String idpaciente = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT idcodigo from tbcodigo");

            rq2.next();
            idpaciente = rq2.getString("idcodigo");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        lblcodigo.setText((secuencia));

    }

    void No_Factura() {
        String idpaciente = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT codfactura from factura");

            rq2.next();
            idpaciente = rq2.getString("codfactura");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        factura = ((secuencia));
        jTextField1.setText(secuencia);

    }

    public static void habilitar() {
        txtnombre.setEnabled(true);
        txttelefono.setEnabled(true);
        txtemail.setEnabled(true);
        txtcedula.setEnabled(true);
        txtdireccion.setEnabled(true);
        dtregistro.setEnabled(true);
        dtpago.setEnabled(true);
        txtmensualidad.setEnabled(true);
        txtmeses.setEnabled(true);
        txtmonto.setEnabled(true);

        txtnombre.setText("");
        txttelefono.setText("");
        txtemail.setText("");
        txttelefono.setText("");
        txtcedula.setText("");
        txtdireccion.setText("");
        dtpago.setDate(null);
        txtmensualidad.setText("");
        txtmeses.setText("");
        txtmonto.setText("");

    }

    void inhabilitar() {
        txtnombre.setEnabled(false);
        txttelefono.setEnabled(false);
        txtemail.setEnabled(false);
        txtcedula.setEnabled(false);
        txtdireccion.setEnabled(false);
        dtregistro.setEnabled(false);
        dtpago.setEnabled(false);
        txtmensualidad.setEnabled(false);
        txtmeses.setEnabled(false);
        txtmonto.setEnabled(false);

        txtnombre.setText("");
        txttelefono.setText("");
        txtemail.setText("");
        txttelefono.setText("");
        txtcedula.setText("");
        txtdireccion.setText("");
        dtpago.setDate(null);
        txtmensualidad.setText("");
        txtmeses.setText("");
        txtmonto.setText("");

    }

    void guardar() {

        if (lblcodigo.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBE INGRESAR UN CODIGO DE CLIENTE");
            lblcodigo.requestFocus();
            return;
        }

        if (txtnombre.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBE INGRESAR UN NOMBRE DE CLIENTE");
            txtnombre.requestFocus();
            return;
        }

        if (txtemail.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBE INGRESAR EL EMAIL DEL CLIENTE");
            txtemail.requestFocus();
            return;
        }

        if (txtdireccion.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBE INGRESAR LA DIRECCION  DEL CLIENTE");
            txtdireccion.requestFocus();
            return;
        }

        if (txtmensualidad.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBE INGRESAR EL MONTO MENSUAL DEL CLIENTE");
            txtmensualidad.requestFocus();
            return;
        }

        dtcliente dts = new dtcliente();
        FRCliente func = new FRCliente();

        dts.setCodigo(Integer.parseInt(lblcodigo.getText()));
        dts.setNombre(txtnombre.getText());
        dts.setTelefono(txttelefono.getText());
        dts.setEmail(txtemail.getText());
        dts.setCedula(txtcedula.getText());
        dts.setDireccion(txtdireccion.getText());

        Calendar cal;
        int d, m, a;
        cal = dtregistro.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setRegistro(new Date(a, m, d));

        cal = dtpago.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setPagos(new Date(a, m, d));

        dts.setMensualidad(txtmensualidad.getText());
        dts.setMeses(txtmeses.getText());
        dts.setMonto(txtmonto.getText());

        if (rdactivo.isSelected()) {
            estado = "Activo";
            dts.setEstado(estado);
        } else if (rdinactivo.isSelected()) {
            estado = "Inactivo";
            dts.setEstado(estado);

        }

        if (accion.equals("guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(null, "Cliente Registrado");
                this.dispose();
                VistaCliente.mostrar("");
            }
        } else if (accion.equals("editar")) {
            dts.setId(Integer.parseInt(id));
            if (func.editar(dts)) {
                JOptionPane.showMessageDialog(null, "Cliente Modificado");
                this.dispose();
                VistaCliente.mostrar("");
            }

        }
        inhabilitar();
        ActualizarCodigo();

    }

    void ActualizarCodigo() {

        try {
            PreparedStatement psU = cn.prepareStatement("Update tbcodigo set idcodigo= idcodigo+1");
            psU.executeUpdate();

        } catch (Exception e) {

            JOptionPane.showConfirmDialog(rootPane, e);

        }
        Codigo();

    }

    void ActualizarFactura() {

        try {
            PreparedStatement psU = cn.prepareStatement("Update factura set codfactura= codfactura+1");
            psU.executeUpdate();

        } catch (Exception e) {

            JOptionPane.showConfirmDialog(rootPane, e);

        }
        Codigo();

    }

    void convertirFecha() {
        String dia = Integer.toString(dtpago.getCalendar().get(Calendar.DAY_OF_MONTH));
        String mes = Integer.toString(dtpago.getCalendar().get(Calendar.MONTH) + 1);
        String year = Integer.toString(dtpago.getCalendar().get(Calendar.YEAR));
        String fecha = (year + "-" + mes + "-" + dia);
        fechapago = (fecha);
    }

    void SumarFecha() {
        String idpaciente = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT DATE_ADD('" + fechapago + "',INTERVAL'" + txtmeses.getText() + "'MONTH)AS SumaFecha");

            rq2.next();
            idpaciente = rq2.getString("SumaFecha");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        pagomensual = ((secuencia));

    }

    void ActualizarFecha() {

        try {
            PreparedStatement psU = cn.prepareStatement("Update tbcliente set pagos='" + pagomensual + "'WHERE codigo='" + lblcodigo.getText() + "'");
            psU.executeUpdate();

        } catch (Exception e) {

            JOptionPane.showConfirmDialog(rootPane, e);

        }
        Codigo();

    }

    void GuardarFactura() {
        dtfactura dts = new dtfactura();
        FRfactura func = new FRfactura();

        dts.setNofactura(jTextField1.getText());
        dts.setCliente(txtnombre.getText());
        dts.setCodigocli(codigocli);
        Calendar cal;
        int d, m, a;
        cal = dtregistro1.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha(new Date(a, m, d));
        dts.setCantidadmes(Integer.parseInt(txtmeses.getText()));
        dts.setDescripcion(txtservicio.getText());
        dts.setMonto(monto1);
        dts.setEmpleado(empleado);
        dts.setPrecio(txtmonto.getText());

        if (func.insertar(dts)) {
        }

    }

    void imprimir() {

        try {
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile("src/Reportes/Factura.jasper");
            HashMap parametro = new HashMap();
            parametro.put("nofactura", jTextField1.getText());

            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
            JasperPrintManager.printReport(jp, false);
            JasperPrintManager.printReport(jp, false);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void Eliminar() {

        dtcliente dts = new dtcliente();
        FRCliente func = new FRCliente();

        dts.setId(Integer.parseInt(id));
        
        accion="guardar";

        if (func.eliminar(dts)) {
            JOptionPane.showMessageDialog(null, "Cliente Eliminado!");
        }

    }
    public static String accion = "guardar";
    public static String id;
    private String estado;
    private String fechapago;
    private String pagomensual;
    private String factura;
    private String descripcion = "Mensualidad";
    private String empleado = dts.getNombre();
    public static String codigocli;
    private String monto1;
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        buttonGroup1 = new javax.swing.ButtonGroup();
        btnuevo1 = new javax.swing.JPanel();
        btncobro4 = new javax.swing.JLabel();
        dtregistro1 = new com.toedter.calendar.JDateChooser();
        jTextField1 = new javax.swing.JTextField();
        buttonGroup2 = new javax.swing.ButtonGroup();
        jPanel1 = new javax.swing.JPanel();
        txtnombre = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        lblnombre = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        txtemail = new javax.swing.JTextField();
        jSeparator3 = new javax.swing.JSeparator();
        txtcedula = new javax.swing.JFormattedTextField();
        jLabel5 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        jLabel6 = new javax.swing.JLabel();
        txtmensualidad = new javax.swing.JTextField();
        jSeparator5 = new javax.swing.JSeparator();
        jLabel7 = new javax.swing.JLabel();
        dtregistro = new com.toedter.calendar.JDateChooser();
        jLabel8 = new javax.swing.JLabel();
        dtpago = new com.toedter.calendar.JDateChooser();
        txttelefono = new javax.swing.JFormattedTextField();
        lblmeses = new javax.swing.JLabel();
        txtmeses = new javax.swing.JTextField();
        jsmeses = new javax.swing.JSeparator();
        txtmonto = new javax.swing.JTextField();
        jsmonto = new javax.swing.JSeparator();
        lbltol = new javax.swing.JLabel();
        btnguardar2 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        jLabel11 = new javax.swing.JLabel();
        lblpago = new javax.swing.JLabel();
        lbltotal = new javax.swing.JLabel();
        jLabel14 = new javax.swing.JLabel();
        txtdireccion = new javax.swing.JTextField();
        jSeparator9 = new javax.swing.JSeparator();
        rdactivo = new javax.swing.JRadioButton();
        rdinactivo = new javax.swing.JRadioButton();
        jLabel9 = new javax.swing.JLabel();
        btncobrar = new javax.swing.JButton();
        lblcodigo = new javax.swing.JTextField();
        jSeparator6 = new javax.swing.JSeparator();
        lblmeses1 = new javax.swing.JLabel();
        txtservicio = new javax.swing.JTextField();
        jsmeses1 = new javax.swing.JSeparator();
        loginBtn9 = new javax.swing.JPanel();
        btneliminar = new javax.swing.JLabel();
        btnbuscar = new javax.swing.JButton();

        btnuevo1.setBackground(new java.awt.Color(0, 134, 190));

        btncobro4.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncobro4.setForeground(new java.awt.Color(255, 255, 255));
        btncobro4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncobro4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        btncobro4.setText("Nuevo");
        btncobro4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncobro4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncobro4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncobro4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncobro4MouseExited(evt);
            }
        });

        javax.swing.GroupLayout btnuevo1Layout = new javax.swing.GroupLayout(btnuevo1);
        btnuevo1.setLayout(btnuevo1Layout);
        btnuevo1Layout.setHorizontalGroup(
            btnuevo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnuevo1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btncobro4, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        btnuevo1Layout.setVerticalGroup(
            btnuevo1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnuevo1Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btncobro4, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jTextField1.setText("jTextField1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("EnerGym");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtnombre.setBackground(new java.awt.Color(204, 255, 204));
        txtnombre.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtnombre.setBorder(null);
        txtnombre.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtnombreKeyTyped(evt);
            }
        });
        jPanel1.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 90, 260, 20));

        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 80, 260, 10));

        lblnombre.setFont(new java.awt.Font("Tahoma", 1, 18)); // NOI18N
        lblnombre.setText("Nombre:");
        jPanel1.add(lblnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(330, 20, 210, -1));

        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator2, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 150, 260, 10));

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel3.setText("Telefono:");
        jPanel1.add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 130, 70, -1));

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Email:");
        jPanel1.add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 170, 50, -1));

        txtemail.setBackground(new java.awt.Color(204, 255, 204));
        txtemail.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtemail.setBorder(null);
        txtemail.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtemailActionPerformed(evt);
            }
        });
        jPanel1.add(txtemail, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 170, 260, 20));

        jSeparator3.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator3, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 190, 260, 10));

        txtcedula.setBackground(new java.awt.Color(204, 255, 204));
        txtcedula.setBorder(null);
        try {
            txtcedula.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("###-#######-#")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txtcedula, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 210, 260, 20));

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Cedula:");
        jPanel1.add(jLabel5, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 210, 60, -1));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 230, 260, 10));

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel6.setText("Registro:");
        jPanel1.add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 300, 70, -1));

        txtmensualidad.setBackground(new java.awt.Color(204, 255, 204));
        txtmensualidad.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtmensualidad.setBorder(null);
        txtmensualidad.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtmensualidadActionPerformed(evt);
            }
        });
        jPanel1.add(txtmensualidad, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 380, 270, 20));

        jSeparator5.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator5, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 400, 260, 10));

        jLabel7.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel7.setText("Direccion:");
        jPanel1.add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 250, 70, -1));

        dtregistro.setBackground(new java.awt.Color(204, 255, 204));
        jPanel1.add(dtregistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 290, 150, 30));

        jLabel8.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel8.setText("Mensualidad:");
        jPanel1.add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 380, 100, -1));

        dtpago.setBackground(new java.awt.Color(204, 255, 204));
        jPanel1.add(dtpago, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 330, 150, 30));

        txttelefono.setBackground(new java.awt.Color(204, 255, 204));
        txttelefono.setBorder(null);
        try {
            txttelefono.setFormatterFactory(new javax.swing.text.DefaultFormatterFactory(new javax.swing.text.MaskFormatter("(###)-###-####")));
        } catch (java.text.ParseException ex) {
            ex.printStackTrace();
        }
        jPanel1.add(txttelefono, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 130, 260, 20));

        lblmeses.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblmeses.setText("Servicio:");
        jPanel1.add(lblmeses, new org.netbeans.lib.awtextra.AbsoluteConstraints(490, 70, 60, -1));

        txtmeses.setBackground(new java.awt.Color(204, 255, 204));
        txtmeses.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtmeses.setBorder(null);
        txtmeses.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmesesKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtmesesKeyTyped(evt);
            }
        });
        jPanel1.add(txtmeses, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 100, 100, 20));

        jsmeses.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jsmeses, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 90, 150, 10));

        txtmonto.setBackground(new java.awt.Color(204, 255, 204));
        txtmonto.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtmonto.setBorder(null);
        txtmonto.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtmontoKeyPressed(evt);
            }
        });
        jPanel1.add(txtmonto, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 130, 100, 20));

        jsmonto.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jsmonto, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 150, 100, 10));

        lbltol.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        lbltol.setForeground(new java.awt.Color(0, 204, 51));
        lbltol.setText("0.00");
        jPanel1.add(lbltol, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 170, 130, 20));

        btnguardar2.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout btnguardar2Layout = new javax.swing.GroupLayout(btnguardar2);
        btnguardar2.setLayout(btnguardar2Layout);
        btnguardar2Layout.setHorizontalGroup(
            btnguardar2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnguardar2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnguardar, javax.swing.GroupLayout.PREFERRED_SIZE, 130, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        btnguardar2Layout.setVerticalGroup(
            btnguardar2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, btnguardar2Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btnguardar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(btnguardar2, new org.netbeans.lib.awtextra.AbsoluteConstraints(130, 470, 130, 30));

        jLabel11.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel11.setText("CODIGO:");
        jPanel1.add(jLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 60, 60, -1));

        lblpago.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblpago.setText("Monto A Pagar:");
        jPanel1.add(lblpago, new org.netbeans.lib.awtextra.AbsoluteConstraints(450, 130, -1, -1));

        lbltotal.setFont(new java.awt.Font("Tahoma", 1, 24)); // NOI18N
        lbltotal.setText("TOTAL:");
        jPanel1.add(lbltotal, new org.netbeans.lib.awtextra.AbsoluteConstraints(470, 170, 110, 20));

        jLabel14.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel14.setText("Fecha Pagos:");
        jPanel1.add(jLabel14, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 340, 100, -1));

        txtdireccion.setBackground(new java.awt.Color(204, 255, 204));
        txtdireccion.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        txtdireccion.setBorder(null);
        txtdireccion.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdireccionActionPerformed(evt);
            }
        });
        jPanel1.add(txtdireccion, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 250, 260, 20));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 270, 260, 10));

        buttonGroup1.add(rdactivo);
        rdactivo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        rdactivo.setText("Activo");
        jPanel1.add(rdactivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 420, -1, -1));

        buttonGroup1.add(rdinactivo);
        rdinactivo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        rdinactivo.setText("Inactivo");
        jPanel1.add(rdinactivo, new org.netbeans.lib.awtextra.AbsoluteConstraints(190, 420, -1, -1));

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("Nombre:");
        jPanel1.add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 90, 70, -1));

        btncobrar.setBackground(new java.awt.Color(0, 134, 190));
        btncobrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncobrar.setText("COBRAR");
        btncobrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncobrarActionPerformed(evt);
            }
        });
        jPanel1.add(btncobrar, new org.netbeans.lib.awtextra.AbsoluteConstraints(560, 210, -1, 30));

        lblcodigo.setBackground(new java.awt.Color(204, 255, 204));
        lblcodigo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        lblcodigo.setBorder(null);
        lblcodigo.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                lblcodigoKeyTyped(evt);
            }
        });
        jPanel1.add(lblcodigo, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 260, 20));

        jSeparator6.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator6, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 110, 260, 10));

        lblmeses1.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        lblmeses1.setText("Meses:");
        jPanel1.add(lblmeses1, new org.netbeans.lib.awtextra.AbsoluteConstraints(500, 100, 50, -1));

        txtservicio.setBackground(new java.awt.Color(204, 255, 204));
        txtservicio.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        txtservicio.setBorder(null);
        txtservicio.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtservicioKeyPressed(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtservicioKeyTyped(evt);
            }
        });
        jPanel1.add(txtservicio, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 60, 170, 30));

        jsmeses1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jsmeses1, new org.netbeans.lib.awtextra.AbsoluteConstraints(570, 120, 100, 10));

        loginBtn9.setBackground(new java.awt.Color(0, 134, 190));

        btneliminar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btneliminar.setForeground(new java.awt.Color(255, 255, 255));
        btneliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btneliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/boton-eliminar.png"))); // NOI18N
        btneliminar.setText("Eliminar");
        btneliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btneliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneliminarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn9Layout = new javax.swing.GroupLayout(loginBtn9);
        loginBtn9.setLayout(loginBtn9Layout);
        loginBtn9Layout.setHorizontalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btneliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        loginBtn9Layout.setVerticalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btneliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(loginBtn9, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 470, -1, 30));

        btnbuscar.setBackground(new java.awt.Color(0, 134, 190));
        btnbuscar.setForeground(new java.awt.Color(0, 134, 190));
        btnbuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        btnbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarActionPerformed(evt);
            }
        });
        jPanel1.add(btnbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(750, 60, 40, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 839, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 536, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtnombreKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtnombreKeyTyped
        char validar = evt.getKeyChar();
        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Letras");
        }
        char validar1 = evt.getKeyChar();
        if (Character.isLowerCase(validar1)) {
            evt.setKeyChar(Character.toUpperCase(validar1));

        }     // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreKeyTyped

    private void txtemailActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtemailActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtemailActionPerformed

    private void txtmensualidadActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtmensualidadActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtmensualidadActionPerformed

    private void txtmesesKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmesesKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {
            txtmonto.requestFocus();
        }
    }//GEN-LAST:event_txtmesesKeyPressed

    private void txtmesesKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmesesKeyTyped
        char validar = evt.getKeyChar();
        if (Character.isLetter(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Numeros");
        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtmesesKeyTyped

    private void txtmontoKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtmontoKeyPressed
        if (evt.getKeyCode() == KeyEvent.VK_ENTER) {

            DecimalFormat formateador = new DecimalFormat("###,###,###.00");
            DecimalFormat formateador1 = new DecimalFormat("#########.00");

            double mes = Double.parseDouble(txtmeses.getText());
            double monto = Double.parseDouble(txtmonto.getText());

            Double total = mes * monto;
            lbltol.setText(formateador.format(total));
            monto1 = formateador1.format(total);
            btncobrar.setEnabled(true);

        }        // TODO add your handling code here:
    }//GEN-LAST:event_txtmontoKeyPressed

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        guardar();
    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered

    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited

    }//GEN-LAST:event_btnguardarMouseExited

    private void txtdireccionActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdireccionActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdireccionActionPerformed

    private void btncobrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncobrarActionPerformed
        if (txtmeses.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debe Ingresar La Cantidad De Meses A Pagar");
            txtmeses.requestFocus();
            return;

        }

        if (txtmonto.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debe Ingresar un Monto");
            txtmonto.requestFocus();
            return;

        }

        if (txtservicio.getText().equalsIgnoreCase("MENSUALIDAD")) {
            convertirFecha();
            SumarFecha();
            ActualizarFecha();

        }

        if (txtservicio.getText().equalsIgnoreCase("INSCRIPCION")) {
            convertirFecha();
            SumarFecha();
            ActualizarFecha();

        }

        if (txtservicio.getText().equalsIgnoreCase("REINSCRIPCION")) {
            convertirFecha();
            SumarFecha();
            ActualizarFecha();

        }

        if (txtservicio.getText().equalsIgnoreCase("PERSONALIZADO")) {
            new Personalizado().setVisible(true);
            Personalizado.nombre = txtnombre.getText();
            Personalizado.codigocli = codigocli;
            Personalizado.dtregistro.setDate(dtregistro1.getDate());
            Personalizado.mes = txtmeses.getText();
            Personalizado.servicio = txtservicio.getText();
            Personalizado.monto = txtmonto.getText();
            Personalizado.empleado = empleado;
            Personalizado.precio = txtmonto.getText();
            this.dispose();

            return;

        }

        No_Factura();
        GuardarFactura();
        ActualizarFactura();
        imprimir();
//        imprimir();
        VistaCliente.txtcodigo.setText("");
        VistaCliente.txtnombre.setText("");
        VistaCliente.mostrar("");
        this.dispose();
    }//GEN-LAST:event_btncobrarActionPerformed

    private void lblcodigoKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_lblcodigoKeyTyped
        char c = evt.getKeyChar();
        if (c < '0' || c > '9') {
            evt.consume();
        }        // TODO add your handling code here:
    }//GEN-LAST:event_lblcodigoKeyTyped

    private void btncobro4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseClicked
        habilitar();
        txtnombre.requestFocus();
        accion = "guardar";
    }//GEN-LAST:event_btncobro4MouseClicked

    private void btncobro4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro4MouseEntered

    private void btncobro4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro4MouseExited

    private void txtservicioKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtservicioKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtservicioKeyPressed

    private void txtservicioKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtservicioKeyTyped
        // TODO add your handling code here:
    }//GEN-LAST:event_txtservicioKeyTyped

    private void btneliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseClicked
        if (btneliminar.isEnabled() == false) {
            return;
        }
        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estás seguro de Eliminar El Cliente?", "Confirmar", 2);

        if (confirmacion == 0) {

            Eliminar();
            VistaCliente.mostrar("");
            VistaCliente.txtcodigo.setText("");
            VistaCliente.txtnombre.setText("");

            this.dispose();
        }

    }//GEN-LAST:event_btneliminarMouseClicked

    private void btneliminarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseEntered

    private void btneliminarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseExited

    private void btnbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarActionPerformed

        new VistaServicio().setVisible(true);

// TODO add your handling code here:
    }//GEN-LAST:event_btnbuscarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FRclientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FRclientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FRclientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FRclientes.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FRclientes().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnbuscar;
    public static javax.swing.JButton btncobrar;
    public static javax.swing.JLabel btncobro4;
    public static javax.swing.JLabel btneliminar;
    public static javax.swing.JLabel btnguardar;
    public static javax.swing.JPanel btnguardar2;
    public static javax.swing.JPanel btnuevo1;
    private javax.swing.ButtonGroup buttonGroup1;
    private javax.swing.ButtonGroup buttonGroup2;
    public static com.toedter.calendar.JDateChooser dtpago;
    public static com.toedter.calendar.JDateChooser dtregistro;
    public static com.toedter.calendar.JDateChooser dtregistro1;
    private javax.swing.JLabel jLabel11;
    private javax.swing.JLabel jLabel14;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    private javax.swing.JSeparator jSeparator3;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator5;
    private javax.swing.JSeparator jSeparator6;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JTextField jTextField1;
    public static javax.swing.JSeparator jsmeses;
    public static javax.swing.JSeparator jsmeses1;
    public static javax.swing.JSeparator jsmonto;
    public static javax.swing.JTextField lblcodigo;
    public static javax.swing.JLabel lblmeses;
    public static javax.swing.JLabel lblmeses1;
    public static javax.swing.JLabel lblnombre;
    public static javax.swing.JLabel lblpago;
    public static javax.swing.JLabel lbltol;
    public static javax.swing.JLabel lbltotal;
    public static javax.swing.JPanel loginBtn9;
    public static javax.swing.JRadioButton rdactivo;
    public static javax.swing.JRadioButton rdinactivo;
    public static javax.swing.JFormattedTextField txtcedula;
    public static javax.swing.JTextField txtdireccion;
    public static javax.swing.JTextField txtemail;
    public static javax.swing.JTextField txtmensualidad;
    public static javax.swing.JTextField txtmeses;
    public static javax.swing.JTextField txtmonto;
    public static javax.swing.JTextField txtnombre;
    public static javax.swing.JTextField txtservicio;
    public static javax.swing.JFormattedTextField txttelefono;
    // End of variables declaration//GEN-END:variables
}
