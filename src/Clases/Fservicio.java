package Clases;

import Datos.FconfCorreo;
import Datos.dtservicio;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Fservicio {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "SERVICIOS"};

        String[] registro = new String[8];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From servicio  where concat (nombre) like '%" + buscar + "%' order by id asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("nombre");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(dtservicio dts) {
        sSQL = "insert into servicio (id,nombre)"
                + "VALUES(?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getId());
            pst.setString(2, dts.getNombre());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(dtservicio dts) {
        sSQL = "UPDATE servicio set nombre=?"
                + "where id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getNombre());
            pst.setInt(2, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }
//
//    public boolean eliminar(Fregimen dts) {
//        sSQL = "DELETE FROM correo WHERE idcorreo=?";
//        try {
//            PreparedStatement pst = cn.prepareStatement(sSQL);
//            pst.setInt(1, dts.getId());
//
//            int n = pst.executeUpdate();
//
//            if (n != 0) {
//                return true;
//            } else {
//                return false;
//            }
//
//        } catch (Exception e) {
//            JOptionPane.showConfirmDialog(null, e);
//        }
//        return false;
//
//    }

}
