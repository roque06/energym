/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Clases.FRfactura;
import Clases.conexion;
import Datos.dtfactura;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.HashMap;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperPrintManager;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.util.JRLoader;

/**
 *
 * @author roque
 */
public class Personalizado extends javax.swing.JFrame {

    /**
     * Creates new form Personalizado
     */
    public Personalizado() {
        initComponents();
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/KILO.png")).getImage());
    }
    conexion cc = new conexion();
    Connection cn = cc.conectar();

    void No_Factura() {
        String idpaciente = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT codfactura from factura");

            rq2.next();
            idpaciente = rq2.getString("codfactura");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        nofactura = ((secuencia));

    }

    void GuardarFactura() {
        dtfactura dts = new dtfactura();
        FRfactura func = new FRfactura();

        dts.setNofactura(nofactura);
        dts.setCliente(nombre);
        dts.setCodigocli(codigocli);
        Calendar cal;
        int d, m, a;
        cal = dtregistro.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha(new Date(a, m, d));
        dts.setCantidadmes(Integer.parseInt(mes));
        dts.setDescripcion(servicio);
        dts.setMonto(montoreal);
        dts.setEmpleado(empleado);
        dts.setPrecio(montoreal);

        if (func.insertar(dts)) {
        }

    }

    void MontoReal() {
        DecimalFormat formateador1 = new DecimalFormat("#########.00");

        double monto1 = Double.parseDouble(monto);

        double total = monto1 * 0.20;
        montoreal = formateador1.format(total);

    }

    void ActualizarFactura() {

        try {
            PreparedStatement psU = cn.prepareStatement("Update factura set codfactura= codfactura+1");
            psU.executeUpdate();

        } catch (Exception e) {

            JOptionPane.showConfirmDialog(rootPane, e);

        }

    }

    void imprimir() {

        try {
            JasperReport jr = (JasperReport) JRLoader.loadObjectFromFile("src/Reportes/FCPersonalizado.jasper");
            HashMap parametro = new HashMap();
            parametro.put("nofactura", nofactura);
            parametro.put("monto", Double.parseDouble(monto));
            parametro.put("entrenador", txtentranador.getText());

            JasperPrint jp = JasperFillManager.fillReport(jr, parametro, cn);
            JasperPrintManager.printReport(jp, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String nombre;
    public static String codigocli;
    public static String mes;
    public static String servicio;
    public static String monto;
    public static String empleado;
    public static String precio;
    public static String montoreal;
    private String nofactura;
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dtregistro = new com.toedter.calendar.JDateChooser();
        jPanel1 = new javax.swing.JPanel();
        jLabel9 = new javax.swing.JLabel();
        txtentranador = new javax.swing.JTextField();
        btncobrar = new javax.swing.JButton();

        dtregistro.setBackground(new java.awt.Color(204, 255, 204));

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("EnerGym");
        setBackground(new java.awt.Color(204, 204, 255));

        jPanel1.setBackground(new java.awt.Color(204, 204, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "PERSONALIZADO", javax.swing.border.TitledBorder.CENTER, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 18))); // NOI18N

        jLabel9.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel9.setText("NOMBRE DEL ENTRENADOR:");

        txtentranador.setBackground(new java.awt.Color(204, 255, 204));
        txtentranador.setFont(new java.awt.Font("Tahoma", 0, 16)); // NOI18N
        txtentranador.setBorder(null);
        txtentranador.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtentranadorKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                txtentranadorKeyTyped(evt);
            }
        });

        btncobrar.setBackground(new java.awt.Color(0, 134, 190));
        btncobrar.setFont(new java.awt.Font("Tahoma", 1, 11)); // NOI18N
        btncobrar.setText("COBRAR");
        btncobrar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncobrarActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(27, 27, 27)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel9)
                    .addComponent(txtentranador, javax.swing.GroupLayout.PREFERRED_SIZE, 315, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap(46, Short.MAX_VALUE))
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGap(126, 126, 126)
                .addComponent(btncobrar)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel9, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(txtentranador, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(btncobrar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(21, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtentranadorKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtentranadorKeyReleased
        // TODO add your handling code here:
    }//GEN-LAST:event_txtentranadorKeyReleased

    private void txtentranadorKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtentranadorKeyTyped
        char validar = evt.getKeyChar();
        if (Character.isDigit(validar)) {
            getToolkit().beep();
            evt.consume();
            JOptionPane.showMessageDialog(null, "Solo Se Admiten Letras");
        }
        char validar1 = evt.getKeyChar();
        if (Character.isLowerCase(validar1)) {
            evt.setKeyChar(Character.toUpperCase(validar1));

        }     // TODO add your handling code here:
    }//GEN-LAST:event_txtentranadorKeyTyped

    private void btncobrarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncobrarActionPerformed
        FRclientes cerrar = new FRclientes();
        cerrar.setVisible(false);
        No_Factura();
        MontoReal();

        GuardarFactura();
        ActualizarFactura();
        imprimir();
        VistaCliente.txtcodigo.setText("");
        VistaCliente.txtnombre.setText("");
        VistaCliente.mostrar("");

        this.dispose();
    }//GEN-LAST:event_btncobrarActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Personalizado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Personalizado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Personalizado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Personalizado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Personalizado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btncobrar;
    public static com.toedter.calendar.JDateChooser dtregistro;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    public static javax.swing.JTextField txtentranador;
    // End of variables declaration//GEN-END:variables
}
