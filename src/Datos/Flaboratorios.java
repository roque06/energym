
package Datos;

import java.sql.Date;


public class Flaboratorios {
    
    private int id;
    private String orden;
    private Date fecha;
    private String paciente;
    private String laboratorio;
    private String tratamiento;
    private Date fecharecibo;
    private String recibido;
    private String codpaciente;

    public Flaboratorios() {
    }

    public Flaboratorios(int id, String orden, Date fecha, String paciente, String laboratorio, String tratamiento, Date fecharecibo, String recibido, String codpaciente) {
        this.id = id;
        this.orden = orden;
        this.fecha = fecha;
        this.paciente = paciente;
        this.laboratorio = laboratorio;
        this.tratamiento = tratamiento;
        this.fecharecibo = fecharecibo;
        this.recibido = recibido;
        this.codpaciente = codpaciente;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getOrden() {
        return orden;
    }

    public void setOrden(String orden) {
        this.orden = orden;
    }

    public Date getFecha() {
        return fecha;
    }

    public void setFecha(Date fecha) {
        this.fecha = fecha;
    }

    public String getPaciente() {
        return paciente;
    }

    public void setPaciente(String paciente) {
        this.paciente = paciente;
    }

    public String getLaboratorio() {
        return laboratorio;
    }

    public void setLaboratorio(String laboratorio) {
        this.laboratorio = laboratorio;
    }

    public String getTratamiento() {
        return tratamiento;
    }

    public void setTratamiento(String tratamiento) {
        this.tratamiento = tratamiento;
    }

    public Date getFecharecibo() {
        return fecharecibo;
    }

    public void setFecharecibo(Date fecharecibo) {
        this.fecharecibo = fecharecibo;
    }

    public String getRecibido() {
        return recibido;
    }

    public void setRecibido(String recibido) {
        this.recibido = recibido;
    }

    public String getCodpaciente() {
        return codpaciente;
    }

    public void setCodpaciente(String codpaciente) {
        this.codpaciente = codpaciente;
    }

    
}
