
package Datos;


public class FconfCorreo {
    private int id;
    private String correo;
    private String asunto;
    private String mensaje;
    private String contraseña;

    public FconfCorreo() {
    }

    public FconfCorreo(int id, String correo, String asunto, String mensaje, String contraseña) {
        this.id = id;
        this.correo = correo;
        this.asunto = asunto;
        this.mensaje = mensaje;
        this.contraseña = contraseña;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCorreo() {
        return correo;
    }

    public void setCorreo(String correo) {
        this.correo = correo;
    }

    public String getAsunto() {
        return asunto;
    }

    public void setAsunto(String asunto) {
        this.asunto = asunto;
    }

    public String getMensaje() {
        return mensaje;
    }

    public void setMensaje(String mensaje) {
        this.mensaje = mensaje;
    }

    public String getContraseña() {
        return contraseña;
    }

    public void setContraseña(String contraseña) {
        this.contraseña = contraseña;
    }

  
    
}
