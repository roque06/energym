-- phpMyAdmin SQL Dump
-- version 4.9.1
-- https://www.phpmyadmin.net/
--
-- Servidor: localhost
-- Tiempo de generación: 21-03-2022 a las 20:20:15
-- Versión del servidor: 8.0.17
-- Versión de PHP: 7.3.10

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `energym`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `correo`
--

CREATE TABLE `correo` (
  `idcorreo` int(11) NOT NULL,
  `correo` varchar(50) NOT NULL,
  `contraseña` varchar(50) NOT NULL,
  `asunto` varchar(50) NOT NULL,
  `mensaje` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `correo`
--

INSERT INTO `correo` (`idcorreo`, `correo`, `contraseña`, `asunto`, `mensaje`) VALUES
(1, 'roqueadame06@gmail.com', 'roque19951608', 'Promocion de mes', 'Buenas Aprovechamos para ofrecerle un bono de mes');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `factura`
--

CREATE TABLE `factura` (
  `codfactura` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `factura`
--

INSERT INTO `factura` (`codfactura`) VALUES
(29);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `logo`
--

CREATE TABLE `logo` (
  `logo` longblob NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `logo`
--

INSERT INTO `logo` (`logo`) VALUES
(0xffd8ffe000104a46494600010101006000600000ffe100224578696600004d4d002a00000008000101120003000000010001000000000000ffdb0043000201010201010202020202020202030503030303030604040305070607070706070708090b0908080a0807070a0d0a0a0b0c0c0c0c07090e0f0d0c0e0b0c0c0cffdb004301020202030303060303060c0807080c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0c0cffc0001108002f006403012200021101031101ffc4001f0000010501010101010100000000000000000102030405060708090a0bffc400b5100002010303020403050504040000017d01020300041105122131410613516107227114328191a1082342b1c11552d1f02433627282090a161718191a25262728292a3435363738393a434445464748494a535455565758595a636465666768696a737475767778797a838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae1e2e3e4e5e6e7e8e9eaf1f2f3f4f5f6f7f8f9faffc4001f0100030101010101010101010000000000000102030405060708090a0bffc400b51100020102040403040705040400010277000102031104052131061241510761711322328108144291a1b1c109233352f0156272d10a162434e125f11718191a262728292a35363738393a434445464748494a535455565758595a636465666768696a737475767778797a82838485868788898a92939495969798999aa2a3a4a5a6a7a8a9aab2b3b4b5b6b7b8b9bac2c3c4c5c6c7c8c9cad2d3d4d5d6d7d8d9dae2e3e4e5e6e7e8e9eaf2f3f4f5f6f7f8f9faffda000c03010002110311003f00fdd4f8cdf1b3c23fb3bfc36d53c61e3af11693e15f0c68f1892f352d4ae160822c90aab93f79dd8aaaa2e59d9955416201fcdffda0bfe0e7cf09782af6e87c3bf853e20f16e9768e47f6bf89b574f0ad9dd2678922430dcdd2a9f4b8b785f3fc23ad7c2bff000513fdbd754ff82827ed0171e2ebc9e74f87fa0dccb0f81346673e4dbda0ca0d4dd0800ddddae64dcc37430c890ae0f9cd2fc5bf1a7fe0993f19be3efec33e30fda12d7c41ff0009e59fc3ef15be89aa786744d3dddf45d2d6c16f1f56900202a209234902a39504bb3ec8db6fcec7379e2b172c2611a8f2ef27ab7df9568b4eadbf447ee188f0d70bc37c394388b89a9d4a92c435ece94251828a6b9a2eb4da9493946ed4211bab5a524dd97ec37c35ff0083afedf53f1558d9f8abe016a91d8ea322456efe0ff16a6bf7b70ce70a23b79ed2cc484923016424e78c9e0fe977807f69187e3758e99ff088e99a9c171a858c37f7435cb292c25d1a3901c2cf6efb65f341565f2fe50595be7c2923f9a3ff008216fc49f167ec29f1b7e3b782fc47f0f6f35ff881e1bd1f6e9ba145691cfaa5aea304e52e6d22b84dcd1af96f2c93c3116765b476d87c97c6e7847f6c1f8bda47ed6307c70d2fc4cd6bf132cdf6432c88eba7cb69bb274a9a053ce9ef8dad167729c4aac2755947c956ceb3bab9d54cae6e34f0f4b95caa2d6a4f995d4631da11dd4a6d36ecd42d2d63f8e71b71770ae59ec2ae1685484aa2d632973c636d1cafcb16d27d2edb5ab4be197ed87fc14f3fe0bdff00057fe091df137c33e09f893a77c41f107883c49a41d6625f0e5859dcfd9edfcd6855e6f3ae60da6478e50bb411fba6e9c67e66ff0088d37f655ffa127e3d7fe08b4aff00e58d7e277fc17cff006fb4ff00828cff00c144352f1c59dadf69ba469be1ed2345b1d3af41f3f4964b549eeed58e006315fdc5e26f51b5f1b972181afa63fe0933f1f2e7ed5f047e07eabfb027c25f1841adeb567a3def8f7c57e0292fb509e0bdbe2f2decf3c90619228a762b962ab1c4801daa057e9d1b5b4385493575b1fa99f1bffe0edafd9afe00eb5a069badf84be334d7daff0086f4bf142c367a4e992b595bea36b1de5b47366fc0598db4d0ca5577285993e62720745adffc1d2dfb37e87fb16e83f1c9f48f89f3787bc41e2cbaf0743a445a6589d660bbb6b64b9964922378221088e580ee5959b3711fca3248fe657fe0a33f196c3f68ff00dbdbe2a78a3c3b1daffc237a97896ead3c356f656eb0c16fa45bbfd974d822893e5448ece1b78d513e50100000c0af30d47e276b5aafc2ed1fc193de33f87741d56fb5ab2b4da310dddec3670dc499ea77c7616a30781e5f1d4d319fd4ae8bff00076ffecdbae7c02f127c478fc1bf1b21d03c31aee97e1d9d25d174d1717375a8417f3c5e528bf2aca91e9d3990b32eddf1001b7f1c77fc469bfb2aff00d093f1ebff00045a57ff002c6bf013e384abf0e3fe09d9f02fc2ab232de78eb55f107c46bbd9f766b469e2d12c91fd5a29b47d55873c0bb3c7209f5aff0082677ed457bfb2c7c1ed6164fd8bfc07fb4343e24d47ed907887c55e119b58fb2451a795f67b77f25d1503890b6d392c707ee0c007eea7883fe0ed9fd9bfc31f007c2bf11af3c1bf1b63d17c61ae6aba169d6ffd89a77daddf4e86c259e72a6ff67927fb4228d183925e29c151b013c67fc469bfb2affd093f1ebff045a57ff2c6bf1d3fe0e22f88167a87ed97e13f02d87813c2bf0bd7e1af80344b2d5bc2be18b35b2d1b4cd66fad86aba87910a8555225be11336d0cdf675ddc8af997e3b7c4cf026bdfb38fc15f09f84f41b1b5f1178634bd4eebc63ad0b1586eb55d46ef5299a285a5c6f961b7b186cc26e3857967da0649600feb73f632ff0082eb7ece7fb6e7c1e6f19683e2cbaf0cdbc37d269d71a6f896d459dfdbcc891b9cac6d246ca56442191d87246410402be4dff83537f604d034cff824de9de26f1c785ec352bcf889e27d47c41a79beb7fdec564160b28c60f3b59ace4914f759411904125007e50f8efc4171e1af82da178cbc63e20f82ff000c2c7c4561fda5e1df075deafabdf78e353d3cc4925bdcdc436f04f696eb710bac90f9a2cd660559708416fd12fd8c7f6009ff00629f00afc64d3fc5de09f1df8d3e21e81169d69a5269bf6bb1d5349b847b811e9b74963797b24ef1cab2b086ce686e2012c2f6edbe2bbb6fce1f0bff00c11c7e267edffe20f8937b6bad5ae8daf7c0dd4b49f0e78b346d46d669eeb4cb79ef2fade79a33106324764f6d2131851fe8f19285b62a3fed6fc09f889e01ff008232fec3df047e20788bc457be26f877e287d3fc23a5ea00cdad6a71e937b6a6e6c27830ec4826069a5b6b48d80864444f3fec51bcbf99e7993e371599e0bfb1ab3c2a8b72ace308b4e0b450bc95949cb476bbe5bc9ecaff00aa67dc699cbcb71193e6f8afac52e7528b94b9a4d59494949b6e30b72c92d35765b492fa07e1cfc1cf1dfed47f02fc59e11f8bdf093c0ff0e74fff008485353f06e9da76b0f75268366ad98a769ec6582587535b88a7b8125a4b12a25e42a93178e615f25f85bfe0db45d5bc41e3a9f54f8b7e39b7d726d6dee34cbad52cedf56b0bcb79608a569c16717b266e649d4adc5d1994440334b95b99bf44fe08f8d6d3e2278a2e3c5ba3de6b1ad683e3ad1f4ed634d9af1059369766d1b98626b396386e22594b4932bcaaf233c9323f9421453f0e7fc16d3e1ff8bb46f06f84be30dfeafa85c7c48f0778eb49b4f84be11b7d305f784fed3797f0d9b8d724787cb46ba82e1a369a6b88e3b62b18b67f359cdc7e87530746a3bd48a93b5aed2bdbd4fc7b30cb70b8da7ec7174d4e3d9abdbd3b3f347e0cfedf9ff04d7f1d69dfb5dfc44d364d57e13e8b71e14d5c687a88d67e21e87a19babb4b7826696de1beba866961923b88650e10edf37636d911d57ec8fd98ff00e0aa9fb6c58f8f6587e217ed3df03cf84ecfc39ac8b5b0b6f1d7802dc5dea2ba55d0d2ed83dbbabc48f7df645670d1aa26f25d00cd7e607fc141ff00680d4bf6c4fdbbbe2878fae12dee2fbc63e28bcb8822d3ee9afe01199992de1825f2e369912211468fe5a17081b6296da3e91f147ec35fb0bdd5fdbfd9ff006dcd5bc352c5616705fe9edf08b55d6561bf4b6892f192e925856489ee96678f0802a3a2ee7dbbdb7a70508a82d9686d85c3c30f46187a7f0c128aeba2565af5d3a9d87fc13eff00e08f7e38fd993f6defd9e7e237c7ed5fe107c39f84375afc1e29b5f106aff13bc32d63addb586cbb5fb305be637713c82da2630ab802e50b614ee1e4be2eff00821cfc60d6d3e2078b3c0175f097c5bf09fc1bac5c59b78b6cfe2c785db4cb7b7f3ca5b3dc4c7500b6ed2a344c16628dfbc50402715fa6bff059aff8261e83f07bfe0d90f82a7c1faccde2cb3f82b7b6be23875ebed29f4bbbbdd375cb895a6ff4491d9e02d73a859b1898b32087071838fc2cf0bfc73f1ae8bf06b5ef863a4eb5a945e10f19eab61aa6a7a3dbfdcd4aeed1674b62d81b8edfb4c87603b59846cc19a38cad1b9f667ed77ff04eff001d7ed17f18743d27e1cf8abe03f8a3c3df0ffc01a468d67f60f8c7e13ff55a6e8d1cfabde088ea5e6241f6c1a95dbb3a8088ceed8504d7ac7ec75fb4bfedc1fb2d7c3ff05fc2bf06fed35fb3b7c3ff0086ba2dd0b48963f895f0def5347b7b8bb69ae273bae249e5c3cd2ca465dcf2067815ee1e3cff00827447ff000436ff008377fe2778cbc64b6b67fb427ed11058784663b0349a0e9f79324f368c8e7077b59c172f71b70ad2aa27ef160491bf0de803f47bfe0a15ff0004affda0ff00697f8fff0011bf68ebbb5f86ba77c2bf8a5e35d46f742f15ea9f163c2f1e912dbcd7938b6852efede22768e28f66d525bf72c319522a8ffc1537fe09d7e13f157eda9e2697f679f137ece30fc27b1b1d2ec3479d3e34f84acffb51e1d3ada3b8b930cda92bc6d25c2cbbb7052cc0b9fbf93f39fed07ff0505f12fc6ffd873e03fc0556b8b1f06fc1ab6d4ee2587ce25757d4ef753bcb9fb4b80769586da78a18c15dc84dc90d89701fff000498fd999bf6c2ff0082957c15f876d62ba9586bbe2ab49755b62c17cdd36d9bed57dc904716b04e7a76a00fecaff631f86daf7c1afd8f7e14783fc5534571e28f0a783b48d1f5896360c92de5bd9430ceca4704191188238a2bd2a8a00f92fe34fec9ff00123c15f177fe17b7c1db5f02db7c4eba9197c59e18bd83ec969e3fd30a5ac6b693dfc6a5e3be822b44fb35cc892471b34919558a591abf2c7fe0acff00b6278dbe37f8e3c2ba57c5df84abf00f4ff0096f221d674e58cea37ac1849226aa13ecd2da217611416b3c919602691de4f292dbfa02a2bcba395428e13ea94e72b6adb6f9a4dc9b726dbeedb76564b68a51b23cde21cbe59ae16786751d3e7566e165a5adcbaa7a5b47b37d5eaeff00ce2fec4dff000508d4be1ff8cbc2b61e02d634bf1d78c7e1ece745d16d2c3e1d6a1f102fef3c2b7cd2dc9851b4fbb884b36937d132dac33cb6e6decf529d62924def137e887ed33f01ff00686ff8282fecefe24f14fc4ff09ffc23be0ef87ba0ea1e23f06fc264114fad7c46f10db5bcd2e9971af7d9e492186dd26485e2d2a19660d2c83ed12c9e52c55fa55457a1460e10506ef6ea6d97e1a586c353c3ce6e6e292e67bbb2b5df9f73f85ff83df0bfc71f053e3c785f5ed6be13f8b75c87c23af5a6a37fa15de997969fda0b6f7092496923aa6f8b78428580dcbb891c8aee3f62ff00f826a7c57fdb87f6b2f0bf82f4bf871e2ed3f4ef106b300d5ef23d1ee21b1f0f69ef32fda2e6496452b1c5146588dec4b10aa373b2a9fe8f7f6d9d4be2c78ebf6e0f8951f87f45f1878934ad0fc3d61a1f85f49b1f12db69da636a0ef6f7535f4f14d308d8a4724f161e16326e0015c2c89e45e12ff82727c7c5d5f49ba9be0fdb6a12e9de029743697c5d73e1dd616ef5d99ee25fed3759a6ba07c992e144726048c967046ca232f19f1aa671515470a7465249b5757e8edfcad777bedea8fdab03e17e06a60a18ac666b468ca7084d464e9ddf3439edad68cb4bc62df2db99b4afc92b7bb7fc1ca7fb4368df0cbfe08f3f137c33e1fb8d32fbc45e2f5b0f0c699a55a4697526d96ea269b10a9ca85b48ae0ab01f2b2a1f4afc29ff0083707f67ed2ed7fe0aa1e08f1a7c55b6b1f0cf82be1cd9def8a0dcf89adfecb6973770c5e55a885a50034f15c4f15ca85cb2fd959f184247ed1f837fe0957f15e0f835e08f0ac7f0d7e0e69773e1bd4ac2f757d5af3c452c57de22b78ef64bebbb295e0b1942acb3185637c318a388ae183607a258ff00c136fe39ea3f11fe24f89af7c5de0d82f7e22e9b1e93fe9fa95c6bb26956c21991a257fb15a07512cc278bca481a278221b9d3cd595ac7e3256b52b6dd1e9a3befcbb3b2f3bdf6415383b85a87b455730e7e57249a9d34a494a0a2d28fb56b9a2e73d7e150e57794925f097fc1d95f1cefbf6e3f857f053c27f07659bc65e0e8b54d4f5ad5f50b65f274d96f2148aded523b894a472c88b2deee542c543ae719afce1f86dff0494f885e25fd82590781f53ff859df117e2668d61e1559e1db6b73a64361ab25c18af066dd8cd773dba6cdfbbfd0ddb1b63723f7cbc69ff042cf127c53f863e05f0aeb7f163c3b63a7f812d5ad6d5b4ff07cf35ddcab470a7ef2e27d45db68f28bac48a91a34d2ed50a5123efbc2dff0460b1d23c45e0dd66efe23dd36b5e07b08f4db1d46c7c29a547737104766b651aced7115c79a12dd1515186cc8de54c859ce71c466ce7674e2a3a6ba5fa5fed74d6da6a7756c9bc35a58653a78eab3abcd53dd7cdcb65cdecaf6c3af8ed1726a4f9536927bafe72bf67bff008253f8b354fd973e256bd2697a66a5aeeb36dfd9da06a26f5574fd16dec91355d5ee9988131b95b386de144446478efe752448d0a3fd8fff000440ff00827bf8b3fe096fff000511f851e3af88ba2d8f892f7e24e9eda3f84ed34fb968e5d366bd1a7992fe53247feae3b1bd932a8092cee9b834522afec2689ff0454f877a3f81f5af0cb78cfe224fe1ff00105ecf7f7f611ff645846f24fe479c9135ae9f13c1049f65b60d6f0b242c208c1421401ea5e01ff82767c38f02fc56f0f78de41e2ad7fc51e13f3bfb1eef58f105ddd47a79961f2242906f100668f0a498f3f221ea8856a9d3cd24973c927a5f6efaf47d36d77ed6d7971d8bf0fe8ce4f0d4275236a9caaf539afc8fd95df3534bf78d73da2fdd8dd26e565ee828a28af78fc74fffd9);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `servicio`
--

CREATE TABLE `servicio` (
  `id` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `servicio`
--

INSERT INTO `servicio` (`id`, `nombre`) VALUES
(1, 'MENSUALIDAD'),
(2, 'INSCRIPCION'),
(3, 'PERSONALIZADO');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcliente`
--

CREATE TABLE `tbcliente` (
  `id` int(11) NOT NULL,
  `codigo` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `email` varchar(50) NOT NULL,
  `cedula` varchar(50) NOT NULL,
  `direccion` varchar(50) NOT NULL,
  `registro` date NOT NULL,
  `pagos` date NOT NULL,
  `mensualidad` decimal(12,2) NOT NULL,
  `meses` varchar(2) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `monto` varchar(50) CHARACTER SET utf8mb4 COLLATE utf8mb4_0900_ai_ci DEFAULT NULL,
  `estado` varchar(20) NOT NULL,
  `telefono` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tbcliente`
--

INSERT INTO `tbcliente` (`id`, `codigo`, `nombre`, `email`, `cedula`, `direccion`, `registro`, `pagos`, `mensualidad`, `meses`, `monto`, `estado`, `telefono`) VALUES
(4, 1000, 'ROQUE ADAME BURGOS MEDINA', 'roqueadame06@gmail.com', '402-2467955-1', 'SANTIAGO MONTERICO 2 CALLE 15 CON 6', '2022-03-06', '2022-11-03', '700.00', '', '', 'Activo', ''),
(6, 2408, 'JUAN PEREZ RODRIGUEZ', 'jperez@gmail.com', '402-7898451-1', 'Santiago Rodriguez', '2022-03-07', '2023-04-01', '700.00', '', '', 'Activo', '(829)-246-0396'),
(7, 2405, 'ENERNESTO FELIX', 'efeliz@gmil.com', '402-8795112-1', 'Santiago Cien Fuego', '2022-03-08', '2022-11-15', '700.00', '', '', 'Activo', '(829)-538-3557'),
(8, 1003, 'JUAN HERNANDEZ', 'jhernandez@gmail.com', '402-4784154-1', 'Santiago Licey', '2022-03-08', '2022-10-15', '700.00', '', '', 'Activo', '(809)-789-5236');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbcodigo`
--

CREATE TABLE `tbcodigo` (
  `idcodigo` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tbcodigo`
--

INSERT INTO `tbcodigo` (`idcodigo`) VALUES
(1013);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbempleado`
--

CREATE TABLE `tbempleado` (
  `idempleado` int(11) NOT NULL,
  `nombre` varchar(50) NOT NULL,
  `cedula` varchar(50) NOT NULL,
  `telefono` varchar(30) NOT NULL,
  `fecha` date NOT NULL,
  `edad` varchar(2) NOT NULL,
  `usuario` varchar(50) NOT NULL,
  `contraseña` varchar(50) NOT NULL,
  `permiso` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tbempleado`
--

INSERT INTO `tbempleado` (`idempleado`, `nombre`, `cedula`, `telefono`, `fecha`, `edad`, `usuario`, `contraseña`, `permiso`) VALUES
(3, 'ROQUE ADAME', '402-2467955-1', '829-246-0396', '1995-02-11', '26', 'radames', '1234', 'Admin');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tbfactura`
--

CREATE TABLE `tbfactura` (
  `idfactura` int(11) NOT NULL,
  `nofactura` varchar(50) NOT NULL,
  `cliente` varchar(50) NOT NULL,
  `codigocli` int(11) NOT NULL,
  `fecha` date NOT NULL,
  `cantidadmes` int(11) NOT NULL,
  `descripcion` varchar(50) NOT NULL,
  `monto` decimal(12,2) NOT NULL,
  `empleado` varchar(50) NOT NULL,
  `precio` decimal(12,2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tbfactura`
--

INSERT INTO `tbfactura` (`idfactura`, `nofactura`, `cliente`, `codigocli`, `fecha`, `cantidadmes`, `descripcion`, `monto`, `empleado`, `precio`) VALUES
(1, '21', 'JUAN CARLOS MARTINEZ', 2303, '2022-03-11', 4, 'Mensualidad', '2.00', 'ROQUE ADAME', '2800.00'),
(2, '22', 'JUAN CARLOS MARTINEZ', 2303, '2022-03-11', 5, 'Mensualidad', '3750.00', 'ROQUE ADAME', '750.00'),
(3, '23', 'JUAN PEREZ RODRIGUEZ', 2408, '2022-03-11', 6, 'Mensualidad', '4200.00', 'ROQUE ADAME', '700.00'),
(4, '24', 'ROQUE ADAME BURGOS MEDINA', 1000, '2022-03-11', 5, 'Mensualidad', '3500.00', 'ROQUE ADAME', '700.00'),
(5, '25', 'JUAN HERNANDEZ', 1003, '2022-03-13', 1, 'Personalizado Adolfo', '3400.00', 'ROQUE ADAME', '3400.00'),
(6, '26', 'JUAN PEREZ RODRIGUEZ', 2408, '2022-03-13', 1, 'Personalizado Adolfo', '4000.00', 'ROQUE ADAME', '4000.00'),
(7, '27', 'JUAN PEREZ RODRIGUEZ', 2408, '2022-03-13', 1, 'mensualidad', '700.00', 'ROQUE ADAME', '700.00'),
(8, '28', 'ENERNESTO FELIX', 2405, '2022-03-13', 1, 'Mensualidad', '700.00', 'JUANA ANACONDA', '700.00');

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `tblpermisos`
--

CREATE TABLE `tblpermisos` (
  `id_permiso` int(11) NOT NULL,
  `nombre_permiso` varchar(50) NOT NULL,
  `clientes` tinyint(1) NOT NULL,
  `empleados` tinyint(1) NOT NULL,
  `diario` tinyint(1) NOT NULL,
  `permiso` tinyint(1) NOT NULL,
  `servicio` tinyint(11) NOT NULL,
  `rptmensual` tinyint(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

--
-- Volcado de datos para la tabla `tblpermisos`
--

INSERT INTO `tblpermisos` (`id_permiso`, `nombre_permiso`, `clientes`, `empleados`, `diario`, `permiso`, `servicio`, `rptmensual`) VALUES
(6, 'Admin', 1, 1, 1, 1, 1, 1),
(7, 'Secretaria', 1, 0, 0, 0, 0, 0);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `correo`
--
ALTER TABLE `correo`
  ADD PRIMARY KEY (`idcorreo`);

--
-- Indices de la tabla `factura`
--
ALTER TABLE `factura`
  ADD PRIMARY KEY (`codfactura`);

--
-- Indices de la tabla `servicio`
--
ALTER TABLE `servicio`
  ADD PRIMARY KEY (`id`);

--
-- Indices de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `codigo` (`codigo`),
  ADD UNIQUE KEY `nombre` (`nombre`),
  ADD UNIQUE KEY `cedula` (`cedula`);

--
-- Indices de la tabla `tbcodigo`
--
ALTER TABLE `tbcodigo`
  ADD PRIMARY KEY (`idcodigo`);

--
-- Indices de la tabla `tbempleado`
--
ALTER TABLE `tbempleado`
  ADD PRIMARY KEY (`idempleado`);

--
-- Indices de la tabla `tbfactura`
--
ALTER TABLE `tbfactura`
  ADD PRIMARY KEY (`idfactura`);

--
-- Indices de la tabla `tblpermisos`
--
ALTER TABLE `tblpermisos`
  ADD PRIMARY KEY (`id_permiso`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `correo`
--
ALTER TABLE `correo`
  MODIFY `idcorreo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT de la tabla `factura`
--
ALTER TABLE `factura`
  MODIFY `codfactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=30;

--
-- AUTO_INCREMENT de la tabla `servicio`
--
ALTER TABLE `servicio`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT de la tabla `tbcliente`
--
ALTER TABLE `tbcliente`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT de la tabla `tbcodigo`
--
ALTER TABLE `tbcodigo`
  MODIFY `idcodigo` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=1014;

--
-- AUTO_INCREMENT de la tabla `tbempleado`
--
ALTER TABLE `tbempleado`
  MODIFY `idempleado` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT de la tabla `tbfactura`
--
ALTER TABLE `tbfactura`
  MODIFY `idfactura` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT de la tabla `tblpermisos`
--
ALTER TABLE `tblpermisos`
  MODIFY `id_permiso` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
