/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Datos.Flaboratorios;
import Logica.Controlador;
import Logica.FRlaboratorio;
import Logica.FRlaboratorioh;
import Logica.conexion;
import java.awt.Graphics;
import java.awt.Image;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Calendar;
import java.util.GregorianCalendar;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author radames
 */
public class Flaboratorio extends javax.swing.JFrame {

    Controlador controlador = new Controlador();

    public Flaboratorio() {
        initComponents();
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        mostrar("");
        Codigo();
        inhabilitar();
        llenar();
        dtfecha.setCalendar(c1);
    }
    Calendar c1 = new GregorianCalendar();

    void llenar() {

        String consulta = "SELECT laboratorio FROM tipolaboratorio ORDER BY laboratorio ASC";
        controlador.llenarCombo(cbolaboratorio, consulta, 1);
    }

    void Codigo() {
        String idpaciente = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT no_orden from numeroorden");

            rq2.next();
            idpaciente = rq2.getString("no_orden");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (idpaciente);
        secuencia = secuencia;
        lblorden1.setText("000" + String.valueOf(secuencia));

    }

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRlaboratorio func = new FRlaboratorio();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            lbltotalregistro.setText("Total Registrados: " + Integer.toString(func.totalregistro));
            ocultar_columnas();
//            inhabilitat();

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ActualizarCodigo() {

        try {
            PreparedStatement psU = cn.prepareStatement("Update numeroorden set no_orden= no_orden+1");
            psU.executeUpdate();

        } catch (Exception e) {

            JOptionPane.showConfirmDialog(rootPane, e);

        }
        Codigo();

    }

    void habilitar() {
        txtnombre.setEnabled(false);
        dtfecha.setEnabled(false);
        txttipo.setEnabled(false);
        cbolaboratorio.setEnabled(false);
        dtrecibo.setEnabled(false);

        txtnombre.setText("");
        dtfecha.setDate(null);
        txttipo.setText("");
        dtrecibo.setDate(null);
        btneliminar.setEnabled(false);
        btnguardar.setEnabled(false);

    }

    void inhabilitar() {
        txtnombre.setEnabled(false);
        dtfecha.setEnabled(false);
        txttipo.setEnabled(false);
        cbolaboratorio.setEnabled(false);
        dtrecibo.setEnabled(false);

        txtnombre.setText("");
        dtfecha.setDate(null);
        txttipo.setText("");
        dtrecibo.setDate(null);
        btneliminar.setEnabled(false);
        btnguardar.setEnabled(false);

    }

    void guardarH() {

        Flaboratorios dts = new Flaboratorios();
        FRlaboratorioh func = new FRlaboratorioh();

        dts.setOrden(lblorden1.getText());
        Calendar cal;
        int d, m, a;
        cal = dtfecha.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha(new Date(a, m, d));

        dts.setPaciente(txtnombre.getText());
        int seleccionado = cbolaboratorio.getSelectedIndex();
        dts.setLaboratorio((String) cbolaboratorio.getItemAt(seleccionado));

        dts.setTratamiento(txttipo.getText());

        if (dtrecibo.getDate() == null) {
            dts.setRecibido(recibido);
        } else {
            cal = dtrecibo.getCalendar();
            d = cal.get(Calendar.DAY_OF_MONTH);
            m = cal.get(Calendar.MONTH);
            a = cal.get(Calendar.YEAR) - 1900;
            dts.setFecharecibo(new Date(a, m, d));
        }

        dts.setCodpaciente(codpaciente);

        if (func.insertar(dts)) {

        }

    }

    void guardar() {

        if (dtfecha.getDate() == null) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR LA FEHCA DEL TRABAJO");
            return;
        }

        if (txtnombre.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR EL NOMBRE DEL PACIEINTE");
            txtnombre.requestFocus();
            return;

        }

        if (txttipo.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "DEBES INGRESAR EL NOMBRE DEL PACIEINTE");
            return;
        }

        if (cbolaboratorio.getSelectedItem() == null) {
            JOptionPane.showMessageDialog(null, "DEBES SELECCIONAR UN LABORATORIO");
            return;

        }

        Flaboratorios dts = new Flaboratorios();
        FRlaboratorio func = new FRlaboratorio();

        dts.setOrden(lblorden1.getText());
        Calendar cal;
        int d, m, a;
        cal = dtfecha.getCalendar();
        d = cal.get(Calendar.DAY_OF_MONTH);
        m = cal.get(Calendar.MONTH);
        a = cal.get(Calendar.YEAR) - 1900;
        dts.setFecha(new Date(a, m, d));

        dts.setPaciente(txtnombre.getText());
        int seleccionado = cbolaboratorio.getSelectedIndex();
        dts.setLaboratorio((String) cbolaboratorio.getItemAt(seleccionado));

        dts.setTratamiento(txttipo.getText());

        if (dtrecibo.getDate() == null) {
            dts.setRecibido(recibido);
        } else {
            cal = dtrecibo.getCalendar();
            d = cal.get(Calendar.DAY_OF_MONTH);
            m = cal.get(Calendar.MONTH);
            a = cal.get(Calendar.YEAR) - 1900;
            dts.setFecharecibo(new Date(a, m, d));
        }

        dts.setCodpaciente(codpaciente);

        if (accion.equals("guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(null, "TRABAJO GUARDADO CORRECTAMENTE");
                ActualizarCodigo();
                Codigo();

            }
        } else if (accion.equals("editar")) {
            dts.setId(Integer.parseInt(id));
            if (func.editar(dts)) {
                Codigo();
                JOptionPane.showMessageDialog(null, "TRABAJO EDITADO CORRECTAMENTE");
            }
        }

    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(3).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(3).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(3).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(6).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(6).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(6).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(50);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(50);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(50);

        tablalistado.getColumnModel().getColumn(2).setMaxWidth(90);
        tablalistado.getColumnModel().getColumn(2).setMinWidth(90);
        tablalistado.getColumnModel().getColumn(2).setPreferredWidth(90);

        tablalistado.getColumnModel().getColumn(1).setMaxWidth(90);
        tablalistado.getColumnModel().getColumn(1).setMinWidth(90);
        tablalistado.getColumnModel().getColumn(1).setPreferredWidth(90);

        tablalistado.getColumnModel().getColumn(5).setMaxWidth(160);
        tablalistado.getColumnModel().getColumn(5).setMinWidth(160);
        tablalistado.getColumnModel().getColumn(5).setPreferredWidth(160);

        tablalistado.getColumnModel().getColumn(7).setMaxWidth(100);
        tablalistado.getColumnModel().getColumn(7).setMinWidth(100);
        tablalistado.getColumnModel().getColumn(7).setPreferredWidth(100);
    }
    private String accion = "guardar";
    private String recibido = "No Recibido";
    private String id;

    void elimininar() {
        Flaboratorios dts = new Flaboratorios();
        FRlaboratorio func = new FRlaboratorio();

        dts.setId(Integer.parseInt(id));

        if (func.eliminar(dts)) {
//            JOptionPane.showMessageDialog(null, "Registro Eliminado");
        }
        mostrar("");
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        cbolaboratorio = new javax.swing.JComboBox<>();
        jScrollPane1 = new javax.swing.JScrollPane();
        txttipo = new javax.swing.JTextArea();
        dtrecibo = new com.toedter.calendar.JDateChooser();
        jScrollPane2 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        lblorden1 = new javax.swing.JLabel();
        userLabel7 = new javax.swing.JLabel();
        userLabel4 = new javax.swing.JLabel();
        jSeparator4 = new javax.swing.JSeparator();
        userLabel8 = new javax.swing.JLabel();
        txtnombre = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        btnbuscar = new javax.swing.JButton();
        dtfecha = new com.toedter.calendar.JDateChooser();
        lbltotalregistro = new javax.swing.JLabel();
        jSeparator9 = new javax.swing.JSeparator();
        userLabel10 = new javax.swing.JLabel();
        jSeparator10 = new javax.swing.JSeparator();
        userLabel11 = new javax.swing.JLabel();
        jSeparator11 = new javax.swing.JSeparator();
        userLabel12 = new javax.swing.JLabel();
        jSeparator12 = new javax.swing.JSeparator();
        loginBtn1 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        loginBtn = new javax.swing.JPanel();
        loginBtnTxt = new javax.swing.JLabel();
        userLabel13 = new javax.swing.JLabel();
        lblbuscar = new javax.swing.JLabel();
        jSeparator13 = new javax.swing.JSeparator();
        loginBtn2 = new javax.swing.JPanel();
        btneliminar = new javax.swing.JLabel();
        txtbuscar = new javax.swing.JTextField();
        loginBtn3 = new javax.swing.JPanel();
        btneliminar1 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cbolaboratorio.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Item 1", "Item 2", "Item 3", "Item 4" }));
        cbolaboratorio.setOpaque(false);
        jPanel1.add(cbolaboratorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 250, 280, 30));

        jScrollPane1.setBorder(null);

        txttipo.setColumns(20);
        txttipo.setLineWrap(true);
        txttipo.setRows(2);
        txttipo.setTabSize(5);
        txttipo.setBorder(null);
        jScrollPane1.setViewportView(txttipo);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 320, 280, 50));
        jPanel1.add(dtrecibo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 420, 280, 30));

        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane2.setViewportView(tablalistado);

        jPanel1.add(jScrollPane2, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 90, 740, 342));

        lblorden1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lblorden1.setText("#####");
        jPanel1.add(lblorden1, new org.netbeans.lib.awtextra.AbsoluteConstraints(125, 44, -1, -1));

        userLabel7.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel7.setText("Laboratorio:");
        jPanel1.add(userLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, -1, -1));

        userLabel4.setFont(new java.awt.Font("Roboto Light", 1, 24)); // NOI18N
        userLabel4.setText("CONTROL DE TRABAJO LABORATORIO");
        jPanel1.add(userLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 0, -1, -1));

        jSeparator4.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator4, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 30, 450, 20));

        userLabel8.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel8.setText("No. De Orden:");
        jPanel1.add(userLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, -1, -1));

        txtnombre.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtnombre.setBorder(null);
        txtnombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtnombre.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtnombreMousePressed(evt);
            }
        });
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });
        jPanel1.add(txtnombre, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 100, 280, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 370, 280, 20));

        btnbuscar.setBackground(new java.awt.Color(0, 134, 190));
        btnbuscar.setForeground(new java.awt.Color(0, 134, 190));
        btnbuscar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/buscar.png"))); // NOI18N
        btnbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnbuscarActionPerformed(evt);
            }
        });
        jPanel1.add(btnbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 100, 40, 30));

        dtfecha.setBackground(new java.awt.Color(255, 255, 255));
        dtfecha.setDateFormatString("dd-MM-yyyy");
        dtfecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dtfechaKeyPressed(evt);
            }
        });
        jPanel1.add(dtfecha, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 170, 280, 30));

        lbltotalregistro.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lbltotalregistro.setText("Buscar:");
        jPanel1.add(lbltotalregistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 430, -1, 30));

        jSeparator9.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator9, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 80, 220, 20));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Tipo/Tratamiento:");
        jPanel1.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 300, -1, -1));

        jSeparator10.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 450, 280, 20));

        userLabel11.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel11.setText("Fecha Recibido:");
        jPanel1.add(userLabel11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 390, -1, -1));

        jSeparator11.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator11, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 280, 280, 20));

        userLabel12.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel12.setText("Fecha de Envio:");
        jPanel1.add(userLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 150, -1, -1));

        jSeparator12.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 200, 280, 20));

        loginBtn1.setBackground(new java.awt.Color(0, 134, 190));
        loginBtn1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtn1MouseClicked(evt);
            }
        });

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn1Layout = new javax.swing.GroupLayout(loginBtn1);
        loginBtn1.setLayout(loginBtn1Layout);
        loginBtn1Layout.setHorizontalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn1Layout.setVerticalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(170, 480, -1, -1));

        loginBtn.setBackground(new java.awt.Color(0, 134, 190));

        loginBtnTxt.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        loginBtnTxt.setForeground(new java.awt.Color(255, 255, 255));
        loginBtnTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loginBtnTxt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        loginBtnTxt.setText("Nuevo");
        loginBtnTxt.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        loginBtnTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtnLayout = new javax.swing.GroupLayout(loginBtn);
        loginBtn.setLayout(loginBtnLayout);
        loginBtnLayout.setHorizontalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginBtnTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtnLayout.setVerticalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginBtnTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 480, 120, 30));

        userLabel13.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel13.setText("Paciente:");
        jPanel1.add(userLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, -1, -1));

        lblbuscar.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        lblbuscar.setText("Buscar:");
        jPanel1.add(lblbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(380, 50, -1, 30));

        jSeparator13.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator13, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 130, 280, 20));

        loginBtn2.setBackground(new java.awt.Color(0, 134, 190));

        btneliminar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btneliminar.setForeground(new java.awt.Color(255, 255, 255));
        btneliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btneliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/boton-eliminar.png"))); // NOI18N
        btneliminar.setText("Eliminar");
        btneliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btneliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneliminarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn2Layout = new javax.swing.GroupLayout(loginBtn2);
        loginBtn2.setLayout(loginBtn2Layout);
        loginBtn2Layout.setHorizontalGroup(
            loginBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn2Layout.setVerticalGroup(
            loginBtn2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn2, new org.netbeans.lib.awtextra.AbsoluteConstraints(680, 50, -1, -1));

        txtbuscar.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtbuscar.setBorder(null);
        txtbuscar.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtbuscar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtbuscarMousePressed(evt);
            }
        });
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        jPanel1.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(440, 50, 220, 30));

        loginBtn3.setBackground(new java.awt.Color(0, 134, 190));

        btneliminar1.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btneliminar1.setForeground(new java.awt.Color(255, 255, 255));
        btneliminar1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btneliminar1.setText("Historial");
        btneliminar1.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btneliminar1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminar1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneliminar1MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminar1MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn3Layout = new javax.swing.GroupLayout(loginBtn3);
        loginBtn3.setLayout(loginBtn3Layout);
        loginBtn3Layout.setHorizontalGroup(
            loginBtn3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar1, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn3Layout.setVerticalGroup(
            loginBtn3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar1, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn3, new org.netbeans.lib.awtextra.AbsoluteConstraints(990, 50, -1, -1));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1125, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 555, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());

        if (evt.getClickCount() == 1) {

            btneliminar.setEnabled(true);

            accion = "editar";
            btnguardar.setText("Editar");
            habilitar();
            txttipo.setEnabled(true);
            dtrecibo.setEnabled(true);
            btneliminar.setEnabled(true);
            btnguardar.setEnabled(true);

            id = (tablalistado.getValueAt(fila, 0).toString());
            lblorden1.setText(tablalistado.getValueAt(fila, 1).toString());
            dtfecha.setDate(Date.valueOf(tablalistado.getValueAt(fila, 3).toString()));

            txtnombre.setText(tablalistado.getValueAt(fila, 4).toString());

            cbolaboratorio.setSelectedItem(tablalistado.getValueAt(fila, 5).toString());
            txttipo.setText(tablalistado.getValueAt(fila, 6).toString());
            codpaciente = (tablalistado.getValueAt(fila, 8).toString());
            dtrecibo.setDate(Date.valueOf(tablalistado.getValueAt(fila, 7).toString()));
        }
    }//GEN-LAST:event_tablalistadoMouseClicked
    public static String codpaciente;
    private void txtnombreMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtnombreMousePressed

    }//GEN-LAST:event_txtnombreMousePressed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void btnbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnbuscarActionPerformed
        new Vistalaboratorio().setVisible(true);       // TODO add your handling code here:

    }//GEN-LAST:event_btnbuscarActionPerformed

    private void dtfechaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dtfechaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_dtfechaKeyPressed

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        if (btnguardar.isEnabled() == false) {

            return;

        }
        guardar();
        if (dtrecibo.getDate() == null) {

        } else {
            guardarH();
            elimininar();
            accion = "guardar";
            btnguardar.setText("Guardar");
        }
        mostrar("");
        inhabilitar();

        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered
    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited
    }//GEN-LAST:event_btnguardarMouseExited

    private void loginBtn1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtn1MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_loginBtn1MouseClicked

    private void loginBtnTxtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseClicked
        habilitar();
        Codigo();
        llenar();
        dtfecha.setCalendar(c1);
        accion = "guardar";
        btnguardar.setText("Guardar");
    }//GEN-LAST:event_loginBtnTxtMouseClicked

    private void loginBtnTxtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseEntered
    }//GEN-LAST:event_loginBtnTxtMouseEntered

    private void loginBtnTxtMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseExited
    }//GEN-LAST:event_loginBtnTxtMouseExited

    private void btneliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseClicked
        if (btneliminar.isEnabled() == false) {
            return;
        }

        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "ESTAS SEGURO QUE DESEA ELIMINAR EL TRABANO", "Confirmar", 2);

        if (confirmacion == 0) {
            elimininar();
            JOptionPane.showMessageDialog(null, "Registro Eliminado");

        }
        btneliminar.setEnabled(false);
        inhabilitar();
// TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseClicked

    private void btneliminarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseEntered

    private void btneliminarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseExited

    private void txtbuscarMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtbuscarMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarMousePressed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        mostrar(txtbuscar.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btneliminar1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminar1MouseClicked
        new VistaHistorialLab().setVisible(true);        // TODO add your handling code here:
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminar1MouseClicked

    private void btneliminar1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminar1MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminar1MouseEntered

    private void btneliminar1MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminar1MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminar1MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Flaboratorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Flaboratorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Flaboratorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Flaboratorio.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Flaboratorio().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JButton btnbuscar;
    private javax.swing.JLabel btneliminar;
    private javax.swing.JLabel btneliminar1;
    public static javax.swing.JLabel btnguardar;
    public static javax.swing.JComboBox<String> cbolaboratorio;
    public static com.toedter.calendar.JDateChooser dtfecha;
    public static com.toedter.calendar.JDateChooser dtrecibo;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JSeparator jSeparator10;
    private javax.swing.JSeparator jSeparator11;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator13;
    private javax.swing.JSeparator jSeparator4;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JSeparator jSeparator9;
    private javax.swing.JLabel lblbuscar;
    private javax.swing.JLabel lblorden1;
    private javax.swing.JLabel lbltotalregistro;
    private javax.swing.JPanel loginBtn;
    private javax.swing.JPanel loginBtn1;
    private javax.swing.JPanel loginBtn2;
    private javax.swing.JPanel loginBtn3;
    private javax.swing.JLabel loginBtnTxt;
    private javax.swing.JTable tablalistado;
    public static javax.swing.JTextField txtbuscar;
    public static javax.swing.JTextField txtnombre;
    public static javax.swing.JTextArea txttipo;
    private javax.swing.JLabel userLabel10;
    private javax.swing.JLabel userLabel11;
    private javax.swing.JLabel userLabel12;
    private javax.swing.JLabel userLabel13;
    private javax.swing.JLabel userLabel4;
    private javax.swing.JLabel userLabel7;
    private javax.swing.JLabel userLabel8;
    // End of variables declaration//GEN-END:variables
   conexion cc = new conexion();
    Connection cn = cc.conectar();
}
