
package Logica;

import Datos.Fregimen;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;


public class FRtipolaboratoio {
     private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Laboratorio"};

        String[] registro = new String[8];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From tipolaboratorio  where concat (laboratorio) like '%" + buscar + "%' order by laboratorio asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idtipo");
                registro[1] = rs.getString("laboratorio");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fregimen dts) {
        sSQL = "insert into tipolaboratorio (idtipo,laboratorio)"
                + "VALUES(?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getId());
            pst.setString(2, dts.getRegimen());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fregimen dts) {
        sSQL = "UPDATE tipolaboratorio set laboratorio=?"
                + "where idtipo=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getRegimen());

            pst.setInt(2, dts.getId());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fregimen dts) {
        sSQL = "DELETE FROM tipolaboratorio WHERE idtipo=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    
}
