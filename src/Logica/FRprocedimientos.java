package Logica;

import Datos.Fprocedimiento;
import Datos.Fregimen;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRprocedimientos {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID","CodPaciente", "Nombre", "Procedimiento", "Fecha"};

        String[] registro = new String[8];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select id,codigo,nombre,procedimientos,fecha From procedimientos  where concat (nombre,codigo) like '%" + buscar + "%' order by id desc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("codigo");
                registro[2] = rs.getString("nombre");
                registro[3] = rs.getString("procedimientos");
                registro[4] = rs.getString("fecha");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fprocedimiento dts) {
        sSQL = "insert into procedimientos (nombre,procedimientos,codigo,fecha)"
                + "VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getProcedimiento());
            pst.setString(3, dts.getCodigo());
            pst.setString(4, dts.getFecha());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fprocedimiento dts) {
        sSQL = "UPDATE procedimientos set nombre=?,procedimientos=?,codigo=?,fecha=?"
                + "where id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombre());
            pst.setString(2, dts.getProcedimiento());
            pst.setString(3, dts.getCodigo());
            pst.setString(4, dts.getFecha());
            pst.setInt(5, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fprocedimiento dts) {
        sSQL = "DELETE FROM procedimientos WHERE id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
