package Clases;

import Datos.dtcliente;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRCliente {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Codigo", "Nombre", "Email", "Cedula", "Direccion", "Registro", " Fecha Pagos", "Monto Mensual", "Meses", "Monto", "Estado", "TELEFONO", "FECHA","REGISTRO1"};

        String[] registro = new String[16];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select id,codigo,nombre,email,cedula,direccion,DATE_FORMAT(registro,'%d-%m-%YYYY')as registros,DATE_FORMAT(pagos,'%d-%m-%Y')as pago,mensualidad,meses,monto,estado,telefono,pagos,registro From tbcliente where concat (codigo,nombre,cedula) like '%" + buscar + "%' order by nombre asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("codigo");
                registro[2] = rs.getString("nombre");
                registro[3] = rs.getString("email");
                registro[4] = rs.getString("cedula");
                registro[5] = rs.getString("direccion");
                registro[6] = rs.getString("registros");
                registro[7] = rs.getString("pago");
                registro[8] = rs.getString("mensualidad");
                registro[9] = rs.getString("meses");
                registro[10] = rs.getString("monto");
                registro[11] = rs.getString("estado");
                registro[12] = rs.getString("telefono");
                registro[13] = rs.getString("pagos");
                registro[14] = rs.getString("registro");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(dtcliente dts) {
        sSQL = "insert into tbcliente (id,codigo,nombre,email,cedula,direccion,registro,pagos,mensualidad,meses,monto,estado,telefono)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getId());
            pst.setInt(2, dts.getCodigo());
            pst.setString(3, dts.getNombre());
            pst.setString(4, dts.getEmail());
            pst.setString(5, dts.getCedula());
            pst.setString(6, dts.getDireccion());
            pst.setDate(7, dts.getRegistro());
            pst.setDate(8, dts.getPagos());
            pst.setString(9, dts.getMensualidad());
            pst.setString(10, dts.getMeses());
            pst.setString(11, dts.getMonto());
            pst.setString(12, dts.getEstado());
            pst.setString(13, dts.getTelefono());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(dtcliente dts) {
        sSQL = "UPDATE tbcliente set codigo=?,nombre=?,email=?,cedula=?,direccion=?,registro=?,pagos=?,mensualidad=?,meses=?,monto=?,estado=?,telefono=?"
                + "where id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getCodigo());
            pst.setString(2, dts.getNombre());
            pst.setString(3, dts.getEmail());
            pst.setString(4, dts.getCedula());
            pst.setString(5, dts.getDireccion());
            pst.setDate(6, dts.getRegistro());
            pst.setDate(7, dts.getPagos());
            pst.setString(8, dts.getMensualidad());
            pst.setString(9, dts.getMeses());
            pst.setString(10, dts.getMonto());
            pst.setString(11, dts.getEstado());
            pst.setString(12, dts.getTelefono());

            pst.setInt(13, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(dtcliente dts) {
        sSQL = "DELETE FROM tbcliente WHERE id=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
