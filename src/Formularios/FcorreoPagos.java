/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Logica.conexion;
import java.awt.Graphics;
import java.awt.Image;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.Session;
import static javax.mail.Session.getDefaultInstance;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;
import javax.swing.ImageIcon;
import javax.mail.internet.InternetAddress;
import javax.swing.JOptionPane;


public class FcorreoPagos extends javax.swing.JFrame {

    public FcorreoPagos() {
        initComponents();
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        cargarCorreo();
        this.setLocationRelativeTo(null);

    }

    void cargarCorreo() {
        String Correo = "";
        String asunto = "";
        String mensaje = "";
        String contraseña = "";

        try {
            Statement sq2 = cn.createStatement();
            ResultSet rq2 = sq2.executeQuery("SELECT correo,contraseña,asunto,mensaje from correopagos where idcorreo=2");
            rq2.next();
            Correo = rq2.getString("correo");
            contraseña = rq2.getString("contraseña");
            asunto = rq2.getString("asunto");
            mensaje = rq2.getString("mensaje");

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }

        String secuencia = (Correo);
        secuencia = secuencia;
        correo = (String.valueOf(secuencia));

        String secuencia2 = (contraseña);
        secuencia2 = secuencia2;
        pass = (String.valueOf(secuencia2));

        String secuencia1 = (asunto);
        secuencia1 = secuencia1;
        txtasunto.setText(String.valueOf(secuencia1));

        String secuencia3 = (mensaje);
        secuencia3 = secuencia3;
        txtmensaje.setText(String.valueOf(secuencia3));

    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jDesktopPane1 = new javax.swing.JDesktopPane();
        jPanel1 = new javax.swing.JPanel();
        txtdestino = new javax.swing.JTextField();
        userLabel7 = new javax.swing.JLabel();
        jSeparator12 = new javax.swing.JSeparator();
        userLabel9 = new javax.swing.JLabel();
        txtasunto = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        userLabel10 = new javax.swing.JLabel();
        jScrollPane1 = new javax.swing.JScrollPane();
        txtmensaje = new javax.swing.JTextArea();
        loginBtn7 = new javax.swing.JPanel();
        btncobro4 = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jDesktopPane1.setBackground(new java.awt.Color(255, 255, 255));
        jDesktopPane1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "ENVIAR RECORDATORIO DE PAGO", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 12))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        txtdestino.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtdestino.setBorder(null);
        txtdestino.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtdestino.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtdestinoMousePressed(evt);
            }
        });
        txtdestino.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtdestinoActionPerformed(evt);
            }
        });
        jPanel1.add(txtdestino, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 60, 250, 30));

        userLabel7.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel7.setText("Para:");
        jPanel1.add(userLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 30, -1, 30));

        jSeparator12.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator12, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 90, 250, 20));

        userLabel9.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel9.setText("Asunto:");
        jPanel1.add(userLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 110, 60, -1));

        txtasunto.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtasunto.setBorder(null);
        txtasunto.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtasunto.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtasuntoMousePressed(evt);
            }
        });
        txtasunto.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtasuntoActionPerformed(evt);
            }
        });
        jPanel1.add(txtasunto, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 130, 270, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 160, 250, 20));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Mensaje:");
        jPanel1.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 190, -1, -1));

        txtmensaje.setColumns(20);
        txtmensaje.setLineWrap(true);
        txtmensaje.setRows(2);
        txtmensaje.setBorder(null);
        jScrollPane1.setViewportView(txtmensaje);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(60, 220, 260, 50));

        loginBtn7.setBackground(new java.awt.Color(0, 134, 190));

        btncobro4.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btncobro4.setForeground(new java.awt.Color(255, 255, 255));
        btncobro4.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btncobro4.setText("Enviar");
        btncobro4.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btncobro4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btncobro4MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btncobro4MouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btncobro4MouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn7Layout = new javax.swing.GroupLayout(loginBtn7);
        loginBtn7.setLayout(loginBtn7Layout);
        loginBtn7Layout.setHorizontalGroup(
            loginBtn7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro4, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn7Layout.setVerticalGroup(
            loginBtn7Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btncobro4, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn7, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 300, -1, 30));

        jDesktopPane1.add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 380, 400));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 399, Short.MAX_VALUE)
                .addGap(0, 0, 0))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtdestinoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtdestinoMousePressed

    }//GEN-LAST:event_txtdestinoMousePressed

    private void txtdestinoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtdestinoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtdestinoActionPerformed

    private void txtasuntoMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtasuntoMousePressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtasuntoMousePressed

    private void txtasuntoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtasuntoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtasuntoActionPerformed

    private void btncobro4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseClicked
        try {
            Properties props = new Properties();
            props.setProperty("mail.smtp.host", "smtp.gmail.com");
            props.setProperty("mail.smtp.starttls.enable", "true");
            props.setProperty("mail.smtp.port", "587");
            props.setProperty("mail.smtp.auth", "true");

            Session session = Session.getDefaultInstance(props);

            String correoRemitente = correo;
            String passwordRemitente = pass;
            String correoReceptor = txtdestino.getText();
            String asunto = txtasunto.getText();
            String mensaje = txtmensaje.getText();

            MimeMessage message = new MimeMessage(session);
            message.setFrom(new InternetAddress(correoRemitente));

            message.addRecipient(Message.RecipientType.TO, new InternetAddress(correoReceptor));
            message.setSubject(asunto);
            message.setText(mensaje, "ISO-8859-1", "html");

            Transport t = session.getTransport("smtp");
            t.connect(correoRemitente, passwordRemitente);
            t.sendMessage(message, message.getRecipients(Message.RecipientType.TO));
            t.close();

            JOptionPane.showMessageDialog(null, "Correo Electronico Enviado");
            this.dispose();

        } catch (AddressException ex) {
            Logger.getLogger(Fcorreo.class.getName()).log(Level.SEVERE, null, ex);
        } catch (MessagingException ex) {
            Logger.getLogger(Fcorreo.class.getName()).log(Level.SEVERE, null, ex);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro4MouseClicked

    private void btncobro4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro4MouseEntered

    private void btncobro4MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btncobro4MouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btncobro4MouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FcorreoPagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FcorreoPagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FcorreoPagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FcorreoPagos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FcorreoPagos().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btncobro4;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator12;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JPanel loginBtn7;
    public static javax.swing.JTextField txtasunto;
    public static javax.swing.JTextField txtdestino;
    public static javax.swing.JTextArea txtmensaje;
    private javax.swing.JLabel userLabel10;
    private javax.swing.JLabel userLabel7;
    private javax.swing.JLabel userLabel9;
    // End of variables declaration//GEN-END:variables
private String correo;
    private String asunto;
    private String mensaje;
    private String pass;
    conexion cc = new conexion();
    Connection cn = cc.conectar();
}
