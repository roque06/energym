/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Clases.FRCliente;
import static Formularios.FRclientes.codigocli;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Locale;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Roque
 */
public class VistaCliente extends javax.swing.JFrame {

    /**
     * Creates new form VistaCliente
     */
    public VistaCliente() {
        initComponents();
        this.setLocationRelativeTo(null);
        mostrar("");
        btnpago.setEnabled(false);
        txtcodigo.setEditable(false);
        txtnombre.setEditable(false);
        btneditar.setEnabled(false);
        btncorreo.setEnabled(false);
        setIconImage(new ImageIcon(getClass().getResource("/Imagenes/KILO.png")).getImage());

    }

    public static void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRCliente func = new FRCliente();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            ocultar_columnas();
            totalActivo();
            lbltotalregistros.setText("Total Registrados: " + Integer.toString(func.totalregistro));

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
    }

    public static void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(1).setMaxWidth(100);
        tablalistado.getColumnModel().getColumn(1).setMinWidth(100);
        tablalistado.getColumnModel().getColumn(1).setPreferredWidth(100);

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(2).setMaxWidth(290);
        tablalistado.getColumnModel().getColumn(2).setMinWidth(290);
        tablalistado.getColumnModel().getColumn(2).setPreferredWidth(290);

        tablalistado.getColumnModel().getColumn(3).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(3).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(3).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(4).setMaxWidth(150);
        tablalistado.getColumnModel().getColumn(4).setMinWidth(150);
        tablalistado.getColumnModel().getColumn(4).setPreferredWidth(150);

        tablalistado.getColumnModel().getColumn(5).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(5).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(5).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(6).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(6).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(6).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(7).setMaxWidth(130);
        tablalistado.getColumnModel().getColumn(7).setMinWidth(130);
        tablalistado.getColumnModel().getColumn(7).setPreferredWidth(130);

        tablalistado.getColumnModel().getColumn(9).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(9).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(9).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(10).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(10).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(10).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(13).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(13).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(13).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(14).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(14).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(14).setPreferredWidth(0);

    }

    public static void totalActivo() {

        int disponible = 0, asignada = 0;
        for (int i = 0; i < tablalistado.getRowCount(); i++) {
            if (tablalistado.getValueAt(i, 11).toString().equals("Activo")) {
                disponible = disponible + 1;
            } else if (tablalistado.getValueAt(i, 11).toString().equals("Inactivo")) {
                asignada = asignada + 1;
            }

            lblactivo.setText("Total Clientes Activo: " + disponible + "");
            lblinactivo.setText("Total Clientes Inactivo: " + asignada + "");
        }

    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        dtfecha = new com.toedter.calendar.JDateChooser();
        dtfecha1 = new com.toedter.calendar.JDateChooser();
        jLabel1 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        jLabel2 = new javax.swing.JLabel();
        txtcodigo = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        lbltotalregistros = new javax.swing.JLabel();
        txtbuscar = new javax.swing.JTextField();
        txtnombre = new javax.swing.JTextField();
        jLabel4 = new javax.swing.JLabel();
        jSeparator2 = new javax.swing.JSeparator();
        jLabel5 = new javax.swing.JLabel();
        btncliente = new javax.swing.JButton();
        btnpago = new javax.swing.JButton();
        btneditar = new javax.swing.JButton();
        btncorreo = new javax.swing.JButton();
        btneditar1 = new javax.swing.JButton();
        lblactivo = new javax.swing.JLabel();
        lblinactivo = new javax.swing.JLabel();

        dtfecha.setDateFormatString("dd-MM-yyyy");
        dtfecha.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dtfechaKeyPressed(evt);
            }
        });

        dtfecha1.setDateFormatString("dd-MM-yyyy");
        dtfecha1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                dtfecha1KeyPressed(evt);
            }
        });

        jLabel1.setText("jLabel1");

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("EnerGym");
        setBackground(new java.awt.Color(255, 255, 204));
        setResizable(false);

        jPanel2.setBackground(new java.awt.Color(204, 204, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createEtchedBorder(), "VISTA CLIENTE", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Times New Roman", 1, 24)))); // NOI18N

        tablalistado.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablalistado);

        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel2.setText("Codigo:");

        txtcodigo.setBackground(new java.awt.Color(204, 255, 204));
        txtcodigo.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtcodigo.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtcodigo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtcodigoActionPerformed(evt);
            }
        });

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));
        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jSeparator1.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator1.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        lbltotalregistros.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lbltotalregistros.setText("Buscar:");

        txtbuscar.setBackground(new java.awt.Color(204, 255, 204));
        txtbuscar.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });

        txtnombre.setBackground(new java.awt.Color(204, 255, 204));
        txtnombre.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        txtnombre.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtnombre.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtnombreActionPerformed(evt);
            }
        });

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel4.setText("Nombre:");

        jSeparator2.setBackground(new java.awt.Color(0, 0, 0));
        jSeparator2.setForeground(new java.awt.Color(0, 0, 0));
        jSeparator2.setOrientation(javax.swing.SwingConstants.VERTICAL);
        jSeparator2.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel5.setText("Buscar:");

        btncliente.setBackground(new java.awt.Color(0, 134, 190));
        btncliente.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btncliente.setText("Crear Cliente");
        btncliente.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnclienteActionPerformed(evt);
            }
        });

        btnpago.setBackground(new java.awt.Color(0, 134, 190));
        btnpago.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btnpago.setText("Realizar Pago");
        btnpago.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnpagoMouseClicked(evt);
            }
        });
        btnpago.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btnpagoActionPerformed(evt);
            }
        });

        btneditar.setBackground(new java.awt.Color(0, 134, 190));
        btneditar.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btneditar.setText("Edirar Cliente");
        btneditar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneditarActionPerformed(evt);
            }
        });

        btncorreo.setBackground(new java.awt.Color(0, 134, 190));
        btncorreo.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btncorreo.setText("Enviar Correo");
        btncorreo.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btncorreoActionPerformed(evt);
            }
        });

        btneditar1.setBackground(new java.awt.Color(0, 134, 190));
        btneditar1.setFont(new java.awt.Font("Times New Roman", 1, 14)); // NOI18N
        btneditar1.setText("FC");
        btneditar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                btneditar1ActionPerformed(evt);
            }
        });

        lblactivo.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblactivo.setText("Total Activo:");

        lblinactivo.setFont(new java.awt.Font("Tahoma", 1, 16)); // NOI18N
        lblinactivo.setText("Total Activo:");

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(4, 4, 4)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addComponent(lbltotalregistros, javax.swing.GroupLayout.PREFERRED_SIZE, 240, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(38, 38, 38)
                        .addComponent(lblactivo, javax.swing.GroupLayout.PREFERRED_SIZE, 276, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(lblinactivo, javax.swing.GroupLayout.PREFERRED_SIZE, 274, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addGroup(jPanel2Layout.createSequentialGroup()
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(0, 0, 0)
                            .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, 250, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGap(20, 20, 20)
                            .addComponent(jSeparator2, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 60, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addGap(10, 10, 10)
                                    .addComponent(txtcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGap(70, 70, 70)
                                    .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, 210, javax.swing.GroupLayout.PREFERRED_SIZE))
                                .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 80, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGap(36, 36, 36)
                                    .addComponent(btnpago)
                                    .addGap(18, 18, 18)
                                    .addComponent(btncorreo))
                                .addGroup(jPanel2Layout.createSequentialGroup()
                                    .addGap(15, 15, 15)
                                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                    .addComponent(btncliente)
                                    .addGap(18, 18, 18)
                                    .addComponent(btneditar)
                                    .addGap(27, 27, 27)
                                    .addComponent(btneditar1)))
                            .addGap(0, 0, Short.MAX_VALUE))
                        .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 1020, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addGap(1, 1, 1))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtcodigo, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(10, 10, 10)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(txtnombre, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGap(30, 30, 30)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(txtbuscar, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createSequentialGroup()
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btncliente, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btneditar, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btneditar1, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(btnpago, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(btncorreo, javax.swing.GroupLayout.PREFERRED_SIZE, 33, javax.swing.GroupLayout.PREFERRED_SIZE)))
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                        .addComponent(jSeparator2, javax.swing.GroupLayout.Alignment.LEADING, javax.swing.GroupLayout.DEFAULT_SIZE, 71, Short.MAX_VALUE)
                        .addComponent(jSeparator1, javax.swing.GroupLayout.Alignment.LEADING)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 430, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(lbltotalregistros, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblactivo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(lblinactivo, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addContainerGap())
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel2, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void txtcodigoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtcodigoActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtcodigoActionPerformed

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtnombreActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtnombreActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtnombreActionPerformed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        mostrar(txtbuscar.getText());

    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btnclienteActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnclienteActionPerformed

        new FRclientes().setVisible(true);
        FRclientes.lblmeses.setVisible(false);
        FRclientes.lblpago.setVisible(false);
        FRclientes.lbltotal.setVisible(false);
        FRclientes.lbltol.setVisible(false);
        FRclientes.txtmeses.setVisible(false);
        FRclientes.txtmonto.setVisible(false);
        FRclientes.jsmeses.setVisible(false);
        FRclientes.jsmonto.setVisible(false);
        FRclientes.btnuevo1.setVisible(false);
        FRclientes.lblmeses1.setVisible(false);
        FRclientes.jsmeses1.setVisible(false);
        FRclientes.txtservicio.setVisible(false);
        FRclientes.loginBtn9.setVisible(false);
        FRclientes.lblnombre.setText("REGISTRAR CLIENTE");
        FRclientes.habilitar();
        FRclientes.btnbuscar.setVisible(false);
        FRclientes.txtnombre.requestFocus();
        FRclientes.btncobrar.setVisible(false);
        txtcodigo.setText("");
        txtnombre.setText("");
        btncorreo.setEnabled(false);

// TODO add your handling code here:
    }//GEN-LAST:event_btnclienteActionPerformed


    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());

        if (evt.getClickCount() == 1) {
            String valor;
            Date valor1;

            valor = tablalistado.getValueAt(fila, 0).toString();
            id = (valor);
            jLabel1.setText(valor);

            valor = tablalistado.getValueAt(fila, 1).toString();
            txtcodigo.setText(valor);

            valor = tablalistado.getValueAt(fila, 1).toString();
            codigocli = (valor);

            valor = tablalistado.getValueAt(fila, 2).toString();
            txtnombre.setText(valor);

            valor = tablalistado.getValueAt(fila, 3).toString();
            email = (valor);

            valor = tablalistado.getValueAt(fila, 4).toString();
            cedula = (valor);

            valor = tablalistado.getValueAt(fila, 5).toString();
            direccion = (valor);

            dtfecha.setDate(Date.valueOf(tablalistado.getValueAt(fila, 13).toString()));
            dtfecha1.setDate(Date.valueOf(tablalistado.getValueAt(fila, 14).toString()));

            valor = tablalistado.getValueAt(fila, 8).toString();
            mensualdidad = (valor);

            valor = tablalistado.getValueAt(fila, 11).toString();
            estado = (valor);

            valor = tablalistado.getValueAt(fila, 12).toString();
            telefono = (valor);

        }
        btnpago.setEnabled(true);
        btneditar.setEnabled(true);
        btncorreo.setEnabled(true);


    }//GEN-LAST:event_tablalistadoMouseClicked

    private void btnpagoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btnpagoActionPerformed

        new FRclientes().setVisible(true);
        FRclientes.lblnombre.setText("REGISTRAR PAGOS");
        FRclientes.habilitar();

        FRclientes.id = id;
        FRclientes.txtmeses.requestFocus();
        FRclientes.txtmeses.setText("1");
        FRclientes.txtnombre.setEditable(false);
        FRclientes.lblcodigo.setText(codigocli);
        FRclientes.txttelefono.setEditable(false);
        FRclientes.txtemail.setEditable(false);
        FRclientes.lblcodigo.setEditable(false);
        FRclientes.txtcedula.setEditable(false);
        FRclientes.txtdireccion.setEditable(false);
        FRclientes.dtpago.setEnabled(true);
        FRclientes.dtregistro.setEnabled(false);
        FRclientes.txtmensualidad.setEditable(false);
        FRclientes.rdactivo.setEnabled(false);
        FRclientes.rdinactivo.setEnabled(false);
        FRclientes.btnuevo1.setVisible(false);
        FRclientes.btnguardar2.setVisible(false);
        FRclientes.txtnombre.setText(txtnombre.getText());
        FRclientes.txttelefono.setText(telefono);
        FRclientes.txtemail.setText(email);
        FRclientes.txtcedula.setText(cedula);
        FRclientes.txtdireccion.setText(direccion);
        FRclientes.dtpago.setDate(dtfecha.getDate());
        FRclientes.dtregistro.setDate(dtfecha1.getDate());
        FRclientes.txtmensualidad.setText(mensualdidad);
        FRclientes.loginBtn9.setVisible(false);

        if (estado.equalsIgnoreCase("Activo")) {
            FRclientes.rdactivo.setSelected(true);

        } else if (estado.equals("Inactivo")) {
            FRclientes.rdinactivo.setSelected(true);

        }

        txtcodigo.setText("");
        txtnombre.setText("");
        btncorreo.setEnabled(false);
        btnpago.setEnabled(false);
        btneditar.setEnabled(false);

    }//GEN-LAST:event_btnpagoActionPerformed

    private void dtfechaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dtfechaKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_dtfechaKeyPressed

    private void dtfecha1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_dtfecha1KeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_dtfecha1KeyPressed

    private void btneditarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneditarActionPerformed
        new FRclientes().setVisible(true);
        FRclientes.habilitar();
        FRclientes.lblmeses.setVisible(false);
        FRclientes.accion = "editar";
        FRclientes.btnguardar.setText("Modificar");

        if (estado.equals("Activo")) {
            FRclientes.rdactivo.setSelected(true);

        } else if (estado.equals("Inactivo")) {
            FRclientes.rdinactivo.setSelected(true);

        }
        FRclientes.lblpago.setVisible(false);
        FRclientes.lbltotal.setVisible(false);
        FRclientes.lbltol.setVisible(false);
        FRclientes.txtmeses.setVisible(false);
        FRclientes.txtmonto.setVisible(false);
        FRclientes.jsmeses.setVisible(false);
        FRclientes.jsmonto.setVisible(false);
        FRclientes.btnuevo1.setVisible(false);
        FRclientes.lblmeses1.setVisible(false);
        FRclientes.jsmeses1.setVisible(false);
        FRclientes.txtservicio.setVisible(false);
        FRclientes.btnbuscar.setVisible(false);

        FRclientes.lblnombre.setText("MODIFICAR CLIENTE");
        FRclientes.txtnombre.setEditable(true);
        FRclientes.lblcodigo.setText(codigocli);
        FRclientes.id = id;
        FRclientes.btnuevo1.setVisible(false);
        FRclientes.txtnombre.setText(txtnombre.getText());
        FRclientes.txttelefono.setText(telefono);
        FRclientes.txtemail.setText(email);
        FRclientes.txtcedula.setText(cedula);
        FRclientes.txtdireccion.setText(direccion);
        FRclientes.dtpago.setDate(dtfecha.getDate());
        FRclientes.dtregistro.setDate(dtfecha1.getDate());
        FRclientes.txtmensualidad.setText(mensualdidad);
        btneditar.setEnabled(false);
        btncorreo.setEnabled(false);
        btnpago.setEnabled(false);

        FRclientes.btncobrar.setVisible(false);

        txtcodigo.setText("");
        txtnombre.setText("");        // TODO add your handling code here:
    }//GEN-LAST:event_btneditarActionPerformed

    private void btncorreoActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btncorreoActionPerformed
        new Fcorreo().setVisible(true);
        Fcorreo.txtdestino.setText(email);

        txtcodigo.setText("");
        txtnombre.setText("");
        btncorreo.setEnabled(false);
        btnpago.setEnabled(false);
        btneditar.setEnabled(false);

// TODO add your handling code here:
    }//GEN-LAST:event_btncorreoActionPerformed

    private void btneditar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_btneditar1ActionPerformed
        new HistorialFactura().setVisible(true);        // TODO add your handling code here:
    }//GEN-LAST:event_btneditar1ActionPerformed

    private void btnpagoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnpagoMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_btnpagoMouseClicked
    private String id;
    private String telefono;
    private String email;
    private String cedula;
    private String direccion;
    private String pago;
    private String registro;
    private String mensualdidad;
    private String estado;

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;

                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(VistaCliente.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(VistaCliente.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(VistaCliente.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);

        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(VistaCliente.class
                    .getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new VistaCliente().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton btncliente;
    private javax.swing.JButton btncorreo;
    private javax.swing.JButton btneditar;
    private javax.swing.JButton btneditar1;
    private javax.swing.JButton btnpago;
    public static com.toedter.calendar.JDateChooser dtfecha;
    public static com.toedter.calendar.JDateChooser dtfecha1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JSeparator jSeparator2;
    public static javax.swing.JLabel lblactivo;
    public static javax.swing.JLabel lblinactivo;
    public static javax.swing.JLabel lbltotalregistros;
    public static javax.swing.JTable tablalistado;
    private javax.swing.JTextField txtbuscar;
    public static javax.swing.JTextField txtcodigo;
    public static javax.swing.JTextField txtnombre;
    // End of variables declaration//GEN-END:variables
}
