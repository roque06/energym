package Datos;

public class Fregimen {

    private int id;
    private String regimen;

    public Fregimen() {
    }

    public Fregimen(int id, String regimen) {
        this.id = id;
        this.regimen = regimen;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRegimen() {
        return regimen;
    }

    public void setRegimen(String regimen) {
        this.regimen = regimen;
    }

}
