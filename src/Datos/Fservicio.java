
package Datos;

public class Fservicio {
    
    private int id;
    private String servicio;
    private String precio;
    private String codigo;

    public Fservicio() {
    }

    public Fservicio(int id, String servicio, String precio, String codigo) {
        this.id = id;
        this.servicio = servicio;
        this.precio = precio;
        this.codigo = codigo;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getServicio() {
        return servicio;
    }

    public void setServicio(String servicio) {
        this.servicio = servicio;
    }

    public String getPrecio() {
        return precio;
    }

    public void setPrecio(String precio) {
        this.precio = precio;
    }

    public String getCodigo() {
        return codigo;
    }

    public void setCodigo(String codigo) {
        this.codigo = codigo;
    }
    
    
    
    
}
