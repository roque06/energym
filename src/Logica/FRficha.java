package Logica;

import Datos.Fficha;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class FRficha {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "Nombre Pss", "Fecha", "Nombre", "Edad", "NSS", "Regimen", "Cedula", "Afiliado", "Lugar",
                    "Telefono", "Fecha Nacimiento", "Direccion", "Correo", "Cabeza", "Cara", "Cuello", "ATM", "LABIOS", "LENGUA", "ASMA",
                    "ALERGIA", "EPILEPCIA", "anemia", "hepatitis", "tuberculosis", "renal", "diabetes", "hipertencion", "hipotencion", "otros",
                    "caries", "periodontal", "hemorragia", "abscesos", "fistulas", "dolor", "Fracturas", "bruxismo", "abracion", "otros", "Cod-Paciente",
                    "Tipo Seguro"
                };

        String[] registro = new String[47];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From tbficha where CONCAT (nombre,'',cedula,'',telefono,'',codpaciente) like '%" + buscar + "%' order by nombre asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("id");
                registro[1] = rs.getString("nombrepss");
                registro[2] = rs.getString("fecha");
                registro[3] = rs.getString("nombre");
                registro[4] = rs.getString("edad");
                registro[5] = rs.getString("nss");
                registro[6] = rs.getString("regimen");
                registro[7] = rs.getString("cedula");
                registro[8] = rs.getString("afiliado");
                registro[9] = rs.getString("lugar");
                registro[10] = rs.getString("telefono");
                registro[11] = rs.getString("fecha_naci");
                registro[12] = rs.getString("direccion");
                registro[13] = rs.getString("correo");
                registro[14] = rs.getString("cabeza");
                registro[15] = rs.getString("cara");
                registro[16] = rs.getString("cuello");
                registro[17] = rs.getString("atm");
                registro[18] = rs.getString("labios");
                registro[19] = rs.getString("lengua");
                registro[20] = rs.getString("asma");
                registro[21] = rs.getString("alergia");
                registro[22] = rs.getString("epilepsia");
                registro[23] = rs.getString("anemia");
                registro[24] = rs.getString("hepatitis");
                registro[25] = rs.getString("tuberculosis");
                registro[26] = rs.getString("renal");
                registro[27] = rs.getString("diabetes");
                registro[28] = rs.getString("hipertencion");
                registro[29] = rs.getString("hipotencion");
                registro[30] = rs.getString("otros");
                registro[31] = rs.getString("caries");
                registro[32] = rs.getString("periodontal");
                registro[33] = rs.getString("hemorragia");
                registro[34] = rs.getString("abscesos");
                registro[35] = rs.getString("fistulas");
                registro[36] = rs.getString("dolor");
                registro[37] = rs.getString("fracturas");
                registro[38] = rs.getString("bruxismo");
                registro[39] = rs.getString("abrasion");
                registro[40] = rs.getString("otros2");
                registro[41] = rs.getString("codpaciente");
                registro[42] = rs.getString("tiposeguro");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(Fficha dts) {
        sSQL = "insert into tbficha (nombrepss,fecha,nombre,edad,nss,regimen,cedula,afiliado,lugar,telefono,fecha_naci,direccion,correo,"
                + "cabeza,cara,cuello,atm,labios,lengua,asma,alergia,epilepsia,anemia,hepatitis,tuberculosis,renal,diabetes,hipertencion,"
                + "hipotencion,otros,caries,periodontal,hemorragia,abscesos,fistulas,dolor,fracturas,bruxismo,abrasion,otros2,codpaciente,tiposeguro)"
                + "VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombrepss());
            pst.setDate(2, (Date) dts.getFecha());
            pst.setString(3, dts.getNombre());
            pst.setString(4, dts.getEdad());
            pst.setString(5, dts.getNss());
            pst.setString(6, dts.getRegimen());
            pst.setString(7, dts.getCedula());
            pst.setString(8, dts.getAfiliado());
            pst.setString(9, dts.getLugar());
            pst.setString(10, dts.getTelefono());
            pst.setDate(11, (Date) dts.getFechanaci());
            pst.setString(12, dts.getDireccion());
            pst.setString(13, dts.getCorreo());
            pst.setString(14, dts.getCabeza());
            pst.setString(15, dts.getCara());
            pst.setString(16, dts.getCuello());
            pst.setString(17, dts.getAtm());
            pst.setString(18, dts.getLabios());
            pst.setString(19, dts.getLengua());
            pst.setString(20, dts.getAsma());
            pst.setString(21, dts.getAlergia());
            pst.setString(22, dts.getEpilepcia());
            pst.setString(23, dts.getAnemia());
            pst.setString(24, dts.getHepatitis());
            pst.setString(25, dts.getTuberculosis());
            pst.setString(26, dts.getRenal());
            pst.setString(27, dts.getDiabetes());
            pst.setString(28, dts.getHipertencion());
            pst.setString(29, dts.getHipotencion());
            pst.setString(30, dts.getOtros());
            pst.setString(31, dts.getCaries());
            pst.setString(32, dts.getPeriodontal());
            pst.setString(33, dts.getHemorragia());
            pst.setString(34, dts.getAbscesos());
            pst.setString(35, dts.getFistulas());
            pst.setString(36, dts.getDolor());
            pst.setString(37, dts.getFracturas());
            pst.setString(38, dts.getBruxismo());
            pst.setString(39, dts.getAbrasion());
            pst.setString(40, dts.getOtros2());
            pst.setString(41, dts.getCodpaciente());
            pst.setString(42, dts.getTiposeguro());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(Fficha dts) {
        sSQL = "UPDATE tbficha SET nombrepss=?,fecha=?,nombre=?,edad=?,nss=?,regimen=?,cedula=?,afiliado=?,lugar=?,telefono=?,fecha_naci=?,direccion=?,correo=?,"
                + "cabeza=?,cara=?,cuello=?,atm=?,labios=?,lengua=?,asma=?,alergia=?,epilepsia=?,anemia=?,hepatitis=?,tuberculosis=?,renal=?,diabetes=?,hipertencion=?,"
                + "hipotencion=?,otros=?,caries=?,periodontal=?,hemorragia=?,abscesos=?,fistulas=?,dolor=?,fracturas=?,bruxismo=?,abrasion=?,otros2=?,codpaciente=?,tiposeguro=?"
                + "where(id=?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getNombrepss());
            pst.setDate(2, (Date) dts.getFecha());
            pst.setString(3, dts.getNombre());
            pst.setString(4, dts.getEdad());
            pst.setString(5, dts.getNss());
            pst.setString(6, dts.getRegimen());
            pst.setString(7, dts.getCedula());
            pst.setString(8, dts.getAfiliado());
            pst.setString(9, dts.getLugar());
            pst.setString(10, dts.getTelefono());
            pst.setDate(11, (Date) dts.getFechanaci());
            pst.setString(12, dts.getDireccion());
            pst.setString(13, dts.getCorreo());
            pst.setString(14, dts.getCabeza());
            pst.setString(15, dts.getCara());
            pst.setString(16, dts.getCuello());
            pst.setString(17, dts.getAtm());
            pst.setString(18, dts.getLabios());
            pst.setString(19, dts.getLengua());
            pst.setString(20, dts.getAsma());
            pst.setString(21, dts.getAlergia());
            pst.setString(22, dts.getEpilepcia());
            pst.setString(23, dts.getAnemia());
            pst.setString(24, dts.getHepatitis());
            pst.setString(25, dts.getTuberculosis());
            pst.setString(26, dts.getRenal());
            pst.setString(27, dts.getDiabetes());
            pst.setString(28, dts.getHipertencion());
            pst.setString(29, dts.getHipotencion());
            pst.setString(30, dts.getOtros());
            pst.setString(31, dts.getCaries());
            pst.setString(32, dts.getPeriodontal());
            pst.setString(33, dts.getHemorragia());
            pst.setString(34, dts.getAbscesos());
            pst.setString(35, dts.getFistulas());
            pst.setString(36, dts.getDolor());
            pst.setString(37, dts.getFracturas());
            pst.setString(38, dts.getBruxismo());
            pst.setString(39, dts.getAbrasion());
            pst.setString(40, dts.getOtros2());
            pst.setString(41, dts.getCodpaciente());
            pst.setString(42, dts.getTiposeguro());

            pst.setInt(43, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean eliminar(Fficha dts) {
        sSQL = "DELETE FROM tbficha where id=?";

        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

}
