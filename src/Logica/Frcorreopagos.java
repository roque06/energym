package Logica;

import Datos.FconfCorreo;
import Datos.Fregimen;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class Frcorreopagos {

    private conexion mysql = new conexion();
    private Connection cn = mysql.conectar();
    private String sSQL = "";
    public Integer totalregistro;

    public DefaultTableModel mostrar(String buscar) {
        DefaultTableModel modelo;

        String[] titulos
                = {"ID", "CORREO", "Contraseña", "ASUNTO", "MENSAJE"};

        String[] registro = new String[8];
        totalregistro = 0;

        modelo = new DefaultTableModel(null, titulos);

        sSQL = "select *From correopagos  where concat (correo) like '%" + buscar + "%' order by correo asc";

        try {
            Statement st = cn.createStatement();
            ResultSet rs = st.executeQuery(sSQL);

            while (rs.next()) {
                registro[0] = rs.getString("idcorreo");
                registro[1] = rs.getString("correo");
                registro[2] = rs.getString("contraseña");
                registro[3] = rs.getString("asunto");
                registro[4] = rs.getString("mensaje");

                totalregistro = totalregistro + 1;
                modelo.addRow(registro);

            }
            return modelo;

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return null;

        }

    }

    public boolean insertar(FconfCorreo dts) {
        sSQL = "insert into correopagos (correo,contraseña,asunto,mensaje)"
                + "VALUES(?,?,?,?)";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);

            pst.setString(1, dts.getCorreo());
            pst.setString(2, dts.getContraseña());
            pst.setString(3, dts.getAsunto());
            pst.setString(4, dts.getMensaje());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
            return false;
        }

    }

    public boolean editar(FconfCorreo dts) {
        sSQL = "UPDATE correopagos set correo=?,contraseña=?,asunto=?,mensaje=?"
                + "where idcorreo=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setString(1, dts.getCorreo());
            pst.setString(2, dts.getContraseña());
            pst.setString(3, dts.getAsunto());
            pst.setString(4, dts.getMensaje());

            pst.setInt(5, dts.getId());
            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

    public boolean eliminar(Fregimen dts) {
        sSQL = "DELETE FROM correopagos WHERE idcorreo=?";
        try {
            PreparedStatement pst = cn.prepareStatement(sSQL);
            pst.setInt(1, dts.getId());

            int n = pst.executeUpdate();

            if (n != 0) {
                return true;
            } else {
                return false;
            }

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(null, e);
        }
        return false;

    }

}
