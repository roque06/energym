package Formularios;

import Datos.dtPermisos;
import Logica.lgPermisos;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Image;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

public class frPermisos extends javax.swing.JFrame {

    public frPermisos() {
        initComponents();
        this.setLocationRelativeTo(null);
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());

        mostrar("");
    }
    dtPermisos dts = new dtPermisos();

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            lgPermisos func = new lgPermisos();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            ocultar_columnas();
            inhabilitat();

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);
    }

    void guardar() {

        if (txtregimen.getText().length() == 0) {
            JOptionPane.showMessageDialog(null, "Debes asignar un nombre a este permiso!");
            return;
        }

        lgPermisos func = new lgPermisos();

        dts.setNombre_permiso(txtregimen.getText());
        if (cbPacientes.isSelected()) {
            dts.setPacientes(true);
        } else {
            dts.setPacientes(false);
        }
        if (cbPagos.isSelected()) {
            dts.setPagos(true);
        } else {
            dts.setPagos(false);
        }
        if (cbCitas.isSelected()) {
            dts.setCitas(true);
        } else {
            dts.setCitas(false);
        }
        if (cbTrabajosLaboratorios.isSelected()) {
            dts.setTrabajos_laboratorios(true);
        } else {
            dts.setTrabajos_laboratorios(false);
        }

        if (cbregimen.isSelected()) {
            dts.setRegimen(true);
        } else {
            dts.setRegimen(false);
        }

        if (cbservicios.isSelected()) {
            dts.setProducto(true);
        } else {
            dts.setProducto(false);
        }

        if (cblaboratorio.isSelected()) {
            dts.setLaboratorio(true);
        } else {
            dts.setLaboratorio(false);
        }

        if (cbdoctor.isSelected()) {
            dts.setDoctor(true);
        } else {
            dts.setDoctor(false);
        }
        if (cbusuario.isSelected()) {
            dts.setUsuario(true);
        } else {
            dts.setUsuario(false);
        }
        if (cbcorreo.isSelected()) {
            dts.setCorreo(true);
        } else {
            dts.setCorreo(false);
        }
        if (cbreporte.isSelected()) {
            dts.setDiario(true);
        } else {
            dts.setDiario(false);
        }
        if (cbpermisos.isSelected()) {
            dts.setPermiso(true);
        } else {
            dts.setPermiso(false);
        }
        if (cbseguro.isSelected()) {
            dts.setSeguro(true);

        } else {
            dts.setSeguro(false);
        }

        if (cbempresa.isSelected()) {
            dts.setEmpresa(true);

        } else {
            dts.setEmpresa(false);
        }

        if (accion.equals("guardar")) {
            if (func.insertar(dts)) {
                JOptionPane.showMessageDialog(null, "Permisos registrados correctamente!");
                mostrar("");
                txtregimen.setText("");
            }
        } else if (accion.equals("editar")) {
            dts.setId_permiso(Integer.parseInt(id));
            if (func.editar(dts)) {
                JOptionPane.showMessageDialog(null, "Registro modificado correctamente!");
                mostrar("");
                txtregimen.setText("");
            }

        }
        inhabilitat();
    }

    void eliminar() {
        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estas Seguro que Desea Eliminar el Registro", "Confirmar", 2);

        lgPermisos func = new lgPermisos();

        if (confirmacion == 0) {
            dts.setId_permiso(Integer.parseInt(id));
            if (func.eliminar(dts)) {
                JOptionPane.showMessageDialog(null, "Registro Eliminado");
                inhabilitat();
                mostrar("");
            }

        }

    }

    void habilitar() {
        txtregimen.setEnabled(true);
        btneliminar.setEnabled(false);
        cbPacientes.setEnabled(true);
        cbPagos.setEnabled(true);
        cbCitas.setEnabled(true);
        cbTrabajosLaboratorios.setEnabled(true);
        cbregimen.setEnabled(true);
        cbseguro.setEnabled(true);
        cbservicios.setEnabled(true);
        cblaboratorio.setEnabled(true);
        cbdoctor.setEnabled(true);
        cbusuario.setEnabled(true);
        cbcorreo.setEnabled(true);
        cbreporte.setEnabled(true);
        cbpermisos.setEnabled(true);
        cbempresa.setEnabled(true);
        txtregimen.setText("");

        cbPacientes.setSelected(false);
        cbPagos.setSelected(false);
        cbCitas.setSelected(false);
        cbseguro.setSelected(false);
        cbregimen.setSelected(false);
        cbservicios.setSelected(false);
        cblaboratorio.setSelected(false);
        cbdoctor.setSelected(false);
        cbusuario.setSelected(false);
        cbcorreo.setSelected(false);
        cbreporte.setSelected(false);
        cbpermisos.setSelected(false);
        cbempresa.setSelected(false);
        cbTrabajosLaboratorios.setSelected(false);

    }

    void inhabilitat() {
        txtregimen.setEnabled(false);
        btneliminar.setEnabled(false);
        cbPacientes.setEnabled(false);
        cbPagos.setEnabled(false);
        cbCitas.setEnabled(false);
        cbseguro.setEnabled(false);
        cbservicios.setEnabled(false);
        cblaboratorio.setEnabled(false);
        cbdoctor.setEnabled(false);
        cbusuario.setEnabled(false);
        cbcorreo.setEnabled(false);
        cbreporte.setEnabled(false);
        cbpermisos.setEnabled(false);
        cbregimen.setEnabled(false);
        cbempresa.setEnabled(false);

        cbPacientes.setSelected(false);
        cbPagos.setSelected(false);
        cbCitas.setSelected(false);
        cbseguro.setSelected(false);
        cbservicios.setSelected(false);
        cblaboratorio.setSelected(false);
        cbdoctor.setSelected(false);
        cbusuario.setSelected(false);
        cbcorreo.setSelected(false);
        cbreporte.setSelected(false);
        cbpermisos.setSelected(false);
        cbTrabajosLaboratorios.setSelected(false);
        cbregimen.setSelected(false);
        cbempresa.setSelected(false);

        cbTrabajosLaboratorios.setEnabled(false);
        txtregimen.setText("");
        accion = "guardar";
        btnguardar.setText("Guardar");
    }

    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jPanel9 = new javax.swing.JPanel();
        jScrollPane8 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        jPanel2 = new javax.swing.JPanel();
        cbPagos = new javax.swing.JCheckBox();
        cbCitas = new javax.swing.JCheckBox();
        cbTrabajosLaboratorios = new javax.swing.JCheckBox();
        cbPacientes = new javax.swing.JCheckBox();
        userLabel7 = new javax.swing.JLabel();
        txtregimen = new javax.swing.JTextField();
        jSeparator8 = new javax.swing.JSeparator();
        jPanel3 = new javax.swing.JPanel();
        cbdoctor = new javax.swing.JCheckBox();
        cbservicios = new javax.swing.JCheckBox();
        cblaboratorio = new javax.swing.JCheckBox();
        cbregimen = new javax.swing.JCheckBox();
        cbseguro = new javax.swing.JCheckBox();
        jPanel4 = new javax.swing.JPanel();
        cbpermisos = new javax.swing.JCheckBox();
        cbusuario = new javax.swing.JCheckBox();
        cbcorreo = new javax.swing.JCheckBox();
        cbreporte = new javax.swing.JCheckBox();
        cbempresa = new javax.swing.JCheckBox();
        loginBtn = new javax.swing.JPanel();
        loginBtnTxt = new javax.swing.JLabel();
        loginBtn1 = new javax.swing.JPanel();
        btnguardar = new javax.swing.JLabel();
        loginBtn9 = new javax.swing.JPanel();
        btneliminar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED, null, new java.awt.Color(204, 204, 204), null, null), "Asignar Permisos", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        jPanel9.setBackground(new java.awt.Color(255, 255, 255));
        jPanel9.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jPanel9.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablalistado.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {

            },
            new String [] {

            }
        ));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane8.setViewportView(tablalistado);

        jPanel9.add(jScrollPane8, new org.netbeans.lib.awtextra.AbsoluteConstraints(370, 10, 630, 180));

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Registros", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel2.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cbPagos.setBackground(new java.awt.Color(255, 255, 255));
        cbPagos.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbPagos.setText("Modulos de Pagos");
        cbPagos.setBorder(null);
        jPanel2.add(cbPagos, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, -1, 30));

        cbCitas.setBackground(new java.awt.Color(255, 255, 255));
        cbCitas.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbCitas.setText("Modulo de Citas");
        cbCitas.setBorder(null);
        jPanel2.add(cbCitas, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 90, -1, -1));

        cbTrabajosLaboratorios.setBackground(new java.awt.Color(255, 255, 255));
        cbTrabajosLaboratorios.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbTrabajosLaboratorios.setText("Modulo de Trabajos de Laboratorio");
        cbTrabajosLaboratorios.setBorder(null);
        jPanel2.add(cbTrabajosLaboratorios, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 120, 240, -1));

        cbPacientes.setBackground(new java.awt.Color(255, 255, 255));
        cbPacientes.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbPacientes.setText("Modulo de Pacientes");
        cbPacientes.setBorder(null);
        jPanel2.add(cbPacientes, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 150, 30));

        jPanel9.add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 230, 260, 180));

        userLabel7.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel7.setText("Nombre del Permiso");
        jPanel9.add(userLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, -1, -1));

        txtregimen.setFont(new java.awt.Font("Roboto", 0, 12)); // NOI18N
        txtregimen.setBorder(null);
        txtregimen.setDisabledTextColor(new java.awt.Color(0, 0, 0));
        txtregimen.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mousePressed(java.awt.event.MouseEvent evt) {
                txtregimenMousePressed(evt);
            }
        });
        txtregimen.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtregimenActionPerformed(evt);
            }
        });
        jPanel9.add(txtregimen, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 40, 280, 30));

        jSeparator8.setForeground(new java.awt.Color(0, 0, 0));
        jPanel9.add(jSeparator8, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 280, 20));

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Categorias", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel3.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cbdoctor.setBackground(new java.awt.Color(255, 255, 255));
        cbdoctor.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbdoctor.setText("Modulo Doctor");
        cbdoctor.setBorder(null);
        jPanel3.add(cbdoctor, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 220, 30));

        cbservicios.setBackground(new java.awt.Color(255, 255, 255));
        cbservicios.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbservicios.setText("Modulo Servicios o Productos");
        cbservicios.setBorder(null);
        jPanel3.add(cbservicios, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 220, 30));

        cblaboratorio.setBackground(new java.awt.Color(255, 255, 255));
        cblaboratorio.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cblaboratorio.setText("Modulo Laboratorio");
        cblaboratorio.setBorder(null);
        jPanel3.add(cblaboratorio, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 220, 30));

        cbregimen.setBackground(new java.awt.Color(255, 255, 255));
        cbregimen.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbregimen.setText("Modulo Categorias");
        cbregimen.setBorder(null);
        jPanel3.add(cbregimen, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 220, 30));

        cbseguro.setBackground(new java.awt.Color(255, 255, 255));
        cbseguro.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbseguro.setText("Modulo Seguros");
        cbseguro.setBorder(null);
        cbseguro.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cbseguroActionPerformed(evt);
            }
        });
        jPanel3.add(cbseguro, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 220, 30));

        jPanel9.add(jPanel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(290, 230, 270, 180));

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Configuracion", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel4.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        cbpermisos.setBackground(new java.awt.Color(255, 255, 255));
        cbpermisos.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbpermisos.setText("Modulo Permisos");
        cbpermisos.setBorder(null);
        jPanel4.add(cbpermisos, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 110, 220, 30));

        cbusuario.setBackground(new java.awt.Color(255, 255, 255));
        cbusuario.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbusuario.setText("Modulo Registrar Usuario");
        cbusuario.setBorder(null);
        jPanel4.add(cbusuario, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 20, 220, 30));

        cbcorreo.setBackground(new java.awt.Color(255, 255, 255));
        cbcorreo.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbcorreo.setText("Modulo Correo");
        cbcorreo.setBorder(null);
        jPanel4.add(cbcorreo, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 50, 220, 30));

        cbreporte.setBackground(new java.awt.Color(255, 255, 255));
        cbreporte.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbreporte.setText(" Modulo Reporte Diario");
        cbreporte.setBorder(null);
        jPanel4.add(cbreporte, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 80, 220, 30));

        cbempresa.setBackground(new java.awt.Color(255, 255, 255));
        cbempresa.setFont(new java.awt.Font("Calibri", 1, 14)); // NOI18N
        cbempresa.setText("Modulo Informacion Empresa");
        cbempresa.setBorder(null);
        jPanel4.add(cbempresa, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 220, 30));

        jPanel9.add(jPanel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(600, 230, 290, 180));

        loginBtn.setBackground(new java.awt.Color(0, 134, 190));

        loginBtnTxt.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        loginBtnTxt.setForeground(new java.awt.Color(255, 255, 255));
        loginBtnTxt.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        loginBtnTxt.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/notas.png"))); // NOI18N
        loginBtnTxt.setText("Nuevo");
        loginBtnTxt.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        loginBtnTxt.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                loginBtnTxtMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtnLayout = new javax.swing.GroupLayout(loginBtn);
        loginBtn.setLayout(loginBtnLayout);
        loginBtnLayout.setHorizontalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginBtnTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 100, Short.MAX_VALUE)
        );
        loginBtnLayout.setVerticalGroup(
            loginBtnLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(loginBtnTxt, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel9.add(loginBtn, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 190, 100, 30));

        loginBtn1.setBackground(new java.awt.Color(0, 134, 190));

        btnguardar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btnguardar.setForeground(new java.awt.Color(255, 255, 255));
        btnguardar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btnguardar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/disco-flexible.png"))); // NOI18N
        btnguardar.setText("Guardar");
        btnguardar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btnguardar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnguardarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btnguardarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btnguardarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn1Layout = new javax.swing.GroupLayout(loginBtn1);
        loginBtn1.setLayout(loginBtn1Layout);
        loginBtn1Layout.setHorizontalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btnguardar, javax.swing.GroupLayout.DEFAULT_SIZE, 110, Short.MAX_VALUE)
        );
        loginBtn1Layout.setVerticalGroup(
            loginBtn1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(loginBtn1Layout.createSequentialGroup()
                .addComponent(btnguardar)
                .addGap(0, 11, Short.MAX_VALUE))
        );

        jPanel9.add(loginBtn1, new org.netbeans.lib.awtextra.AbsoluteConstraints(120, 190, 110, 30));

        loginBtn9.setBackground(new java.awt.Color(0, 134, 190));

        btneliminar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btneliminar.setForeground(new java.awt.Color(255, 255, 255));
        btneliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btneliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/boton-eliminar.png"))); // NOI18N
        btneliminar.setText("Eliminar");
        btneliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btneliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneliminarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn9Layout = new javax.swing.GroupLayout(loginBtn9);
        loginBtn9.setLayout(loginBtn9Layout);
        loginBtn9Layout.setHorizontalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 103, Short.MAX_VALUE)
        );
        loginBtn9Layout.setVerticalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel9.add(loginBtn9, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 190, -1, 30));

        jPanel1.add(jPanel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(6, 19, 1010, 450));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());
        habilitar();
        btneliminar.setEnabled(true);

        accion = "editar";
        btnguardar.setText("Editar");
        id = (tablalistado.getValueAt(fila, 0).toString());

        txtregimen.setText(tablalistado.getValueAt(fila, 1).toString());

        if ((boolean) tablalistado.getValueAt(fila, 2) == true) {
            cbPacientes.setSelected(true);
        } else {
            cbPacientes.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 3) == true) {
            cbPagos.setSelected(true);
        } else {
            cbPagos.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 4) == true) {
            cbCitas.setSelected(true);
        } else {
            cbCitas.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 5) == true) {
            cbTrabajosLaboratorios.setSelected(true);
        } else {
            cbTrabajosLaboratorios.setSelected(false);
        }

        if ((boolean) tablalistado.getValueAt(fila, 6) == true) {
            cbregimen.setSelected(true);
        } else {
            cbregimen.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 7) == true) {
            cbservicios.setSelected(true);
        } else {
            cbservicios.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 8) == true) {
            cblaboratorio.setSelected(true);
        } else {
            cblaboratorio.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 9) == true) {
            cbdoctor.setSelected(true);
        } else {
            cbdoctor.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 10) == true) {
            cbusuario.setSelected(true);
        } else {
            cbusuario.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 11) == true) {
            cbcorreo.setSelected(true);
        } else {
            cbcorreo.setSelected(false);
        }
        if ((boolean) tablalistado.getValueAt(fila, 12) == true) {
            cbreporte.setSelected(true);
        } else {
            cbreporte.setSelected(false);
        }

        if ((boolean) tablalistado.getValueAt(fila, 13) == true) {
            cbpermisos.setSelected(true);
        } else {
            cbpermisos.setSelected(false);
        }

        if ((boolean) tablalistado.getValueAt(fila, 14) == true) {
            cbseguro.setSelected(true);
        } else {
            cbseguro.setSelected(false);
        }

        if ((boolean) tablalistado.getValueAt(fila, 15) == true) {
            cbempresa.setSelected(true);
        } else {
            cbempresa.setSelected(false);
        }


    }//GEN-LAST:event_tablalistadoMouseClicked

    private void txtregimenMousePressed(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_txtregimenMousePressed

    }//GEN-LAST:event_txtregimenMousePressed

    private void txtregimenActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtregimenActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtregimenActionPerformed

    private void loginBtnTxtMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseClicked
        accion = "guardar";
        btnguardar.setText("Guardar");
        habilitar();
        txtregimen.requestFocus();
    }//GEN-LAST:event_loginBtnTxtMouseClicked

    private void loginBtnTxtMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseEntered
        loginBtn.setBackground(new Color(0, 156, 223));
    }//GEN-LAST:event_loginBtnTxtMouseEntered

    private void loginBtnTxtMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_loginBtnTxtMouseExited
        loginBtn.setBackground(new Color(0, 134, 190));
    }//GEN-LAST:event_loginBtnTxtMouseExited

    private void btnguardarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseClicked
        guardar();

    }//GEN-LAST:event_btnguardarMouseClicked

    private void btnguardarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseEntered
        btnguardar.setBackground(new Color(0, 156, 223));        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseEntered

    private void btnguardarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnguardarMouseExited
        btnguardar.setBackground(new Color(0, 134, 190));        // TODO add your handling code here:
    }//GEN-LAST:event_btnguardarMouseExited

    private void btneliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseClicked
        if (btneliminar.isEnabled() == false) {
            return;
        }
        eliminar();              // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseClicked

    private void btneliminarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseEntered

    private void btneliminarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseExited

    private void cbseguroActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cbseguroActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_cbseguroActionPerformed

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(frPermisos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(frPermisos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(frPermisos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(frPermisos.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(() -> {
            new frPermisos().setVisible(true);
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btneliminar;
    public static javax.swing.JLabel btnguardar;
    private javax.swing.JCheckBox cbCitas;
    private javax.swing.JCheckBox cbPacientes;
    private javax.swing.JCheckBox cbPagos;
    private javax.swing.JCheckBox cbTrabajosLaboratorios;
    private javax.swing.JCheckBox cbcorreo;
    private javax.swing.JCheckBox cbdoctor;
    private javax.swing.JCheckBox cbempresa;
    private javax.swing.JCheckBox cblaboratorio;
    private javax.swing.JCheckBox cbpermisos;
    private javax.swing.JCheckBox cbregimen;
    private javax.swing.JCheckBox cbreporte;
    private javax.swing.JCheckBox cbseguro;
    private javax.swing.JCheckBox cbservicios;
    private javax.swing.JCheckBox cbusuario;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel9;
    private javax.swing.JScrollPane jScrollPane8;
    private javax.swing.JSeparator jSeparator8;
    private javax.swing.JPanel loginBtn;
    private javax.swing.JPanel loginBtn1;
    private javax.swing.JPanel loginBtn9;
    private javax.swing.JLabel loginBtnTxt;
    private javax.swing.JTable tablalistado;
    public static javax.swing.JTextField txtregimen;
    private javax.swing.JLabel userLabel7;
    // End of variables declaration//GEN-END:variables
private String accion = "guardar";
    private String id;

}
