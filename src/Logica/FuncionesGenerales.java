package Logica;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import com.toedter.calendar.JDateChooser;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import javax.swing.JTextField;

/**
 *
 * @author Fray Alberto Helena
 */
public class FuncionesGenerales {
    
    public static void ejecutarPermisos(JFrame ventana, int idpermiso, String strPermiso) {
        if (lgConsultarPermiso.comprobarPermiso(idpermiso, strPermiso) == true) {
            ventana.setVisible(true);
        } else {
            JOptionPane.showMessageDialog(null, "No tienes permisos para acceder a este modulo.");
        }
    }

    public static String sumarMesAFecha(JDateChooser fecha, int mes) {
        if (mes == 0) {
            return fecha + "";
        }
        String[] f = FuncionesGenerales.Fecha_String(fecha).split("-");
        Calendar calendar = Calendar.getInstance();
        calendar.set(Integer.parseInt(f[0]), Integer.parseInt(f[1]) - 1, Integer.parseInt(f[2]));
        calendar.add(Calendar.MONTH, mes);
        SimpleDateFormat fe = new SimpleDateFormat("YYYY-MM-dd");
        return fe.format(calendar.getTime());
    }
    
    public static String Fecha_String(JDateChooser fechaData) {
        String fecha = ((JTextField) fechaData.getDateEditor().getUiComponent()).getText();
        String[] pos = new String[3];
        pos = fecha.split("-");
        String fecha2 = pos[2] + "-" + pos[1] + "-" + pos[0];
        return fecha2;
    }
    
}
