/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Datos.dtEmpleado;
import Logica.FRempleado;
import Logica.lgPermisos;
import java.sql.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author radames
 */
public class FvistaEmpleado extends javax.swing.JFrame {

    /**
     * Creates new form FvistaEmpleado
     */
    public FvistaEmpleado() {
        initComponents();
        mostrar("");
        setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());
        this.setLocationRelativeTo(null);
        btneliminar.setEnabled(false);

    }

    dtEmpleado dts = new dtEmpleado();

    void eliminar() {
        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estas Seguro que Desea Eliminar el Registro", "Confirmar", 2);

        FRempleado func = new FRempleado();

        if (confirmacion == 0) {
            dts.setId(Integer.parseInt(id));
            if (func.eliminar(dts)) {
                JOptionPane.showMessageDialog(null, "Registro Eliminado");

                mostrar("");
            }

        }

    }

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRempleado func = new FRempleado();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            ocultar_columnas();

        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(1).setMaxWidth(250);
        tablalistado.getColumnModel().getColumn(1).setMinWidth(250);
        tablalistado.getColumnModel().getColumn(1).setPreferredWidth(250);
    }
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        userLabel10 = new javax.swing.JLabel();
        txtbuscar = new javax.swing.JTextField();
        jSeparator1 = new javax.swing.JSeparator();
        loginBtn9 = new javax.swing.JPanel();
        btneliminar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);
        setTitle("DentiSoft");
        setResizable(false);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(null, "Vista Empleado", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablalistado);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(40, 67, 874, 498));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Buscar:");
        jPanel1.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(50, 30, -1, 30));

        txtbuscar.setBorder(null);
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        jPanel1.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 30, 240, 30));

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));
        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(110, 60, 240, 20));

        loginBtn9.setBackground(new java.awt.Color(0, 134, 190));

        btneliminar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btneliminar.setForeground(new java.awt.Color(255, 255, 255));
        btneliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btneliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/boton-eliminar.png"))); // NOI18N
        btneliminar.setText("Eliminar");
        btneliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btneliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneliminarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn9Layout = new javax.swing.GroupLayout(loginBtn9);
        loginBtn9.setLayout(loginBtn9Layout);
        loginBtn9Layout.setHorizontalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btneliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE))
        );
        loginBtn9Layout.setVerticalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, loginBtn9Layout.createSequentialGroup()
                .addGap(0, 0, Short.MAX_VALUE)
                .addComponent(btneliminar, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        jPanel1.add(loginBtn9, new org.netbeans.lib.awtextra.AbsoluteConstraints(390, 30, -1, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 961, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 595, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents
private String id;
    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());

        btneliminar.setEnabled(true);
        id = (tablalistado.getValueAt(fila, 0).toString());

        if (evt.getClickCount() == 2) {

            String valor;
            Date valor1;

            Fempleado.accion = "editar";
            Fempleado.btnguardar.setText("Editar");

            valor = tablalistado.getValueAt(fila, 0).toString();
            Fempleado.id = (valor);
//            id = (valor);

            valor = tablalistado.getValueAt(fila, 1).toString();
            Fempleado.txtnombre.setText(valor);

            valor = tablalistado.getValueAt(fila, 2).toString();
            Fempleado.txtcedula.setText(valor);

            valor = tablalistado.getValueAt(fila, 3).toString();
            Fempleado.txttelefono.setText(valor);

            valor1 = Date.valueOf(tablalistado.getValueAt(fila, 4).toString());
            Fempleado.dtnaci.setDate(valor1);

            valor = tablalistado.getValueAt(fila, 5).toString();
            Fempleado.txtedad.setText(valor);

            valor = tablalistado.getValueAt(fila, 6).toString();
            Fempleado.txtusuario.setText(valor);

            valor = tablalistado.getValueAt(fila, 7).toString();
            Fempleado.txtcontraseña.setText(valor);

            valor = tablalistado.getValueAt(fila, 8).toString();
            Fempleado.cbopermiso.setSelectedItem(valor);

            Fempleado.txtnombre.setEnabled(true);
            Fempleado.txtcedula.setEnabled(true);
            Fempleado.dtnaci.setEnabled(true);
            Fempleado.txtedad.setEnabled(true);
            Fempleado.txtusuario.setEnabled(true);
            Fempleado.txtcontraseña.setEnabled(true);
            Fempleado.cbopermiso.setEnabled(true);
            Fempleado.btnguardar.setEnabled(true);
            Fempleado.txttelefono.setEnabled(true);

            this.dispose();
        }

    }//GEN-LAST:event_tablalistadoMouseClicked

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        mostrar(txtbuscar.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btneliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseClicked
        if (btneliminar.isEnabled() == false) {
            return;
        }
        eliminar();
        btneliminar.setEnabled(true);// TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseClicked

    private void btneliminarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseEntered

    private void btneliminarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(FvistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(FvistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(FvistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(FvistaEmpleado.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new FvistaEmpleado().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btneliminar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JPanel loginBtn9;
    private javax.swing.JTable tablalistado;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JLabel userLabel10;
    // End of variables declaration//GEN-END:variables
}
