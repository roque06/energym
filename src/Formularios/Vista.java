/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Formularios;

import Datos.Fficha;
import static Formularios.Ficha.id;
import Logica.FRficha;
import Logica.Metodos;
import java.awt.Graphics;
import java.awt.Image;
import java.sql.Date;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Roque
 */
public class Vista extends javax.swing.JFrame {

    /**
     * Creates new form Vista
     */
    public Vista() {
        initComponents();
        mostrar("");
        this.setLocationRelativeTo(null);
         setIconImage(new ImageIcon(getClass().getResource("/Iconos/diente.png")).getImage());

    }

    void mostrar(String buscar) {
        try {
            DefaultTableModel modelo;
            FRficha func = new FRficha();
            modelo = func.mostrar(buscar);

            tablalistado.setModel(modelo);
            lbltotalregistro.setText("Total Registrados: " + Integer.toString(func.totalregistro));
            ocultar_columnas();
            btneliminar.setEnabled(false);
        } catch (Exception e) {
            JOptionPane.showConfirmDialog(rootPane, e);
        }
    }

    void ocultar_columnas() {

        tablalistado.getColumnModel().getColumn(3).setMaxWidth(300);
        tablalistado.getColumnModel().getColumn(3).setMinWidth(300);
        tablalistado.getColumnModel().getColumn(3).setPreferredWidth(300);

        tablalistado.getColumnModel().getColumn(0).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(0).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(0).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(1).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(1).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(1).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(2).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(2).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(2).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(9).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(9).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(9).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(11).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(11).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(11).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(12).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(12).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(12).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(13).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(13).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(13).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(14).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(14).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(14).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(15).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(15).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(15).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(16).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(16).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(16).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(17).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(17).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(17).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(18).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(18).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(18).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(19).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(19).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(19).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(20).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(20).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(20).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(21).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(21).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(21).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(22).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(22).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(22).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(23).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(23).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(23).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(24).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(24).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(24).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(25).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(25).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(25).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(26).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(26).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(26).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(27).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(27).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(27).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(28).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(28).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(28).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(29).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(29).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(29).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(30).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(30).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(30).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(31).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(31).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(31).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(32).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(32).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(32).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(33).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(33).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(33).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(34).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(34).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(34).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(35).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(35).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(35).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(36).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(36).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(36).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(37).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(37).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(37).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(38).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(38).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(38).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(39).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(39).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(39).setPreferredWidth(0);

        tablalistado.getColumnModel().getColumn(40).setMaxWidth(0);
        tablalistado.getColumnModel().getColumn(40).setMinWidth(0);
        tablalistado.getColumnModel().getColumn(40).setPreferredWidth(0);

    }

    void eliminar() {
        int confirmacion = JOptionPane.showConfirmDialog(rootPane, "Estás seguro de Eliminar El Paciente?", "Confirmar", 2);

        if (confirmacion == 0) {
            Fficha dts = new Fficha();
            FRficha func = new FRficha();

            dts.setId(Integer.parseInt(id));
            if (func.eliminar(dts)) {
                JOptionPane.showMessageDialog(null, "Paciente Eliminado");
                mostrar("");

            }

        }
    }


    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        tablalistado = new javax.swing.JTable();
        txtbuscar = new javax.swing.JTextField();
        lbltotalregistro = new javax.swing.JLabel();
        userLabel10 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        loginBtn9 = new javax.swing.JPanel();
        btneliminar = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DISPOSE_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED), "Buscar Paciente", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 1, 14))); // NOI18N
        jPanel1.setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        tablalistado.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tablalistado.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null},
                {null, null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3", "Title 4"
            }
        ));
        tablalistado.setGridColor(new java.awt.Color(255, 255, 255));
        tablalistado.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                tablalistadoMouseClicked(evt);
            }
        });
        jScrollPane1.setViewportView(tablalistado);

        jPanel1.add(jScrollPane1, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 70, 990, 460));

        txtbuscar.setBorder(null);
        txtbuscar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                txtbuscarActionPerformed(evt);
            }
        });
        txtbuscar.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                txtbuscarKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                txtbuscarKeyReleased(evt);
            }
        });
        jPanel1.add(txtbuscar, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 30, 240, 30));

        lbltotalregistro.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        lbltotalregistro.setText("jLabel2");
        jPanel1.add(lbltotalregistro, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 530, -1, -1));

        userLabel10.setFont(new java.awt.Font("Roboto Light", 1, 14)); // NOI18N
        userLabel10.setText("Buscar:");
        jPanel1.add(userLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 29, -1, 30));

        jSeparator1.setBackground(new java.awt.Color(0, 0, 0));
        jSeparator1.setForeground(new java.awt.Color(0, 0, 0));
        jPanel1.add(jSeparator1, new org.netbeans.lib.awtextra.AbsoluteConstraints(70, 60, 240, 20));

        loginBtn9.setBackground(new java.awt.Color(0, 134, 190));

        btneliminar.setFont(new java.awt.Font("Roboto Condensed", 1, 14)); // NOI18N
        btneliminar.setForeground(new java.awt.Color(255, 255, 255));
        btneliminar.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        btneliminar.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Iconos/boton-eliminar.png"))); // NOI18N
        btneliminar.setText("Eliminar");
        btneliminar.setCursor(new java.awt.Cursor(java.awt.Cursor.DEFAULT_CURSOR));
        btneliminar.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btneliminarMouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                btneliminarMouseEntered(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                btneliminarMouseExited(evt);
            }
        });

        javax.swing.GroupLayout loginBtn9Layout = new javax.swing.GroupLayout(loginBtn9);
        loginBtn9.setLayout(loginBtn9Layout);
        loginBtn9Layout.setHorizontalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 120, Short.MAX_VALUE)
        );
        loginBtn9Layout.setVerticalGroup(
            loginBtn9Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(btneliminar, javax.swing.GroupLayout.DEFAULT_SIZE, 30, Short.MAX_VALUE)
        );

        jPanel1.add(loginBtn9, new org.netbeans.lib.awtextra.AbsoluteConstraints(340, 30, -1, 30));

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 1017, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, 560, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void tablalistadoMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_tablalistadoMouseClicked
        int fila = tablalistado.rowAtPoint(evt.getPoint());

        if (evt.getClickCount() == 2) {
            Metodos.habilitar();
            String valor;
            Date valor1;

            Ficha.accion = "editar";
            Ficha.btnguardar.setText("Editar");
            Ficha.btnguardar2.setText("Editar");
            
            

            valor = tablalistado.getValueAt(fila, 0).toString();
            Ficha.id = (valor);
            valor = tablalistado.getValueAt(fila, 1).toString();
            Ficha.txtpss.setSelectedItem(valor);
            valor1 = Date.valueOf(tablalistado.getValueAt(fila, 2).toString());
            Ficha.dtfecha.setDate(valor1);
            valor = tablalistado.getValueAt(fila, 3).toString();
            Ficha.txtnombre.setText(valor);
            valor = tablalistado.getValueAt(fila, 4).toString();
            Ficha.txtedad.setText(valor);
            valor = tablalistado.getValueAt(fila, 5).toString();
            Ficha.txtnss.setText(valor);
            valor = tablalistado.getValueAt(fila, 6).toString();
            Ficha.cboregimen.setSelectedItem(valor);
            valor = tablalistado.getValueAt(fila, 7).toString();
            Ficha.txtcedula.setText(valor);
            valor = tablalistado.getValueAt(fila, 8).toString();
            Ficha.cboafiliado.setSelectedItem(valor);
            valor = tablalistado.getValueAt(fila, 9).toString();
            Ficha.txtlugar.setText(valor);
            valor = tablalistado.getValueAt(fila, 10).toString();
            Ficha.txttelefono.setText(valor);
            valor1 = Date.valueOf(tablalistado.getValueAt(fila, 11).toString());
            Ficha.dtnaci.setDate(valor1);
            valor = tablalistado.getValueAt(fila, 12).toString();
            Ficha.txtdireccion.setText(valor);
            valor = tablalistado.getValueAt(fila, 13).toString();
            Ficha.txtcorreo.setText(valor);
            valor = tablalistado.getValueAt(fila, 14).toString();
            cabeza = (valor);

            if (cabeza.equalsIgnoreCase("Cabeza")) {
                Ficha.checkcabeza.setSelected(true);
            }
            valor = tablalistado.getValueAt(fila, 15).toString();
            cara = (valor);

            if (cara.equalsIgnoreCase("Cara")) {

                Ficha.checkcara.setSelected(true);
            }
            valor = tablalistado.getValueAt(fila, 16).toString();
            cuello = (valor);
            if (cuello.equalsIgnoreCase("Cuello")) {
                Ficha.checkcuello.setSelected(true);
            }
            valor = tablalistado.getValueAt(fila, 17).toString();
            atm = (valor);
            if (atm.equalsIgnoreCase("ATM")) {
                Ficha.checkatm.setSelected(true);
            }
            valor = tablalistado.getValueAt(fila, 18).toString();
            labios = (valor);

            if (labios.equalsIgnoreCase("Labios")) {
                Ficha.checklabios.setSelected(true);
            }
            valor = tablalistado.getValueAt(fila, 19).toString();
            lengua = (valor);
            if (lengua.equalsIgnoreCase("Lengua")) {
                Ficha.checklengua.setSelected(true);
            }

            valor = tablalistado.getValueAt(fila, 20).toString();
            Ficha.cboasma.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 21).toString();
            Ficha.cboalergia.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 22).toString();
            Ficha.cboepilepsia.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 23).toString();
            Ficha.cboanemia.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 24).toString();
            Ficha.cbohepatitis.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 25).toString();
            Ficha.cbotuberculosis.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 26).toString();
            Ficha.cborenal.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 27).toString();
            Ficha.cbodiabetes.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 28).toString();
            Ficha.cbohipertencion.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 29).toString();
            Ficha.cbohipotension.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 30).toString();
            Ficha.txtotros.setText(valor);
            valor=tablalistado.getValueAt(fila, 31).toString();
            Ficha.cbocaries.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 32).toString();
            Ficha.cboperiodontal.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 33).toString();
            Ficha.cbohemorragia.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 34).toString();
            Ficha.cboabscesos.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 35).toString();
            Ficha.cbofistulas.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 36).toString();
            Ficha.cbodolor.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 37).toString();
            Ficha.cbofracturas.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 38).toString();
            Ficha.cbobruxismo.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 39).toString();
            Ficha.cboabrasion.setSelectedItem(valor);
            valor=tablalistado.getValueAt(fila, 40).toString();
            Ficha.txtotros2.setText(valor);
            valor=tablalistado.getValueAt(fila, 41).toString();
            Ficha.lblcodigo.setText(valor);
             valor=tablalistado.getValueAt(fila, 42).toString();
            Ficha.cboseguro.setSelectedItem(valor);

            this.dispose();
        } else if (evt.getClickCount() == 1) {
            id = (tablalistado.getValueAt(fila, 0).toString());
            btneliminar.setEnabled(true);
        }
        // TODO add your handling code here:
    }//GEN-LAST:event_tablalistadoMouseClicked

    private void txtbuscarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_txtbuscarActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarActionPerformed

    private void txtbuscarKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyPressed

    private void txtbuscarKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_txtbuscarKeyReleased
        mostrar(txtbuscar.getText());        // TODO add your handling code here:
    }//GEN-LAST:event_txtbuscarKeyReleased

    private void btneliminarMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseClicked
if(btneliminar.isEnabled()==false){return;}
        eliminar();              // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseClicked

    private void btneliminarMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseEntered

    private void btneliminarMouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btneliminarMouseExited
        // TODO add your handling code here:
    }//GEN-LAST:event_btneliminarMouseExited

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Vista.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Vista().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    public static javax.swing.JLabel btneliminar;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JSeparator jSeparator1;
    private javax.swing.JLabel lbltotalregistro;
    private javax.swing.JPanel loginBtn9;
    private javax.swing.JTable tablalistado;
    private javax.swing.JTextField txtbuscar;
    private javax.swing.JLabel userLabel10;
    // End of variables declaration//GEN-END:variables
public static String cabeza;
    private String cara;
    private String atm;
    private String cuello;
    private String lengua;
    private String labios;
    private String id;

}
